package it.ditech.ci.dbfixer.executor;

/**
 * Enum che identifica il livello di priviegio con cui deve eseguire un patchset
 */
public enum PatchsetExecutionRole {
	
	NORMAL, SYSDBA

}

package it.ditech.ci.dbfixer.executor.callback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import liquibase.changelog.ChangeSet;
import liquibase.changelog.ChangeSet.ExecType;
import liquibase.changelog.DatabaseChangeLog;
import liquibase.database.Database;

/**
 * Implementazione di una callback per la gestione degli eventi Liquibase di applicazione con successo di un patchset
 * 
 * @author mgambini
 */
public class LiquibaseSuccessCustomCallback extends LiquibaseCustomCallback {
	
	private final Logger log = LoggerFactory.getLogger(LiquibaseSuccessCustomCallback.class);
	

	public LiquibaseSuccessCustomCallback(Long buildId){
		this(Boolean.TRUE, buildId);
	}
	
	public LiquibaseSuccessCustomCallback(boolean enabled, Long buildId){
		super(CallbackType.SUCCESS, enabled, buildId);
	}

	@Override
	public void process(ChangeSet changeSet, DatabaseChangeLog databaseChangeLog, Database database, ExecType execType, Exception exception) {
		
		log.debug("Ran callback...");
		
		boolean isSucceded = execType.equals(ExecType.EXECUTED) || execType.equals(ExecType.RERAN);
		String logMsg = isSucceded ? "Patchset applicato con successo." : "Fallimento durante l'applicazione del patchset.";
		this.persistActivityInfo(database, isSucceded, changeSet, logMsg);
	
		log.debug("Patchset execution state: "+execType.name());
	}
	
}

package it.ditech.ci.dbfixer.executor;

/**
 * Eccezione sollevata durante l'interazione con l'esecutore delle migrazioni
 * 
 * @author mgambini
 *
 */
public class ExecutorException extends Exception {

	private static final long serialVersionUID = -7858870186845213862L;
	
	public ExecutorException(ExecutorOperation operation, String msg){
		this(operation, msg, null);
	}
	
	public ExecutorException(ExecutorOperation operation, String msg, Throwable cause){
		super("Exception in operation "+operation.name()+" : "+msg, cause);
		
	}
}

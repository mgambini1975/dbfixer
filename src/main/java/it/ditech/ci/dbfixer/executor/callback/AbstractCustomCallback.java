package it.ditech.ci.dbfixer.executor.callback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.ditech.ci.dbfixer.cache.MigrationsCache;
import it.ditech.ci.dbfixer.domain.Migration;
import liquibase.changelog.ChangeSet;
import liquibase.database.Database;

/**
 * Ancestor delle callback che rispondono agli eventi del motore di applicazione dei patchset
 * 
 * @author mgambini
 *
 */
public abstract class AbstractCustomCallback {
	
	private CallbackType callbackType;
	private boolean enabled;
	private Long buildId;
	
	private final Logger log = LoggerFactory.getLogger(AbstractCustomCallback.class);
	
	
	AbstractCustomCallback(CallbackType type, boolean enabled, Long buildId) {
		this.callbackType = type;
		this.enabled = enabled;
		this.buildId = buildId;
	}
	
	void persistActivityInfo(Database database, boolean migrationSuccess, ChangeSet changeSet, String logMsg){
		String schemaName = database.getConnection().getConnectionUserName();	
		Migration aMigration = new Migration();
		String[] nameVerDesc = changeSet.getId().split(":");
		String version = nameVerDesc[0];
		String author = changeSet.getAuthor();
		String scriptName = nameVerDesc[1];
		String desc = nameVerDesc[2];
		
		//version
		aMigration.setVersion(version);			
		//scriptName
		aMigration.setScriptName(scriptName);
		//Author
		aMigration.setAuthor(author);
		//Success
		aMigration.setSuccess(migrationSuccess);
		//Description
		aMigration.setDescription(desc);
		//Schema
		aMigration.setInstalledBy(schemaName);
		//Log (success)
		aMigration.setLog(logMsg);
		//Liquibase Filename
		aMigration.setFileName(changeSet.getFilePath());
		//Liquibase Md5sum
		aMigration.setMd5sum(changeSet.generateCheckSum().toString());
							
		MigrationsCache.getInstance().setMigrationByBuildId(getBuildId(), aMigration);
		log.debug("VER:"+version+" ; SCRIPT_NAME: "+scriptName+" ; AUTHOR: "+author);
	}
	
	public CallbackType getCallbackType() {
		return callbackType;
	}

	public boolean isEnabled() {
		return enabled;
	}
	
	public Long getBuildId() {
		return buildId;
	}


}

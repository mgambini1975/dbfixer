package it.ditech.ci.dbfixer.executor.callback;

import liquibase.changelog.ChangeSet;
import liquibase.changelog.ChangeSet.ExecType;
import liquibase.changelog.DatabaseChangeLog;
import liquibase.database.Database;

/**
 * Contratto per un gestore custom di callback che rispondono agli eventi Liquibase
 * 
 * @author mgambini
 *
 */
public interface ILiquibaseCustomCallbackHandler {
	
	public boolean processCallbacks(CallbackType callbackType, 
			ChangeSet changeSet, 
			DatabaseChangeLog databaseChangeLog, 
			Database database, 
			ExecType execType, 
			Exception exception);
}

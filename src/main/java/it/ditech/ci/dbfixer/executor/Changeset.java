package it.ditech.ci.dbfixer.executor;

public class Changeset {
	
	private String version;
	private String author;
	private String scriptname;
	private String description;
	private String filepath;
	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getScriptname() {
		return scriptname;
	}
	public void setScriptname(String scriptname) {
		this.scriptname = scriptname;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getFilepath() {
		return filepath;
	}
	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
	
}

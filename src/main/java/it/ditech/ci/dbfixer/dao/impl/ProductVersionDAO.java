package it.ditech.ci.dbfixer.dao.impl;

import static it.ditech.ci.dbfixer.sql.SQLQueries.PRODUCT_VERSION_SELECT_ALL_BY_ID;
import static it.ditech.ci.dbfixer.sql.SQLQueries.PRODUCT_VERSION_SELECT_ALL_BY_PRODUCT_ID;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ditech.ci.dbfixer.dao.IProductVersionDAO;
import it.ditech.ci.dbfixer.service.dto.ProductVersionDTO;

/**
 * Gestore della persistenza per le versioni di un prodotto
 * 
 * @author mgambini
 */
@Repository
public class ProductVersionDAO extends GenericDAO implements IProductVersionDAO {

	@Override
	public List<ProductVersionDTO> getProductVersionsByProductId(Long productId) throws MigrationDAOException {
		List<ProductVersionDTO> toRet = new ArrayList<ProductVersionDTO>();
		String sql = PRODUCT_VERSION_SELECT_ALL_BY_PRODUCT_ID ;		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setLong(1, productId);
			rs = ps.executeQuery();
			
			while (rs.next()) {	
				ProductVersionDTO aProductVersion = new ProductVersionDTO();
				aProductVersion.setId(rs.getLong(1));
				aProductVersion.setName(rs.getString(2));
				aProductVersion.setDescription(rs.getString(3));
				aProductVersion.setProductId(rs.getLong(5));
				toRet.add(aProductVersion);
			}
			
		} catch (SQLException e) {
			DAOExceptionHandler.handleMigrationDAOException(e, this.getClass());
		} finally {
			closeResourceGracefully(rs, ps, conn);
		}
		return toRet;
	}

	@Override
	public ProductVersionDTO getProductVersionById(Long productVersionId) throws MigrationDAOException {
		ProductVersionDTO aProductVersion = null;
		String sql = PRODUCT_VERSION_SELECT_ALL_BY_ID;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setLong(1, productVersionId);
			rs = ps.executeQuery();
			
			if (rs.next()) {	
				aProductVersion = new ProductVersionDTO();
				aProductVersion.setId(rs.getLong(1));
				aProductVersion.setName(rs.getString(2));
				aProductVersion.setDescription(rs.getString(3));
				aProductVersion.setProductId(rs.getLong(5));
			}
			
		} catch (SQLException e) {
			DAOExceptionHandler.handleMigrationDAOException(e, this.getClass());
		} finally {
			closeResourceGracefully(rs, ps, conn);
		}
		return aProductVersion;
	}

}

package it.ditech.ci.dbfixer.dao.impl;

import static it.ditech.ci.dbfixer.config.Constants.LATEST;
import static it.ditech.ci.dbfixer.config.Constants.UNKNOWN;
import static it.ditech.ci.dbfixer.sql.SQLQueries.ACTIVITY_INSERT;
import static it.ditech.ci.dbfixer.sql.SQLQueries.ACTIVITY_SELECT_ALL;
import static it.ditech.ci.dbfixer.sql.SQLQueries.ACTIVITY_SELECT_ALL_BY_ID;
import static it.ditech.ci.dbfixer.sql.SQLQueries.ACTIVITY_SELECT_ALL_BY_VERSION_AND_FILENAME;
import static it.ditech.ci.dbfixer.sql.SQLQueries.ACTIVITY_SELECT_ALL_BY_VERSION_AND_FILENAME_AND_SUCCESS;
import static it.ditech.ci.dbfixer.sql.SQLQueries.ACTIVITY_SELECT_MAXID_BY_PRODUCTID_AND_SQL_OR_METADATA_AND_TARGETENVTYPE;
import static it.ditech.ci.dbfixer.sql.SQLQueries.ACTIVITY_SELECT_MAXID_BY_PRODUCTID_AND_TAG_AND_TARGETENVTYPE;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import it.ditech.ci.dbfixer.dao.IActivityDAO;
import it.ditech.ci.dbfixer.domain.Activity;
import it.ditech.ci.dbfixer.domain.DbVersionMetadata;
import it.ditech.ci.dbfixer.domain.enumeration.ActivityType;
import it.ditech.ci.dbfixer.domain.enumeration.StageEnvType;

/**
 * Gestore della persistenza per le attività di migrazione
 * 
 * @author agranifero
 *
 */
@Repository
public class ActivityDAO extends GenericDAO implements IActivityDAO {
	
	private final Logger log = LoggerFactory.getLogger(ActivityDAO.class);
	
	@Override
	public Activity getActivity(String version, String fileName, Boolean success) throws ActivityDAOException {
		String sql = ACTIVITY_SELECT_ALL_BY_VERSION_AND_FILENAME_AND_SUCCESS;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Activity toRet = null;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, version);
			ps.setString(2, fileName);
			ps.setBoolean(3, success);
			rs = ps.executeQuery();
			Activity aActivity = null;
			if (rs.next()){
				aActivity = new Activity();
				aActivity.setId(rs.getLong(1));
				aActivity.setBuildId(rs.getLong(2));
				aActivity.setTargetEnvType(StageEnvType.valueOf(rs.getLong(3)));
				aActivity.setCustomerId(rs.getLong(4));
				aActivity.setProductId(rs.getLong(5));
				aActivity.setVersion(rs.getString(6));
				aActivity.setActType(ActivityType.valueOf(rs.getLong(7)));
				aActivity.setDescription(rs.getString(8));
				aActivity.setScriptName(rs.getString(9));
				aActivity.setAuthor(rs.getString(10));
				aActivity.setUsername(rs.getString(11));
				aActivity.setChecksum(rs.getLong(12));
				aActivity.setApplyDate(rs.getTimestamp(13));
				aActivity.setExecTime(rs.getTime(14));
				aActivity.setLog(rs.getString(15));
				aActivity.setSuccess(rs.getBoolean(16));
				aActivity.setL_filename(rs.getString(17));
				aActivity.setL_md5sum(rs.getString(18));
			}
			
			toRet = aActivity;
			
		} catch (SQLException e) {
			DAOExceptionHandler.handleActivityDAOException(e, this.getClass());
		} finally {
			closeResourceGracefully(rs, ps, conn);
		}
		return toRet;
	}
	

	@Override
	public Activity getActivity(String version, String fileName) throws ActivityDAOException {
		String sql = ACTIVITY_SELECT_ALL_BY_VERSION_AND_FILENAME;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Activity toRet = null;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setString(1, version);
			ps.setString(2, fileName);
			rs = ps.executeQuery();
			Activity aActivity = null;
			if (rs.next()){
				aActivity = new Activity();
				aActivity.setId(rs.getLong(1));
				aActivity.setBuildId(rs.getLong(2));
				aActivity.setTargetEnvType(StageEnvType.valueOf(rs.getLong(3)));
				aActivity.setCustomerId(rs.getLong(4));
				aActivity.setProductId(rs.getLong(5));
				aActivity.setVersion(rs.getString(6));
				aActivity.setActType(ActivityType.valueOf(rs.getLong(7)));
				aActivity.setDescription(rs.getString(8));
				aActivity.setScriptName(rs.getString(9));
				aActivity.setAuthor(rs.getString(10));
				aActivity.setUsername(rs.getString(11));
				aActivity.setChecksum(rs.getLong(12));
				aActivity.setApplyDate(rs.getTimestamp(13));
				aActivity.setExecTime(rs.getTime(14));
				aActivity.setLog(rs.getString(15));
				aActivity.setSuccess(rs.getBoolean(16));
				aActivity.setL_filename(rs.getString(17));
				aActivity.setL_md5sum(rs.getString(18));
			}
			
			toRet = aActivity;
			
		} catch (SQLException e) {
			DAOExceptionHandler.handleActivityDAOException(e, this.getClass());
		} finally {
			closeResourceGracefully(rs, ps, conn);
		}
		return toRet;
	}
	
	@Override
	public Activity getActivity(Long activityId) throws ActivityDAOException {
		String sql = ACTIVITY_SELECT_ALL_BY_ID;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Activity toRet = null;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setLong(1, activityId);
			rs = ps.executeQuery();
			Activity aActivity = null;
			if (rs.next()){
				aActivity = new Activity();
				aActivity.setId(rs.getLong(1));
				aActivity.setBuildId(rs.getLong(2));
				aActivity.setTargetEnvType(StageEnvType.valueOf(rs.getLong(3)));
				aActivity.setCustomerId(rs.getLong(4));
				aActivity.setProductId(rs.getLong(5));
				aActivity.setVersion(rs.getString(6));
				aActivity.setActType(ActivityType.valueOf(rs.getLong(7)));
				aActivity.setDescription(rs.getString(8));
				aActivity.setScriptName(rs.getString(9));
				aActivity.setAuthor(rs.getString(10));
				aActivity.setUsername(rs.getString(11));
				aActivity.setChecksum(rs.getLong(12));
				aActivity.setApplyDate(rs.getTimestamp(13));
				aActivity.setExecTime(rs.getTime(14));
				aActivity.setLog(rs.getString(15));
				aActivity.setSuccess(rs.getBoolean(16));
				aActivity.setL_filename(rs.getString(17));
				aActivity.setL_md5sum(rs.getString(18));
			}
			
			toRet = aActivity;
			
		} catch (SQLException e) {
			DAOExceptionHandler.handleActivityDAOException(e, this.getClass());
		} finally {
			closeResourceGracefully(rs, ps, conn);
		}
		return toRet;
	}
	
	@Override
	public List<Activity> getActivities(Long productId, Long stageId) throws ActivityDAOException {
		List<Activity> toRet = new ArrayList<Activity>();
		String sql = ACTIVITY_SELECT_ALL;
		if (productId != null || stageId != null){
			sql = sql + " WHERE ";
		}
		if (productId != null){
			sql = sql + "PRODUCTID = ?";
		}
		if (productId != null && stageId != null){
			sql = sql + " AND ";
		}
		if (stageId != null){
			sql = sql + "TARGETENVTYPE = ?";
		}
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(sql);
			if (productId != null && stageId == null)
				ps.setLong(1, productId);
			if (productId == null && stageId != null)
				ps.setLong(1, stageId);
			if (productId != null && stageId != null){
				ps.setLong(1, productId);
				ps.setLong(2, stageId);
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				Activity aActivity = new Activity();
				aActivity.setId(rs.getLong(1));
				aActivity.setBuildId(rs.getLong(2));
				aActivity.setTargetEnvType(StageEnvType.valueOf(rs.getLong(3)));
				aActivity.setCustomerId(rs.getLong(4));
				aActivity.setProductId(rs.getLong(5));
				aActivity.setVersion(rs.getString(6));
				aActivity.setActType(ActivityType.valueOf(rs.getLong(7)));
				aActivity.setDescription(rs.getString(8));
				aActivity.setScriptName(rs.getString(9));
				aActivity.setUsername(rs.getString(10));
				aActivity.setAuthor(rs.getString(11));
				aActivity.setChecksum(rs.getLong(12));
				aActivity.setApplyDate(rs.getTimestamp(13));
				aActivity.setExecTime(rs.getTime(14));
				aActivity.setLog(rs.getString(15));
				aActivity.setSuccess(rs.getBoolean(16));
				toRet.add(aActivity);
			}
			
		} catch (SQLException e) {
			DAOExceptionHandler.handleActivityDAOException(e, this.getClass());
		} finally {
			closeResourceGracefully(rs, ps, conn);
		}
		return toRet;
	}

	@Override
	public int setActivities(List<Activity> activities) throws ActivityDAOException {
		int toRet = 0;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = dataSource.getConnection();
			stmt = conn.prepareStatement(ACTIVITY_INSERT);
			
			for (Activity activity : activities){
				stmt.setLong(1, activity.getBuildId());
				stmt.setLong(2, StageEnvType.valueOf(activity.getTargetEnvType()));
				stmt.setLong(3, activity.getCustomerId());
				stmt.setLong(4, activity.getProductId());
				stmt.setString(5, activity.getVersion());
				stmt.setLong(6, ActivityType.valueOf(activity.getActType()));
				stmt.setString(7, activity.getDescription());
				stmt.setString(8, activity.getScriptName());
				stmt.setString(9, activity.getAuthor());
				stmt.setString(10, activity.getUsername());				
				stmt.setLong(11, activity.getChecksum() != null ? activity.getChecksum() : -1);
				stmt.setTimestamp(12, activity.getApplyDate());
				stmt.setTime(13, activity.getExecTime() != null ? activity.getExecTime() : null);
				int lastIdx;
				if (activity.getLog().length() < 5000) {
					lastIdx = activity.getLog().length();
				} else {
					lastIdx = 4999;
				}
					
				stmt.setString(14, activity.getLog().substring(0, lastIdx));
				stmt.setBoolean(15, activity.isSuccess());
				stmt.setString(16, activity.getL_filename());
				stmt.setString(17, activity.getL_md5sum());
				toRet = toRet + stmt.executeUpdate();				
			}
			
		} catch (SQLException e) {
			DAOExceptionHandler.handleActivityDAOException(e, this.getClass());
		} finally { 
			closeResourceGracefully(null, stmt, conn);
		}	
		return toRet;
	}
	
	/**
	 * Estraggo l'ActivityID corrispondente alla più recente esecuzione di patch od applicazione di metadati 
	 * per il prodotto e per il DBEnv passati in argomento 
	 * @param productId
	 * @param stageId
	 * @return
	 * @throws ActivityDAOException
	 */
	private Long getLastActivityIdByProductIdAndStageIdAndSqlOrAddMetadataActivityType(Long productId, Long stageId) throws ActivityDAOException{
		Long maxId = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = ACTIVITY_SELECT_MAXID_BY_PRODUCTID_AND_SQL_OR_METADATA_AND_TARGETENVTYPE;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setLong(1, productId);
			//SQL OR ADDMETADATA
			ps.setLong(2, ActivityType.valueOf(ActivityType.SQL));
			ps.setLong(3, ActivityType.valueOf(ActivityType.ADDMETADATA));
			ps.setLong(4, stageId);
			rs = ps.executeQuery();
			if (rs.next()){
				maxId = rs.getLong(1);
			}			
		} catch (SQLException e) {
			DAOExceptionHandler.handleActivityDAOException(e, this.getClass());
		} finally {
			closeResourceGracefully(rs, ps, conn);
		}
		return maxId;
	}
	
	/**
	 * Estraggo l'ActivityID corrispondente al TAG più recente per il prodotto e per il DBEnv passati in argomento 
	 * @param productId
	 * @param stageId
	 * @return
	 * @throws ActivityDAOException
	 */
	private Long getLastActivityIdByProductIdAndStageIdAndTagActivityType(Long productId, Long stageId) throws ActivityDAOException{
		Long maxId = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = ACTIVITY_SELECT_MAXID_BY_PRODUCTID_AND_TAG_AND_TARGETENVTYPE;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setLong(1, productId);
			//TAG
			ps.setLong(2, ActivityType.valueOf(ActivityType.TAG));
			ps.setLong(3, stageId);
			rs = ps.executeQuery();
			if (rs.next()){
				maxId = rs.getLong(1);
			}
		} catch (SQLException e) {
			DAOExceptionHandler.handleActivityDAOException(e, this.getClass());
		} finally {
			closeResourceGracefully(rs, ps, conn);
		}
		return maxId;
	}

	@Override
	public DbVersionMetadata getDbVersionMetadata(Long productId, Long stageId) throws ActivityDAOException {
		DbVersionMetadata toRet = new DbVersionMetadata();
		Activity anActivity = null;
		//Recupero nome e data di aggiornamento dell'ultima versione applicata al DB target
		Long maxId = getLastActivityIdByProductIdAndStageIdAndTagActivityType(productId, stageId);
		if (maxId!=null) {
			anActivity = getActivity(maxId);
			if (anActivity!=null){
				if (StageEnvType.CI.equals(anActivity.getTargetEnvType())){
					toRet.setVersion(LATEST.toUpperCase());
				} else {
					toRet.setVersion(anActivity.getVersion());
				}
				toRet.setVersionAppliedDate(new Date(anActivity.getApplyDate().getTime()));
			} else {
				toRet.setVersion(UNKNOWN);
			}
		}
		//Recupero ID e data di applicazione dell'ultimo patchset eseguito e dell'ultimo metadata applicato al DB target
		maxId = getLastActivityIdByProductIdAndStageIdAndSqlOrAddMetadataActivityType(productId, stageId);
		if (maxId!=null) {
			anActivity = getActivity(maxId);
			if (anActivity!=null){
				if (anActivity.getVersion().indexOf(".")>0) {
					toRet.setPatchset(anActivity.getVersion().substring(0, anActivity.getVersion().indexOf(".")));
				} else {
					toRet.setPatchset(anActivity.getVersion());
				}
				toRet.setPatchsetAppliedDate(new Date(anActivity.getApplyDate().getTime()));
			} else {
				toRet.setPatchset(UNKNOWN);
			} 
		}
		return toRet;
	}	
}


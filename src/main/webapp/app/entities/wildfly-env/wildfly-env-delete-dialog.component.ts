import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { WildflyEnv } from './wildfly-env.model';
import { WildflyEnvPopupService } from './wildfly-env-popup.service';
import { WildflyEnvService } from './wildfly-env.service';

@Component({
    selector: 'jhi-wildfly-env-delete-dialog',
    templateUrl: './wildfly-env-delete-dialog.component.html'
})
export class WildflyEnvDeleteDialogComponent {

    wildflyEnv: WildflyEnv;

    constructor(
        private wildflyEnvService: WildflyEnvService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.wildflyEnvService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'wildflyEnvListModification',
                content: 'Deleted an wildflyEnv'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-wildfly-env-delete-popup',
    template: ''
})
export class WildflyEnvDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private wildflyEnvPopupService: WildflyEnvPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.wildflyEnvPopupService
                .open(WildflyEnvDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

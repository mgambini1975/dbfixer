package it.ditech.ci.dbfixer;

import static it.ditech.ci.dbfixer.config.Constants.DBFIXER_DS_NAME;
import static it.ditech.ci.dbfixer.config.Constants.SPRING_DS_PREFIX;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import io.github.jhipster.config.JHipsterConstants;

/**
 * Configurazione dei datasource applicativi
 * @author mgambini
 *
 */
@Configuration
@Profile("!cloud")
public class DatasourceConfiguration {
		
	@Primary
	@Bean(name=DBFIXER_DS_NAME)
	@ConfigurationProperties(prefix=SPRING_DS_PREFIX)
	public DataSource appBuilderDataSource() {
	    return DataSourceBuilder.create().build();
	}
}


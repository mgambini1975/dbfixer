--liquibase formatted sql
--changeset mgambini:insert into Customers

--
-- Dump dei dati per la tabella `customer`
--

INSERT INTO `customer` (`id`, `name`, `description`) VALUES
(1, 'MAXIDI', 'Brendolan'),
(2, 'NORDICONAD', 'Nordiconad'),
(3, 'CEDIMARCHE', 'Cedimarche'),
(4, 'CRAI', 'Crai'),
(5, 'UNIMAX', 'Unimax');
package it.ditech.ci.dbfixer.executor.configuration;

import static it.ditech.ci.dbfixer.config.Constants.FILE_SEPARATOR;
import static it.ditech.ci.dbfixer.config.Constants.TMP_DIRNAME;

import java.net.URL;
import java.util.List;

import javax.sql.DataSource;

import it.ditech.ci.dbfixer.domain.enumeration.StageEnvType;
import it.ditech.ci.dbfixer.service.dto.AliasDTO;
import it.ditech.ci.dbfixer.service.dto.CustomerDTO;
import it.ditech.ci.dbfixer.service.dto.ProductDTO;

/**
 * 
 * @author mgambini
 *
 */
public abstract class ExecutorConfiguration implements IExecutorConfiguration {
	
	DataSource datasource;	
	URL repoUrl;
	StageEnvType stageEnvType; 
	AliasDTO aliasDTO;
	ProductDTO productDTO;
	CustomerDTO customerDTO;
	List<CustomerDTO> supportedCustomers;

	public ExecutorConfiguration(URL repoUrl, DataSource ds, StageEnvType envType, AliasDTO aliasDto, ProductDTO productDto, CustomerDTO customerDto, List<CustomerDTO> supportedCustomers) {
		this.repoUrl = repoUrl;
		this.datasource = ds;
		this.stageEnvType = envType;
		this.aliasDTO = aliasDto;
		this.productDTO = productDto;
		this.customerDTO = customerDto;
		this.supportedCustomers = supportedCustomers;
	}
	
	public CustomerDTO getCustomerDTO() {
		return customerDTO;
	}
	
	@Override
	public ProductDTO getProductDTO() {
		return productDTO;
	}

	@Override
	public DataSource getDatasource() {
		return this.datasource;
	}

	@Override
	public URL getRepoUrl() {
		return this.repoUrl;
	}

	@Override
	public StageEnvType getStageEnvType() {
		return this.stageEnvType;
	}
	
	@Override
	public String getBasePath(){
		String fileSeparator = System.getProperty(FILE_SEPARATOR);
		String repoURI = getRepoUrl().toString();
		String repoName = repoURI.substring(repoURI.lastIndexOf('/')+1, repoURI.lastIndexOf('.'));
		return fileSeparator+TMP_DIRNAME+fileSeparator+repoName;
	}

	public AliasDTO getAliasDTO() {
		return aliasDTO;
	}

	public List<CustomerDTO> getSupportedCustomers() {
		return supportedCustomers;
	}

	
}

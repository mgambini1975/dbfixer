import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { OracleDbEnv } from './oracle-db-env.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class OracleDbEnvService {

    private resourceUrl = 'api/oracle-db-envs';

    constructor(private http: Http) { }

    create(oracleDbEnv: OracleDbEnv): Observable<OracleDbEnv> {
        const copy = this.convert(oracleDbEnv);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(oracleDbEnv: OracleDbEnv): Observable<OracleDbEnv> {
        const copy = this.convert(oracleDbEnv);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<OracleDbEnv> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(oracleDbEnv: OracleDbEnv): OracleDbEnv {
        const copy: OracleDbEnv = Object.assign({}, oracleDbEnv);
        return copy;
    }
}

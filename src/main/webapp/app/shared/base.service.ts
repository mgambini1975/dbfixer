import { Injectable, ErrorHandler } from '@angular/core';
import { Headers } from '@angular/http';
import { ProfileService } from '../layouts/profiles/profile.service';
import { DEBUG_INFO_ENABLED } from '../app.constants';

@Injectable()
export class BaseService implements ErrorHandler {

  private _inProduction: boolean; 

  private _baseMockURI = 'assets/mock';
  
  // Read environment variables from browser window
  private browserWindow = window || {};
  private browserWindowEnv = this.browserWindow['__env'] || {};
  private baseUrl = 'http://127.0.0.1:9090';

  constructor(private profileService: ProfileService) { 
  	this._inProduction = !DEBUG_INFO_ENABLED; 
  	if (this.browserWindowEnv.hasOwnProperty('apiUrl')) {
      this.baseUrl = window['__env']['apiUrl'];
    }	
  }
  
  protected getBaseURL(): string {
  	return this.baseUrl;
  }
  
  protected isInProduction(): boolean{
  	return this._inProduction;
  }
  
 
  get baseMockURI() {
    return this._baseMockURI;
  }
  set baseMockURI(baseMockURI: string) {
    this._baseMockURI = baseMockURI;
  }

  protected createHeader(type: string): Headers {
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Access-Control-Allow-Origin', this.getBaseURL());
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', type);
    headers.append('Access-Control-Allow-Origin', '*');
    return headers;
  }

  public handleError(err:any) : void {
    //Raven.captureException(err.originalError || err);
    // Raven.showReportDialog();
  }
  
}

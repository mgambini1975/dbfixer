package it.ditech.ci.dbfixer.domain;

import java.sql.Date;

public class DbVersionMetadata {
	
	private Long productId;
	private Long stageId;
	private String version;
	private Date versionAppliedDate;
	private String patchset;
	private Date patchsetAppliedDate;
	
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Long getStageId() {
		return stageId;
	}
	public void setStageId(Long stageId) {
		this.stageId = stageId;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public Date getVersionAppliedDate() {
		return versionAppliedDate;
	}
	public void setVersionAppliedDate(Date versionAppliedDate) {
		this.versionAppliedDate = versionAppliedDate;
	}
	public String getPatchset() {
		return patchset;
	}
	public void setPatchset(String patchset) {
		this.patchset = patchset;
	}
	public Date getPatchsetAppliedDate() {
		return patchsetAppliedDate;
	}
	public void setPatchsetAppliedDate(Date patchsetAppliedDate) {
		this.patchsetAppliedDate = patchsetAppliedDate;
	}

}

--liquibase formatted sql
--changeset mgambini:insert into tabella db_alias

INSERT INTO `db_alias` (`id`, `env_id`, `comm_name`, `core_name`, `pu_name`, `dcc_name`, `inte_name`, `share_name`, `mntr_name`, `schd_name`, `divu_name`, `logi_name`, `arch_name`, `ecom_name`, `cow_name`, `peng_name`, `cami_name`, `ibc_name`) VALUES
(1, 3, 'BRC_COMM', 'BRC_CORE', 'BRC_PU', 'BRC_DCC', 'DITECH_INTEGRAZIONE', 'DITECH_COMMON', 'DITECH_MONITOR', 'DITECH_SCHEDULER', 'BRC_DIVU', 'BRC_LOGI', 'DITECH_ARCHIVIAZIONE', 'BRC_ECO', '', 'BRC_PE', 'BRC_CAMI', 'BRC_IBC');

INSERT INTO `db_alias` (`id`, `env_id`, `comm_name`, `core_name`, `pu_name`, `dcc_name`, `inte_name`, `share_name`, `mntr_name`, `schd_name`, `divu_name`, `logi_name`, `arch_name`, `ecom_name`, `cow_name`, `peng_name`, `cami_name`, `ibc_name`) VALUES
(2, 7, 'COMM', 'CORE_COMM', 'PU', 'DCC', 'DITECH_INTEGRAZIONE', 'DITECH_COMMON', 'DITECH_MONITOR', 'DITECH_SCHEDULER', 'BRC_DIVU', 'NDC_LOGI', 'DITECH_ARCHIVIAZIONE', '', 'COW', 'PE', '', '');

INSERT INTO `db_alias` (`id`, `env_id`, `comm_name`, `core_name`, `pu_name`, `dcc_name`, `inte_name`, `share_name`, `mntr_name`, `schd_name`, `divu_name`, `logi_name`, `arch_name`, `ecom_name`, `cow_name`, `peng_name`, `cami_name`, `ibc_name`) VALUES
(3, 4, 'BRC_COMM', 'BRC_CORE', 'BRC_PU', 'BRC_DCC', 'DITECH_INTEGRAZIONE', 'DITECH_COMMON', 'DITECH_MONITOR', 'DITECH_SCHEDULER', 'BRC_DIVU', 'BRC_LOGI', 'DITECH_ARCHIVIAZIONE', 'BRC_ECO', '', 'BRC_PE', 'BRC_CAMI', 'BRC_IBC');

INSERT INTO `db_alias` (`id`, `env_id`, `comm_name`, `core_name`, `pu_name`, `dcc_name`, `inte_name`, `share_name`, `mntr_name`, `schd_name`, `divu_name`, `logi_name`, `arch_name`, `ecom_name`, `cow_name`, `peng_name`, `cami_name`, `ibc_name`) VALUES
(4, 5, 'BRC_COMM', 'BRC_CORE', 'BRC_PU', 'BRC_DCC', 'DITECH_INTEGRAZIONE', 'DITECH_COMMON', 'DITECH_MONITOR', 'DITECH_SCHEDULER', 'BRC_DIVU', 'BRC_LOGI', 'DITECH_ARCHIVIAZIONE', 'BRC_ECO', '', 'BRC_PE', 'BRC_CAMI', 'BRC_IBC');

INSERT INTO `db_alias` (`id`, `env_id`, `comm_name`, `core_name`, `pu_name`, `dcc_name`, `inte_name`, `share_name`, `mntr_name`, `schd_name`, `divu_name`, `logi_name`, `arch_name`, `ecom_name`, `cow_name`, `peng_name`, `cami_name`, `ibc_name`) VALUES
(5, 2, 'BRC_COMM', 'BRC_CORE', 'BRC_PU', 'BRC_DCC', 'DITECH_INTEGRAZIONE', 'BRC_COMMON', 'DITECH_MONITOR', 'DIETCH_SCHEDULER', 'BRC_DIVU', 'BRC_LOGI', 'DITECH_ARCHIVIAZIONE', 'BRC_ECO', '', 'BRC_PE', 'BRC_CAMI', 'BRC_IBC');



import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { OracleDbEnv } from './oracle-db-env.model';
import { OracleDbEnvPopupService } from './oracle-db-env-popup.service';
import { OracleDbEnvService } from './oracle-db-env.service';

@Component({
    selector: 'jhi-oracle-db-env-delete-dialog',
    templateUrl: './oracle-db-env-delete-dialog.component.html'
})
export class OracleDbEnvDeleteDialogComponent {

    oracleDbEnv: OracleDbEnv;

    constructor(
        private oracleDbEnvService: OracleDbEnvService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.oracleDbEnvService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'oracleDbEnvListModification',
                content: 'Deleted an oracleDbEnv'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-oracle-db-env-delete-popup',
    template: ''
})
export class OracleDbEnvDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private oracleDbEnvPopupService: OracleDbEnvPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.oracleDbEnvPopupService
                .open(OracleDbEnvDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

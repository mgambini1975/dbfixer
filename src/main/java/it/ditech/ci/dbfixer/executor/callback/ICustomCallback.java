package it.ditech.ci.dbfixer.executor.callback;

/**
 * Contratto per una callback di risposta agli eventi del motore di applicazione delle patches
 * 
 * @author mgambini
 *
 */
public interface ICustomCallback {
	
	public CallbackType getCallbackType();
	public boolean isEnabled();

}

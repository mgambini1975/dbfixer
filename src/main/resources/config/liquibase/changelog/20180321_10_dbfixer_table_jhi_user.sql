--liquibase formatted sql
--changeset mgambini:creazione tabella Jhi_user

-- --------------------------------------------------------

--
-- Struttura della tabella `jhi_user`
--

CREATE TABLE IF NOT EXISTS `jhi_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_bin DEFAULT NULL,
  `first_name` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_name` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `image_url` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `activated` bit(1) NOT NULL,
  `lang_key` varchar(5) COLLATE utf8_bin DEFAULT NULL,
  `activation_key` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `reset_key` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `created_by` varchar(50) COLLATE utf8_bin NOT NULL,
  `created_date` timestamp NOT NULL,
  `reset_date` timestamp NULL DEFAULT NULL,
  `last_modified_by` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_modified_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  UNIQUE KEY `idx_user_login` (`login`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `idx_user_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


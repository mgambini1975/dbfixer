package it.ditech.ci.dbfixer.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import it.ditech.ci.dbfixer.domain.TomcatEnv;
import it.ditech.ci.dbfixer.domain.enumeration.StageEnvType;
import it.ditech.ci.dbfixer.repository.TomcatEnvRepository;
import it.ditech.ci.dbfixer.web.rest.errors.ExceptionTranslator;
/**
 * Test class for the TomcatEnvResource REST controller.
 *
 * @see TomcatEnvResource
 */
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = DbFixerApp.class)
public class TomcatEnvResourceIntTest {

    private static final StageEnvType DEFAULT_ENV_TYPE = StageEnvType.DEV;
    private static final StageEnvType UPDATED_ENV_TYPE = StageEnvType.CI;

    private static final String DEFAULT_HOSTNAME = "AAAAAAAAAA";
    private static final String UPDATED_HOSTNAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_HTTP_PORT = 1;
    private static final Integer UPDATED_HTTP_PORT = 2;

    private static final Integer DEFAULT_HTTPS_PORT = 1;
    private static final Integer UPDATED_HTTPS_PORT = 2;

    private static final Integer DEFAULT_DEBUG_PORT = 1;
    private static final Integer UPDATED_DEBUG_PORT = 2;

    private static final String DEFAULT_CONTEXT_ROOT = "AAAAAAAAAA";
    private static final String UPDATED_CONTEXT_ROOT = "BBBBBBBBBB";

    @Autowired
    private TomcatEnvRepository tomcatEnvRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTomcatEnvMockMvc;

    private TomcatEnv tomcatEnv;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TomcatEnvResource tomcatEnvResource = new TomcatEnvResource(tomcatEnvRepository);
        this.restTomcatEnvMockMvc = MockMvcBuilders.standaloneSetup(tomcatEnvResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TomcatEnv createEntity(EntityManager em) {
        TomcatEnv tomcatEnv = new TomcatEnv()
            .envType(DEFAULT_ENV_TYPE)
            .hostname(DEFAULT_HOSTNAME)
            .httpPort(DEFAULT_HTTP_PORT)
            .httpsPort(DEFAULT_HTTPS_PORT)
            .debugPort(DEFAULT_DEBUG_PORT)
            .contextRoot(DEFAULT_CONTEXT_ROOT);
        return tomcatEnv;
    }

    @Before
    public void initTest() {
        tomcatEnv = createEntity(em);
    }

    //@Test
    @Transactional
    public void createTomcatEnv() throws Exception {
        int databaseSizeBeforeCreate = tomcatEnvRepository.findAll().size();

        // Create the TomcatEnv
        restTomcatEnvMockMvc.perform(post("/api/tomcat-envs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tomcatEnv)))
            .andExpect(status().isCreated());

        // Validate the TomcatEnv in the database
        List<TomcatEnv> tomcatEnvList = tomcatEnvRepository.findAll();
        assertThat(tomcatEnvList).hasSize(databaseSizeBeforeCreate + 1);
        TomcatEnv testTomcatEnv = tomcatEnvList.get(tomcatEnvList.size() - 1);
        assertThat(testTomcatEnv.getEnvType()).isEqualTo(DEFAULT_ENV_TYPE);
        assertThat(testTomcatEnv.getHostname()).isEqualTo(DEFAULT_HOSTNAME);
        assertThat(testTomcatEnv.getHttpPort()).isEqualTo(DEFAULT_HTTP_PORT);
        assertThat(testTomcatEnv.getHttpsPort()).isEqualTo(DEFAULT_HTTPS_PORT);
        assertThat(testTomcatEnv.getDebugPort()).isEqualTo(DEFAULT_DEBUG_PORT);
        assertThat(testTomcatEnv.getContextRoot()).isEqualTo(DEFAULT_CONTEXT_ROOT);
    }

    //@Test
    @Transactional
    public void createTomcatEnvWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tomcatEnvRepository.findAll().size();

        // Create the TomcatEnv with an existing ID
        tomcatEnv.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTomcatEnvMockMvc.perform(post("/api/tomcat-envs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tomcatEnv)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<TomcatEnv> tomcatEnvList = tomcatEnvRepository.findAll();
        assertThat(tomcatEnvList).hasSize(databaseSizeBeforeCreate);
    }

    //@Test
    @Transactional
    public void checkEnvTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = tomcatEnvRepository.findAll().size();
        // set the field null
        tomcatEnv.setEnvType(null);

        // Create the TomcatEnv, which fails.

        restTomcatEnvMockMvc.perform(post("/api/tomcat-envs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tomcatEnv)))
            .andExpect(status().isBadRequest());

        List<TomcatEnv> tomcatEnvList = tomcatEnvRepository.findAll();
        assertThat(tomcatEnvList).hasSize(databaseSizeBeforeTest);
    }

    //@Test
    @Transactional
    public void checkHostnameIsRequired() throws Exception {
        int databaseSizeBeforeTest = tomcatEnvRepository.findAll().size();
        // set the field null
        tomcatEnv.setHostname(null);

        // Create the TomcatEnv, which fails.

        restTomcatEnvMockMvc.perform(post("/api/tomcat-envs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tomcatEnv)))
            .andExpect(status().isBadRequest());

        List<TomcatEnv> tomcatEnvList = tomcatEnvRepository.findAll();
        assertThat(tomcatEnvList).hasSize(databaseSizeBeforeTest);
    }

    //@Test
    @Transactional
    public void checkHttpPortIsRequired() throws Exception {
        int databaseSizeBeforeTest = tomcatEnvRepository.findAll().size();
        // set the field null
        tomcatEnv.setHttpPort(null);

        // Create the TomcatEnv, which fails.

        restTomcatEnvMockMvc.perform(post("/api/tomcat-envs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tomcatEnv)))
            .andExpect(status().isBadRequest());

        List<TomcatEnv> tomcatEnvList = tomcatEnvRepository.findAll();
        assertThat(tomcatEnvList).hasSize(databaseSizeBeforeTest);
    }

    //@Test
    @Transactional
    public void getAllTomcatEnvs() throws Exception {
        // Initialize the database
        tomcatEnvRepository.saveAndFlush(tomcatEnv);

        // Get all the tomcatEnvList
        restTomcatEnvMockMvc.perform(get("/api/tomcat-envs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tomcatEnv.getId().intValue())))
            .andExpect(jsonPath("$.[*].envType").value(hasItem(DEFAULT_ENV_TYPE.toString())))
            .andExpect(jsonPath("$.[*].hostname").value(hasItem(DEFAULT_HOSTNAME.toString())))
            .andExpect(jsonPath("$.[*].httpPort").value(hasItem(DEFAULT_HTTP_PORT)))
            .andExpect(jsonPath("$.[*].httpsPort").value(hasItem(DEFAULT_HTTPS_PORT)))
            .andExpect(jsonPath("$.[*].debugPort").value(hasItem(DEFAULT_DEBUG_PORT)))
            .andExpect(jsonPath("$.[*].contextRoot").value(hasItem(DEFAULT_CONTEXT_ROOT.toString())));
    }

    //@Test
    @Transactional
    public void getTomcatEnv() throws Exception {
        // Initialize the database
        tomcatEnvRepository.saveAndFlush(tomcatEnv);

        // Get the tomcatEnv
        restTomcatEnvMockMvc.perform(get("/api/tomcat-envs/{id}", tomcatEnv.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tomcatEnv.getId().intValue()))
            .andExpect(jsonPath("$.envType").value(DEFAULT_ENV_TYPE.toString()))
            .andExpect(jsonPath("$.hostname").value(DEFAULT_HOSTNAME.toString()))
            .andExpect(jsonPath("$.httpPort").value(DEFAULT_HTTP_PORT))
            .andExpect(jsonPath("$.httpsPort").value(DEFAULT_HTTPS_PORT))
            .andExpect(jsonPath("$.debugPort").value(DEFAULT_DEBUG_PORT))
            .andExpect(jsonPath("$.contextRoot").value(DEFAULT_CONTEXT_ROOT.toString()));
    }

    //@Test
    @Transactional
    public void getNonExistingTomcatEnv() throws Exception {
        // Get the tomcatEnv
        restTomcatEnvMockMvc.perform(get("/api/tomcat-envs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    //@Test
    @Transactional
    public void updateTomcatEnv() throws Exception {
        // Initialize the database
        tomcatEnvRepository.saveAndFlush(tomcatEnv);
        int databaseSizeBeforeUpdate = tomcatEnvRepository.findAll().size();

        // Update the tomcatEnv
        TomcatEnv updatedTomcatEnv = tomcatEnvRepository.findOne(tomcatEnv.getId());
        updatedTomcatEnv
            .envType(UPDATED_ENV_TYPE)
            .hostname(UPDATED_HOSTNAME)
            .httpPort(UPDATED_HTTP_PORT)
            .httpsPort(UPDATED_HTTPS_PORT)
            .debugPort(UPDATED_DEBUG_PORT)
            .contextRoot(UPDATED_CONTEXT_ROOT);

        restTomcatEnvMockMvc.perform(put("/api/tomcat-envs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTomcatEnv)))
            .andExpect(status().isOk());

        // Validate the TomcatEnv in the database
        List<TomcatEnv> tomcatEnvList = tomcatEnvRepository.findAll();
        assertThat(tomcatEnvList).hasSize(databaseSizeBeforeUpdate);
        TomcatEnv testTomcatEnv = tomcatEnvList.get(tomcatEnvList.size() - 1);
        assertThat(testTomcatEnv.getEnvType()).isEqualTo(UPDATED_ENV_TYPE);
        assertThat(testTomcatEnv.getHostname()).isEqualTo(UPDATED_HOSTNAME);
        assertThat(testTomcatEnv.getHttpPort()).isEqualTo(UPDATED_HTTP_PORT);
        assertThat(testTomcatEnv.getHttpsPort()).isEqualTo(UPDATED_HTTPS_PORT);
        assertThat(testTomcatEnv.getDebugPort()).isEqualTo(UPDATED_DEBUG_PORT);
        assertThat(testTomcatEnv.getContextRoot()).isEqualTo(UPDATED_CONTEXT_ROOT);
    }

    //@Test
    @Transactional
    public void updateNonExistingTomcatEnv() throws Exception {
        int databaseSizeBeforeUpdate = tomcatEnvRepository.findAll().size();

        // Create the TomcatEnv

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTomcatEnvMockMvc.perform(put("/api/tomcat-envs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tomcatEnv)))
            .andExpect(status().isCreated());

        // Validate the TomcatEnv in the database
        List<TomcatEnv> tomcatEnvList = tomcatEnvRepository.findAll();
        assertThat(tomcatEnvList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    //@Test
    @Transactional
    public void deleteTomcatEnv() throws Exception {
        // Initialize the database
        tomcatEnvRepository.saveAndFlush(tomcatEnv);
        int databaseSizeBeforeDelete = tomcatEnvRepository.findAll().size();

        // Get the tomcatEnv
        restTomcatEnvMockMvc.perform(delete("/api/tomcat-envs/{id}", tomcatEnv.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TomcatEnv> tomcatEnvList = tomcatEnvRepository.findAll();
        assertThat(tomcatEnvList).hasSize(databaseSizeBeforeDelete - 1);
    }

    //@Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TomcatEnv.class);
        TomcatEnv tomcatEnv1 = new TomcatEnv();
        tomcatEnv1.setId(1L);
        TomcatEnv tomcatEnv2 = new TomcatEnv();
        tomcatEnv2.setId(tomcatEnv1.getId());
        assertThat(tomcatEnv1).isEqualTo(tomcatEnv2);
        tomcatEnv2.setId(2L);
        assertThat(tomcatEnv1).isNotEqualTo(tomcatEnv2);
        tomcatEnv1.setId(null);
        assertThat(tomcatEnv1).isNotEqualTo(tomcatEnv2);
    }
}

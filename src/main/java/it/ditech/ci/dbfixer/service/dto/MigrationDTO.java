package it.ditech.ci.dbfixer.service.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class MigrationDTO implements Serializable {

	private static final long serialVersionUID = -8195043877917279743L;

	private Long installedRank;
	private String version;
	private String description;
	private String type;
	private String scriptName;
	private Long checksum;
	private String installedBy;
	private Timestamp installTime;
	private Long execTime;
	private Boolean success;
	

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public Long getInstalledRank() {
		return installedRank;
	}


	public void setInstalledRank(Long installedRank) {
		this.installedRank = installedRank;
	}


	public String getVersion() {
		return version;
	}


	public void setVersion(String version) {
		this.version = version;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public Long getChecksum() {
		return checksum;
	}


	public void setChecksum(Long checksum) {
		this.checksum = checksum;
	}


	public String getInstalledBy() {
		return installedBy;
	}


	public void setInstalledBy(String installedBy) {
		this.installedBy = installedBy;
	}


	public Timestamp getInstallTime() {
		return installTime;
	}


	public void setInstallTime(Timestamp installTime) {
		this.installTime = installTime;
	}


	public Long getExecTime() {
		return execTime;
	}


	public void setExecTime(Long execTime) {
		this.execTime = execTime;
	}


	public Boolean getSuccess() {
		return success;
	}


	public void setSuccess(Boolean success) {
		this.success = success;
	}


	public String getScriptName() {
		return scriptName;
	}


	public void setScriptName(String scriptName) {
		this.scriptName = scriptName;
	}

}

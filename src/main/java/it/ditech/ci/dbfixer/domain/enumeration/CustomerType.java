package it.ditech.ci.dbfixer.domain.enumeration;

/**
 * Customer type enumeration
 * 
 * @author mgambini
 *
 */
public enum CustomerType {
	
	MAXIDI, NORDICONAD, CEDIMARCHE, CRAI, UNIMAX, DB_0

}

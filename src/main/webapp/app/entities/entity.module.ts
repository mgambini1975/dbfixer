import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { DbFixerCustomerModule } from './customer/customer.module';
import { DbFixerAliasModule } from './alias/alias.module';
import { DbFixerProductModule } from './product/product.module';
import { DbFixerProductVersionModule } from './productversion/productversion.module';
import { DbFixerOracleDbEnvModule } from './oracle-db-env/oracle-db-env.module';
import { DbFixerWildflyEnvModule } from './wildfly-env/wildfly-env.module';
import { DbFixerTomcatEnvModule } from './tomcat-env/tomcat-env.module';
import { DbFixerMigrationModule } from './migration/migration.module';
import { DbFixerMigration2Module } from './migration2/migration2.module';
import { DbFixerOperationModule } from './operation/operation.module';

/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        DbFixerCustomerModule,
		DbFixerAliasModule,
        DbFixerProductModule,
        DbFixerProductVersionModule,
        DbFixerOracleDbEnvModule,
        DbFixerWildflyEnvModule,
        DbFixerTomcatEnvModule,
        DbFixerMigrationModule,
        DbFixerMigration2Module,
        DbFixerOperationModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DbFixerEntityModule {}

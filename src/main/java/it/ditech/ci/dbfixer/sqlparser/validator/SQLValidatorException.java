package it.ditech.ci.dbfixer.sqlparser.validator;

/**
 * Eccezione sollevata in fase di validazione di uno script .sql
 * 
 * @author mgambini
 *
 */
public class SQLValidatorException extends Exception {

	private static final long serialVersionUID = -9217969099445827215L;
	private final String filename; 

	public SQLValidatorException(String msg, Throwable cause, String fileName){
		super(msg, cause);
		this.filename = fileName;
	}
	
		
	public String getFilename() {
		return filename;
	}

}

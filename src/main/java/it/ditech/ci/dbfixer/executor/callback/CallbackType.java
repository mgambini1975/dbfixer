package it.ditech.ci.dbfixer.executor.callback;

/**
 * Tipologie di callback di risposta agli eventi del motore di applicazione delle patches
 * 
 * @author mgambini
 *
 */
public enum CallbackType {
	
	SUCCESS, FAILURE, ALLOUTCOME, WILLRUN

}

package it.ditech.ci.dbfixer.service.dto;

import java.io.Serializable;
import java.sql.Date;


/**
 * Definisce lo stato di versionamento di un DB target, caratterizzato da:
 * 
 * - Prodotto
 * - DbEnv
 * - Versione corrente di MORE
 * - Data di aggiornamento della versione corrente
 * - PatchsetID corrispondente all'ultima patch applicata in ordine temporle
 * - Data di applicazione dell'ultima patch  
 *   
 * @author mgambini
 *
 */
public class DbVersionMetadataDTO implements Serializable {

	private static final long serialVersionUID = -6171641786840359660L;

	private String product;
	private String stage;
	private String version;
	private Date versionAppliedDate;
	private String patchset;
	private Date patchsetAppliedDate;

	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public String getProduct() {
		return product;
	}


	public void setProduct(String product) {
		this.product = product;
	}


	public String getStage() {
		return stage;
	}


	public void setStage(String stage) {
		this.stage = stage;
	}


	public String getVersion() {
		return version;
	}


	public void setVersion(String version) {
		this.version = version;
	}


	public Date getVersionAppliedDate() {
		return versionAppliedDate;
	}


	public void setVersionAppliedDate(Date versionAppliedDate) {
		this.versionAppliedDate = versionAppliedDate;
	}


	public String getPatchset() {
		return patchset;
	}


	public void setPatchset(String patchset) {
		this.patchset = patchset;
	}


	public Date getPatchsetAppliedDate() {
		return patchsetAppliedDate;
	}


	public void setPatchsetAppliedDate(Date patchsetAppliedDate) {
		this.patchsetAppliedDate = patchsetAppliedDate;
	}

}

--liquibase formatted sql
--changeset mgambini:creazione tabella Oracle_db_env

-- --------------------------------------------------------

--
-- Struttura della tabella `oracle_db_env`
--

CREATE TABLE IF NOT EXISTS `oracle_db_env` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `env_type` varchar(255) COLLATE utf8_bin NOT NULL,
  `hostname` varchar(255) COLLATE utf8_bin NOT NULL,
  `listener_port` int(11) NOT NULL,
  `jhi_user` varchar(255) COLLATE utf8_bin NOT NULL,
  `pwd` varchar(255) COLLATE utf8_bin NOT NULL,
  `sid` varchar(255) COLLATE utf8_bin NOT NULL,
  `patchset` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `env_desc` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `patchsetdbrepourl` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_oracle_db_env_product_id` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


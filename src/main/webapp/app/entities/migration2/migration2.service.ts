import { Injectable, Inject, Optional } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Migration2 } from './migration2.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { BaseService } from '../../shared/base.service';
import {Message} from 'primeng/primeng';
import { ProfileService } from '../../layouts/profiles/profile.service';

@Injectable()
export class Migration2Service extends BaseService {

     private resourceUrl = '/migrations-api/activities';

     private contextRestApi: String = '';
     private contextMock: String = '/data-scheda';
     private uriRestApi: String = '';
     private uriMock: String = '';
     private mock  = false;
     
     constructor(private http: Http, profileService: ProfileService, @Inject('URL_BASE') @Optional() baseURI?: string, @Inject('IS_MOCK') @Optional() mock?: boolean) {
      super(profileService);
      if (mock != null) {
       this.mock = mock;
      }
      if (baseURI != null) {
       console.log('baseURI: '+baseURI);
       this.uriRestApi = baseURI + this.contextRestApi;
       this.uriMock = baseURI + this.contextMock;
      } else {
       this.uriRestApi = this.getBaseURL() + this.contextRestApi;
       this.uriMock = this.baseMockURI + this.contextMock;
      }
     }
      
    public loadActivities(msgs: Message[]): Promise<Migration2[]> {
	 	const url = this.getServiceUrl(this.resourceUrl, '');
	 	console.log('migration2service.loadActivities ' + url);
	 	return this.http.get(url, {headers : this.createHeader('GET')}).toPromise().then(this.extractData)
	 		.catch(
	 			(err)  => {
	 					this.handleErrorMsgs(err, msgs);
	 				}
	 		);
	}
	
	public getServiceUrl(uri: String, file: String) {
    	let url = this.uriRestApi + '' + uri;
    	if (this.mock) {
      		url = this.uriMock + '' + file;
    	}
        return url;
    }
	
  public extractData(res: Response) {
    	return res.json();
  }


  public handleErrorMsgs(error: Response | any, msgs: Message[]): Promise<any> {
    	console.log('Error=' + error);
    	let errMsg: string;
    	if (error instanceof Response) {
      		const body = error.json() || '';
      		const err = body.error || JSON.stringify(body);
      		const errCode = `${error.status}`;
      		errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      		if (errCode === '500') {
        		errMsg = 'Errore nell\'invocazione dei servizi di back-end' ;
      		}
    	} else {
      		errMsg = error.message ? error.message : error.toString();
    	}
    	console.log('Detail Error=' + errMsg);
    	msgs.push({severity: 'error', summary: 'Error Message', detail: errMsg});
    	this.handleError(error);
    	return Promise.reject(errMsg);
  }
	
	
    find(id: number): Observable<Migration2> {
    	const url = this.getServiceUrl(this.resourceUrl, '');
    	console.log("URL: "+url);
        return this.http.get(`${url}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(migration: Migration2): Migration2 {
        const copy: Migration2 = Object.assign({}, migration);
        return copy;
    }
}

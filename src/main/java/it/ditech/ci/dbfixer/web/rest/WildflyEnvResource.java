package it.ditech.ci.dbfixer.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import it.ditech.ci.dbfixer.domain.WildflyEnv;
import it.ditech.ci.dbfixer.repository.WildflyEnvRepository;
import it.ditech.ci.dbfixer.web.rest.util.HeaderUtil;
import it.ditech.ci.dbfixer.web.rest.util.PaginationUtil;

/**
 * REST controller for managing WildflyEnv.
 */
@RestController
@RequestMapping("/api")
public class WildflyEnvResource {

    private final Logger log = LoggerFactory.getLogger(WildflyEnvResource.class);

    private static final String ENTITY_NAME = "wildflyEnv";

    private final WildflyEnvRepository wildflyEnvRepository;
    public WildflyEnvResource(WildflyEnvRepository wildflyEnvRepository) {
        this.wildflyEnvRepository = wildflyEnvRepository;
    }

    /**
     * POST  /wildfly-envs : Create a new wildflyEnv.
     *
     * @param wildflyEnv the wildflyEnv to create
     * @return the ResponseEntity with status 201 (Created) and with body the new wildflyEnv, or with status 400 (Bad Request) if the wildflyEnv has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/wildfly-envs")
    @Timed
    public ResponseEntity<WildflyEnv> createWildflyEnv(@Valid @RequestBody WildflyEnv wildflyEnv) throws URISyntaxException {
        log.debug("REST request to save WildflyEnv : {}", wildflyEnv);
        if (wildflyEnv.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new wildflyEnv cannot already have an ID")).body(null);
        }
        WildflyEnv result = wildflyEnvRepository.save(wildflyEnv);
        return ResponseEntity.created(new URI("/api/wildfly-envs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /wildfly-envs : Updates an existing wildflyEnv.
     *
     * @param wildflyEnv the wildflyEnv to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated wildflyEnv,
     * or with status 400 (Bad Request) if the wildflyEnv is not valid,
     * or with status 500 (Internal Server Error) if the wildflyEnv couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/wildfly-envs")
    @Timed
    public ResponseEntity<WildflyEnv> updateWildflyEnv(@Valid @RequestBody WildflyEnv wildflyEnv) throws URISyntaxException {
        log.debug("REST request to update WildflyEnv : {}", wildflyEnv);
        if (wildflyEnv.getId() == null) {
            return createWildflyEnv(wildflyEnv);
        }
        WildflyEnv result = wildflyEnvRepository.save(wildflyEnv);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, wildflyEnv.getId().toString()))
            .body(result);
    }

    /**
     * GET  /wildfly-envs : get all the wildflyEnvs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of wildflyEnvs in body
     */
    @GetMapping("/wildfly-envs")
    @Timed
    public ResponseEntity<List<WildflyEnv>> getAllWildflyEnvs(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of WildflyEnvs");
        Page<WildflyEnv> page = wildflyEnvRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/wildfly-envs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /wildfly-envs/:id : get the "id" wildflyEnv.
     *
     * @param id the id of the wildflyEnv to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the wildflyEnv, or with status 404 (Not Found)
     */
    @GetMapping("/wildfly-envs/{id}")
    @Timed
    public ResponseEntity<WildflyEnv> getWildflyEnv(@PathVariable Long id) {
        log.debug("REST request to get WildflyEnv : {}", id);
        WildflyEnv wildflyEnv = wildflyEnvRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(wildflyEnv));
    }

    /**
     * DELETE  /wildfly-envs/:id : delete the "id" wildflyEnv.
     *
     * @param id the id of the wildflyEnv to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/wildfly-envs/{id}")
    @Timed
    public ResponseEntity<Void> deleteWildflyEnv(@PathVariable Long id) {
        log.debug("REST request to delete WildflyEnv : {}", id);
        wildflyEnvRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}

package it.ditech.ci.dbfixer.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.ditech.ci.dbfixer.service.dto.OracleDbEnvDTO;

/**
 * Service Interface for managing OracleDbEnv.
 */
public interface OracleDbEnvService {

    /**
     * Save a oracleDbEnv.
     *
     * @param oracleDbEnvDTO the entity to save
     * @return the persisted entity
     */
    OracleDbEnvDTO save(OracleDbEnvDTO oracleDbEnvDTO);

    /**
     *  Get all the oracleDbEnvs.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<OracleDbEnvDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" oracleDbEnv.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    OracleDbEnvDTO findOne(Long id);

    /**
     *  Delete the "id" oracleDbEnv.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}

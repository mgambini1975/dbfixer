package it.ditech.ci.dbfixer.sqlparser.validator;

/**
 * Schemi DB M.O.R.E.
 * 
 * @author mgambini
 *
 */
public enum DbAlias {

	COMM, CORE, PU, DCC, INTE, SHARE, MNTR, SCHD, DIVU, LOGI, ARCH, ECOM, COW, PENG, CAMI, IBC, FBDC, WST, SCMSO;

	public static String concatValues() {
		StringBuffer toRet = new StringBuffer();
		for (DbAlias alias : DbAlias.values()) {
			toRet = toRet.append(alias).append(",");
		}
		toRet.deleteCharAt(toRet.length()-1);
		return toRet.toString();
	}

}

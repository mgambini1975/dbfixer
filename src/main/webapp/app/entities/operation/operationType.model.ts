import { BaseEntity } from './../../shared';

export class OperationType implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public desc?: string
    ) {
    }
}
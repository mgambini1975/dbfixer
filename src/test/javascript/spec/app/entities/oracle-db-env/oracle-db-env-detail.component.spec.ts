/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { DbFixerTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { OracleDbEnvDetailComponent } from '../../../../../../main/webapp/app/entities/oracle-db-env/oracle-db-env-detail.component';
import { OracleDbEnvService } from '../../../../../../main/webapp/app/entities/oracle-db-env/oracle-db-env.service';
import { OracleDbEnv } from '../../../../../../main/webapp/app/entities/oracle-db-env/oracle-db-env.model';

describe('Component Tests', () => {

    describe('OracleDbEnv Management Detail Component', () => {
        let comp: OracleDbEnvDetailComponent;
        let fixture: ComponentFixture<OracleDbEnvDetailComponent>;
        let service: OracleDbEnvService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DbFixerTestModule],
                declarations: [OracleDbEnvDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    OracleDbEnvService,
                    JhiEventManager
                ]
            }).overrideTemplate(OracleDbEnvDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(OracleDbEnvDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(OracleDbEnvService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new OracleDbEnv(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.oracleDbEnv).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});

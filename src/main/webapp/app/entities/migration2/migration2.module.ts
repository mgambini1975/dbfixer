import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
/* componenti PrimeNG */
import {DataTableModule} from 'primeng/primeng';



import { DbFixerSharedModule } from '../../shared';
import {
    Migration2Service,
    Migration2Component,
    Migration2DetailComponent,
    migration2Route,
} from './';

const ENTITY_STATES = [
    ...migration2Route,
];

@NgModule({
    imports: [
        DbFixerSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
          /* componenti PrimeNG */
	    DataTableModule
    ],
    declarations: [
        Migration2Component,
        Migration2DetailComponent,
    ],
    entryComponents: [
        Migration2Component,
        Migration2DetailComponent,
    ],
    providers: [
        Migration2Service,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DbFixerMigration2Module {}

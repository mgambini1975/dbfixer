package it.ditech.ci.dbfixer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.ditech.ci.dbfixer.domain.ProductVersion;

/**
 * Spring Data JPA repository for the ProductVersion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductVersionRepository extends JpaRepository<ProductVersion, Long> {

}

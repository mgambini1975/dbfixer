package it.ditech.ci.dbfixer.security;

import java.util.Properties;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.atlassian.crowd.service.client.ClientPropertiesImpl;

/**
 * Implementa l'insieme delle <link>Properties</link> che identificano la webapp presso Crowd
 * 
 * @deprecated FIXME: esternalizzare in file config o parconf DB
 * @author mgambini
 *
 */
@Component
public class CustomClientPropertiesImpl extends ClientPropertiesImpl {

	private final Environment env;
	
	public CustomClientPropertiesImpl(Environment env) {
		this.env = env;
		loadProperties();
		
	}
	
	private void loadProperties() {
		Properties props = new Properties();
		String appName = getEnv().getProperty("crowd.application.name");
		if (!StringUtils.isEmpty(appName)) {
			props.put("application.name", appName);
		}
		String appPassword = getEnv().getProperty("crowd.application.password");
		if (!StringUtils.isEmpty(appPassword)) {
			props.put("application.password", appPassword);
		}
		String crowdServerUrl =  getEnv().getProperty("crowd.server.url");
		if (!StringUtils.isEmpty(crowdServerUrl)) {
			props.put("crowd.server.url", crowdServerUrl);
		}
		props.put("session.validationinterval", 0);
		this.updateProperties(props);
	}

	public Environment getEnv() {
		return this.env;
	}
	
}

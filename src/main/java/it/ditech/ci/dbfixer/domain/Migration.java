package it.ditech.ci.dbfixer.domain;

import java.sql.Time;
import java.sql.Timestamp;
import static it.ditech.ci.dbfixer.config.Constants.SUCCESS;
import static it.ditech.ci.dbfixer.config.Constants.FAILURE;

/**
 * Modella l'esito di una migrazione
 * @author mgambini
 *
 */
public class Migration {
	
	private Long installedRank;
	private String version;
	private String description;
	private String type;
	private String scriptName;
	private String installedBy;
	private String author;
	private Timestamp installTime;
	private Time execTime;
	private Boolean success;
	private String log;
	private String fileName;
	private String md5sum;
	private String outcome;
	
	public Long getInstalledRank() {
		return installedRank;
	}
	public void setInstalledRank(Long installedRank) {
		this.installedRank = installedRank;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getScriptName() {
		return scriptName;
	}
	public void setScriptName(String scriptName) {
		this.scriptName = scriptName;
	}
	public String getInstalledBy() {
		return installedBy;
	}
	public void setInstalledBy(String installedBy) {
		this.installedBy = installedBy;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public Timestamp getInstallTime() {
		return installTime;
	}
	public void setInstallTime(Timestamp installTime) {
		this.installTime = installTime;
	}
	public Time getExecTime() {
		return execTime;
	}
	public void setExecTime(Time execTime) {
		this.execTime = execTime;
	}
	public Boolean getSuccess() {
		return success;
	}
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	public String getLog() {
		return log;
	}
	public void setLog(String log) {
		this.log = log;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getMd5sum() {
		return md5sum;
	}
	public void setMd5sum(String md5sum) {
		this.md5sum = md5sum;
	}
	
	public String getOutcome(){
		if (this.getSuccess()!= null && Boolean.TRUE.equals(this.getSuccess())){
			return SUCCESS;
		} else
			return FAILURE;
	}
	public void setOutcome(String outcome) {
		this.outcome = outcome;
	}
}

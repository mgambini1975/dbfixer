package it.ditech.ci.dbfixer.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import it.ditech.ci.dbfixer.domain.WildflyEnv;
import it.ditech.ci.dbfixer.service.dto.WildflyEnvDTO;

/**
 * Mapper for the entity WildflyEnv and its DTO WildflyEnvDTO.
 */
@Mapper(componentModel = "spring", uses = {ProductMapper.class, })
public interface WildflyEnvMapper extends EntityMapper <WildflyEnvDTO, WildflyEnv> {

    @Mapping(source = "product.id", target = "productId")
    WildflyEnvDTO toDto(WildflyEnv wildflyEnv); 

    @Mapping(source = "productId", target = "product")
    WildflyEnv toEntity(WildflyEnvDTO wildflyEnvDTO); 
    default WildflyEnv fromId(Long id) {
        if (id == null) {
            return null;
        }
        WildflyEnv wildflyEnv = new WildflyEnv();
        wildflyEnv.setId(id);
        return wildflyEnv;
    }
}

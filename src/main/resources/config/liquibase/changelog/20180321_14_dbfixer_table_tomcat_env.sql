--liquibase formatted sql
--changeset mgambini:creazione tabella Tomcat_env

-- --------------------------------------------------------

--
-- Struttura della tabella `tomcat_env`
--

CREATE TABLE IF NOT EXISTS `tomcat_env` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `env_type` varchar(255) COLLATE utf8_bin NOT NULL,
  `hostname` varchar(255) COLLATE utf8_bin NOT NULL,
  `http_port` int(11) NOT NULL,
  `https_port` int(11) DEFAULT NULL,
  `debug_port` int(11) DEFAULT NULL,
  `context_root` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tomcat_env_product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

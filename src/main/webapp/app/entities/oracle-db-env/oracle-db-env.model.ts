import { BaseEntity } from './../../shared';

const enum DEPLOYMENT_ENV_TYPE {
    'DEV',
    'CI',
    'CQ',
    'PREPROD',
    'PROD'
}

export class OracleDbEnv implements BaseEntity {
    constructor(
        public id?: number,
        public envType?: DEPLOYMENT_ENV_TYPE,
        public hostname?: string,
        public listenerPort?: number,
        public user?: string,
        public pwd?: string,
        public syspwd?: string,
        public sid?: string,
        public patchsetdbrepourl?: string,
        public env_desc?: string,
        public product?: BaseEntity,
    ) {
    }
}

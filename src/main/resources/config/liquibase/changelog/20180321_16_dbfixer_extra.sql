--liquibase formatted sql
--changeset mgambini:creazione constraints


--
-- Limiti per la tabella `jhi_persistent_audit_evt_data`
--
ALTER TABLE `jhi_persistent_audit_evt_data`
  ADD CONSTRAINT `fk_evt_pers_audit_evt_data` FOREIGN KEY (`event_id`) REFERENCES `jhi_persistent_audit_event` (`event_id`);

--
-- Limiti per la tabella `jhi_user_authority`
--
ALTER TABLE `jhi_user_authority`
  ADD CONSTRAINT `fk_authority_name` FOREIGN KEY (`authority_name`) REFERENCES `jhi_authority` (`name`),
  ADD CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`);

--
-- Limiti per la tabella `oracle_db_env`
--
ALTER TABLE `oracle_db_env`
  ADD CONSTRAINT `fk_oracle_db_env_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);

--
-- Limiti per la tabella `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_product_customer_id` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`);

--
-- Limiti per la tabella `tomcat_env`
--
ALTER TABLE `tomcat_env`
  ADD CONSTRAINT `fk_tomcat_env_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);

--
-- Limiti per la tabella `wildfly_env`
--
ALTER TABLE `wildfly_env`
  ADD CONSTRAINT `fk_wildfly_env_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);

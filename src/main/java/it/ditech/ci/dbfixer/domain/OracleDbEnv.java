package it.ditech.ci.dbfixer.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.ditech.ci.dbfixer.domain.enumeration.StageEnvType;

/**
 * A OracleDbEnv.
 */
@Entity
@Table(name = "oracle_db_env")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class OracleDbEnv implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "env_type", nullable = false)
    private StageEnvType envType;

    @NotNull
    @Column(name = "hostname", nullable = false)
    private String hostname;

    @NotNull
    @Column(name = "listener_port", nullable = false)
    private Integer listenerPort;

    @NotNull
    @Column(name = "jhi_user", nullable = false)
    private String user;

    @NotNull
    @Column(name = "pwd", nullable = false)
    private String pwd;
    
    @NotNull
    @Column(name = "syspwd", nullable = true)
    private String syspwd;

	@NotNull
    @Column(name = "sid", nullable = false)
    private String sid;

    
    @Column(name = "patchsetdbrepourl")
    private String patchsetdbrepourl;
    
    @Column(name = "env_desc")
    private String env_desc;


	@ManyToOne
    private Product product;
	
	@JsonIgnore
	@OneToOne(mappedBy = "dbEnv")
    private Alias alias;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StageEnvType getEnvType() {
        return envType;
    }

    public OracleDbEnv envType(StageEnvType envType) {
        this.envType = envType;
        return this;
    }

    public void setEnvType(StageEnvType envType) {
        this.envType = envType;
    }

    public String getHostname() {
        return hostname;
    }

    public OracleDbEnv hostname(String hostname) {
        this.hostname = hostname;
        return this;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public Integer getListenerPort() {
        return listenerPort;
    }

    public OracleDbEnv listenerPort(Integer listenerPort) {
        this.listenerPort = listenerPort;
        return this;
    }

    public void setListenerPort(Integer listenerPort) {
        this.listenerPort = listenerPort;
    }

    public String getUser() {
        return user;
    }

    public OracleDbEnv user(String user) {
        this.user = user;
        return this;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPwd() {
        return pwd;
    }

    public OracleDbEnv pwd(String pwd) {
        this.pwd = pwd;
        return this;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getSid() {
        return sid;
    }

    public OracleDbEnv sid(String sid) {
        this.sid = sid;
        return this;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public Product getProduct() {
        return product;
    }

    public OracleDbEnv product(Product product) {
        this.product = product;
        return this;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
    
    public String getPatchsetdbrepourl() {
		return patchsetdbrepourl;
	}

	public void setPatchsetdbrepourl(String patchsetDBRepoUrl) {
		this.patchsetdbrepourl = patchsetDBRepoUrl;
	}

	public String getEnv_desc() {
		return env_desc;
	}

	public void setEnv_desc(String env_desc) {
		this.env_desc = env_desc;
	}
	
	public String getSyspwd() {
		return syspwd;
	}

	public void setSyspwd(String syspwd) {
		this.syspwd = syspwd;
	}
	
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OracleDbEnv oracleDbEnv = (OracleDbEnv) o;
        if (oracleDbEnv.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), oracleDbEnv.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OracleDbEnv{" +
            "id=" + getId() +
            ", envType='" + getEnvType() + "'" +
            ", hostname='" + getHostname() + "'" +
            ", listenerPort='" + getListenerPort() + "'" +
            ", user='" + getUser() + "'" +
            ", pwd='" + "******" + "'" +
            ", sid='" + getSid() + "'" +
            ", env_desc'" + getEnv_desc()  + "'" +
            ", patchsetdbrepourl'" + getPatchsetdbrepourl()  + "'" +
            "}";
    }

	public Alias getAlias() {
		return alias;
	}

	public void setAlias(Alias alias) {
		this.alias = alias;
	}

	
}

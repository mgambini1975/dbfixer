import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { TomcatEnvComponent } from './tomcat-env.component';
import { TomcatEnvDetailComponent } from './tomcat-env-detail.component';
import { TomcatEnvPopupComponent } from './tomcat-env-dialog.component';
import { TomcatEnvDeletePopupComponent } from './tomcat-env-delete-dialog.component';

export const tomcatEnvRoute: Routes = [
    {
        path: 'tomcat-env',
        component: TomcatEnvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.tomcatEnv.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tomcat-env/:id',
        component: TomcatEnvDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.tomcatEnv.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const tomcatEnvPopupRoute: Routes = [
    {
        path: 'tomcat-env-new',
        component: TomcatEnvPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.tomcatEnv.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'tomcat-env/:id/edit',
        component: TomcatEnvPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.tomcatEnv.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'tomcat-env/:id/delete',
        component: TomcatEnvDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.tomcatEnv.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];

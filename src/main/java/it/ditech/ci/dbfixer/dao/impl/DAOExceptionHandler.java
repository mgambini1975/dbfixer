package it.ditech.ci.dbfixer.dao.impl;

import static it.ditech.ci.dbfixer.config.Constants.TIPO_ENV;
import static it.ditech.ci.dbfixer.config.Constants.RELEASE;
import static it.ditech.ci.dbfixer.config.Constants.EXCEPTION_CAUGHT;
import io.sentry.Sentry;
import io.sentry.event.Event;
import io.sentry.event.EventBuilder;
import io.sentry.event.interfaces.ExceptionInterface;

/**
 * Gestore delle eccezioni DAO
 * 
 * @author mgambini
 *
 */
public class DAOExceptionHandler {
	
	public static void handleActivityDAOException(Exception e, Class loggerClass) throws ActivityDAOException {		
		 notifySentry(e, loggerClass);
		 throw new ActivityDAOException(e);
	}
	
	public static void handleMigrationDAOException(Exception e, Class loggerClass) throws MigrationDAOException {
		notifySentry(e, loggerClass);
		throw new MigrationDAOException(e);
	}
	
	private static void notifySentry(Exception e, Class loggerClass) {
		 EventBuilder eventBuilder = new EventBuilder()
	                .withMessage(EXCEPTION_CAUGHT)
	                .withLevel(Event.Level.ERROR)
	                .withLogger(loggerClass.getName())
	                .withEnvironment(System.getProperty(TIPO_ENV))
	                .withRelease(System.getProperty(RELEASE))
	                .withSentryInterface(new ExceptionInterface(e));
		 Sentry.capture(eventBuilder);
	}

}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TomcatEnv } from './tomcat-env.model';
import { TomcatEnvPopupService } from './tomcat-env-popup.service';
import { TomcatEnvService } from './tomcat-env.service';

@Component({
    selector: 'jhi-tomcat-env-delete-dialog',
    templateUrl: './tomcat-env-delete-dialog.component.html'
})
export class TomcatEnvDeleteDialogComponent {

    tomcatEnv: TomcatEnv;

    constructor(
        private tomcatEnvService: TomcatEnvService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.tomcatEnvService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'tomcatEnvListModification',
                content: 'Deleted an tomcatEnv'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-tomcat-env-delete-popup',
    template: ''
})
export class TomcatEnvDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tomcatEnvPopupService: TomcatEnvPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.tomcatEnvPopupService
                .open(TomcatEnvDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

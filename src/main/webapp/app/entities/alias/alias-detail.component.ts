import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Alias } from './alias.model';
import { AliasService } from './alias.service';

@Component({
    selector: 'jhi-alias-detail',
    templateUrl: './alias-detail.component.html'
})
export class AliasDetailComponent implements OnInit, OnDestroy {

    alias: Alias;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private aliasService: AliasService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAliass();
    }

    load(id) {
        this.aliasService.find(id).subscribe((alias) => {
            this.alias = alias;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAliass() {
        this.eventSubscriber = this.eventManager.subscribe(
            'aliasListModification',
            (response) => this.load(this.alias.id)
        );
    }
}

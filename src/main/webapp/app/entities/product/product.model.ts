import { BaseEntity } from './../../shared';

export class Product implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public version?: string,
        public customer?: BaseEntity,
        public wildflyEnvs?: BaseEntity[],
        public oracleDbEnvs?: BaseEntity[],
        public tomcatEnvs?: BaseEntity[],
    ) {
    }
}

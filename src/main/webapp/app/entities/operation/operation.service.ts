import { Injectable, Inject, Optional } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Product } from '../product/product.model';
import { OracleDbEnv } from '../oracle-db-env/oracle-db-env.model';
import { Version } from './version.model';
import { Operation } from './operation.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { BaseService } from '../../shared/base.service';
import {Message} from 'primeng/primeng';
import { ProfileService } from '../../layouts/profiles/profile.service';

@Injectable()
export class OperationService extends BaseService {

     private buildId: number = 10000;

     private upgradeUrl = '/migrations-api';
     private prodottiUrl = '/api/products';
     private ambientiUrl = '/api/oracle-db-envs';
     private versioniUrl = '/api/productVersions';

     private contextRestApi: String = '';
     private contextMock: String = '/data-scheda';
     private uriRestApi: String = '';
     private uriMock: String = '';
     private mock  = false;
     
     constructor(private http: Http, profileService: ProfileService, @Inject('URL_BASE') @Optional() baseURI?: string, @Inject('IS_MOCK') @Optional() mock?: boolean) {
      super(profileService);
      if (mock != null) {
       this.mock = mock;
      }
      if (baseURI != null) {
       console.log('baseURI: '+baseURI);
       this.uriRestApi = baseURI + this.contextRestApi;
       this.uriMock = baseURI + this.contextMock;
      } else {
       this.uriRestApi = this.getBaseURL() + this.contextRestApi;
       this.uriMock = this.baseMockURI + this.contextMock;
      }
    }

    public doEmulateRequest(selProdotto: string,
	selAmbiente: string,
    	selVersione: string,
    	msgs: Message[]): Promise<boolean> {

	let baseUrl = this.getServiceUrl(this.upgradeUrl, '');
    	let ambienteId : number;
    	if (selAmbiente === 'DEV'){
    		ambienteId = 1;
    	} else if (selAmbiente === 'CI'){
    		ambienteId = 2;
    	} else if (selAmbiente === 'PREPROD'){
    		ambienteId = 4;
    	} else if (selAmbiente === 'PROD'){
    		ambienteId = 5;
    	} else {
    		ambienteId = -1;
    	}
    	
    	let versionName: string;
    	if (selVersione === 'MASTER') {
    		versionName = 'latest';
    	}
    	else {
    		versionName = selVersione;
    	}
	this.buildId = this.buildId + 1;
	baseUrl = baseUrl +'/product/'+selProdotto+'/stage/'+ambienteId+'/version/'+versionName+'/sqloutput?buildId='+this.buildId;

	//alert('URL: '+baseUrl);
    	return this.http.post(baseUrl,null).toPromise().then(this.extractData)
	 	.catch(
	 			(err)  => {
	 					this.handleErrorMsgs(err, msgs);
	 				}
	 	);
    }

    public doTagRequest(selProdotto: string,
	selAmbiente: string,
    	selVersione: string,
    	msgs: Message[]): Promise<boolean> {

	let baseUrl = this.getServiceUrl(this.upgradeUrl, '');
    	let ambienteId : number;
    	if (selAmbiente === 'DEV'){
    		ambienteId = 1;
    	} else if (selAmbiente === 'CI'){
    		ambienteId = 2;
    	} else if (selAmbiente === 'PREPROD'){
    		ambienteId = 4;
    	} else if (selAmbiente === 'PROD'){
    		ambienteId = 5;
    	} else {
    		ambienteId = -1;
    	}
    	
    	let versionName: string;
    	if (selVersione === 'MASTER') {
    		versionName = 'latest';
    	}
    	else {
    		versionName = selVersione;
    	}
	this.buildId = this.buildId + 1;
	baseUrl = baseUrl +'/product/'+selProdotto+'/stage/'+ambienteId+'/version/'+versionName+'/tag?buildId='+this.buildId;

	//alert('URL: '+baseUrl);
    	return this.http.post(baseUrl,null).toPromise().then(this.extractData)
	 	.catch(
	 			(err)  => {
	 					this.handleErrorMsgs(err, msgs);
	 				}
	 	);
    }

    public doFixRequest(selProdotto: string,
    	selAmbiente: string,
    	selVersione: string,
    	activityId: string,
    	msgs: Message[]): Promise<boolean> {
	
	let baseUrl = this.getServiceUrl(this.upgradeUrl, '');
    	let ambienteId : number;
    	if (selAmbiente === 'DEV'){
    		ambienteId = 1;
    	} else if (selAmbiente === 'CI'){
    		ambienteId = 2;
    	} else if (selAmbiente === 'PREPROD'){
    		ambienteId = 4;
    	} else if (selAmbiente === 'PROD'){
    		ambienteId = 5;
    	} else {
    		ambienteId = -1;
    	}
    	
    	let versionName: string;
    	if (selVersione === 'MASTER') {
    		versionName = 'latest';
    	}
    	else {
    		versionName = selVersione;
    	}
	this.buildId = this.buildId + 1;
	baseUrl = baseUrl +'/product/'+selProdotto+'/stage/'+ambienteId+'/version/'+versionName+'/activity/'+activityId+'?buildId='+this.buildId;

	//alert('URL: '+baseUrl);
    	return this.http.patch(baseUrl,null).toPromise().then(this.extractData)
	 	.catch(
	 			(err)  => {
	 					this.handleErrorMsgs(err, msgs);
	 				}
	 	);
    }
     
    public doUpgradeRequest(selProdotto: string,
    	selAmbiente: string,
    	selVersione: string,
    	selTipoAggiornamenti: string,
    	patchId: string,
    	metadataOnly: boolean,
    	msgs: Message[]): Promise<boolean>{
    	
    	let baseUrl = this.getServiceUrl(this.upgradeUrl, '');
    	let ambienteId : number;
    	if (selAmbiente === 'DEV'){
    		ambienteId = 1;
    	} else if (selAmbiente === 'CI'){
    		ambienteId = 2;
    	} else if (selAmbiente === 'PREPROD'){
    		ambienteId = 4;
    	} else if (selAmbiente === 'PROD'){
    		ambienteId = 5;
    	} else {
    		ambienteId = -1;
    	}
    	
    	let versionName: string;
    	if (selVersione === 'MASTER') {
    		versionName = 'latest';
    	}
    	else {
    		versionName = selVersione;
    	}
    	baseUrl = baseUrl +'/product/'+selProdotto+'/stage/'+ambienteId+'/version/'+versionName;
    	this.buildId = this.buildId + 1;
    	if (selTipoAggiornamenti === '1'){
    		if (patchId.length === 1) {
    			patchId = '000'+patchId;
    		} else if (patchId.length === 2) {
    			patchId = '00'+patchId;
    		} else if (patchId.length === 3) {
    			patchId = '0'+patchId;
    		}
    		baseUrl = baseUrl + '/migration/'+patchId+'?metadataOnly='+metadataOnly+'&buildId='+this.buildId;
    	} else 
    		baseUrl = baseUrl + '/migrations?buildId='+this.buildId;
    	
    	//alert('URL: '+baseUrl);
    	return this.http.post(baseUrl,null).toPromise().then(this.extractData)
	 	.catch(
	 			(err)  => {
	 					this.handleErrorMsgs(err, msgs);
	 				}
	 	);
   }
      
   public loadProdotti(msgs: Message[]): Promise<Product[]> {
	 	const url = this.getServiceUrl(this.prodottiUrl, '');
	 	console.log('operationservice.loadProducts' + url);
	 	return this.http.get(url, {headers : this.createHeader('GET')}).toPromise().then(this.extractData)
	 		.catch(
	 			(err)  => {
	 					this.handleErrorMsgs(err, msgs);
	 				}
	 		);
   }
	
   public loadAmbienti(msgs: Message[]): Promise<OracleDbEnv[]> {
	 	const url = this.getServiceUrl(this.ambientiUrl, '');
	 	console.log('operationservice.loadOracleDbEnvs' + url);
	 	return this.http.get(url, {headers : this.createHeader('GET')}).toPromise().then(this.extractData)
	 		.catch(
	 			(err)  => {
	 					this.handleErrorMsgs(err, msgs);
	 				}
	 		);
   }


  public loadVersioni(msgs: Message[]): Promise<Version[]> {
	 	const url = this.getServiceUrl(this.versioniUrl, '');
	 	console.log('operationservice.loadProductVersions' + url);
	 	return this.http.get(url, {headers : this.createHeader('GET')}).toPromise().then(this.extractData)
	 		.catch(
	 			(err)  => {
	 					this.handleErrorMsgs(err, msgs);
	 				}
	 		);
   }
	
   public getServiceUrl(uri: String, file: String) {
    		let url = this.uriRestApi + '' + uri;
    		if (this.mock) {
      			url = this.uriMock + '' + file;
    		}
    		return url;
   }

   public extractData(res: Response) {
	    	return res.json();
   }
	
	
   public handleErrorMsgs(error: Response | any, msgs: Message[]): Promise<any> {
	    	console.log('Error=' + error);
	    	let errMsg: string;
	    	if (error instanceof Response) {
	      		const body = error.json() || '';
	      		const err = body.error || JSON.stringify(body);
	      		const errCode = `${error.status}`;
	      		errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
	      		if (errCode === '500') {
	        		errMsg = 'Errore nell\'invocazione dei servizi di back-end' ;
	      		}
	    	} else {
	      		errMsg = error.message ? error.message : error.toString();
	    	}
	    	console.log('Detail Error=' + errMsg);
	    	msgs.push({severity: 'error', summary: 'Error Message', detail: errMsg});
	    	this.handleError(error);
	    	return Promise.reject(errMsg);
   }
		
		
   find(id: number): Observable<Operation> {
	    	const url = this.getServiceUrl(this.prodottiUrl, '');
	    	console.log("URL: "+url);
	        return this.http.get(`${url}/${id}`).map((res: Response) => {
	            return res.json();
	        });
   }
	
   query(req?: any): Observable<ResponseWrapper> {
	        const options = createRequestOption(req);
	        return this.http.get(this.prodottiUrl, options)
	            .map((res: Response) => this.convertResponse(res));
   }
	
   private convertResponse(res: Response): ResponseWrapper {
	        const jsonResponse = res.json();
	        return new ResponseWrapper(res.headers, jsonResponse, res.status);
   }
	
   private convert(migration: Operation): Operation {
	        const copy: Operation = Object.assign({}, migration);
	        return copy;
   }
}

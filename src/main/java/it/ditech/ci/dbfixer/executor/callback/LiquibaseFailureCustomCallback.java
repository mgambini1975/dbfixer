package it.ditech.ci.dbfixer.executor.callback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import liquibase.changelog.ChangeSet;
import liquibase.changelog.ChangeSet.ExecType;
import liquibase.changelog.DatabaseChangeLog;
import liquibase.database.Database;

/**
 * Implementazione di una callback per la gestione degli eventi Liquibase di applicazione fallita di un patchset
 * 
 * @author mgambini
 */
public class LiquibaseFailureCustomCallback extends LiquibaseCustomCallback {
	
	private final Logger log = LoggerFactory.getLogger(LiquibaseFailureCustomCallback.class);
	
	
	public LiquibaseFailureCustomCallback(Long buildId){
		this(Boolean.TRUE, buildId);
	}
	
	public LiquibaseFailureCustomCallback(boolean enabled, Long buildId){
		super(CallbackType.FAILURE, enabled, buildId);
	}

	@Override
	public void process(ChangeSet changeSet, DatabaseChangeLog databaseChangeLog, Database database, ExecType execType, Exception exception) {
		log.debug("Run failed callback...");
		
		String logMsg = "Fallimento durante l'applicazione del patchset: "+exception.getMessage();	
		this.persistActivityInfo(database, Boolean.FALSE, changeSet, logMsg);
			
		log.debug("Fallimento durante l'applicazione del patchset.");
	}

	

}

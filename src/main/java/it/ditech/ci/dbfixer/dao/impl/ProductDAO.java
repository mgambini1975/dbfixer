package it.ditech.ci.dbfixer.dao.impl;

import static it.ditech.ci.dbfixer.sql.SQLQueries.PRODUCT_SELECT_ALL_BY_CUSTOMER_ID;
import static it.ditech.ci.dbfixer.sql.SQLQueries.PRODUCT_SELECT_ALL_BY_ID;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import it.ditech.ci.dbfixer.dao.IProductDAO;
import it.ditech.ci.dbfixer.service.dto.ProductDTO;

/**
 * Gestore della persistenza per i prodotti
 * 
 * @author mgambini
 */
@Repository
public class ProductDAO extends GenericDAO implements IProductDAO {
	
	private final Logger log = LoggerFactory.getLogger(ProductDAO.class);
	
	@Override
	public List<ProductDTO> getProductsByCustomerId(Long customerId) throws MigrationDAOException {
		List<ProductDTO> toRet = new ArrayList<ProductDTO>();
		String sql = PRODUCT_SELECT_ALL_BY_CUSTOMER_ID ;		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setLong(1, customerId);
			rs = ps.executeQuery();
			
			while (rs.next()) {	
				ProductDTO aProduct = new ProductDTO();
				aProduct.setId(rs.getLong(1));
				aProduct.setName(rs.getString(2));
				aProduct.setDescription(rs.getString(3));
				aProduct.setVersion(rs.getString(4));
				aProduct.setCustomerId(rs.getLong(5));
				toRet.add(aProduct);
			}
			
		} catch (SQLException e) {
			DAOExceptionHandler.handleMigrationDAOException(e, this.getClass());
		} finally {
			closeResourceGracefully(rs, ps, conn);
		}
		return toRet;
	}

	@Override
	public ProductDTO getProductFromId(Long productId) throws MigrationDAOException {
		ProductDTO aProduct = null;
		String sql = PRODUCT_SELECT_ALL_BY_ID;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setLong(1, productId);
			rs = ps.executeQuery();
			
			if (rs.next()) {	
				aProduct = new ProductDTO();
				aProduct.setId(rs.getLong(1));
				aProduct.setName(rs.getString(2));
				aProduct.setDescription(rs.getString(3));
				aProduct.setVersion(rs.getString(4));
				aProduct.setCustomerId(rs.getLong(5));
			}
			
		} catch (SQLException e) {
			DAOExceptionHandler.handleMigrationDAOException(e, this.getClass());
		} finally {
			closeResourceGracefully(rs, ps, conn);
		}
		return aProduct;
	}

}

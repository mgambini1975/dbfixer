package it.ditech.ci.dbfixer.dao.impl;

import static it.ditech.ci.dbfixer.sql.SQLQueries.DBALIAS_SELECT_ALL_BY_ENV_ID;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import it.ditech.ci.dbfixer.dao.IAliasDAO;
import it.ditech.ci.dbfixer.service.dto.AliasDTO;

/**
 * Gestore della persistenza per i nomi degli schemi DB
 * @author mgambini
 *
 */
@Repository
public class AliasDAO extends GenericDAO implements IAliasDAO {
	
	private final Logger log = LoggerFactory.getLogger(AliasDAO.class);

	/**
	 * Restituisce i nomi degli schemi DB associati all'EnvID passato in argomento
	 * 
	 * @param envId l'ID dell'ambiente Oracle
	 * @return un DTO contenente i nomi degli schemi, null se non trovati
	 */
	@Override
	public AliasDTO getDbAliasesByEnvId(Long envId) throws MigrationDAOException {
		AliasDTO anAliasDTO = new AliasDTO();
		String sql = DBALIAS_SELECT_ALL_BY_ENV_ID;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(sql);		
			ps.setLong(1, envId);		
			rs = ps.executeQuery();
			if (rs.next()) {	
				log.debug("Found DbAlias with ID: "+rs.getLong(1)+" for DbEnvID: "+envId );
				anAliasDTO.setId(rs.getLong(1));
				anAliasDTO.setDbEnvId(rs.getLong(2));				
				anAliasDTO.setCommSchemaName(rs.getString(3));
				anAliasDTO.setCoreSchemaName(rs.getString(4));
				anAliasDTO.setPuSchemaName(rs.getString(5));
				anAliasDTO.setDccSchemaName(rs.getString(6));
				anAliasDTO.setInteSchemaName(rs.getString(7));
				anAliasDTO.setShareSchemaName(rs.getString(8));
				anAliasDTO.setMntrSchemaName(rs.getString(9));
				anAliasDTO.setSchdSchemaName(rs.getString(10));
				anAliasDTO.setDivuSchemaName(rs.getString(11));
				anAliasDTO.setLogiSchemaName(rs.getString(12));
				anAliasDTO.setArchSchemaName(rs.getString(13));
				anAliasDTO.setEcomSchemaName(rs.getString(14));
				anAliasDTO.setCowSchemaName(rs.getString(15));
				anAliasDTO.setPengSchemaName(rs.getString(16));
				anAliasDTO.setCamiSchemaName(rs.getString(17));
				anAliasDTO.setIbcSchemaName(rs.getString(18));
				anAliasDTO.setFbdcSchemaName(rs.getString(19));
				anAliasDTO.setWstSchemaName(rs.getString(20));
				anAliasDTO.setScmsoSchemaName(rs.getString(21));
			} else {
				log.debug("No DbAlias found for DbEnvID: "+envId);
			}
			
		} catch (SQLException e) {
			DAOExceptionHandler.handleMigrationDAOException(e, this.getClass());
		} finally {
			closeResourceGracefully(rs, ps, conn);
		}
		return anAliasDTO;
	}
	
}

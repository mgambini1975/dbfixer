import { BaseEntity } from './../../shared';

export class Alias implements BaseEntity {
    constructor(
        public id?: number,
        public commSchemaName?: string,
        public coreSchemaName?: string,
        public puSchemaName?: string,
        public dccSchemaName?: string,
        public inteSchemaName?: string,
        public shareSchemaName?: string,
        public mntrSchemaName?: string,
        public schdSchemaName?: string,
        public divuSchemaName?: string,
        public logiSchemaName?: string,
        public archSchemaName?: string,
        public ecomSchemaName?: string,
        public cowSchemaName?: string,
        public pengSchemaName?: string,
        public camiSchemaName?: string,
        public ibcSchemaName?: string,
	public fbdcSchemaName?: string,
	public wstSchemaName?: string,
	public scmsoSchemaName?: string,
        public dbEnvId?: number
    ) {
    }
}

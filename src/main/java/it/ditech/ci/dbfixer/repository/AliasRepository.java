package it.ditech.ci.dbfixer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.ditech.ci.dbfixer.domain.Alias;

/**
 * Spring Data JPA repository for Alias entity.
 */
@Repository
public interface AliasRepository extends JpaRepository<Alias, Long> {

}

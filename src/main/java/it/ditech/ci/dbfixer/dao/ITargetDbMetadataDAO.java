package it.ditech.ci.dbfixer.dao;

import javax.sql.DataSource;

import it.ditech.ci.dbfixer.dao.impl.MigrationDAOException;
import it.ditech.ci.dbfixer.domain.Activity;
import it.ditech.ci.dbfixer.service.dto.DbVersionMetadataDTO;

/**
 * Contratto DAO per la gestione dei metadati sul DbTarget
 * 
 * @author mgambini
 *
 */
public interface ITargetDbMetadataDAO extends IGenericDAO {
	
	
	public int fixMetadata(DataSource targetDS, Activity anActivity) throws MigrationDAOException;
	
	public boolean findMigration(DataSource targetDS, String checksumKey) throws MigrationDAOException;
	
	public int insertMetadataOnTargetSchema(DbVersionMetadataDTO metadataDTO, String coreSchemaName, DataSource ds) throws MigrationDAOException;

}

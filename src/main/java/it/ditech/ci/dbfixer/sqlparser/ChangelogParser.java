package it.ditech.ci.dbfixer.sqlparser;

import static it.ditech.ci.dbfixer.config.Constants.BACKSLASH;
import static it.ditech.ci.dbfixer.config.Constants.BASE_PATH;
import static it.ditech.ci.dbfixer.config.Constants.CHANGELOG_FILENAME;
import static it.ditech.ci.dbfixer.config.Constants.SUPPORTED_CUSTOMERS;
import static it.ditech.ci.dbfixer.config.Constants.CHANGELOG_PATH;
import static it.ditech.ci.dbfixer.config.Constants.CHANGESET_PREFIX;
import static it.ditech.ci.dbfixer.config.Constants.CHUNK_SEPARATOR;
import static it.ditech.ci.dbfixer.config.Constants.COMMA;
import static it.ditech.ci.dbfixer.config.Constants.CUSTOMERS;
import static it.ditech.ci.dbfixer.config.Constants.CUSTOMER_PREFIX;
import static it.ditech.ci.dbfixer.config.Constants.DOT;
import static it.ditech.ci.dbfixer.config.Constants.EMPTY_STRING;
import static it.ditech.ci.dbfixer.config.Constants.ENV_PREFIX;
import static it.ditech.ci.dbfixer.config.Constants.FILE_SEPARATOR;
import static it.ditech.ci.dbfixer.config.Constants.METADATA_EXT_QUALIFIER;
import static it.ditech.ci.dbfixer.config.Constants.METADATA_QUALIFIER;
import static it.ditech.ci.dbfixer.config.Constants.METADATA_SEPARATOR;
import static it.ditech.ci.dbfixer.config.Constants.OBJECT;
import static it.ditech.ci.dbfixer.config.Constants.PARENT_PATH;
import static it.ditech.ci.dbfixer.config.Constants.PATCHES;
import static it.ditech.ci.dbfixer.config.Constants.PATH_PARTS;
import static it.ditech.ci.dbfixer.config.Constants.SLASH;
import static it.ditech.ci.dbfixer.config.Constants.SQLPLUS_CONNECT;
import static it.ditech.ci.dbfixer.config.Constants.SQLPLUS_DEFINE;
import static it.ditech.ci.dbfixer.config.Constants.SQLSCRIPT_TERMINATOR;
import static it.ditech.ci.dbfixer.config.Constants.SYSDBA_PREFIX;
import static it.ditech.ci.dbfixer.config.Constants.TEMP;
import static it.ditech.ci.dbfixer.config.Constants.UTF_8;
import static it.ditech.ci.dbfixer.config.Constants.XML;
import static it.ditech.ci.dbfixer.config.Constants.XML_FILE_PATH;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.ditech.ci.dbfixer.dao.impl.MigrationDAOException;
import it.ditech.ci.dbfixer.executor.Changeset;
import it.ditech.ci.dbfixer.executor.IExecutor;
import it.ditech.ci.dbfixer.service.dto.AliasDTO;
import it.ditech.ci.dbfixer.service.dto.CustomerDTO;
import it.ditech.ci.dbfixer.sqlparser.validator.DbAlias;
import it.ditech.ci.dbfixer.sqlparser.validator.ExternalSQLScriptValidator;
import it.ditech.ci.dbfixer.sqlparser.validator.SQLChangelogValidator;
import it.ditech.ci.dbfixer.sqlparser.validator.SQLValidatorException;

/**
 * SQL file parser. 
 * Produces an equivalent XML Liquibase patchset 
 * 
 * @author mgambini
 *
 */
public class ChangelogParser {

	
	private final Logger log = LoggerFactory.getLogger(ChangelogParser.class);
	
	public static final String CHANGELOG_HEADER = "<databaseChangeLog xmlns=\"http://www.liquibase.org/xml/ns/dbchangelog\" \n" +
			"xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:ext=\"http://www.liquibase.org/xml/ns/dbchangelog-ext\" \n" +
			"xsi:schemaLocation=\"http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.5.xsd \n" +
			"http://www.liquibase.org/xml/ns/dbchangelog-ext http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-ext.xsd\">";
	
	
	public static boolean isToExecAsSysDba(File fileToParse) throws SQLValidatorException {
		String changeLogFilePath = fileToParse.getAbsolutePath();
		File toScanFile = new File(changeLogFilePath);
		Scanner input = null;
		try {
			input = new Scanner(toScanFile);
			while(input.hasNext()) {
				 String nextLine = input.nextLine();
				 if (nextLine.toUpperCase().startsWith(SYSDBA_PREFIX)){
					 return Boolean.TRUE;
				 }
			}
		} catch (FileNotFoundException e) {
			throw new SQLValidatorException("Changelog file not exist: "+fileToParse.getName(), null, fileToParse.getName());
		} finally {
			if (input != null)
				input.close();
		}		
		return Boolean.FALSE;
	}
	

	
	/**
	 * Costruisce il patchset XML di Liquibase corrispondente al patchset SQL passato in argomento
	 * 
	 * @param executor l'esecutore Liquibase
	 * @param basePath percorso base del changeset
	 * @param changelogFilename changeset filename with '.sql' suffix
	 * @param failOnError true if execution stops due to validation or runtime errors
	 * @return true if parsing succeded
	 * @throws SQLValidatorException
	 * @throws UnsupportedEncodingException
	 * @throws MigrationDAOException 
	 */
	public Changeset parseChangelog(IExecutor executor, String basePath, String changelogFilename, boolean failOnError, List<CustomerDTO> supportedCustomers) throws SQLValidatorException, UnsupportedEncodingException, MigrationDAOException {

		Changeset toRet = new Changeset();
		String destFileName = changelogFilename.substring(0, changelogFilename.lastIndexOf(DOT)).concat(DOT+XML);
		String fileSeparator = System.getProperty(FILE_SEPARATOR);
		String changeLogFilePath = basePath+fileSeparator+changelogFilename;
		toRet.setFilepath(changeLogFilePath);
		
		//Valido metadati del changelog file
		Map<String,Object> params = new HashMap<String,Object>();
		params.put(CHANGELOG_PATH, changeLogFilePath);
		SQLChangelogValidator chLogValidator = new SQLChangelogValidator();
		chLogValidator.validate(params);
		
		//Predisposizione mappa schema alias
		AliasDTO aliasDTO = executor.getConfiguration().getAliasDTO();
		Map<String,String> aliases = populateAliasesMap(aliasDTO);
		
		//Predisposizione parametri
		int patchsetCounter = 0;
		String[] verAndName = changelogFilename.split(METADATA_SEPARATOR);
		String scriptName;
		if (verAndName.length == 3){
			scriptName = verAndName[1]+METADATA_SEPARATOR+verAndName[2];
		} else {
			scriptName = verAndName[1];
		}
		toRet.setScriptname(scriptName);
		String patchsetId = verAndName[0];
		toRet.setVersion(patchsetId);
		
		String author = EMPTY_STRING;
		String desc = EMPTY_STRING;
		String contexts = EMPTY_STRING;
		String[] envsList = new String[0];
		String[] customersList = new String[0];
		boolean openingChangset = Boolean.FALSE;
		
		//Predisposizione file XML
		String pathToXMLFile = fileSeparator+PARENT_PATH+fileSeparator+XML_FILE_PATH;
		new File(basePath+pathToXMLFile).mkdir();
		PrintWriter writer = null;
		Scanner input = null;
		File filename = null;
		try {
			writer = new PrintWriter(basePath+pathToXMLFile+fileSeparator+destFileName, UTF_8);
			writer.println(CHANGELOG_HEADER);
			writer.println();
		
			//Parsing e sostituzione riferimenti a script PL/SQL esterni
			filename = new File(changeLogFilePath);
			input = new Scanner(filename);
			boolean firstLine = Boolean.TRUE;
			//Pre-parsing per autore e descrizione
			String envs = EMPTY_STRING;
			String customers = EMPTY_STRING;
			while(input.hasNext()) {
				 
				 String nextLine = input.nextLine();
				
				 //Scarto eventuali righe vuote
				 if (nextLine.isEmpty() || nextLine.trim().length()==0)
					 continue;
				 
				//Scarto la prima riga
				 if (firstLine){
					 firstLine = Boolean.FALSE;
					 continue;
				 }
				 	 
				 //Recupero author e desc
				 if (nextLine.toUpperCase().startsWith(CHANGESET_PREFIX)){
					 String stringToSplit = nextLine.substring(11).replace(" ", EMPTY_STRING);
					 String[] authAndDesc = stringToSplit.split(CHUNK_SEPARATOR);
					 author = authAndDesc[0];
					 toRet.setAuthor(author);
					 desc = authAndDesc[1];
					 toRet.setDescription(desc);
					 continue;			 
				 }
				 //Recupero env
				 if (nextLine.toUpperCase().startsWith(ENV_PREFIX.toUpperCase())){
					 String[] envsLine = nextLine.split(CHUNK_SEPARATOR);
					 envs = envsLine[1];
					 envsList = envs.split(COMMA);
					 continue;
				 }
				 
				 //Recupero customer
				 if (nextLine.toUpperCase().startsWith(CUSTOMER_PREFIX.toUpperCase())){
					 String[] customersLine = nextLine.split(CHUNK_SEPARATOR);
					 customers = customersLine[1];
					 customersList = customers.split(COMMA); 
					 continue;
				 }
				 contexts = createContexts(envsList, customersList).toString();
				 writer.println("<changeSet author=\""+author+"\" id=\""+patchsetId+DOT+patchsetCounter+CHUNK_SEPARATOR+scriptName+CHUNK_SEPARATOR+desc+"\" failOnError=\""+failOnError+"\" context=\""+contexts+"\" >");
				 writer.println("<sql splitStatements=\"true\" endDelimiter=\"(?m)^/$\" >");
				 patchsetCounter++;
				 break;
			}
			input.close();
			
			//Parsing
			input = new Scanner(filename);
			firstLine = Boolean.TRUE;
			while(input.hasNext()) {
				 String nextLine = input.nextLine();
				
				 //Scarto eventuali righe vuote o con metacommenti liquibase
				 if (nextLine.isEmpty() || nextLine.trim().length()==0 || nextLine.startsWith(METADATA_QUALIFIER) || 
						 nextLine.toUpperCase().contains(SQLPLUS_DEFINE) || 
						 nextLine.toUpperCase().contains(SQLPLUS_CONNECT))
					 continue;
				 
				 //Sostituzione alias schema DB
				 nextLine = replaceAliases(nextLine, aliases);
				 
				 //Sostituzione caratteri speciali
				 nextLine = replaceSpecialChars(nextLine);
				 
				 				 				 
				 if (nextLine.startsWith(METADATA_EXT_QUALIFIER)){
					 //chiudo <sql> e <patchset> precedente
					 if (!openingChangset){
						 writer.println(SLASH);
						 writer.println("</sql>");
						 writer.println("</changeSet>");
						 writer.println();
					 }
					 
					 //Introduco script esterno	se non già eseguito 
					 if (!alreadyRun(executor, destFileName,nextLine,basePath,author,patchsetId,patchsetCounter,desc))
						 resolveExternalSQL(changelogFilename, writer,nextLine,basePath,author,patchsetId,patchsetCounter,desc, contexts, aliases, supportedCustomers);
					 
					 patchsetCounter++;
					 openingChangset = Boolean.TRUE;
					 
				 } else {
					 if (openingChangset){
						 contexts = createContexts(envsList, customersList).toString();
						 writer.println("<changeSet author=\""+author+"\" id=\""+patchsetId+DOT+patchsetCounter+CHUNK_SEPARATOR+scriptName+CHUNK_SEPARATOR+desc+"\" runOnChange=\"false\" failOnError=\"true\" context=\""+contexts+"\" >");
						 writer.println("<sql splitStatements=\"true\" endDelimiter=\"(?m)^/$\" >");
						 patchsetCounter++;
						 openingChangset = Boolean.FALSE;
					 }
					 //Scrivo sul writer lo statement SQL
					 writer.println(nextLine);
				 }
			}
			
			if(!openingChangset){
				writer.println(SLASH);
				writer.println("</sql>");
				writer.println("</changeSet>");
			}
			
			writer.println("</databaseChangeLog>");
			return toRet;
			 
		} catch (FileNotFoundException e) {
			throw new SQLValidatorException("Changelog file not exist: "+filename, null, scriptName);
		} finally {
			if (writer != null)
				writer.close();
			if (input != null)
				input.close();
		}
	}
	
	private StringBuffer createContexts(String[] envsList, String[] customersList) {
		 StringBuffer contexts = new StringBuffer();
		 if (envsList.length >= 1 && customersList.length >= 1) {
			 for (String env : Arrays.asList(envsList)) {
				 for (String customer : Arrays.asList(customersList)) {
					 contexts.append(env);
					 contexts.append(" and ");
					 contexts.append(customer);
					 contexts.append(",");
				 }
			 }
		 } else if (envsList.length == 0) {
			 for (String customer : Arrays.asList(customersList)) {
				 contexts.append(customer);
				 contexts.append(",");
			 }
		 } else if (customersList.length == 0) {
			 for (String env : Arrays.asList(envsList)) {
				 contexts.append(env);
				 contexts.append(",");
			 }
		 }
		 if (contexts.toString().endsWith(","))
			 contexts.deleteCharAt(contexts.length()-1);	
		 return contexts;
	}
	
	private String replaceSpecialChars(String aLine) {
		String escapedAmpersendLine = new String(aLine.replace("&", "&amp;"));
		String escapedLTLine = new String(escapedAmpersendLine.replace("<", "&lt;"));
		String escapedGTLine = new String(escapedLTLine.replace(">", "&gt;"));
		return escapedGTLine;
	}
	
	private String replaceAliases(String aLine, Map<String,String> aliases) {
		StringBuilder sb = new StringBuilder(aLine);
		for (String alias : aliases.keySet()) {
			String aux = Pattern.compile("&"+alias+".", Pattern.CASE_INSENSITIVE).matcher(sb).replaceAll(aliases.get(alias));
			sb.setLength(0);
		    sb.append(aux);
		}		
		return sb.toString();
	}
	

	private Map<String, String> populateAliasesMap(AliasDTO anAliasDTO){
		Map<String, String> toRet = new HashMap<String, String>();
		toRet.put(DbAlias.COMM.name(), anAliasDTO.getCommSchemaName());
		toRet.put(DbAlias.CORE.name(), anAliasDTO.getCoreSchemaName());
		toRet.put(DbAlias.PU.name(), anAliasDTO.getPuSchemaName());
		toRet.put(DbAlias.DCC.name(), anAliasDTO.getDccSchemaName());
		toRet.put(DbAlias.INTE.name(), anAliasDTO.getInteSchemaName());
		toRet.put(DbAlias.SHARE.name(), anAliasDTO.getShareSchemaName());
		toRet.put(DbAlias.MNTR.name(), anAliasDTO.getMntrSchemaName());
		toRet.put(DbAlias.SCHD.name(), anAliasDTO.getSchdSchemaName());
		toRet.put(DbAlias.DIVU.name(), anAliasDTO.getDivuSchemaName());
		toRet.put(DbAlias.LOGI.name(), anAliasDTO.getLogiSchemaName());
		toRet.put(DbAlias.ARCH.name(), anAliasDTO.getArchSchemaName());
		toRet.put(DbAlias.ECOM.name(), anAliasDTO.getEcomSchemaName());
		toRet.put(DbAlias.COW.name(), anAliasDTO.getCowSchemaName());
		toRet.put(DbAlias.PENG.name(), anAliasDTO.getPengSchemaName());
		toRet.put(DbAlias.CAMI.name(), anAliasDTO.getPengSchemaName());
		toRet.put(DbAlias.IBC.name(), anAliasDTO.getIbcSchemaName());
		toRet.put(DbAlias.FBDC.name(), anAliasDTO.getFbdcSchemaName());
		toRet.put(DbAlias.WST.name(), anAliasDTO.getWstSchemaName());
		toRet.put(DbAlias.SCMSO.name(), anAliasDTO.getScmsoSchemaName());
		return toRet;
	}
	
	/**
	 * Verifica se il changeset relativo ad un oggetto SQL (package, proc, function, ecc.) è già stato eseguito ricercando nei metadati Liquibase la KEY associata al relativo patchset
	 * Questo evita di sollevare eccezione (bloccante) di ckecksum invalido nel caso in cui l'oggetto è stato modificato 
	 * 
	 * @param executor l'esecutore dei patchset
	 * @param destFileName lo script XML di destinazione
	 * @param nextLine lo script SQL da valutare
	 * @param basePath percorso base
	 * @param author nome dell'autore del patchset
	 * @param patchsetId progessivo di identificazione del patchset
	 * @param patchsetCounter progressivo di identificazione del changeset Liquibase
	 * @param desc descrizione del patchset
	 * @return true se il changeset è già stato eseguito
	 * @throws MigrationDAOException
	 */
	private boolean alreadyRun(IExecutor executor, String destFileName, String nextLine, String basePath, String author, String patchsetId, int patchsetCounter, String desc) throws MigrationDAOException{
		boolean alreadyRun = Boolean.FALSE;
		
		log.debug("destFileName: "+destFileName);
		log.debug("nextLine: "+nextLine);
		log.debug("basePath: "+basePath);
		log.debug("author: "+author);
		log.debug("version: "+patchsetId);
		log.debug("patchsetCounter: "+patchsetCounter);
		log.debug("desc: "+desc);
		
			
		//nome dello script
		String extScriptRelativePath = nextLine.replace(METADATA_EXT_QUALIFIER+DOT+SLASH, EMPTY_STRING).trim();
		String[] parts = extScriptRelativePath.split("/");
		String checksumKey = patchsetId+DOT+patchsetCounter+CHUNK_SEPARATOR+parts[2]+CHUNK_SEPARATOR+desc;
		
		if (executor.getConfiguration().getMetadataDAO().findMigration(executor.getConfiguration().getDatasource(), checksumKey))
		  alreadyRun = Boolean.TRUE;
		else
		  alreadyRun = Boolean.FALSE;
		
		return alreadyRun;
	}
	
	/**
	 * Costruisce il frammento XML corrispondente all'elaborazione di uno script SQL esterno
	 * 
	 * @param writer
	 * @param nextLine
	 * @param basePath
	 * @param author
	 * @param patchsetId
	 * @param patchsetCounter
	 * @param desc
	 * @throws SQLValidatorException
	 * @throws UnsupportedEncodingException
	 */
	private void resolveExternalSQL(String changelogFilename, PrintWriter writer, String nextLine, String basePath, String author, String patchsetId, int patchsetCounter, String desc, String contexts, Map<String, String> aliases, List<CustomerDTO> supportedCustomers) throws SQLValidatorException, UnsupportedEncodingException {
		String fileSeparator = System.getProperty(FILE_SEPARATOR);
		
		StringBuffer normalizedExtScriptRelativePath = new StringBuffer();
		if (nextLine.contains(SLASH)) {
			normalizedExtScriptRelativePath = new StringBuffer(nextLine.replace(SLASH, fileSeparator));
		}
		if (nextLine.contains(BACKSLASH) ) {
			normalizedExtScriptRelativePath = new StringBuffer(nextLine.replace(BACKSLASH, fileSeparator));
		}
		String extScriptRelativePath = normalizedExtScriptRelativePath.toString().replace(METADATA_EXT_QUALIFIER+DOT+fileSeparator, EMPTY_STRING).trim();
		String[] fileNamePart = extScriptRelativePath.split(fileSeparator);
		Map<String,Object> params = new HashMap<String,Object>();
		params.put(CHANGELOG_PATH, extScriptRelativePath);
		params.put(PATH_PARTS,fileNamePart);
		params.put(BASE_PATH, basePath);
		params.put(CHANGELOG_FILENAME, changelogFilename);
		params.put(SUPPORTED_CUSTOMERS, supportedCustomers);
		ExternalSQLScriptValidator extSQLValidator = new ExternalSQLScriptValidator();
		extSQLValidator.validate(params);
		
		//Sostituzione alias schema DB
		String realPath; 
		String extScriptOriginPath;
		File tempDir;
		String extScriptDestPath;
		if (fileNamePart.length == 3) {
			realPath = basePath.substring(0, basePath.indexOf(PATCHES))+OBJECT;
			extScriptOriginPath = realPath+fileSeparator+fileNamePart[0]+fileSeparator+fileNamePart[1]+fileSeparator+fileNamePart[2];
			tempDir = new File(realPath+fileSeparator+fileNamePart[0]+fileSeparator+fileNamePart[1]+fileSeparator+TEMP);
			extScriptDestPath = realPath+fileSeparator+fileNamePart[0]+fileSeparator+fileNamePart[1]+fileSeparator+TEMP+fileSeparator+fileNamePart[2];
		} else {
			realPath = basePath.substring(0, basePath.indexOf(PATCHES))+CUSTOMERS;
			extScriptOriginPath = realPath+fileSeparator+fileNamePart[0]+fileSeparator+fileNamePart[1]+fileSeparator+fileNamePart[2]+fileSeparator+fileNamePart[3];
			tempDir = new File(realPath+fileSeparator+fileNamePart[0]+fileSeparator+fileNamePart[1]+fileSeparator+fileNamePart[2]+fileSeparator+TEMP);
			extScriptDestPath = realPath+fileSeparator+fileNamePart[0]+fileSeparator+fileNamePart[1]+fileSeparator+fileNamePart[2]+fileSeparator+TEMP+fileSeparator+fileNamePart[3];
		}
		
		if (!tempDir.exists())
			tempDir.mkdir();
		
		replaceAliasDBSchema(extScriptOriginPath, extScriptDestPath, aliases);
		String extScriptName = extScriptDestPath.substring(extScriptDestPath.lastIndexOf(SLASH)+1).trim();
		//-----------------------------------------------------------------------------------------------------------------------------------------------------
		
		writer.println();
		writer.println("<changeSet author=\""+author+"\" id=\""+patchsetId+DOT+patchsetCounter+CHUNK_SEPARATOR+extScriptName+CHUNK_SEPARATOR+desc+"\" runOnChange=\"false\" failOnError=\"true\" context=\""+contexts+"\" >");
		writer.println("<sqlFile path=\""+ extScriptDestPath + "\" splitStatements=\"true\" endDelimiter=\""+SQLSCRIPT_TERMINATOR+"\" />");
		writer.println("</changeSet>");
		writer.println();
	}
	
	private void replaceAliasDBSchema(String extScriptOriginPath, String extScriptDestPath, Map<String, String> aliases) {
		File toScanFile = new File(extScriptOriginPath);
		File replacedAliasFile = new File(extScriptDestPath);
		Scanner input = null;
		PrintWriter writer = null;
		try {
			input = new Scanner(toScanFile);
			writer = new PrintWriter(replacedAliasFile, UTF_8);
			while(input.hasNext()) {
				 String nextLine = input.nextLine();
				 nextLine = replaceAliases(nextLine, aliases);
				 writer.println(nextLine);
			}
		} catch (FileNotFoundException e) {
			throw new RuntimeException("Changelog file not exist: "+ e.getMessage());
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("File encoding not supported: "+ e.getMessage());
		} finally {
			if (input != null)
				input.close();
			if (writer != null)
				writer.close();
		}		
		
	}
	
}


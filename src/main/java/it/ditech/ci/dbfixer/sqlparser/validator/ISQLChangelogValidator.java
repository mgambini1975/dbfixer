package it.ditech.ci.dbfixer.sqlparser.validator;

import java.util.Map;

/**
 * Interfaccia di un validatore per patchset .sql
 * 
 * @author mgambini
 *
 */
public interface ISQLChangelogValidator {
	
	public void validate(Map<String,Object> values) throws SQLValidatorException;

}

package it.ditech.ci.dbfixer.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import it.ditech.ci.dbfixer.domain.Alias;
import it.ditech.ci.dbfixer.service.dto.AliasDTO;

/**
 * Mapper for the entity Alias and its DTO AliasDTO.
 */
@Mapper(componentModel = "spring", uses = {ProductMapper.class,})
public interface AliasMapper extends EntityMapper <AliasDTO, Alias> {
	
	@Mapping(source = "dbEnvId", target = "dbEnvId")
	AliasDTO toDto(Alias alias);
	
	@Mapping(source = "dbEnvId", target = "dbEnvId")
	@Mapping(target = "dbEnv", ignore = true)
    Alias toEntity(AliasDTO aliasDTO); 
    default Alias fromId(Long id) {
        if (id == null) {
            return null;
        }
        Alias alias = new Alias();
        alias.setId(id);
        return alias;
    }

}

import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { Migration2Component } from './migration2.component';
import { Migration2DetailComponent } from './migration2-detail.component';

export const migration2Route: Routes = [
    {
        path: 'migration2',
        component: Migration2Component,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.migration.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'migration2/:id',
        component: Migration2DetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.migration.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

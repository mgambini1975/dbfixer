package it.ditech.ci.dbfixer.service.dto;

import java.io.Serializable;
import java.sql.Time;
import java.sql.Timestamp;

import it.ditech.ci.dbfixer.domain.enumeration.StageEnvType;

public class ActivityDTO implements Serializable {

	private static final long serialVersionUID = -5148328680910897506L;

	private Long id;
	private Long buildId;
	private StageEnvType targetEnvType;
	private String customerName;
	private String productName;
	private Long installedRank;
	private String version;
	private String actType;
	private String description;
	private String scriptName;
	private String username;
	private String author;
	private Long checksum;
	private Timestamp applyDate;
	private Time execTime;
	private String log;
	private String success;
	
	public Long getBuildId() {
		return buildId;
	}
	public void setBuildId(Long buildId) {
		this.buildId = buildId;
	}
	
	public String getSuccess() {
		return success;
	}
	public void setSuccess(String success) {
		this.success = success;
	}
	public Long getInstalledRank() {
		return installedRank;
	}
	public void setInstalledRank(Long installedRank) {
		this.installedRank = installedRank;
	}
	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getScriptName() {
		return scriptName;
	}
	public void setScriptName(String scriptName) {
		this.scriptName = scriptName;
	}	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Long getChecksum() {
		return checksum;
	}
	public void setChecksum(Long checksum) {
		this.checksum = checksum;
	}
	public Timestamp getApplyDate() {
		return applyDate;
	}
	public void setApplyDate(Timestamp date) {
		this.applyDate = date;
	}
	public String getLog() {
		return log;
	}
	public void setLog(String log) {
		this.log = log;
	}
	
	public StageEnvType getTargetEnvType() {
		return targetEnvType;
	}
	public void setTargetEnvType(StageEnvType targetEnvType) {
		this.targetEnvType = targetEnvType;
	}
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getActType() {
		return actType;
	}
	public void setActType(String actType) {
		this.actType = actType;
	}
	public Time getExecTime() {
		return execTime;
	}
	public void setExecTime(Time execTime) {
		this.execTime = execTime;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
}

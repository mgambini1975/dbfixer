--liquibase formatted sql
--changeset mgambini:insert into tabella oracle_db_env

INSERT INTO `oracle_db_env` (`id`, `env_type`, `hostname`, `listener_port`, `jhi_user`, `pwd`, `syspwd`, `sid`, `product_id`, `env_desc`, `patchsetdbrepourl`) VALUES
(2, 'DEV', '192.168.7.94', 1521, 'BRC_COMM', 'BRC_COMM', 'blabla', 'brcusers', 1, 'Ambiente Sviluppo  MORE-CEDI per Maxidi', 'http://BitbucketTechUser@bitbucket.ditechsw.it:7990/scm/mcedi/commercialedb.git'),

(3, 'CI', '10.100.105.149', 1521, 'BRC_COMM', 'BRC_COMM', '0racle2018', 'moreci', 1, 'Ambiente CI MORE-CEDI per Maxidi', 'http://BitbucketTechUser@bitbucket.ditechsw.it:7990/scm/mcedi/commercialedb.git'),

(4, 'PREPROD', '10.100.105.149', 1521, 'BRC_COMM', 'BRC_COMM', 'UTENTE_DBA', 'moretest', 1, 'Ambiente test manuale MORE-CEDI per Maxidi', 'http://BitbucketTechUser@bitbucket.ditechsw.it:7990/scm/mcedi/commercialedb.git'),

(5, 'PROD', '10.100.105.220', 1521, 'BRC_COMM', 'BRC_COMM', 'UTENTE_DBA', 'moreusers', 1, 'Ambiente PROD MORE-CEDI per Maxidi', 'http://BitbucketTechUser@bitbucket.ditechsw.it:7990/scm/mcedi/commercialedb.git'),

(6, 'DEV', '127.0.0.1', 1521, 'BRC_COMM', 'BRC_COMM', 'UTENTE_DBA', 'brcusers', 4, 'Ambiente Sviluppo MORE-CEDI per Nordiconad', 'http://www.oracle.it'),

(7, 'CI', '127.0.0.1', 1521, 'BRC_COMM', 'BRC_COMM', 'UTENTE_DBA', 'moreci', 4, 'Ambiente CI MORE-CEDI per Nordiconad', 'http://www.oracle.com'),

(8, 'PREPROD', '127.0.0.1', 1521, 'BRC_COMM', 'BRC_COMM', 'UTENTE_DBA', 'moretest', 4, 'Ambiente test manuale MORE-CEDI per Nordiconad', 'http://www.oracle.com'),

(9, 'PROD', '127.0.0.1', 1521, 'BRC_COMM', 'BRC_COMM', 'UTENTE_DBA', 'moreusers', 4, 'Ambiente PROD MORE-CEDI per Nordiconad', 'http://www.oracle.com');

package it.ditech.ci.dbfixer.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.ditech.ci.dbfixer.service.IGenericService;

/**
 * Ancestor della gerarchia dei servizi di business
 * 
 * @author mgambini
 */
public abstract class GenericServiceImpl implements IGenericService {
	
	private final Logger log = LoggerFactory.getLogger(GenericServiceImpl.class);
	
}

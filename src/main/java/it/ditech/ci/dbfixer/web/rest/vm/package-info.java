/**
 * View Models used by Spring MVC REST controllers.
 */
package it.ditech.ci.dbfixer.web.rest.vm;

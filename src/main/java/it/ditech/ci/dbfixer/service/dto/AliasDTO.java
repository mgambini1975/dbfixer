package it.ditech.ci.dbfixer.service.dto;

import java.io.Serializable;
import java.util.Objects;

import it.ditech.ci.dbfixer.domain.Alias;

public class AliasDTO implements Serializable {

	private static final long serialVersionUID = -2677519727890391637L;

	 private Long id;
	 private Long dbEnvId;
	 private String commSchemaName;	 
	 private String coreSchemaName;
	 private String puSchemaName;
	 private String dccSchemaName;
	 private String inteSchemaName;
	 private String shareSchemaName;
	 private String mntrSchemaName;
	 private String schdSchemaName;
	 private String divuSchemaName;
	 private String logiSchemaName;
	 private String archSchemaName;
	 private String ecomSchemaName;
	 private String cowSchemaName;
	 private String pengSchemaName;
	 private String camiSchemaName;
	 private String ibcSchemaName;
	 private String fbdcSchemaName;
	 private String wstSchemaName;
	 private String scmsoSchemaName;
	 

	@Override
	 public boolean equals(Object o) {
	        if (this == o) {
	            return true;
	        }
	        if (o == null || getClass() != o.getClass()) {
	            return false;
	        }
	        Alias alias = (Alias) o;
	        if (alias.getId() == null || getId() == null) {
	            return false;
	        }
	        return Objects.equals(getId(), alias.getId());
	 }

	 @Override
	 public int hashCode() {
	        return Objects.hashCode(getId());
	 }

	 @Override
	 public String toString() {
	        return "Alias{" +
	            "id=" + getId() +
	            ", dbEnvId='" + getDbEnvId() + "'" +
	            ", comm='" + getCommSchemaName() + "'" +
	            ", core='" + getCoreSchemaName() + "'" +
	            ", pu='" + getPuSchemaName() + "'" +
	            ", dcc='" + getDccSchemaName() + "'" +
	            ", inte='" + getInteSchemaName() + "'" +
	            ", share='" + getShareSchemaName() + "'" +
	            ", mntr='" + getMntrSchemaName() + "'" +
	            ", schd='" + getSchdSchemaName() + "'" +
	            ", divu='" + getDivuSchemaName() + "'" +
	            ", logi='" + getLogiSchemaName() + "'" +
	            ", arch='" + getArchSchemaName() + "'" + 
	            ", ecom='" + getEcomSchemaName() + "'" + 
	            ", cow='" + getCowSchemaName() + "'" + 
	            ", peng='" + getPengSchemaName() + "'" + 
	            ", cami='" + getCamiSchemaName() + "'" + 
	            ", ibc='" + getIbcSchemaName() + "'" + 
	            ", fbdc='" + getFbdcSchemaName() + "'" + 
	            ", wst='" + getWstSchemaName() + "'" +
	            ", scmso='" + getScmsoSchemaName() + "'" +
	            "}";
	 }
	 
	 public Long getId() {
		 return id;
	 }

	 public void setId(Long id) {
		 this.id = id;
	 }

	public String getCommSchemaName() {
		return commSchemaName;
	}

	public void setCommSchemaName(String commSchemaName) {
		this.commSchemaName = commSchemaName;
	}

	public String getCoreSchemaName() {
		return coreSchemaName;
	}

	public void setCoreSchemaName(String coreSchemaName) {
		this.coreSchemaName = coreSchemaName;
	}

	public String getPuSchemaName() {
		return puSchemaName;
	}

	public void setPuSchemaName(String puSchemaName) {
		this.puSchemaName = puSchemaName;
	}

	public String getDccSchemaName() {
		return dccSchemaName;
	}

	public void setDccSchemaName(String dccSchemaName) {
		this.dccSchemaName = dccSchemaName;
	}

	public String getInteSchemaName() {
		return inteSchemaName;
	}

	public void setInteSchemaName(String inteSchemaName) {
		this.inteSchemaName = inteSchemaName;
	}

	public String getShareSchemaName() {
		return shareSchemaName;
	}

	public void setShareSchemaName(String shareSchemaName) {
		this.shareSchemaName = shareSchemaName;
	}

	public String getMntrSchemaName() {
		return mntrSchemaName;
	}

	public void setMntrSchemaName(String mntrSchemaName) {
		this.mntrSchemaName = mntrSchemaName;
	}

	public String getSchdSchemaName() {
		return schdSchemaName;
	}

	public void setSchdSchemaName(String schdSchemaName) {
		this.schdSchemaName = schdSchemaName;
	}

	public String getDivuSchemaName() {
		return divuSchemaName;
	}

	public void setDivuSchemaName(String divuSchemaName) {
		this.divuSchemaName = divuSchemaName;
	}

	public String getLogiSchemaName() {
		return logiSchemaName;
	}

	public void setLogiSchemaName(String logiSchemaName) {
		this.logiSchemaName = logiSchemaName;
	}

	public String getArchSchemaName() {
		return archSchemaName;
	}

	public void setArchSchemaName(String archSchemaName) {
		this.archSchemaName = archSchemaName;
	}

	public Long getDbEnvId() {
		return dbEnvId;
	}

	public void setDbEnvId(Long dbEnvId) {
		this.dbEnvId = dbEnvId;
	}

	public String getEcomSchemaName() {
		return ecomSchemaName;
	}

	public void setEcomSchemaName(String ecommSchemaName) {
		this.ecomSchemaName = ecommSchemaName;
	}

	public String getCowSchemaName() {
		return cowSchemaName;
	}

	public void setCowSchemaName(String cowSchemaName) {
		this.cowSchemaName = cowSchemaName;
	}

	public String getPengSchemaName() {
		return pengSchemaName;
	}

	public void setPengSchemaName(String pengSchemaName) {
		this.pengSchemaName = pengSchemaName;
	}

	public String getCamiSchemaName() {
		return camiSchemaName;
	}

	public void setCamiSchemaName(String camiSchemaName) {
		this.camiSchemaName = camiSchemaName;
	}

	public String getIbcSchemaName() {
		return ibcSchemaName;
	}

	public void setIbcSchemaName(String ibcSchemaName) {
		this.ibcSchemaName = ibcSchemaName;
	}

	 public String getFbdcSchemaName() {
		return fbdcSchemaName;
	}

	public void setFbdcSchemaName(String fbdcSchemaName) {
		this.fbdcSchemaName = fbdcSchemaName;
	}

	public String getWstSchemaName() {
		return wstSchemaName;
	}

	public void setWstSchemaName(String wstSchemaName) {
		this.wstSchemaName = wstSchemaName;
	}

	public String getScmsoSchemaName() {
		return scmsoSchemaName;
	}

	public void setScmsoSchemaName(String scmsoSchemaName) {
		this.scmsoSchemaName = scmsoSchemaName;
	}

}

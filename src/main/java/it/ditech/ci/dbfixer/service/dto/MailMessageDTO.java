package it.ditech.ci.dbfixer.service.dto;


public class MailMessageDTO extends MessageDTO {
	
	private static final long serialVersionUID = -8246819618951315711L;
	private String[] toRecipients;
	private String[] bccRecipients;
	private String replyTo;
	private boolean isMultipart;
	private boolean isHtml;
	
	public String[] getToRecipients() {
		return toRecipients;
	}
	public void setToRecipients(String[] toRecipients) {
		this.toRecipients = toRecipients;
	}
	public String[] getBccRecipients() {
		return bccRecipients;
	}
	public void setBccRecipients(String[] bccRecipients) {
		this.bccRecipients = bccRecipients;
	}
	public String getReplyTo() {
		return replyTo;
	}
	public void setReplyTo(String replyTo) {
		this.replyTo = replyTo;
	}
	public boolean isMultipart() {
		return isMultipart;
	}
	public void setMultipart(boolean isMultipart) {
		this.isMultipart = isMultipart;
	}
	public boolean isHtml() {
		return isHtml;
	}
	public void setHtml(boolean isHtml) {
		this.isHtml = isHtml;
	}

}

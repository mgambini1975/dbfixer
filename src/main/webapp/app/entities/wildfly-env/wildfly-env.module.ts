import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DbFixerSharedModule } from '../../shared';
import {
    WildflyEnvService,
    WildflyEnvPopupService,
    WildflyEnvComponent,
    WildflyEnvDetailComponent,
    WildflyEnvDialogComponent,
    WildflyEnvPopupComponent,
    WildflyEnvDeletePopupComponent,
    WildflyEnvDeleteDialogComponent,
    wildflyEnvRoute,
    wildflyEnvPopupRoute,
} from './';

const ENTITY_STATES = [
    ...wildflyEnvRoute,
    ...wildflyEnvPopupRoute,
];

@NgModule({
    imports: [
        DbFixerSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        WildflyEnvComponent,
        WildflyEnvDetailComponent,
        WildflyEnvDialogComponent,
        WildflyEnvDeleteDialogComponent,
        WildflyEnvPopupComponent,
        WildflyEnvDeletePopupComponent,
    ],
    entryComponents: [
        WildflyEnvComponent,
        WildflyEnvDialogComponent,
        WildflyEnvPopupComponent,
        WildflyEnvDeleteDialogComponent,
        WildflyEnvDeletePopupComponent,
    ],
    providers: [
        WildflyEnvService,
        WildflyEnvPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DbFixerWildflyEnvModule {}

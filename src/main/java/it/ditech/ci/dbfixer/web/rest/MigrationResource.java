package it.ditech.ci.dbfixer.web.rest;
 
import static it.ditech.ci.dbfixer.config.Constants.ACTIVITIES;
import static it.ditech.ci.dbfixer.config.Constants.ACTIVITY;
import static it.ditech.ci.dbfixer.config.Constants.ALIAS;
import static it.ditech.ci.dbfixer.config.Constants.BUILD_ID;
import static it.ditech.ci.dbfixer.config.Constants.CUSTOMER;
import static it.ditech.ci.dbfixer.config.Constants.CUSTOMERS;
import static it.ditech.ci.dbfixer.config.Constants.CUSTOMER_ID;
import static it.ditech.ci.dbfixer.config.Constants.DBENV;
import static it.ditech.ci.dbfixer.config.Constants.METADATA;
import static it.ditech.ci.dbfixer.config.Constants.METADATA_ONLY;
import static it.ditech.ci.dbfixer.config.Constants.MIGRATION;
import static it.ditech.ci.dbfixer.config.Constants.MIGRATIONS;
import static it.ditech.ci.dbfixer.config.Constants.PATCH_ID;
import static it.ditech.ci.dbfixer.config.Constants.PATCH_NAME_PREFIX;
import static it.ditech.ci.dbfixer.config.Constants.PRODUCT;
import static it.ditech.ci.dbfixer.config.Constants.PRODUCTS;
import static it.ditech.ci.dbfixer.config.Constants.PRODUCT_ID;
import static it.ditech.ci.dbfixer.config.Constants.SQLOUTPUT;
import static it.ditech.ci.dbfixer.config.Constants.STAGE;
import static it.ditech.ci.dbfixer.config.Constants.STAGE_ID;
import static it.ditech.ci.dbfixer.config.Constants.TAG;
import static it.ditech.ci.dbfixer.config.Constants.VERSION;
import static it.ditech.ci.dbfixer.config.Constants.VERSIONS;
import static it.ditech.ci.dbfixer.config.Constants.VERSION_ID;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import it.ditech.ci.dbfixer.executor.ExecutorException;
import it.ditech.ci.dbfixer.service.IMigrationService;
import it.ditech.ci.dbfixer.service.dto.ActivityDTO;
import it.ditech.ci.dbfixer.service.dto.AliasDTO;
import it.ditech.ci.dbfixer.service.dto.CustomerDTO;
import it.ditech.ci.dbfixer.service.dto.DbVersionMetadataDTO;
import it.ditech.ci.dbfixer.service.dto.OracleDbEnvDTO;
import it.ditech.ci.dbfixer.service.dto.ProductDTO;
import it.ditech.ci.dbfixer.service.dto.ProductVersionDTO;
import it.ditech.ci.dbfixer.service.validator.MigrationValidator;
import it.ditech.ci.dbfixer.service.validator.MigrationValidatorException;

/**
 * REST controller for managing SQL migrations
 * 
 * @author mgambini
 *
 */
@RestController
@RequestMapping("/migrations-api")
@CrossOrigin
@Api(description="Migrations handling endpoints")
public class MigrationResource {
	
	private IMigrationService migrationService;
	
	private MigrationValidator migrationValidator;
		
	private final Logger log = LoggerFactory.getLogger(MigrationResource.class);
	
	public MigrationResource(IMigrationService migrationService, MigrationValidator migrationValidator){
		this.migrationService = migrationService;
		this.migrationValidator = migrationValidator;
	}
	
	@Timed
	@RequestMapping(method=RequestMethod.GET, value="/"+ACTIVITIES)
	@ApiOperation(value = "View the migrations list related to all target DBs", response = ResponseEntity.class)
	public ResponseEntity<List<ActivityDTO>> getActivities() throws MigrationValidatorException{
		log.debug("REST request to get all Activities");
		return new ResponseEntity<>(migrationService.findAll(null, null), HttpStatus.OK);
	}
	
	@Timed
	@RequestMapping(method=RequestMethod.GET, value="/"+PRODUCT+"/{"+PRODUCT_ID+"}/"+STAGE+"/{"+STAGE_ID+"}/"+ACTIVITIES)
	@ApiOperation(value = "View the migrations list related to the DB identified by ProductID and StageID", response = ResponseEntity.class)
	public ResponseEntity<List<ActivityDTO>> getActivities(@PathVariable Long productId, @PathVariable Long stageId) throws MigrationValidatorException{
		log.debug("REST request to get Activities details for ProductID = "+productId+" and StageID= "+stageId);
		return new ResponseEntity<>(migrationService.findAll(productId, stageId), HttpStatus.OK);
	}
	
	@Timed
	@RequestMapping(method=RequestMethod.GET, value="/"+ACTIVITIES+"/{id}")
	@ApiOperation(value = "View the details related to the migration identified by MigrationID", response = ResponseEntity.class)
	public ResponseEntity<ActivityDTO> getActivityDetails(@PathVariable Long id) throws MigrationValidatorException{
		log.debug("REST request to get Activity details for ActivityID = "+id);
		return new ResponseEntity<>(migrationService.findOne(id), HttpStatus.OK);
	}
	
	@Timed
	@RequestMapping(method=RequestMethod.GET, value="/"+PRODUCT+"/{"+PRODUCT_ID+"}/"+STAGE+"/{"+STAGE_ID+"}/"+METADATA)
	@ApiOperation(value = "View the product version and last patches version related to the DB identified by ProductID and StageID", response = ResponseEntity.class)
	public ResponseEntity<DbVersionMetadataDTO> getDbMetadata(@PathVariable Long productId, @PathVariable Long stageId) throws MigrationValidatorException{
		log.debug("REST request to get metadata for ProductID = "+productId+" and StageID= "+stageId);
		return new ResponseEntity<>(migrationService.findDbVersionMetadataByProductIdAndStageId(productId, stageId), HttpStatus.OK);
	}
	
	@Timed
	@RequestMapping(method=RequestMethod.GET, value="/"+CUSTOMERS)
	@ApiOperation(value = "View the Customers list", response = ResponseEntity.class)
	public ResponseEntity<List<CustomerDTO>> getCustomers(){
		log.debug("REST request to get all Customers");
		return new ResponseEntity<>(migrationService.getCustomers(), HttpStatus.OK);
	}
	
	@Timed
	@RequestMapping(method=RequestMethod.GET, value="/"+CUSTOMER+"/{"+CUSTOMER_ID+"}/"+PRODUCTS)
	@ApiOperation(value = "View the Products list related to a Customer identified by CustomerID", response = ResponseEntity.class)
	public ResponseEntity<List<ProductDTO>> getProductsByCustomer(@PathVariable Long customerId) {
		log.debug("REST request to get Products for CustomerID = "+customerId);	
		return new ResponseEntity<>(migrationService.getProductsByCustomerId(customerId), HttpStatus.OK);
	}
	
	@Timed
	@RequestMapping(method=RequestMethod.GET, value="/"+PRODUCT+"/{"+PRODUCT_ID+"}/"+VERSIONS)
	@ApiOperation(value = "View the ProductVersions list related to a Product identified by ProductID", response = ResponseEntity.class)
	public ResponseEntity<List<ProductVersionDTO>> getProductVersionsByProduct(@PathVariable Long productId) {
		log.debug("REST request to get ProductVersions for ProductID = "+productId);	
		return new ResponseEntity<>(migrationService.getProductVersionsByProductId(productId), HttpStatus.OK);
	}
	
	@Timed
	@RequestMapping(method=RequestMethod.GET, value="/"+PRODUCT+"/{"+PRODUCT_ID+"}/"+STAGE+"/{"+STAGE_ID+"}/"+DBENV)
	@ApiOperation(value = "View the details related to the Stage identified by ProductID and StageID", response = ResponseEntity.class)
	public ResponseEntity<OracleDbEnvDTO> getDbEnvByProductAndStage(
			@PathVariable(required = true, value = PRODUCT_ID) Long productId, 
			@PathVariable(required = true, value = STAGE_ID) Long stageId) {
		log.debug("REST request to get DbEnvs for ProductID = "+productId+" and StageID= "+stageId);
		return new ResponseEntity<>(migrationService.getDbEnvByProductIdAndStageId(productId, stageId), HttpStatus.OK);
	}
	
	@Timed
	@RequestMapping(method=RequestMethod.GET, value="/"+PRODUCT+"/{"+PRODUCT_ID+"}/"+STAGE+"/{"+STAGE_ID+"}/"+ALIAS)
	@ApiOperation(value = "REST request to get DbEnv aliases identified by ProductID and StageID", response = ResponseEntity.class)
	public ResponseEntity<AliasDTO> getAliasesByProductIdAndEnvId(	
			@PathVariable(required = true, value = PRODUCT_ID) Long productId,
			@PathVariable(required = true, value = STAGE_ID) Long stageId) {
		OracleDbEnvDTO envDto = migrationService.getDbEnvByProductIdAndStageId(productId, stageId);	
		log.debug("REST request to get DbEnv aliases for EnvID= "+envDto.getId());
		return new ResponseEntity<>(migrationService.getDbAliasesByEnvId(envDto.getId()), HttpStatus.OK);
	}
	
	@Timed
	@RequestMapping(method=RequestMethod.POST, value="/"+PRODUCT+"/{"+PRODUCT_ID+"}/"+STAGE+"/{"+STAGE_ID+"}/"+VERSION+"/{"+VERSION_ID+":.+}/"+SQLOUTPUT)
	@ApiOperation(value = "Produces a SQL log file about migrations that should apply in the version identified by VersionID on the DB identified by ProductID and StageID", response = ResponseEntity.class)
	public boolean emulateUpgrade(
			@PathVariable(required = true, value = PRODUCT_ID) Long productId, 
			@PathVariable(required = true, value = STAGE_ID) Long stageId,
			@PathVariable(required = true, value = VERSION_ID) String versionId,		
			@RequestParam(required = true, value = BUILD_ID) Long buildId) throws MigrationValidatorException {
		log.debug("REST request to emulate upgrade DbEnv = "+"version "+versionId+", stage "+stageId+ ", productID = "+productId);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(PRODUCT_ID, productId);
		params.put(STAGE_ID, stageId);
		params.put(VERSION_ID, versionId);
		params.put(BUILD_ID, buildId);
		migrationValidator.validate(params);
		OracleDbEnvDTO dbDto = migrationService.getDbEnvByProductIdAndStageId(productId, stageId);
		dbDto.setProductId(productId);
		return migrationService.generateSQLChangeLog(dbDto, versionId, buildId);
	}
	
	@Timed
	@RequestMapping(method=RequestMethod.POST, value="/"+PRODUCT+"/{"+PRODUCT_ID+"}/"+STAGE+"/{"+STAGE_ID+"}/"+VERSION+"/{"+VERSION_ID+":.+}/"+MIGRATIONS)
	@ApiOperation(value = "Applies the migrations related to the version identified by VersionID on the DB identified by ProductID and StageID", response = ResponseEntity.class)
	public boolean massiveUpgrade(
			@PathVariable(required = true, value = PRODUCT_ID) Long productId, 
			@PathVariable(required = true, value = STAGE_ID) Long stageId,
			@PathVariable(required = true, value = VERSION_ID) String versionId, 
			@RequestParam(required = true, value = BUILD_ID) Long buildId) throws MigrationValidatorException {
		log.debug("REST request to upgrade DbEnv = "+"version "+versionId+", stage "+stageId+ ", productID = "+productId);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(PRODUCT_ID, productId);
		params.put(STAGE_ID, stageId);
		params.put(VERSION_ID, versionId);
		params.put(BUILD_ID, buildId);
		migrationValidator.validate(params);
		OracleDbEnvDTO dbDto = migrationService.getDbEnvByProductIdAndStageId(productId, stageId);
		dbDto.setProductId(productId);
		return migrationService.doMigration(dbDto, versionId, buildId);
	}
	
	@Timed
	@RequestMapping(method=RequestMethod.POST, value="/"+PRODUCT+"/{"+PRODUCT_ID+"}/"+STAGE+"/{"+STAGE_ID+"}/"+VERSION+"/{"+VERSION_ID+":.+}/"+MIGRATION+"/{"+PATCH_NAME_PREFIX+"}")
	@ApiOperation(value = "Applies the migration (or metadata only) identified by PatchID and related to VersionID on the DB identified by ProductID and StageID", response = ResponseEntity.class)
	public boolean singleUpgrade(
			@PathVariable(required = true, value = PRODUCT_ID) Long productId,
			@PathVariable(required = true, value = STAGE_ID) Long stageId,
			@PathVariable(required = true, value = VERSION_ID) String versionId, 
			@PathVariable(required = true, value = PATCH_NAME_PREFIX) String patchNamePrefixId,	
			@RequestParam(required = true, value = METADATA_ONLY) Boolean metadataOnly,
			@RequestParam(required = true, value = BUILD_ID) Long buildId) throws MigrationValidatorException {
		boolean toRet = Boolean.FALSE;
		log.debug("REST request to upgrade DbEnv = "+
			"version "+versionId+", stage "+stageId+ ", productID = "+productId+ "patchNamePrefixID = "+patchNamePrefixId + "with Metadata only: "+metadataOnly.booleanValue());
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(PRODUCT_ID, productId);
		params.put(STAGE_ID, stageId);
		params.put(VERSION_ID, versionId);
		params.put(BUILD_ID, buildId);
		params.put(PATCH_NAME_PREFIX, patchNamePrefixId);
		migrationValidator.validate(params);
		OracleDbEnvDTO dbDto = migrationService.getDbEnvByProductIdAndStageId(productId, stageId);
		dbDto.setProductId(productId);
		if (metadataOnly) {
			toRet = migrationService.addDbMetadata(dbDto, versionId, buildId, patchNamePrefixId);
		} else {
			toRet = migrationService.doMigration(dbDto, versionId, buildId, patchNamePrefixId); 
		}
		return toRet;
	}
	
	@Timed
	@RequestMapping(method=RequestMethod.PATCH, value="/"+PRODUCT+"/{"+PRODUCT_ID+"}/"+STAGE+"/{"+STAGE_ID+"}/"+VERSION+"/{"+VERSION_ID+":.+}/"+ACTIVITY+"/{"+PATCH_ID+"}")
	@ApiOperation(value = "Fixes metadata related to the migration Activity identified by PatchID and VersionID on the DB identified by ProductID and StageID", response = ResponseEntity.class)
	public boolean fixDbMetadata(
			@PathVariable(required = true, value = PRODUCT_ID) Long productId, 
			@PathVariable(required = true, value = STAGE_ID) Long stageId,
			@PathVariable(required = true, value = VERSION_ID) String versionId, 
			@PathVariable(required = true, value = PATCH_ID) Long patchId,
			@RequestParam(required = true, value = BUILD_ID) Long buildId) throws MigrationValidatorException, ExecutorException {
		log.debug("REST request to fix DbEnv metadata = "+"stage "+stageId+ " , productID = "+productId+ "patchID = "+patchId);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(PRODUCT_ID, productId);
		params.put(STAGE_ID, stageId);
		params.put(VERSION_ID, versionId);
		params.put(BUILD_ID, buildId);
		params.put(PATCH_ID, patchId);
		migrationValidator.validate(params);
		OracleDbEnvDTO dbDto = migrationService.getDbEnvByProductIdAndStageId(productId, stageId);
		dbDto.setProductId(productId);
		return migrationService.doFixMetadata(dbDto, buildId, patchId);
	}
	
	
	@Timed
	@RequestMapping(method=RequestMethod.POST, value="/"+PRODUCT+"/{"+PRODUCT_ID+"}/"+STAGE+"/{"+STAGE_ID+"}/"+VERSION+"/{"+VERSION_ID+":.+}/"+TAG)
	@ApiOperation(value = "Applies the Tag identified by VersionID on the DB identified by ProductID and StageID", response = ResponseEntity.class)
	public boolean tagDbVersion(
			@PathVariable(required = true, value = PRODUCT_ID) Long productId, 
			@PathVariable(required = true, value = STAGE_ID) Long stageId,
			@PathVariable(required = true, value = VERSION_ID) String versionId, 		
			@RequestParam(required = true, value = BUILD_ID) Long buildId) throws MigrationValidatorException{
		log.debug("REST request to tag DbEnv = "+stageId+" with version= "+"version "+versionId+", stage "+stageId+ " and productID = "+productId);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(PRODUCT_ID, productId);
		params.put(STAGE_ID, stageId);
		params.put(VERSION_ID, versionId);
		params.put(BUILD_ID, buildId);
		migrationValidator.validate(params);
		OracleDbEnvDTO dbDto = migrationService.getDbEnvByProductIdAndStageId(productId, stageId);
		return migrationService.doTag(dbDto, versionId, buildId);
	}
	
}

package it.ditech.ci.dbfixer.security;

import org.springframework.stereotype.Component;

import com.atlassian.crowd.integration.rest.service.RestCrowdClient;
import com.atlassian.crowd.service.client.ClientProperties;

@Component
public class CustomCrowdClient extends RestCrowdClient {

	/**
	 * Costruttore che accetta una bag di properties 
	 * @deprecated FIXME: usare l'altro costruttore
	 * @param clientProperties
	 */
	public CustomCrowdClient(ClientProperties clientProperties) {
		super(clientProperties);
		
	}

}

package it.ditech.ci.dbfixer.dao.impl;

import static it.ditech.ci.dbfixer.sql.SQLQueries.AMPERSEND;
import static it.ditech.ci.dbfixer.sql.SQLQueries.CORE_ALIAS;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import it.ditech.ci.dbfixer.dao.ITargetDbMetadataDAO;
import it.ditech.ci.dbfixer.domain.Activity;
import it.ditech.ci.dbfixer.service.dto.DbVersionMetadataDTO;

import static it.ditech.ci.dbfixer.sql.SQLQueries.LIQUIBASE_METADATA_DELETE_BY_ID;
import static it.ditech.ci.dbfixer.sql.SQLQueries.LIQUIBASE_METADATA_SELECT_ALL_BY_ID;
import static it.ditech.ci.dbfixer.sql.SQLQueries.LIQUIBASE_METADATA_INSERT;
import static it.ditech.ci.dbfixer.sql.SQLQueries.BRC_CORE_VERSIONI_INSERT;

/**
 * DAO per l'accesso alle tabelle di metadati dell'applicativo target
 * @author mgambini
 *
 */
@Repository(value="executorDAO")
public class TargetDbMetadataDAO extends GenericDAO implements ITargetDbMetadataDAO {
	
	private final Logger log = LoggerFactory.getLogger(TargetDbMetadataDAO.class);
	
	/**
	 * Verifica se il patchset identificato dalla chiave passata in argomento è già stato applicato
	 * 
	 * @param targetDS il target data source
	 * @param checksumKey l'identificatoredel patchset
	 * 
	 */
	@Override
	public boolean findMigration(DataSource targetDS, String checksumKey) throws MigrationDAOException {
		boolean toRet = Boolean.FALSE;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = targetDS.getConnection();
			stmt = conn.prepareStatement(LIQUIBASE_METADATA_SELECT_ALL_BY_ID);
			stmt.setString(1, checksumKey);
			toRet = stmt.executeUpdate()>0;
		} catch (SQLException e) {
			DAOExceptionHandler.handleMigrationDAOException(e, this.getClass());
		} finally { 
			closeResourceGracefully(null, stmt, conn);
		}	
		return toRet;
	}
	
	/**
	 * Inserisce i metadati corrispondenti al patchset passato in argomento (applicato manualmente)
	 * 
	 * @param targetDS il target data source
	 * @param anActivity il patchset applicato manualmente
	 */
	@Override
	public int fixMetadata(DataSource targetDS, Activity anActivity) throws MigrationDAOException {
		int toRet = -1;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = targetDS.getConnection();
			
			stmt = conn.prepareStatement(LIQUIBASE_METADATA_DELETE_BY_ID);
			stmt.setString(1,  anActivity.getVersion()+":"+anActivity.getScriptName()+":"+anActivity.getDescription());
			stmt.executeUpdate();
			
			stmt = conn.prepareStatement(LIQUIBASE_METADATA_INSERT);
			
			stmt.setString(1, anActivity.getVersion()+":"+anActivity.getScriptName()+":"+anActivity.getDescription());
			stmt.setString(2, anActivity.getAuthor());
			stmt.setString(3, anActivity.getL_filename());
			stmt.setDate(4, new Date(anActivity.getApplyDate().getTime()));
			stmt.setLong(5, -1);
			stmt.setString(6, "EXECUTED");
			stmt.setString(7, anActivity.getL_md5sum());
			stmt.setString(8, anActivity.getDescription());
			stmt.setString(9, null);
			stmt.setString(10, null);
			stmt.setString(11, null);
			stmt.setString(12, null);
			stmt.setString(13, null);
			stmt.setString(14, null);
			
			toRet = stmt.executeUpdate();	
		} catch (SQLException e) {
			DAOExceptionHandler.handleMigrationDAOException(e, this.getClass());
		} finally { 
			closeResourceGracefully(null, stmt, conn);
		}	
		return toRet;
	}

	/**
	 * Persiste i metadati relativi allo stato di aggiornamento del target data source sulla tabella BRC_CORE.VERSIONE
	 * per conesentirne la visualizzazione al client M.O.R.E.
	 * Workaround alla mancanza di visibilità del DbFixer in caso di installazione in-house di M.O.R.E.
	 */
	@Override
	public int insertMetadataOnTargetSchema(DbVersionMetadataDTO metadataDTO, String coreSchemaName, DataSource ds) throws MigrationDAOException {
		int toRet = -1;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = ds.getConnection();
			String sqlQuery = BRC_CORE_VERSIONI_INSERT.replaceAll(AMPERSEND+CORE_ALIAS, coreSchemaName);
			stmt = conn.prepareStatement(sqlQuery);
			stmt.setDate(1, metadataDTO.getVersionAppliedDate());
			stmt.setString(2, metadataDTO.getPatchset());
			stmt.setString(3, metadataDTO.getVersion());
			stmt.setString(4, "");
			
			toRet = stmt.executeUpdate();	
		} catch (SQLException e) {
			DAOExceptionHandler.handleMigrationDAOException(e, this.getClass());
		} finally { 
			closeResourceGracefully(null, stmt, conn);
		}	
		return toRet;
	}

}

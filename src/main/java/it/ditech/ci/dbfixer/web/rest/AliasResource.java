package it.ditech.ci.dbfixer.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import it.ditech.ci.dbfixer.domain.Alias;
import it.ditech.ci.dbfixer.repository.AliasRepository;
import it.ditech.ci.dbfixer.web.rest.util.HeaderUtil;
import it.ditech.ci.dbfixer.web.rest.util.PaginationUtil;

/**
 * REST controller for managing Alias.
 */
@RestController
@RequestMapping("/api")
public class AliasResource {

	 private final Logger log = LoggerFactory.getLogger(AliasResource.class);

	    private static final String ENTITY_NAME = "alias";

	    private final AliasRepository aliasRepository;
	    public AliasResource(AliasRepository aliasRepository) {
	        this.aliasRepository = aliasRepository;
	    }

	    /**
	     * POST  /alias : Create a new alias.
	     *
	     * @param alias the alias to create
	     * @return the ResponseEntity with status 201 (Created) and with body the new alias, or with status 400 (Bad Request) if the alias has already an ID
	     * @throws URISyntaxException if the Location URI syntax is incorrect
	     */
	    @PostMapping("/alias")
	    @Timed
	    public ResponseEntity<Alias> createAlias(@Valid @RequestBody Alias alias) throws URISyntaxException {
	        log.debug("REST request to save Alias : {}", alias);
	        if (alias.getId() != null) {
	            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new alias cannot already have an ID")).body(null);
	        }
	        Alias result = aliasRepository.save(alias);
	        return ResponseEntity.created(new URI("/api/alias/" + result.getId()))
	            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
	            .body(result);
	    }

	    /**
	     * PUT  /alias : Updates an existing alias.
	     *
	     * @param alias the alias to update
	     * @return the ResponseEntity with status 200 (OK) and with body the updated alias,
	     * or with status 400 (Bad Request) if the alias is not valid,
	     * or with status 500 (Internal Server Error) if the alias couldn't be updated
	     * @throws URISyntaxException if the Location URI syntax is incorrect
	     */
	    @PutMapping("/alias")
	    @Timed
	    public ResponseEntity<Alias> updateAlias(@Valid @RequestBody Alias alias) throws URISyntaxException {
	        log.debug("REST request to update Alias : {}", alias);
	        if (alias.getId() == null) {
	            return createAlias(alias);
	        }
	        Alias result = aliasRepository.save(alias);
	        return ResponseEntity.ok()
	            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, alias.getId().toString()))
	            .body(result);
	    }

	    /**
	     * GET  /alias : get all the alias.
	     *
	     * @param pageable the pagination information
	     * @return the ResponseEntity with status 200 (OK) and the list of alias in body
	     */
	    @GetMapping("/alias")
	    @Timed
	    public ResponseEntity<List<Alias>> getAllAliass(@ApiParam Pageable pageable) {
	        log.debug("REST request to get a page of Aliass");
	        Page<Alias> page = aliasRepository.findAll(pageable);
	        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/alias");
	        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	    }

	    /**
	     * GET  /alias/:id : get the "id" alias.
	     *
	     * @param id the id of the alias to retrieve
	     * @return the ResponseEntity with status 200 (OK) and with body the alias, or with status 404 (Not Found)
	     */
	    @GetMapping("/alias/{id}")
	    @Timed
	    public ResponseEntity<Alias> getAlias(@PathVariable Long id) {
	        log.debug("REST request to get Alias : {}", id);
	        Alias alias = aliasRepository.findOne(id);
	        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(alias));
	    }

	    /**
	     * DELETE  /alias/:id : delete the "id" alias.
	     *
	     * @param id the id of the alias to delete
	     * @return the ResponseEntity with status 200 (OK)
	     */
	    @DeleteMapping("/alias/{id}")
	    @Timed
	    public ResponseEntity<Void> deleteAlias(@PathVariable Long id) {
	        log.debug("REST request to delete Alias : {}", id);
	        aliasRepository.delete(id);
	        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	    }

}

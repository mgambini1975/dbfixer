import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { OracleDbEnv } from './oracle-db-env.model';
import { OracleDbEnvService } from './oracle-db-env.service';

@Component({
    selector: 'jhi-oracle-db-env-detail',
    templateUrl: './oracle-db-env-detail.component.html'
})
export class OracleDbEnvDetailComponent implements OnInit, OnDestroy {

    oracleDbEnv: OracleDbEnv;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private oracleDbEnvService: OracleDbEnvService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInOracleDbEnvs();
    }

    load(id) {
        this.oracleDbEnvService.find(id).subscribe((oracleDbEnv) => {
            this.oracleDbEnv = oracleDbEnv;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInOracleDbEnvs() {
        this.eventSubscriber = this.eventManager.subscribe(
            'oracleDbEnvListModification',
            (response) => this.load(this.oracleDbEnv.id)
        );
    }
}

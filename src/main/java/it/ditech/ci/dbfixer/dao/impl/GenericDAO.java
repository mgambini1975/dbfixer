package it.ditech.ci.dbfixer.dao.impl;

import static it.ditech.ci.dbfixer.config.Constants.DBFIXER_DS_NAME;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import it.ditech.ci.dbfixer.dao.IGenericDAO;

/**
 * Ancestor della gerarchia dei Data Access Object
 * 
 * @author mgambini
 *
 */
public abstract class GenericDAO implements IGenericDAO {
		
	@Autowired
	@Qualifier(DBFIXER_DS_NAME)
	DataSource dataSource;
	
	private final Logger log = LoggerFactory.getLogger(GenericDAO.class);

	@Override
	public String getJdbcDAOName() {
		return this.getClass().getName();
	}

	@Override
	public DataSource getDataSource() {
		return this.dataSource;
	}
	
	public void closeResourceGracefully(ResultSet rs, PreparedStatement ps, Connection conn){
		try {
			if (rs != null) {
				rs.close();
			}
			if (ps != null) {
				ps.close();
			}
			if (conn != null) {				
				conn.close();
			}
		} catch (SQLException e) {
			log.error("Eccezione in chiusura JDBC connection: "+e.getMessage());
		}
	}
}

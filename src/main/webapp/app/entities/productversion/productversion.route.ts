import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ProductVersionComponent } from './productversion.component';
import { ProductVersionDetailComponent } from './productversion-detail.component';
import { ProductVersionPopupComponent } from './productversion-dialog.component';
import { ProductVersionDeletePopupComponent } from './productversion-delete-dialog.component';

export const productversionRoute: Routes = [
    {
        path: 'productversion',
        component: ProductVersionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.productversion.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'productversion/:id',
        component: ProductVersionDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.productversion.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const productversionPopupRoute: Routes = [
    {
        path: 'productversion-new',
        component: ProductVersionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.productversion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'productversion/:id/edit',
        component: ProductVersionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.productversion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'productversion/:id/delete',
        component: ProductVersionDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.productversion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];

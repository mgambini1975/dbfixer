package it.ditech.ci.dbfixer.service.validator;

import static it.ditech.ci.dbfixer.config.Constants.BUILD_ID;
import static it.ditech.ci.dbfixer.config.Constants.DBENV;
import static it.ditech.ci.dbfixer.config.Constants.PATCH_ID;
import static it.ditech.ci.dbfixer.config.Constants.PATCH_NAME_PREFIX;
import static it.ditech.ci.dbfixer.config.Constants.PRODUCT_ID;
import static it.ditech.ci.dbfixer.config.Constants.STAGE_ID;
import static it.ditech.ci.dbfixer.config.Constants.VERSION_ID;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.ditech.ci.dbfixer.dao.IAliasDAO;
import it.ditech.ci.dbfixer.dao.IDBEnvDAO;
import it.ditech.ci.dbfixer.dao.IProductDAO;
import it.ditech.ci.dbfixer.dao.impl.MigrationDAOException;
import it.ditech.ci.dbfixer.service.dto.AliasDTO;
import it.ditech.ci.dbfixer.service.dto.OracleDbEnvDTO;

/**
 * Valida i query parameter dei RESTful endpoint
 * @author mgambini
 *
 */
@Component
public class MigrationValidator implements IMigrationValidator {
	
	@Autowired
	private IProductDAO productDAO;
		
	@Autowired
	private IDBEnvDAO dbEnvDAO;
	
	@Autowired
	private IAliasDAO aliasDAO;
	
	private final static Logger log = LoggerFactory.getLogger(MigrationValidator.class);
	
	@Override
	public void validate(Map<String, Object> values) throws MigrationValidatorException {
		for (String key : values.keySet()){
			if (PRODUCT_ID.equals(key)){
				validateProductId((Long)values.get(key));
			}
			else if (BUILD_ID.equals(key)  && values.get(key)!=null){
				validateBuildId((Long)values.get(key));
			}
			else if (STAGE_ID.equals(key)  && values.get(key)!=null){
				validateStageId((Long)values.get(key));
			}
			else if (VERSION_ID.equals(key)  && values.get(key)!=null){
				validateVersionId((String)values.get(key));
			} 
			else if (PATCH_ID.equals(key) && values.get(key)!=null){
				validatePatchId((Long)values.get(key));
			}	
			else if (PATCH_NAME_PREFIX.equals(key) && values.get(key)!=null){
				validatePatchPrefixId((String)values.get(key));
			}
		}
		validateEnvAndProductId((Long)values.get(PRODUCT_ID), (Long)values.get(STAGE_ID));
	}
	
	private void validatePatchPrefixId(String patchPrefixId)  throws MigrationValidatorException {
		try {
			if (Long.parseLong(patchPrefixId) < 0)
				throw new MigrationValidatorException(PATCH_NAME_PREFIX+" must be a positive integer: "+patchPrefixId);
		} catch (NumberFormatException e) {
			throw new MigrationValidatorException(PATCH_NAME_PREFIX+" must be a positive integer: "+patchPrefixId);
		}
	}
	
	private void validatePatchId(Long patchId)  throws MigrationValidatorException {
		try {
			if (patchId < 0)
				throw new MigrationValidatorException(PATCH_ID+" must be a positive integer: "+patchId);
		} catch (NumberFormatException e) {
			throw new MigrationValidatorException(PATCH_ID+" must be a positive integer: "+patchId);
		}
	}
	
	private void validateEnvAndProductId(Long productId, Long stageId) throws MigrationValidatorException {	
		try {
			OracleDbEnvDTO dbEnvDTO = dbEnvDAO.getDbEnvByProductAndStage(productId, stageId); 
			if (dbEnvDTO.getId() == null)
				throw new MigrationValidatorException(STAGE_ID+": "+stageId+" not found for "+PRODUCT_ID+": "+productId);
			AliasDTO aliasDTO = aliasDAO.getDbAliasesByEnvId(dbEnvDTO.getId());
			if ((aliasDTO.getId()) == null) {
				throw new MigrationValidatorException("No DbAlias found for "+PRODUCT_ID+": "+productId+" and "+STAGE_ID+": "+stageId);
			}
		} catch (MigrationDAOException e) {
			throw new MigrationValidatorException("Error validating "+PRODUCT_ID+": "+productId+" and "+STAGE_ID+". "+e.getMessage());
		}
	}

	/**
	 * Valida l'esistenza dell'ID passato in argomento
	 * @param productId
	 * @throws MigrationValidatorException
	 */
	private void validateProductId(Long productId) throws MigrationValidatorException {
		try {
			if (productDAO.getProductFromId(productId) == null)
				throw new MigrationValidatorException(PRODUCT_ID+" not found: "+productId);
		} catch (MigrationDAOException e) {
			throw new MigrationValidatorException(PRODUCT_ID+" not found: "+productId);
		}
	}
	
	/**
	 * Valida l'esistenza dell'ID passato in argomento
	 * @param envId
	 * @throws MigrationValidatorException
	 */
	private void validateStageId(Long envId) throws MigrationValidatorException {
		try {
			if (dbEnvDAO.getDbEnvById(envId) == null)
				throw new MigrationValidatorException(DBENV+" not found: "+envId);
		} catch (MigrationDAOException e) {
			throw new MigrationValidatorException(DBENV+" not found: "+envId);
		}
	}
	
	/**
	 * Valida l'esistenza dell'ID passato in argomento
	 * @param buildId
	 * @throws MigrationValidatorException
	 */
	private void validateBuildId(Long buildId) throws MigrationValidatorException {
		//Nothing TODO here
	}
	
	/**
	 * Valida l'esistenza del versionId passato in argomento
	 * @param versionId
	 * @throws MigrationValidatorException
	 */
	private void validateVersionId(String versionId) throws MigrationValidatorException {
		//Nothing TODO here
	}
	
	public IProductDAO getProductDAO() {
		return productDAO;
	}

	public void setProductDAO(IProductDAO productDAO) {
		this.productDAO = productDAO;
	}

	public IDBEnvDAO getDbEnvDAO() {
		return dbEnvDAO;
	}

	public void setDbEnvDAO(IDBEnvDAO dbEnvDAO) {
		this.dbEnvDAO = dbEnvDAO;
	}

}

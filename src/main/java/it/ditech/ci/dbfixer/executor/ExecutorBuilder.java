package it.ditech.ci.dbfixer.executor;

import static it.ditech.ci.dbfixer.config.Constants.CHANGELOG;
import static it.ditech.ci.dbfixer.config.Constants.CHANGELOG_MASTER_FILENAME;
import static it.ditech.ci.dbfixer.config.Constants.FILE_SEPARATOR;
import static it.ditech.ci.dbfixer.config.Constants.MAIN;
import static it.ditech.ci.dbfixer.config.Constants.RESOURCES;
import static it.ditech.ci.dbfixer.config.Constants.SOURCE;

import java.net.URL;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import it.ditech.ci.dbfixer.config.ApplicationProperties;
import it.ditech.ci.dbfixer.dao.ITargetDbMetadataDAO;
import it.ditech.ci.dbfixer.domain.enumeration.ActivityType;
import it.ditech.ci.dbfixer.domain.enumeration.StageEnvType;
import it.ditech.ci.dbfixer.executor.callback.LiquibaseCustomCallbackHandler;
import it.ditech.ci.dbfixer.executor.callback.LiquibaseFailureCustomCallback;
import it.ditech.ci.dbfixer.executor.callback.LiquibaseSuccessCustomCallback;
import it.ditech.ci.dbfixer.executor.configuration.LiquibaseExecutorConfiguration;
import it.ditech.ci.dbfixer.service.dto.AliasDTO;
import it.ditech.ci.dbfixer.service.dto.CustomerDTO;
import it.ditech.ci.dbfixer.service.dto.ProductDTO;
import liquibase.Liquibase;
import liquibase.database.DatabaseConnection;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.FileSystemResourceAccessor;


/**
 * Factory dell'esecutore degli aggiornamenti DB
 * @author mgambini
 *
 */
@Component
public class ExecutorBuilder {
	
	public enum EXECUTOR_TYPE { FLYWAY, LIQUIBASE }
	
	private final static Logger log = LoggerFactory.getLogger(ExecutorBuilder.class);
	
	private ApplicationProperties applicationProperties;
	
	public ExecutorBuilder(ApplicationProperties appProps) {
		this.applicationProperties = appProps;
	}
			

	/**
	 * Restituisce l'istanza dell'esecutore specificato nell'environment applicativo
	 * 
	 * @param ds il data source del target DB
	 * @param envType il tipo di ambiente su cui eseguire l'operazione corrente
	 * @param repoUrl il repo contenente i patchset
	 * @param metadataDAO il DAO per l'accesso alla tabella metadati del motore di applicazione delle patches
	 * @param activityType il tipo dell'operazione corrente
	 * @param buildId identificativo della build corrente
	 * @param aliasDto DTO di un AliasDBEnv
	 * @param productDto DTO di un prodotto
	 * @param customerDto DTO di un cliente
	 * @return l'istanza del motore di applicazione delle patches
	 * @throws ExecutorException 
	 */
	public IExecutor createExecutor(DataSource ds, StageEnvType envType, URL repoUrl, ITargetDbMetadataDAO metadataDAO, 
			ActivityType activityType, Long buildId, AliasDTO aliasDto, ProductDTO productDto, CustomerDTO customerDto, List<CustomerDTO> supportedCustomers) throws ExecutorException{
		IExecutor toRet = null;
		String currentTarget = getApplicationProperties().getEngine();
		if (ExecutorBuilder.EXECUTOR_TYPE.FLYWAY.name().toLowerCase().equals(currentTarget)){
				String msg = ExecutorBuilder.EXECUTOR_TYPE.FLYWAY.name()+" executor not supported.";
				log.error(msg);
				throw new RuntimeException(msg);
		} else if (ExecutorBuilder.EXECUTOR_TYPE.LIQUIBASE.name().toLowerCase().equals(currentTarget)) {
				LiquibaseCustomCallbackHandler changeExecListener = new LiquibaseCustomCallbackHandler();
				
				//Istanzio e registro i callbackHandler a seconda del tipo di operazione
				if (ActivityType.SQL.name().equals(activityType.name())){
					changeExecListener.registerCallback(new LiquibaseSuccessCustomCallback(buildId));
					//changeExecListener.registerCallback(new LiquibaseSysdbaExecCustomCallback(buildId));
				}
				changeExecListener.registerCallback(new LiquibaseFailureCustomCallback(buildId));
				
				FileSystemResourceAccessor ra = new FileSystemResourceAccessor();
				LiquibaseExecutorConfiguration execConfiguration = new LiquibaseExecutorConfiguration(repoUrl, CHANGELOG_MASTER_FILENAME, ra, ds, changeExecListener, envType, metadataDAO, aliasDto, productDto, customerDto, supportedCustomers);
				Liquibase executor;
				DatabaseConnection conn;
				try {
					conn = new JdbcConnection(ds.getConnection());
					String fileSeparator = System.getProperty(FILE_SEPARATOR);
					String changelogFilename = execConfiguration.getBasePath()+fileSeparator+SOURCE+fileSeparator+MAIN+fileSeparator+RESOURCES+fileSeparator+CHANGELOG+fileSeparator+CHANGELOG_MASTER_FILENAME;			
					executor = new Liquibase(changelogFilename, execConfiguration.getResourceAccessor(), conn);
					executor.setChangeExecListener(changeExecListener);
					toRet =  new LiquibaseExecutor(executor, execConfiguration);
					log.debug(ExecutorBuilder.EXECUTOR_TYPE.LIQUIBASE.name()+" executor instantiated.");
				} catch (SQLException | LiquibaseException ex) {
					throw new ExecutorException(ExecutorOperation.CONFIGURE, ex.getMessage(), ex.getCause());
				}			
		}
		return toRet;
	}


	public ApplicationProperties getApplicationProperties() {
		return applicationProperties;
	}
	


	
}
 
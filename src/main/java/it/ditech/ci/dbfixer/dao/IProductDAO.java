package it.ditech.ci.dbfixer.dao;

import java.util.List;

import it.ditech.ci.dbfixer.dao.impl.MigrationDAOException;
import it.ditech.ci.dbfixer.service.dto.ProductDTO;

/**
 * Contratto DAO per la gestione dei prodotti
 * 
 * author @mgambini
 */
public interface IProductDAO extends IGenericDAO {
	
	/**
	 * Restituisce l'elenco dei prodotti per l'ID cliente passato in argomento
	 * @param customerId id del cliente
	 * @return l'elenco dei prodotti 
	 */
	public List<ProductDTO> getProductsByCustomerId(Long customerId) throws MigrationDAOException;
	
	/**
	 * Restituisce le info del prodotto corrispondente all'ID cliente passato in argomento 
	 * @param productId
	 * @return le info sul prodotto
	 * @throws MigrationDAOException
	 */
	public ProductDTO getProductFromId(Long productId) throws MigrationDAOException;

}

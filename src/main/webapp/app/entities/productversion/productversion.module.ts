import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DbFixerSharedModule } from '../../shared';
import {
    ProductVersionService,
    ProductVersionPopupService,
    ProductVersionComponent,
    ProductVersionDetailComponent,
    ProductVersionDialogComponent,
    ProductVersionPopupComponent,
    ProductVersionDeletePopupComponent,
    ProductVersionDeleteDialogComponent,
    productversionRoute,
    productversionPopupRoute,
} from './';

const ENTITY_STATES = [
    ...productversionRoute,
    ...productversionPopupRoute,
];

@NgModule({
    imports: [
        DbFixerSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ProductVersionComponent,
        ProductVersionDetailComponent,
        ProductVersionDialogComponent,
        ProductVersionDeleteDialogComponent,
        ProductVersionPopupComponent,
        ProductVersionDeletePopupComponent,
    ],
    entryComponents: [
        ProductVersionComponent,
        ProductVersionDialogComponent,
        ProductVersionPopupComponent,
        ProductVersionDeleteDialogComponent,
        ProductVersionDeletePopupComponent,
    ],
    providers: [
        ProductVersionService,
        ProductVersionPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DbFixerProductVersionModule {}

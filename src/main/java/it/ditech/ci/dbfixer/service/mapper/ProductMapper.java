package it.ditech.ci.dbfixer.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import it.ditech.ci.dbfixer.domain.Product;
import it.ditech.ci.dbfixer.service.dto.ProductDTO;

/**
 * Mapper for the entity Product and its DTO ProductDTO.
 */
@Mapper(componentModel = "spring", uses = {CustomerMapper.class, })
public interface ProductMapper extends EntityMapper <ProductDTO, Product> {

    @Mapping(source = "customer.id", target = "customerId")
    ProductDTO toDto(Product product); 

    @Mapping(source = "customerId", target = "customer")
    @Mapping(target = "wildflyEnvs", ignore = true)
    @Mapping(target = "oracleDbEnvs", ignore = true)
    @Mapping(target = "tomcatEnvs", ignore = true)
    Product toEntity(ProductDTO productDTO); 
    default Product fromId(Long id) {
        if (id == null) {
            return null;
        }
        Product product = new Product();
        product.setId(id);
        return product;
    }
}

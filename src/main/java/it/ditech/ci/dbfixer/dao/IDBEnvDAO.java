package it.ditech.ci.dbfixer.dao;

import it.ditech.ci.dbfixer.dao.impl.MigrationDAOException;
import it.ditech.ci.dbfixer.service.dto.OracleDbEnvDTO;

/**
 * Contratto DAO per la gestione degli ambienti DB target
 * 
 * @author mgambini
 */
public interface IDBEnvDAO extends IGenericDAO {
	
	/**
	 * Restituisce la configurazioni DB per l'ID del prodotto e del tipo ambiente passato in argomento
	 * @param productId id del prodotto
	 * @param stageId id dell'ambiente
	 * @return l'elenco delle configurazioni DB
	 * @throws MigrationDAOException
	 */
	public OracleDbEnvDTO getDbEnvByProductAndStage(Long productId, Long stageId) throws MigrationDAOException;

	/**
	 * Restituisce la configurazione DB per l'ID passato in argomento
	 * @return la configurazione DB
	 * @throws MigrationDAOException
	 */
	public OracleDbEnvDTO getDbEnvById(Long envId) throws MigrationDAOException;
}

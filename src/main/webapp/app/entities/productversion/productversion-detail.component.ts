import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { ProductVersion } from './productversion.model';
import { ProductVersionService } from './productversion.service';

@Component({
    selector: 'jhi-productversion-detail',
    templateUrl: './productversion-detail.component.html'
})
export class ProductVersionDetailComponent implements OnInit, OnDestroy {

    productversion: ProductVersion;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private productversionService: ProductVersionService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInProducts();
    }

    load(id) {
        this.productversionService.find(id).subscribe((productversion) => {
            this.productversion = productversion;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInProducts() {
        this.eventSubscriber = this.eventManager.subscribe(
            'productListModification',
            (response) => this.load(this.productversion.id)
        );
    }
}

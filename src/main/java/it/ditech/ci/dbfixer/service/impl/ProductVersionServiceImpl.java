package it.ditech.ci.dbfixer.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.ditech.ci.dbfixer.domain.ProductVersion;
import it.ditech.ci.dbfixer.repository.ProductVersionRepository;
import it.ditech.ci.dbfixer.service.ProductVersionService;
import it.ditech.ci.dbfixer.service.dto.ProductVersionDTO;
import it.ditech.ci.dbfixer.service.mapper.ProductVersionMapper;

/**
 * Service Implementation for managing ProductVersion.
 */
@Service
@Transactional
public class ProductVersionServiceImpl implements ProductVersionService {
	
	private final Logger log = LoggerFactory.getLogger(ProductVersionServiceImpl.class);

	private final ProductVersionRepository productVersionRepository;
	private final ProductVersionMapper productVersionMapper;

	public ProductVersionServiceImpl(ProductVersionRepository productVersionRepository, ProductVersionMapper productVersionMapper) {
	        this.productVersionRepository = productVersionRepository;
	        this.productVersionMapper = productVersionMapper;
	}

	@Override
	public ProductVersionDTO save(ProductVersionDTO productVersionDTO) {
		log.debug("Request to save ProductVersion : {}", productVersionDTO);
        ProductVersion productVersion = productVersionMapper.toEntity(productVersionDTO);
        productVersion = productVersionRepository.save(productVersion);
        return productVersionMapper.toDto(productVersion);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<ProductVersionDTO> findAll(Pageable pageable) {
		log.debug("Request to get all ProductVersions");
        return productVersionRepository.findAll(pageable)
            .map(productVersionMapper::toDto);
	}

	@Override
	@Transactional(readOnly = true)
	public ProductVersionDTO findOne(Long id) {
		log.debug("Request to get ProductVersion : {}", id);
        ProductVersion productVersion = productVersionRepository.findOne(id);
        return productVersionMapper.toDto(productVersion);
	}

	@Override
	public void delete(Long id) {
		 log.debug("Request to delete ProductVersion : {}", id);
	      productVersionRepository.delete(id);
	}

	public ProductVersionRepository getProductVersionRepository() {
		return productVersionRepository;
	}

	public ProductVersionMapper getProductVersionMapper() {
		return productVersionMapper;
	}

}

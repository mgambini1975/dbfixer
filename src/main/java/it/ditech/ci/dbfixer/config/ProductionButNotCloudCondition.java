package it.ditech.ci.dbfixer.config;

import static it.ditech.ci.dbfixer.config.Constants.CLOUD;
import static it.ditech.ci.dbfixer.config.Constants.PROD_ENV;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * Valuta la condizione di ambiente PROD ma non CLOUD
 * @author mgambini
 *
 */
public class ProductionButNotCloudCondition implements Condition {

	@Override
	public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
		if (context.getEnvironment() != null) {
			final String activeProfiles = context.getEnvironment().getProperty("spring.profiles.active");
			if (activeProfiles.contains(PROD_ENV) && !activeProfiles.contains(CLOUD))
				return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

}

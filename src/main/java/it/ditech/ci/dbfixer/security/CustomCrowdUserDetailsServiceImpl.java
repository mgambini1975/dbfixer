package it.ditech.ci.dbfixer.security;

import static it.ditech.ci.dbfixer.config.Constants.ROLE_ADMIN;
import static it.ditech.ci.dbfixer.config.Constants.ROLE_USER;
import static it.ditech.ci.dbfixer.config.Constants.CROWD;
import static it.ditech.ci.dbfixer.config.Constants.CROWD_DBFIXER_ADMIN_GROUP;
import static it.ditech.ci.dbfixer.config.Constants.IT_LANGUAGE;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.atlassian.crowd.integration.rest.entity.UserEntity;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetailsServiceImpl;

import it.ditech.ci.dbfixer.domain.Authority;
import it.ditech.ci.dbfixer.domain.User;
import it.ditech.ci.dbfixer.repository.UserRepository;

/**
 * Implementa il servizio di recupero degli attributi utente mediante 
 * integrazione con Crowd
 * 
 * @author mgambini
 *
 */
@Component
public class CustomCrowdUserDetailsServiceImpl extends CrowdUserDetailsServiceImpl {
	
	private final UserRepository userRepository;
		
	public CustomCrowdUserDetailsServiceImpl(UserRepository userRepository){
		super();
		this.userRepository = userRepository;
	}
	
	@Override
	public CrowdUserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {		
		CrowdUserDetails userDetails = super.loadUserByUsername(username);
		//Cerco match su DB
		Optional<User> user = userRepository.findOneByLogin(username);	
		//Predispongo ruoli
		Set<Authority> authorities = new HashSet<Authority>();
		Authority authority = new Authority();
		authority.setName(ROLE_USER);
		authorities.add(authority);
		
		//Verifico possesso ruolo ADMIN
		Collection<GrantedAuthority> groups = userDetails.getAuthorities();
		boolean isAdmin = Boolean.FALSE;
		for (GrantedAuthority group : groups) {
			if (group.getAuthority().equalsIgnoreCase(CROWD_DBFIXER_ADMIN_GROUP)) {
				isAdmin = Boolean.TRUE;
				break;
			}
		}
		if (isAdmin){
			authority = new Authority();
			authority.setName(ROLE_ADMIN);
			authorities.add(authority);
		}	
		
		if (!user.isPresent()) {			
				
			//Inserisco in DB
			User newUser = new User();
		    newUser.setLogin(userDetails.getUsername());
		    newUser.setFirstName(userDetails.getFirstName());
		    newUser.setLastName(userDetails.getLastName());
		    newUser.setEmail(userDetails.getEmail());
		    newUser.setImageUrl(null);
		    newUser.setLangKey(IT_LANGUAGE); // default language		        	        
		    String encryptedPassword = null;
		    newUser.setPassword(encryptedPassword);   
		    newUser.setActivated(Boolean.TRUE);
		    newUser.setAuthorities(authorities);
		    newUser.setCreatedBy(CROWD);
		    newUser.setLastModifiedBy(CROWD);
		    userRepository.save(newUser);
		} 
		Date currentDate = new Date();
		UserEntity ue = new UserEntity(userDetails.getUsername(), 
				userDetails.getFirstName(), 
				userDetails.getLastName(), 
				userDetails.getFullName(), 
				userDetails.getEmail(), 
				null,
				userDetails.isEnabled(),
				null,
				currentDate, 
				currentDate);
				
		CrowdUserDetails cud = new CrowdUserDetails(ue, this.getAuthorities(authorities));
		return cud;
	}
	
	
	private Collection<GrantedAuthority> getAuthorities(Set<Authority> authorities) {
		ArrayList userGroups = new ArrayList();
		for (Authority auth : authorities){
			userGroups.add(new SimpleGrantedAuthority(auth.getName()));
		}
		return userGroups;
	}

}

package it.ditech.ci.dbfixer.executor.configuration;

import java.net.URL;
import java.util.List;

import javax.sql.DataSource;

import it.ditech.ci.dbfixer.dao.ITargetDbMetadataDAO;
import it.ditech.ci.dbfixer.domain.enumeration.StageEnvType;
import it.ditech.ci.dbfixer.service.dto.AliasDTO;
import it.ditech.ci.dbfixer.service.dto.CustomerDTO;
import it.ditech.ci.dbfixer.service.dto.ProductDTO;
import liquibase.changelog.visitor.ChangeExecListener;
import liquibase.resource.ResourceAccessor;

/**
 * Configurazione di un esecutore Liquibase
 * Caratterizzata da:
 *  - URL repo git contenente i patchset
 *  - nome del master changelog file
 *  - StageEnvType il tipo di target DB
 *  - DataSource per l'accesso al target DB
 *  - ChangeExecListener per la gestione delle callback di applicazione dei patchset
 *  - ResourceAccessor
 *  - LiquibaseMetadataDAO per l'accesso diretto alla tabella metadati di Liquibase
 *  
 * @author mgambini
 *
 */
public class LiquibaseExecutorConfiguration extends ExecutorConfiguration {
	
	private ChangeExecListener changeExecListener;
	private String changeLogFilename;
	private ResourceAccessor resourceAccessor;
	private ITargetDbMetadataDAO metadataDAO;
	
		
	public LiquibaseExecutorConfiguration(URL repoURL, String chLogFile, ResourceAccessor resAccessor, DataSource ds, ChangeExecListener changeListener, 
			StageEnvType envType, ITargetDbMetadataDAO metadataDao, AliasDTO aliasDto, ProductDTO productDto, CustomerDTO customerDto, List<CustomerDTO> supportedCustomer){
		super(repoURL, ds, envType, aliasDto, productDto, customerDto, supportedCustomer);
		this.changeExecListener = changeListener; 
		this.changeLogFilename = chLogFile;
		this.resourceAccessor = resAccessor;
		this.metadataDAO = metadataDao;
	}
	
	public ChangeExecListener getChangeExecListener() {
		return this.changeExecListener;
	}

	public String getChangeLogFilename() {
		return this.changeLogFilename;
	}
	
	public ResourceAccessor getResourceAccessor() {
		return this.resourceAccessor;
	}
	
	public void setResourceAccessor(ResourceAccessor ra) {
		this.resourceAccessor = ra;
	}
	
	public ITargetDbMetadataDAO getMetadataDAO() {
		return metadataDAO;
	}

}

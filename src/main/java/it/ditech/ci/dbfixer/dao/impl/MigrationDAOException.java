package it.ditech.ci.dbfixer.dao.impl;

public class MigrationDAOException extends Exception {

	private static final long serialVersionUID = 1352252858693667094L;
	
	public MigrationDAOException(Throwable cause){
		super("Exception operating on target schema: ", cause);
	}
	
	public MigrationDAOException(String msg) {
		super(msg);
	}

}

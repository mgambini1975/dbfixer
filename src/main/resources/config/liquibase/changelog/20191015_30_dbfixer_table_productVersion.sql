--liquibase formatted sql
--changeset mgambini:creazione tabella productVersion

-- --------------------------------------------------------

--
-- Struttura della tabella `productVersion`
--

CREATE TABLE IF NOT EXISTS `productVersion` (
	`id` bigint(20) NOT NULL AUTO_INCREMENT,
  	`name` varchar(255) COLLATE utf8_bin NOT NULL,
  	`description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
	`product_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_productVersion_product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Constraints per la tabella `productVersion`
--
ALTER TABLE `productVersion`
  ADD CONSTRAINT `fk_productVersion_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);


package it.ditech.ci.dbfixer.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import it.ditech.ci.dbfixer.domain.OracleDbEnv;
import it.ditech.ci.dbfixer.domain.enumeration.StageEnvType;
import it.ditech.ci.dbfixer.repository.OracleDbEnvRepository;
import it.ditech.ci.dbfixer.web.rest.errors.ExceptionTranslator;
/**
 * Test class for the OracleDbEnvResource REST controller.
 *
 * @see OracleDbEnvResource
 */
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = DbFixerApp.class)
public class OracleDbEnvResourceIntTest {

    private static final StageEnvType DEFAULT_ENV_TYPE = StageEnvType.DEV;
    private static final StageEnvType UPDATED_ENV_TYPE = StageEnvType.CI;

    private static final String DEFAULT_HOSTNAME = "AAAAAAAAAA";
    private static final String UPDATED_HOSTNAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_LISTENER_PORT = 1;
    private static final Integer UPDATED_LISTENER_PORT = 2;

    private static final String DEFAULT_USER = "AAAAAAAAAA";
    private static final String UPDATED_USER = "BBBBBBBBBB";

    private static final String DEFAULT_PWD = "AAAAAAAAAA";
    private static final String UPDATED_PWD = "BBBBBBBBBB";

    private static final String DEFAULT_SID = "AAAAAAAAAA";
    private static final String UPDATED_SID = "BBBBBBBBBB";

    @Autowired
    private OracleDbEnvRepository oracleDbEnvRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restOracleDbEnvMockMvc;

    private OracleDbEnv oracleDbEnv;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OracleDbEnvResource oracleDbEnvResource = new OracleDbEnvResource(oracleDbEnvRepository);
        this.restOracleDbEnvMockMvc = MockMvcBuilders.standaloneSetup(oracleDbEnvResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OracleDbEnv createEntity(EntityManager em) {
        OracleDbEnv oracleDbEnv = new OracleDbEnv()
            .envType(DEFAULT_ENV_TYPE)
            .hostname(DEFAULT_HOSTNAME)
            .listenerPort(DEFAULT_LISTENER_PORT)
            .user(DEFAULT_USER)
            .pwd(DEFAULT_PWD)
            .sid(DEFAULT_SID);
        return oracleDbEnv;
    }

    @Before
    public void initTest() {
        oracleDbEnv = createEntity(em);
    }

    //@Test
    @Transactional
    public void createOracleDbEnv() throws Exception {
        int databaseSizeBeforeCreate = oracleDbEnvRepository.findAll().size();

        // Create the OracleDbEnv
        restOracleDbEnvMockMvc.perform(post("/api/oracle-db-envs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(oracleDbEnv)))
            .andExpect(status().isCreated());

        // Validate the OracleDbEnv in the database
        List<OracleDbEnv> oracleDbEnvList = oracleDbEnvRepository.findAll();
        assertThat(oracleDbEnvList).hasSize(databaseSizeBeforeCreate + 1);
        OracleDbEnv testOracleDbEnv = oracleDbEnvList.get(oracleDbEnvList.size() - 1);
        assertThat(testOracleDbEnv.getEnvType()).isEqualTo(DEFAULT_ENV_TYPE);
        assertThat(testOracleDbEnv.getHostname()).isEqualTo(DEFAULT_HOSTNAME);
        assertThat(testOracleDbEnv.getListenerPort()).isEqualTo(DEFAULT_LISTENER_PORT);
        assertThat(testOracleDbEnv.getUser()).isEqualTo(DEFAULT_USER);
        assertThat(testOracleDbEnv.getPwd()).isEqualTo(DEFAULT_PWD);
        assertThat(testOracleDbEnv.getSid()).isEqualTo(DEFAULT_SID);
    }

    //@Test
    @Transactional
    public void createOracleDbEnvWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = oracleDbEnvRepository.findAll().size();

        // Create the OracleDbEnv with an existing ID
        oracleDbEnv.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOracleDbEnvMockMvc.perform(post("/api/oracle-db-envs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(oracleDbEnv)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<OracleDbEnv> oracleDbEnvList = oracleDbEnvRepository.findAll();
        assertThat(oracleDbEnvList).hasSize(databaseSizeBeforeCreate);
    }

    //@Test
    @Transactional
    public void checkEnvTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = oracleDbEnvRepository.findAll().size();
        // set the field null
        oracleDbEnv.setEnvType(null);

        // Create the OracleDbEnv, which fails.

        restOracleDbEnvMockMvc.perform(post("/api/oracle-db-envs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(oracleDbEnv)))
            .andExpect(status().isBadRequest());

        List<OracleDbEnv> oracleDbEnvList = oracleDbEnvRepository.findAll();
        assertThat(oracleDbEnvList).hasSize(databaseSizeBeforeTest);
    }

    //@Test
    @Transactional
    public void checkHostnameIsRequired() throws Exception {
        int databaseSizeBeforeTest = oracleDbEnvRepository.findAll().size();
        // set the field null
        oracleDbEnv.setHostname(null);

        // Create the OracleDbEnv, which fails.

        restOracleDbEnvMockMvc.perform(post("/api/oracle-db-envs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(oracleDbEnv)))
            .andExpect(status().isBadRequest());

        List<OracleDbEnv> oracleDbEnvList = oracleDbEnvRepository.findAll();
        assertThat(oracleDbEnvList).hasSize(databaseSizeBeforeTest);
    }

    //@Test
    @Transactional
    public void checkListenerPortIsRequired() throws Exception {
        int databaseSizeBeforeTest = oracleDbEnvRepository.findAll().size();
        // set the field null
        oracleDbEnv.setListenerPort(null);

        // Create the OracleDbEnv, which fails.

        restOracleDbEnvMockMvc.perform(post("/api/oracle-db-envs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(oracleDbEnv)))
            .andExpect(status().isBadRequest());

        List<OracleDbEnv> oracleDbEnvList = oracleDbEnvRepository.findAll();
        assertThat(oracleDbEnvList).hasSize(databaseSizeBeforeTest);
    }

    //@Test
    @Transactional
    public void checkUserIsRequired() throws Exception {
        int databaseSizeBeforeTest = oracleDbEnvRepository.findAll().size();
        // set the field null
        oracleDbEnv.setUser(null);

        // Create the OracleDbEnv, which fails.

        restOracleDbEnvMockMvc.perform(post("/api/oracle-db-envs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(oracleDbEnv)))
            .andExpect(status().isBadRequest());

        List<OracleDbEnv> oracleDbEnvList = oracleDbEnvRepository.findAll();
        assertThat(oracleDbEnvList).hasSize(databaseSizeBeforeTest);
    }

    //@Test
    @Transactional
    public void checkPwdIsRequired() throws Exception {
        int databaseSizeBeforeTest = oracleDbEnvRepository.findAll().size();
        // set the field null
        oracleDbEnv.setPwd(null);

        // Create the OracleDbEnv, which fails.

        restOracleDbEnvMockMvc.perform(post("/api/oracle-db-envs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(oracleDbEnv)))
            .andExpect(status().isBadRequest());

        List<OracleDbEnv> oracleDbEnvList = oracleDbEnvRepository.findAll();
        assertThat(oracleDbEnvList).hasSize(databaseSizeBeforeTest);
    }

    //@Test
    @Transactional
    public void checkSidIsRequired() throws Exception {
        int databaseSizeBeforeTest = oracleDbEnvRepository.findAll().size();
        // set the field null
        oracleDbEnv.setSid(null);

        // Create the OracleDbEnv, which fails.

        restOracleDbEnvMockMvc.perform(post("/api/oracle-db-envs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(oracleDbEnv)))
            .andExpect(status().isBadRequest());

        List<OracleDbEnv> oracleDbEnvList = oracleDbEnvRepository.findAll();
        assertThat(oracleDbEnvList).hasSize(databaseSizeBeforeTest);
    }

    //@Test
    @Transactional
    public void getAllOracleDbEnvs() throws Exception {
        // Initialize the database
        oracleDbEnvRepository.saveAndFlush(oracleDbEnv);

        // Get all the oracleDbEnvList
        restOracleDbEnvMockMvc.perform(get("/api/oracle-db-envs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(oracleDbEnv.getId().intValue())))
            .andExpect(jsonPath("$.[*].envType").value(hasItem(DEFAULT_ENV_TYPE.toString())))
            .andExpect(jsonPath("$.[*].hostname").value(hasItem(DEFAULT_HOSTNAME.toString())))
            .andExpect(jsonPath("$.[*].listenerPort").value(hasItem(DEFAULT_LISTENER_PORT)))
            .andExpect(jsonPath("$.[*].user").value(hasItem(DEFAULT_USER.toString())))
            .andExpect(jsonPath("$.[*].pwd").value(hasItem(DEFAULT_PWD.toString())))
            .andExpect(jsonPath("$.[*].sid").value(hasItem(DEFAULT_SID.toString())));

    }

    //@Test
    @Transactional
    public void getOracleDbEnv() throws Exception {
        // Initialize the database
        oracleDbEnvRepository.saveAndFlush(oracleDbEnv);

        // Get the oracleDbEnv
        restOracleDbEnvMockMvc.perform(get("/api/oracle-db-envs/{id}", oracleDbEnv.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(oracleDbEnv.getId().intValue()))
            .andExpect(jsonPath("$.envType").value(DEFAULT_ENV_TYPE.toString()))
            .andExpect(jsonPath("$.hostname").value(DEFAULT_HOSTNAME.toString()))
            .andExpect(jsonPath("$.listenerPort").value(DEFAULT_LISTENER_PORT))
            .andExpect(jsonPath("$.user").value(DEFAULT_USER.toString()))
            .andExpect(jsonPath("$.pwd").value(DEFAULT_PWD.toString()))
            .andExpect(jsonPath("$.sid").value(DEFAULT_SID.toString()));

    }

    //@Test
    @Transactional
    public void getNonExistingOracleDbEnv() throws Exception {
        // Get the oracleDbEnv
        restOracleDbEnvMockMvc.perform(get("/api/oracle-db-envs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    //@Test
    @Transactional
    public void updateOracleDbEnv() throws Exception {
        // Initialize the database
        oracleDbEnvRepository.saveAndFlush(oracleDbEnv);
        int databaseSizeBeforeUpdate = oracleDbEnvRepository.findAll().size();

        // Update the oracleDbEnv
        OracleDbEnv updatedOracleDbEnv = oracleDbEnvRepository.findOne(oracleDbEnv.getId());
        updatedOracleDbEnv
            .envType(UPDATED_ENV_TYPE)
            .hostname(UPDATED_HOSTNAME)
            .listenerPort(UPDATED_LISTENER_PORT)
            .user(UPDATED_USER)
            .pwd(UPDATED_PWD)
            .sid(UPDATED_SID);

        restOracleDbEnvMockMvc.perform(put("/api/oracle-db-envs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedOracleDbEnv)))
            .andExpect(status().isOk());

        // Validate the OracleDbEnv in the database
        List<OracleDbEnv> oracleDbEnvList = oracleDbEnvRepository.findAll();
        assertThat(oracleDbEnvList).hasSize(databaseSizeBeforeUpdate);
        OracleDbEnv testOracleDbEnv = oracleDbEnvList.get(oracleDbEnvList.size() - 1);
        assertThat(testOracleDbEnv.getEnvType()).isEqualTo(UPDATED_ENV_TYPE);
        assertThat(testOracleDbEnv.getHostname()).isEqualTo(UPDATED_HOSTNAME);
        assertThat(testOracleDbEnv.getListenerPort()).isEqualTo(UPDATED_LISTENER_PORT);
        assertThat(testOracleDbEnv.getUser()).isEqualTo(UPDATED_USER);
        assertThat(testOracleDbEnv.getPwd()).isEqualTo(UPDATED_PWD);
        assertThat(testOracleDbEnv.getSid()).isEqualTo(UPDATED_SID);
    }

    //@Test
    @Transactional
    public void updateNonExistingOracleDbEnv() throws Exception {
        int databaseSizeBeforeUpdate = oracleDbEnvRepository.findAll().size();

        // Create the OracleDbEnv

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restOracleDbEnvMockMvc.perform(put("/api/oracle-db-envs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(oracleDbEnv)))
            .andExpect(status().isCreated());

        // Validate the OracleDbEnv in the database
        List<OracleDbEnv> oracleDbEnvList = oracleDbEnvRepository.findAll();
        assertThat(oracleDbEnvList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    //@Test
    @Transactional
    public void deleteOracleDbEnv() throws Exception {
        // Initialize the database
        oracleDbEnvRepository.saveAndFlush(oracleDbEnv);
        int databaseSizeBeforeDelete = oracleDbEnvRepository.findAll().size();

        // Get the oracleDbEnv
        restOracleDbEnvMockMvc.perform(delete("/api/oracle-db-envs/{id}", oracleDbEnv.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<OracleDbEnv> oracleDbEnvList = oracleDbEnvRepository.findAll();
        assertThat(oracleDbEnvList).hasSize(databaseSizeBeforeDelete - 1);
    }

    //@Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OracleDbEnv.class);
        OracleDbEnv oracleDbEnv1 = new OracleDbEnv();
        oracleDbEnv1.setId(1L);
        OracleDbEnv oracleDbEnv2 = new OracleDbEnv();
        oracleDbEnv2.setId(oracleDbEnv1.getId());
        assertThat(oracleDbEnv1).isEqualTo(oracleDbEnv2);
        oracleDbEnv2.setId(2L);
        assertThat(oracleDbEnv1).isNotEqualTo(oracleDbEnv2);
        oracleDbEnv1.setId(null);
        assertThat(oracleDbEnv1).isNotEqualTo(oracleDbEnv2);
    }
}

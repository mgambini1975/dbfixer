package it.ditech.ci.dbfixer.dao;

import javax.sql.DataSource;

/**
 * Contratto generico per Data Access Object
 * 
 * @author mgambini
 *
 */
public interface IGenericDAO {
	
	/**
	 * Restituisce il nome della classe
	 * @return
	 */
	public String getJdbcDAOName();
	
	/**
	 * Restituisce il DataSource associato al DAO
	 * @return
	 */
	public DataSource getDataSource();
		
}

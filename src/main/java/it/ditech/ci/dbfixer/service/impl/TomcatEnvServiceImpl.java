package it.ditech.ci.dbfixer.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.ditech.ci.dbfixer.domain.TomcatEnv;
import it.ditech.ci.dbfixer.repository.TomcatEnvRepository;
import it.ditech.ci.dbfixer.service.TomcatEnvService;
import it.ditech.ci.dbfixer.service.dto.TomcatEnvDTO;
import it.ditech.ci.dbfixer.service.mapper.TomcatEnvMapper;


/**
 * Service Implementation for managing TomcatEnv.
 */
@Service
@Transactional
public class TomcatEnvServiceImpl implements TomcatEnvService{

    private final Logger log = LoggerFactory.getLogger(TomcatEnvServiceImpl.class);

    private final TomcatEnvRepository tomcatEnvRepository;

    private final TomcatEnvMapper tomcatEnvMapper;
    public TomcatEnvServiceImpl(TomcatEnvRepository tomcatEnvRepository, TomcatEnvMapper tomcatEnvMapper) {
        this.tomcatEnvRepository = tomcatEnvRepository;
        this.tomcatEnvMapper = tomcatEnvMapper;
    }

    /**
     * Save a tomcatEnv.
     *
     * @param tomcatEnvDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public TomcatEnvDTO save(TomcatEnvDTO tomcatEnvDTO) {
        log.debug("Request to save TomcatEnv : {}", tomcatEnvDTO);
        TomcatEnv tomcatEnv = tomcatEnvMapper.toEntity(tomcatEnvDTO);
        tomcatEnv = tomcatEnvRepository.save(tomcatEnv);
        return tomcatEnvMapper.toDto(tomcatEnv);
    }

    /**
     *  Get all the tomcatEnvs.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<TomcatEnvDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TomcatEnvs");
        return tomcatEnvRepository.findAll(pageable)
            .map(tomcatEnvMapper::toDto);
    }

    /**
     *  Get one tomcatEnv by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public TomcatEnvDTO findOne(Long id) {
        log.debug("Request to get TomcatEnv : {}", id);
        TomcatEnv tomcatEnv = tomcatEnvRepository.findOne(id);
        return tomcatEnvMapper.toDto(tomcatEnv);
    }

    /**
     *  Delete the  tomcatEnv by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TomcatEnv : {}", id);
        tomcatEnvRepository.delete(id);
    }
}

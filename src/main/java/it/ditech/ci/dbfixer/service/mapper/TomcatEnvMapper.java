package it.ditech.ci.dbfixer.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import it.ditech.ci.dbfixer.domain.TomcatEnv;
import it.ditech.ci.dbfixer.service.dto.TomcatEnvDTO;

/**
 * Mapper for the entity TomcatEnv and its DTO TomcatEnvDTO.
 */
@Mapper(componentModel = "spring", uses = {ProductMapper.class, })
public interface TomcatEnvMapper extends EntityMapper <TomcatEnvDTO, TomcatEnv> {

    @Mapping(source = "product.id", target = "productId")
    TomcatEnvDTO toDto(TomcatEnv tomcatEnv); 

    @Mapping(source = "productId", target = "product")
    TomcatEnv toEntity(TomcatEnvDTO tomcatEnvDTO); 
    default TomcatEnv fromId(Long id) {
        if (id == null) {
            return null;
        }
        TomcatEnv tomcatEnv = new TomcatEnv();
        tomcatEnv.setId(id);
        return tomcatEnv;
    }
}

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { WildflyEnv } from './wildfly-env.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class WildflyEnvService {

    private resourceUrl = 'api/wildfly-envs';

    constructor(private http: Http) { }

    create(wildflyEnv: WildflyEnv): Observable<WildflyEnv> {
        const copy = this.convert(wildflyEnv);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(wildflyEnv: WildflyEnv): Observable<WildflyEnv> {
        const copy = this.convert(wildflyEnv);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<WildflyEnv> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(wildflyEnv: WildflyEnv): WildflyEnv {
        const copy: WildflyEnv = Object.assign({}, wildflyEnv);
        return copy;
    }
}

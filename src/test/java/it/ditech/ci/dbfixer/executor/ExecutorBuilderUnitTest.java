package it.ditech.ci.dbfixer.executor;

import java.net.MalformedURLException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import it.ditech.ci.dbfixer.config.ApplicationProperties;
import it.ditech.ci.dbfixer.dao.ITargetDbMetadataDAO;
import it.ditech.ci.dbfixer.service.dto.AliasDTO;
import it.ditech.ci.dbfixer.service.dto.CustomerDTO;
import it.ditech.ci.dbfixer.service.dto.ProductDTO;

public class ExecutorBuilderUnitTest {
	
	private ExecutorBuilder testingExecutorBuilder;
	
	@Mock
	private ITargetDbMetadataDAO metadataDAO;
	@Mock
	private AliasDTO aliasDto;
	@Mock
	private ProductDTO productDto;
	@Mock
	private CustomerDTO customerDto;
	@Mock
	List<CustomerDTO> supportedCustomers;

	 @Before
	 public void setup() {
		MockitoAnnotations.initMocks(this);
		ApplicationProperties appProps = new ApplicationProperties();
		ReflectionTestUtils.setField(appProps, "engine", ExecutorBuilder.EXECUTOR_TYPE.LIQUIBASE.name().toLowerCase());
		testingExecutorBuilder = new ExecutorBuilder(appProps);	
	 }
	
	@Test
	public void createExecutorShouldReturnOneInstance() throws MalformedURLException, ExecutorException{
		//assertThat(getTestingExecutorBuilder()
		//		.createExecutor(ds, StageEnvType.DEV, new URL("http://fakeUrl:8080/scm/mcedi/commercialedb.git"), metadataDAO, ActivityType.DRYRUN, new Long(1), aliasDto, productDto, customerDto, supportedCustomers))
		//.isInstanceOf(ExecutorBuilder.class); 
	}

	public ExecutorBuilder getTestingExecutorBuilder() {
		return testingExecutorBuilder;
	}
	
}

package it.ditech.ci.dbfixer.sqlparser.validator;

import static it.ditech.ci.dbfixer.config.Constants.BASE_PATH;
import static it.ditech.ci.dbfixer.config.Constants.CHANGELOG_FILENAME;
import static it.ditech.ci.dbfixer.config.Constants.CHANGELOG_PATH;
import static it.ditech.ci.dbfixer.config.Constants.CUSTOMERS;
import static it.ditech.ci.dbfixer.config.Constants.DOT;
import static it.ditech.ci.dbfixer.config.Constants.FILE_SEPARATOR;
import static it.ditech.ci.dbfixer.config.Constants.OBJECT;
import static it.ditech.ci.dbfixer.config.Constants.PATCHES;
import static it.ditech.ci.dbfixer.config.Constants.PATH_PARTS;
import static it.ditech.ci.dbfixer.config.Constants.SQL;
import static it.ditech.ci.dbfixer.config.Constants.SUPPORTED_CUSTOMERS;

import java.io.File;
import java.util.List;
import java.util.Map;

import it.ditech.ci.dbfixer.service.dto.CustomerDTO;

/**
 * Validator della sinstassi degli script .sql che manipolano gli oggetti del DB
 *   
 * @author mgambini
 *
 */
public class ExternalSQLScriptValidator implements ISQLChangelogValidator {

	@Override
	public void validate(Map<String, Object> values) throws SQLValidatorException {
		String fileSeparator = System.getProperty(FILE_SEPARATOR);
		String changelogFilename = (String) values.get(CHANGELOG_FILENAME);
		//Verifico specifica del path
		if (values.get(PATH_PARTS) == null || 
				values.get(CHANGELOG_PATH) == null || 
				values.get(BASE_PATH) == null)
			throw new SQLValidatorException("No path defined for external file", null, changelogFilename);
			
		//Verifico struttura del filename
		String[] filenameParts = (String[]) values.get(PATH_PARTS);
		if (filenameParts.length < 3 || filenameParts.length > 4) {
			throw new SQLValidatorException("External SQL file path must be '[<customerName>]"+fileSeparator+"<schemaName>"+fileSeparator+"<objectType>"+fileSeparator+"<fileName>.sql' : "+values.get(CHANGELOG_PATH), null, changelogFilename);
		}
		
		if (filenameParts.length == 4) {
			//Verifico esistenza del customer
			boolean customerFound = false;
			for (CustomerDTO customer : (List<CustomerDTO>)values.get(SUPPORTED_CUSTOMERS) ) {
				if (customer.getName().toUpperCase().equals(filenameParts[0])) {
					customerFound = true;
					break;
				}
			}
			if (!customerFound)
				throw new SQLValidatorException("Customer not supported: "+filenameParts[0], null, changelogFilename);		
		} 
			
		//Verifico esistenza dello schema DB
		boolean found = false;
		int schemaIndex = (filenameParts.length == 4) ? 1 : 0;
		DbAlias[] aliases = DbAlias.values();
		for (DbAlias schema : aliases){ 
			if (schema.name().equals(filenameParts[schemaIndex])){
				found = true;
				break;
			}
		}
		if (!found) {
			throw new SQLValidatorException("External SQL file path is incorrect: "+ filenameParts[schemaIndex]+". Should be in: "+DbAlias.concatValues(), null, changelogFilename);
		}
		
		//Verifico esistenza del tipo di oggetto
		found = false;
		int objectTypeIndex = (filenameParts.length == 4) ? 2 : 1;
		for (SQLObjectType objType : SQLObjectType.values()){
			if (objType.name().toLowerCase().equals(filenameParts[objectTypeIndex])){
				found = true;
				break;
			}
		}
		if (!found) {
			throw new SQLValidatorException("External SQL file path is incorrect: "+ filenameParts[objectTypeIndex]+". Should be in: "+SQLObjectType.concatValues(), null, changelogFilename);
		}
		
		//Verifico suffisso del SQL file
		int filenameIndex = 2;
		if (filenameParts.length == 4) {
			filenameIndex = 3;
		}
		if (!filenameParts[filenameIndex].endsWith(DOT+SQL)) {
			throw new SQLValidatorException("External SQL file path suffix is incorrect: "+ filenameParts[filenameIndex], null, changelogFilename);
		}
		
		
		
		//Verifico esistenza del SQL file
		StringBuffer baseObjectPath = new StringBuffer();
		if (filenameParts.length == 3) {
			baseObjectPath.append(((String)values.get(BASE_PATH)).substring(0, ((String)values.get(BASE_PATH)).indexOf(PATCHES))+OBJECT);
		} else {
			baseObjectPath.append(((String)values.get(BASE_PATH)).substring(0, ((String)values.get(BASE_PATH)).indexOf(PATCHES))+CUSTOMERS);
		}
		
		File filename;
		if (filenameParts.length == 3) {
			filename = new File(baseObjectPath+fileSeparator+filenameParts[0]+fileSeparator+filenameParts[1]+fileSeparator+filenameParts[2]);
		} else {
			filename = new File(baseObjectPath+fileSeparator+filenameParts[0]+fileSeparator+filenameParts[1]+fileSeparator+filenameParts[2]+fileSeparator+filenameParts[3]);
		}

		if(!filename.isFile()) { 
			throw new SQLValidatorException("External SQL file not exist: "+values.get(CHANGELOG_PATH), null, changelogFilename);
		}
		
		//Verifica sintattica contenuto del SQL file
		//String realPath = (((String)values.get(BASE_PATH)).substring(0, ((String)values.get(BASE_PATH)).indexOf(PATCHES)))+OBJECT;
		//String[] fileNamePart = (String[]) values.get(PATH_PARTS);
		//String extScriptOriginPath = realPath+fileSeparator+fileNamePart[0]+fileSeparator+fileNamePart[1]+fileSeparator+fileNamePart[2];
		
		/*try {
			List<String> filteredLines = new ArrayList<String>();
			Files.lines(new File(extScriptOriginPath).toPath())
			.map(s -> s.trim())
		    .filter(s -> !((s.length() == 0) || s.startsWith(SLASH) || s.startsWith(METADATA_QUALIFIER)))
		    .forEach(new Consumer<String>() {
		    	
				@Override
				public void accept(String line) {
					filteredLines.add(line);

				}
		    	
			});
			
			String lastLine = filteredLines.get(filteredLines.size()-1);
			if (!lastLine.endsWith(SEMICOLON)) {
				throw new SQLValidatorException("Last END keyword must be followed by \";\" : "+lastLine, null, changelogFilename);
			}
			
			
		} catch (IOException e) {
			throw new SQLValidatorException("Error validating SQL file: "+values.get(CHANGELOG_PATH), null, changelogFilename);
		}*/
		
	}

}

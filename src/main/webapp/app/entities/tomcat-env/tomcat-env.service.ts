import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { TomcatEnv } from './tomcat-env.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class TomcatEnvService {

    private resourceUrl = 'api/tomcat-envs';

    constructor(private http: Http) { }

    create(tomcatEnv: TomcatEnv): Observable<TomcatEnv> {
        const copy = this.convert(tomcatEnv);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(tomcatEnv: TomcatEnv): Observable<TomcatEnv> {
        const copy = this.convert(tomcatEnv);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<TomcatEnv> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(tomcatEnv: TomcatEnv): TomcatEnv {
        const copy: TomcatEnv = Object.assign({}, tomcatEnv);
        return copy;
    }
}

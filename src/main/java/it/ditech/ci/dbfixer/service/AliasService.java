package it.ditech.ci.dbfixer.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.ditech.ci.dbfixer.service.dto.AliasDTO;

public interface AliasService {
	
	  /**
     * Save an alias.
     *
     * @param aliasDTO the entity to save
     * @return the persisted entity
     */
    AliasDTO save(AliasDTO aliasDTO);

    /**
     *  Get all the alias.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<AliasDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" alias.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    AliasDTO findOne(Long id);

    /**
     *  Delete the "id" alias.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);


}

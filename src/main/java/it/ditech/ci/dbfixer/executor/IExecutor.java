package it.ditech.ci.dbfixer.executor;

import java.net.URL;
import java.util.List;

import it.ditech.ci.dbfixer.domain.Activity;
import it.ditech.ci.dbfixer.executor.configuration.IExecutorConfiguration;

/**
 * Contratto per un generico esecutore
 * 
 * @author mgambini
 *
 */
public interface IExecutor {

	/**
	 * Resituisce il bean di configurazione del motore di applicazione delle patches
	 * 
	 * @return il bean di configurazione
	 */
	public IExecutorConfiguration getConfiguration();

	
	/**
	 * Applica i patchset appartenenti al gruppo passato in argomento
	 * 
	 * @param patchsetGroup il gruppo di patchset da applicare
	 * @throws ExecutorException
	 */
	public void upgrade(List<PatchsetDescriptor> patchsetGroup) throws ExecutorException;
	
	
	/**
	 * Corregge i metadati relatvi all'<code>Activity</code> passata in argomento
	 * 
	 * @param activity l'<code>Activity</code> da registrare
	 * @return il numero delle <code>Activity</code> corrette: 1 in caso di successo
	 * @throws ExecutorException
	 */
	public int fixMetadata(Activity activity) throws ExecutorException;
	
	
	/**
	 * Aggiunge i metadati relativi al patchset passato in argomento
	 * @param patchsetGroup il patchset di cui aggiungere i metadati
	 * @param buildId l'ID della build corrente
	 * @return il numero delle <code>Activity</code> aggiunte: 1 in caso di successo
	 * @throws ExecutorException
	 */
	public int addMetadata(List<PatchsetDescriptor> patchsetGroup, Long buildId) throws ExecutorException;
	
	
	/**
	 * Ottiene l'elenco delle <code>Activity</code> eseguite
	 * 
	 * @throws ExecutorException
	 */
	public void getInfo() throws ExecutorException;
	
	/**
	 * Svuota il DB target
	 * 
	 * N.B.: da usare solo in ambiente di sviluppo!
	 * @throws ExecutorException
	 */
	public void clean() throws ExecutorException;

	/**
	 * Produce il log su file delle istruzioni PL/SQL corrispondenti ai patchset da applicare
	 * 
	 * @param patchsetGroup il gruppo di patchset da applicare
	 * @throws ExecutorException
	 */
	public void generateSQLChangeLog(List<PatchsetDescriptor> patchsetGroup) throws ExecutorException;
	
	/**
	 * Suddivide i patchset da applicare in gruppi in base al livello di privilegi con cui devono essere eseguiti
	 * 
	 * @return
	 * @throws ExecutorException
	 */
	public List<List<PatchsetDescriptor>> splitPatchsetByExecRole() throws ExecutorException;
	
	/**
	 * Struttura in una lista il patchset da applicare 
	 * 
	 * @param patchPrefixId identificativo del patchset 
	 * @return 
	 * @throws ExecutorException
	 */
	public List<List<PatchsetDescriptor>> splitPatchsetByPatchPrefixId(String patchPrefixId, ExecutorOperation execOp) throws ExecutorException;
	
	/**
	 * Esegue il clone su file-system locale del repo identificato dall'URL passato in argomento
	 * 
	 * @param repoUrl l'URL del repo da clonare su file-system
	 * @param versionId la versione del prodotto da convertire in branch name
	 * 
	 */
	public void cloneRepo(URL repoUrl, String versionId) throws ExecutorException;
	
	public void deleteRepo(URL repoUrl) throws ExecutorException;
	
	public void deleteSQLChangelog() throws ExecutorException;
}

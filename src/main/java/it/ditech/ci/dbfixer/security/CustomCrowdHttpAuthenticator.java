package it.ditech.ci.dbfixer.security;

import org.springframework.stereotype.Component;

import com.atlassian.crowd.integration.http.CrowdHttpAuthenticatorImpl;

@Component
public class CustomCrowdHttpAuthenticator extends CrowdHttpAuthenticatorImpl {

	public CustomCrowdHttpAuthenticator(CustomCrowdClient authenticationManager, 
			CustomClientPropertiesImpl clientProperties) {
		super(authenticationManager, clientProperties, null);
		
	}

}

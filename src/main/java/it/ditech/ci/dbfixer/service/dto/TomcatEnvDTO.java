package it.ditech.ci.dbfixer.service.dto;


import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;

import it.ditech.ci.dbfixer.domain.enumeration.StageEnvType;

/**
 * A DTO for the TomcatEnv entity.
 */
public class TomcatEnvDTO implements Serializable {

	private static final long serialVersionUID = 8352341859450575272L;

	private Long id;

    @NotNull
    private StageEnvType envType;

    @NotNull
    private String hostname;

    @NotNull
    private Integer httpPort;

    private Integer httpsPort;

    private Integer debugPort;

    private String contextRoot;

    private Long productId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StageEnvType getEnvType() {
        return envType;
    }

    public void setEnvType(StageEnvType envType) {
        this.envType = envType;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public Integer getHttpPort() {
        return httpPort;
    }

    public void setHttpPort(Integer httpPort) {
        this.httpPort = httpPort;
    }

    public Integer getHttpsPort() {
        return httpsPort;
    }

    public void setHttpsPort(Integer httpsPort) {
        this.httpsPort = httpsPort;
    }

    public Integer getDebugPort() {
        return debugPort;
    }

    public void setDebugPort(Integer debugPort) {
        this.debugPort = debugPort;
    }

    public String getContextRoot() {
        return contextRoot;
    }

    public void setContextRoot(String contextRoot) {
        this.contextRoot = contextRoot;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TomcatEnvDTO tomcatEnvDTO = (TomcatEnvDTO) o;
        if(tomcatEnvDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tomcatEnvDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TomcatEnvDTO{" +
            "id=" + getId() +
            ", envType='" + getEnvType() + "'" +
            ", hostname='" + getHostname() + "'" +
            ", httpPort='" + getHttpPort() + "'" +
            ", httpsPort='" + getHttpsPort() + "'" +
            ", debugPort='" + getDebugPort() + "'" +
            ", contextRoot='" + getContextRoot() + "'" +
            "}";
    }
}

import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { OracleDbEnvComponent } from './oracle-db-env.component';
import { OracleDbEnvDetailComponent } from './oracle-db-env-detail.component';
import { OracleDbEnvPopupComponent } from './oracle-db-env-dialog.component';
import { OracleDbEnvDeletePopupComponent } from './oracle-db-env-delete-dialog.component';

export const oracleDbEnvRoute: Routes = [
    {
        path: 'oracle-db-env',
        component: OracleDbEnvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.oracleDbEnv.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'oracle-db-env/:id',
        component: OracleDbEnvDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.oracleDbEnv.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const oracleDbEnvPopupRoute: Routes = [
    {
        path: 'oracle-db-env-new',
        component: OracleDbEnvPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.oracleDbEnv.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'oracle-db-env/:id/edit',
        component: OracleDbEnvPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.oracleDbEnv.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'oracle-db-env/:id/delete',
        component: OracleDbEnvDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.oracleDbEnv.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];

DROP TABLE 'ACTIVITY';

CREATE TABLE `activity` (
  `id` int(11) NOT NULL,
  `buildId` int(11) NOT NULL,
  `targetEnvType` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `version` varchar(50) DEFAULT NULL,
  `actionType` int(11) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `scriptName` varchar(100) DEFAULT NULL,
  `author` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `username` varchar(25) NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `applyDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `execTime` time DEFAULT NULL,
  `log` varchar(3000) DEFAULT NULL,
  `success` tinyint(1) DEFAULT NULL
) DEFAULT CHARSET=utf8;


--
-- Indici per le tabelle `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per la tabella `activity`
--
ALTER TABLE `activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
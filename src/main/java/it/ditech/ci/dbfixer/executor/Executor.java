package it.ditech.ci.dbfixer.executor;

import static it.ditech.ci.dbfixer.config.Constants.BRANCH_PREFIX_NAME;
import static it.ditech.ci.dbfixer.config.Constants.DOT;
import static it.ditech.ci.dbfixer.config.Constants.FILE_SEPARATOR;
import static it.ditech.ci.dbfixer.config.Constants.GIT;
import static it.ditech.ci.dbfixer.config.Constants.GIT_PWD;
import static it.ditech.ci.dbfixer.config.Constants.GIT_USN;
import static it.ditech.ci.dbfixer.config.Constants.LATEST;
import static it.ditech.ci.dbfixer.config.Constants.MAIN;
import static it.ditech.ci.dbfixer.config.Constants.MASTER;
import static it.ditech.ci.dbfixer.config.Constants.RESOURCES;
import static it.ditech.ci.dbfixer.config.Constants.SLASH;
import static it.ditech.ci.dbfixer.config.Constants.SOURCE;
import static it.ditech.ci.dbfixer.config.Constants.SQLOUTPUT_FILENAME;
import static it.ditech.ci.dbfixer.config.Constants.TMP_DIRNAME;
import static it.ditech.ci.dbfixer.config.Constants.XML_FILE_PATH;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.JGitInternalException;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import it.ditech.ci.dbfixer.executor.configuration.IExecutorConfiguration;

/**
 * Ancestor della gerarchia di motori di applicazione delle patches
 * Centralizza le operazioni di integrazione con il repo Git
 * 
 * @author mgambini
 */
public abstract class Executor implements IExecutor {
	
	@Override
	public abstract IExecutorConfiguration getConfiguration();
	

	@Override
	public abstract void getInfo() throws ExecutorException;

	@Override
	public abstract void clean() throws ExecutorException;

	
	@Override
	public abstract void generateSQLChangeLog(List<PatchsetDescriptor> patchsetGroup) throws ExecutorException;
	
	/**
	 * Esegue la cancellazione dal file-system locale del repo identificato dall'URL passato in argomento
	 * 
	 * @param repoUrl l'URL del repo da clonare su file-system
	 */
	@Override
	public void deleteRepo(URL repoUrl) throws ExecutorException {
		String fileSeparator = System.getProperty(FILE_SEPARATOR);
		String repoURI = repoUrl.toString();
		String repoName = repoURI.substring(repoURI.lastIndexOf(fileSeparator)+1, repoURI.lastIndexOf(DOT));
		try {			
			FileUtils.deleteDirectory(new File(fileSeparator+TMP_DIRNAME+fileSeparator+repoName));
		} catch (IOException e) {
			throw new ExecutorException(ExecutorOperation.DELETE_REPO, "Eccezione in cancellazione repo: "+repoURI, e.getCause());
		}
	}
	
	@Override
	public void deleteSQLChangelog() throws ExecutorException {
		String fileSeparator = System.getProperty(FILE_SEPARATOR); 
		String logFilename = fileSeparator+TMP_DIRNAME+fileSeparator+SQLOUTPUT_FILENAME;
		try {
			File changelog = new File(logFilename);
			if (changelog.exists()){
				FileUtils.forceDelete(changelog);
			}
		} catch (IOException e) {
			throw new ExecutorException(ExecutorOperation.DELETE_REPO, "Eccezione in cancellazione file: "+logFilename, e.getCause());
		}
	}
	
	public void deleteTempDir() throws ExecutorException {
		String fileSeparator = System.getProperty(FILE_SEPARATOR);
		String pathToDelete = getConfiguration().getBasePath()+fileSeparator+SOURCE+fileSeparator+MAIN+fileSeparator+RESOURCES+fileSeparator+XML_FILE_PATH+fileSeparator;
		try {
			File dirToDelete = new File(pathToDelete);
			FileUtils.deleteDirectory(dirToDelete);
		} catch (IOException e) {
			throw new ExecutorException(ExecutorOperation.DELETE_REPO, "Eccezione in cancellazione repo: "+pathToDelete, e.getCause());
		}
	}

	/**
	 * Esegue il clone su file-system locale del repo identificato dall'URL passato in argomento
	 * 
	 * @param repoUrl l'URL del repo da clonare su file-system
	 * @param versionId la versione del prodotto da convertire in branch name
	 * 
	 */
	@Override
	public void cloneRepo(URL repoUrl, String versionId) throws ExecutorException {
		String repoURI = repoUrl.toString();
		String repoName = repoURI.substring(repoURI.lastIndexOf(SLASH)+1, repoURI.lastIndexOf('.'));
		CloneCommand cloneCommand = Git.cloneRepository();
		cloneCommand.setURI(repoURI);
		cloneCommand.setBranch(getBranchName(versionId));
	    cloneCommand.setCredentialsProvider( new UsernamePasswordCredentialsProvider( System.getProperty(GIT_USN), System.getProperty(GIT_PWD) ) );
	    cloneCommand.setDirectory(new File(SLASH+TMP_DIRNAME+SLASH+repoName));
	    try {
	    	cloneCommand.call();
	    } catch (GitAPIException | JGitInternalException e) {
			throw new ExecutorException(ExecutorOperation.CLONE_REPO, "Errore in cloning Git repo: "+repoURI, e);
		}
	    validateCloning(repoName, versionId);
	}	
	
	/**
	 * Verifica se il repo è vuoto ed in tal caso solleva eccezione
	 * @param repoName nome del repo
	 * @param versionId versione del branch
	 * @throws ExecutorException in caso di repo vuoto
	 */
	private void validateCloning(String repoName, String versionId) throws ExecutorException {
		File repoDir = new File(SLASH+TMP_DIRNAME+SLASH+repoName);
		if (repoDir.isDirectory() && repoDir.listFiles().length == 1 && 
				repoDir.listFiles()[0].isHidden() && 
				repoDir.listFiles()[0].getName().equalsIgnoreCase(DOT+GIT)){
			throw new ExecutorException(ExecutorOperation.CLONE_REPO, "Errore in cloning Git repo. Check branch name:  "+versionId);
		}
	}
	
	/**
	 * Ricava il branch name dalla versione di prodotto 
	 * @param versionId
	 * @return
	 */
	private String getBranchName(String versionId) {
		if (versionId.equalsIgnoreCase(LATEST)) {
			return MASTER;
		} else {
			return BRANCH_PREFIX_NAME+versionId;
		}
		
	}
}

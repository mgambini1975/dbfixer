package it.ditech.ci.dbfixer.config;

import static it.ditech.ci.dbfixer.config.Constants.PROD_ENV;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.data.repository.query.SecurityEvaluationContextExtension;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.filter.CorsFilter;

import io.github.jhipster.security.Http401UnauthorizedEntryPoint;
import it.ditech.ci.dbfixer.security.AuthoritiesConstants;
import it.ditech.ci.dbfixer.security.CustomCrowdClient;
import it.ditech.ci.dbfixer.security.CustomCrowdUserDetailsServiceImpl;
import it.ditech.ci.dbfixer.security.CustomRemoteCrowdAuthenticationProvider;
import it.ditech.ci.dbfixer.security.jwt.JWTConfigurer;
import it.ditech.ci.dbfixer.security.jwt.TokenProvider;

/**
 * Configura SpringSecurity tramite integrazione con Crowd e con JWT token generator
 * 
 * @author mgambini
 *
 */
@Configuration
@Profile(PROD_ENV)
@Conditional(ProductionButNotCloudCondition.class)
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class CrowdSecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final CustomRemoteCrowdAuthenticationProvider crowdAuthenticationProvider;
    private final CustomCrowdUserDetailsServiceImpl userDetailsService;  
    private final CustomCrowdClient crowdClient;
 
    private final TokenProvider tokenProvider;

    private final CorsFilter corsFilter;
    
    public CrowdSecurityConfiguration(AuthenticationManagerBuilder authenticationManagerBuilder,
    		TokenProvider tokenProvider,
    	    CorsFilter corsFilter, 
    	    CustomRemoteCrowdAuthenticationProvider crowdAuthenticationProvider,
    	    CustomCrowdUserDetailsServiceImpl userDetailsService,
    	    CustomCrowdClient crowdClient) {

        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.tokenProvider = tokenProvider;
        this.corsFilter = corsFilter;
        this.crowdAuthenticationProvider = crowdAuthenticationProvider;
        this.userDetailsService = userDetailsService;
        this.crowdClient = crowdClient;
    }

    @PostConstruct
    public void init() {
        try {
        	authenticationManagerBuilder
        		.authenticationProvider(this.crowdAuthenticationProvider);
        	userDetailsService.setCrowdClient(crowdClient);
        } catch (Exception e) {
            throw new BeanInitializationException("Security configuration failed", e);
        }
    }

    @Bean
    public Http401UnauthorizedEntryPoint http401UnauthorizedEntryPoint() {
        return new Http401UnauthorizedEntryPoint();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
            .antMatchers(HttpMethod.OPTIONS, "/**")
            .antMatchers("/app/**/*.{js,html}")
            .antMatchers("/i18n/**")
            .antMatchers("/content/**")
            .antMatchers("/swagger-ui/index.html")
            .antMatchers("/test/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .addFilterBefore(corsFilter, UsernamePasswordAuthenticationFilter.class)
            .exceptionHandling()
            .authenticationEntryPoint(http401UnauthorizedEntryPoint())
        .and()
            .csrf()
            .disable()
            .headers()
            .frameOptions()
            .disable()
        .and()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
            .authorizeRequests()
            .antMatchers("/api/register").permitAll()
            .antMatchers("/api/activate").permitAll()
            .antMatchers("/api/authenticate").permitAll()
            .antMatchers("/api/account/reset_password/init").permitAll()
            .antMatchers("/api/account/reset_password/finish").permitAll()
            .antMatchers("/api/profile-info").permitAll()
            .antMatchers("/api/**").authenticated()
            .antMatchers("/migrations-api/**").authenticated()
            .antMatchers("/management/health").permitAll()
            .antMatchers("/management/**").hasAuthority(AuthoritiesConstants.ADMIN)
            .antMatchers("/v2/api-docs/**").permitAll()
            .antMatchers("/swagger-resources/configuration/ui").permitAll()
            .antMatchers("/swagger-ui/index.html").hasAuthority(AuthoritiesConstants.ADMIN)
        .and()
            .apply(securityConfigurerAdapter());

    }

    private JWTConfigurer securityConfigurerAdapter() {
        return new JWTConfigurer(tokenProvider);
    }

    @Bean
    public SecurityEvaluationContextExtension securityEvaluationContextExtension() {
        return new SecurityEvaluationContextExtension();
    }
}

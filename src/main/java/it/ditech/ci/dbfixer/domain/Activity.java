package it.ditech.ci.dbfixer.domain;

import java.sql.Time;
import java.sql.Timestamp;

import it.ditech.ci.dbfixer.domain.enumeration.ActivityType;
import it.ditech.ci.dbfixer.domain.enumeration.StageEnvType;
import static it.ditech.ci.dbfixer.config.Constants.SUCCESS;
import static it.ditech.ci.dbfixer.config.Constants.FAILURE;

/**
 * Modella l'esito di una attività utente
 * @author agranifero
 *Activity
 */
public class Activity {
	
	private Long id;
	private Long buildId;
	private StageEnvType targetEnvType;
	private Long customerId;
	private Long productId;
	private Long installedRank;
	private String version;
	private ActivityType actType;
	private String description;
	private String scriptName;
	private String author;
	private String username;
	private Long checksum;
	private Timestamp applyDate;
	private Time execTime;
	private String log;
	private boolean success;
	private String l_filename;
	private String l_md5sum;
	private String outcome;
	
	
	public String getOutcome() {
		if (this.success){
			return SUCCESS;
		} 
		return FAILURE;
	}
	public void setOutcome(String outcome) {
		this.outcome = outcome;
	}
	public String getL_filename() {
		return l_filename;
	}
	public void setL_filename(String l_filename) {
		this.l_filename = l_filename;
	}
	public String getL_md5sum() {
		return l_md5sum;
	}
	public void setL_md5sum(String l_md5sum) {
		this.l_md5sum = l_md5sum;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	
	public Long getBuildId() {
		return buildId;
	}
	public void setBuildId(Long buildId) {
		this.buildId = buildId;
	}
		
	public boolean isSuccess() {
		return success;
	}
	
	public void setSuccess(boolean success) {
		this.success = success;
		if (success){
			this.outcome = SUCCESS;
		} else
		this.outcome = FAILURE;
	}
	
	public Long getInstalledRank() {
		return installedRank;
	}
	public void setInstalledRank(Long installedRank) {
		this.installedRank = installedRank;
	}	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getScriptName() {
		return scriptName;
	}
	public void setScriptName(String scriptName) {
		this.scriptName = scriptName;
	}	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Long getChecksum() {
		return checksum;
	}
	public void setChecksum(Long checksum) {
		this.checksum = checksum;
	}
	public Timestamp getApplyDate() {
		return applyDate;
	}
	public void setApplyDate(Timestamp date) {
		this.applyDate = date;
	}
	public String getLog() {
		return log;
	}
	public void setLog(String log) {
		this.log = log;
	}
	
	public StageEnvType getTargetEnvType() {
		return targetEnvType;
	}
	public void setTargetEnvType(StageEnvType targetEnvType) {
		this.targetEnvType = targetEnvType;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public ActivityType getActType() {
		return actType;
	}
	public void setActType(ActivityType actType) {
		this.actType = actType;
	}
	public Time getExecTime() {
		return execTime;
	}
	public void setExecTime(Time execTime) {
		this.execTime = execTime;
	}
}

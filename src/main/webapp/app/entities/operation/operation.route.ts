import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { OperationComponent } from './operation.component';
import { OperationDetailComponent } from './operation-detail.component';

export const operationRoute: Routes = [
    {
        path: 'operation',
        component: OperationComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.operation.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'operation/:id',
        component: OperationDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.operation.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

package it.ditech.ci.dbfixer.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import it.ditech.ci.dbfixer.domain.OracleDbEnv;
import it.ditech.ci.dbfixer.repository.OracleDbEnvRepository;
import it.ditech.ci.dbfixer.web.rest.util.HeaderUtil;
import it.ditech.ci.dbfixer.web.rest.util.PaginationUtil;

/**
 * REST controller for managing OracleDbEnv.
 */
@RestController
@RequestMapping("/api")
public class OracleDbEnvResource {

    private final Logger log = LoggerFactory.getLogger(OracleDbEnvResource.class);

    private static final String ENTITY_NAME = "oracleDbEnv";

    private final OracleDbEnvRepository oracleDbEnvRepository;
    public OracleDbEnvResource(OracleDbEnvRepository oracleDbEnvRepository) {
        this.oracleDbEnvRepository = oracleDbEnvRepository;
    }

    /**
     * POST  /oracle-db-envs : Create a new oracleDbEnv.
     *
     * @param oracleDbEnv the oracleDbEnv to create
     * @return the ResponseEntity with status 201 (Created) and with body the new oracleDbEnv, or with status 400 (Bad Request) if the oracleDbEnv has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/oracle-db-envs")
    @Timed
    public ResponseEntity<OracleDbEnv> createOracleDbEnv(@Valid @RequestBody OracleDbEnv oracleDbEnv) throws URISyntaxException {
        log.debug("REST request to save OracleDbEnv : {}", oracleDbEnv);
        if (oracleDbEnv.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new oracleDbEnv cannot already have an ID")).body(null);
        }
        OracleDbEnv result = oracleDbEnvRepository.save(oracleDbEnv);
        return ResponseEntity.created(new URI("/api/oracle-db-envs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /oracle-db-envs : Updates an existing oracleDbEnv.
     *
     * @param oracleDbEnv the oracleDbEnv to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated oracleDbEnv,
     * or with status 400 (Bad Request) if the oracleDbEnv is not valid,
     * or with status 500 (Internal Server Error) if the oracleDbEnv couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/oracle-db-envs")
    @Timed
    public ResponseEntity<OracleDbEnv> updateOracleDbEnv(@Valid @RequestBody OracleDbEnv oracleDbEnv) throws URISyntaxException {
        log.debug("REST request to update OracleDbEnv : {}", oracleDbEnv);
        if (oracleDbEnv.getId() == null) {
            return createOracleDbEnv(oracleDbEnv);
        }
        OracleDbEnv result = oracleDbEnvRepository.save(oracleDbEnv);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, oracleDbEnv.getId().toString()))
            .body(result);
    }

    /**
     * GET  /oracle-db-envs : get all the oracleDbEnvs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of oracleDbEnvs in body
     */
    @GetMapping("/oracle-db-envs")
    @Timed
    public ResponseEntity<List<OracleDbEnv>> getAllOracleDbEnvs(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of OracleDbEnvs");
        Page<OracleDbEnv> page = oracleDbEnvRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/oracle-db-envs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /oracle-db-envs/:id : get the "id" oracleDbEnv.
     *
     * @param id the id of the oracleDbEnv to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the oracleDbEnv, or with status 404 (Not Found)
     */
    @GetMapping("/oracle-db-envs/{id}")
    @Timed
    public ResponseEntity<OracleDbEnv> getOracleDbEnv(@PathVariable Long id) {
        log.debug("REST request to get OracleDbEnv : {}", id);
        OracleDbEnv oracleDbEnv = oracleDbEnvRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(oracleDbEnv));
    }

    /**
     * DELETE  /oracle-db-envs/:id : delete the "id" oracleDbEnv.
     *
     * @param id the id of the oracleDbEnv to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/oracle-db-envs/{id}")
    @Timed
    public ResponseEntity<Void> deleteOracleDbEnv(@PathVariable Long id) {
        log.debug("REST request to delete OracleDbEnv : {}", id);
        oracleDbEnvRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}

package it.ditech.ci.dbfixer.service.validator;

public class MigrationValidatorException extends Exception {

	private static final long serialVersionUID = -9217969099445827215L;

	public MigrationValidatorException(String msg, Throwable cause){
		super(msg, cause);
	}
	
	public MigrationValidatorException(String msg){
		this(msg, null);
	}

}

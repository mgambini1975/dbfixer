import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { OracleDbEnv } from './oracle-db-env.model';
import { OracleDbEnvPopupService } from './oracle-db-env-popup.service';
import { OracleDbEnvService } from './oracle-db-env.service';
import { Product, ProductService } from '../product';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-oracle-db-env-dialog',
    templateUrl: './oracle-db-env-dialog.component.html'
})
export class OracleDbEnvDialogComponent implements OnInit {

    oracleDbEnv: OracleDbEnv;
    isSaving: boolean;

    products: Product[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private oracleDbEnvService: OracleDbEnvService,
        private productService: ProductService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.productService.query()
            .subscribe((res: ResponseWrapper) => { this.products = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.oracleDbEnv.id !== undefined) {
            this.subscribeToSaveResponse(
                this.oracleDbEnvService.update(this.oracleDbEnv));
        } else {
            this.subscribeToSaveResponse(
                this.oracleDbEnvService.create(this.oracleDbEnv));
        }
    }

    private subscribeToSaveResponse(result: Observable<OracleDbEnv>) {
        result.subscribe((res: OracleDbEnv) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: OracleDbEnv) {
        this.eventManager.broadcast({ name: 'oracleDbEnvListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackProductById(index: number, item: Product) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-oracle-db-env-popup',
    template: ''
})
export class OracleDbEnvPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private oracleDbEnvPopupService: OracleDbEnvPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.oracleDbEnvPopupService
                    .open(OracleDbEnvDialogComponent as Component, params['id']);
            } else {
                this.oracleDbEnvPopupService
                    .open(OracleDbEnvDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

package it.ditech.ci.dbfixer.config;

/**
 * Application constants.
 */
public final class Constants {
	
	public static final String EMPTY_STRING = ""; 
	public static final String PROD_ENV = "prod";
	public static final String DEV_ENV = "dev";
	public static final String CLOUD = "cloud";
	public static final String GIT = "git";
	public static final String BASE_PATH = "basePath";
	public static final String PATCHSET_PATH = "patchsetPath";
	public static final String CHANGELOG_FILENAME = "changelogFilename";
	public static final String SUPPORTED_CUSTOMERS = "supportedCustomers";
	public static final String CHANGELOG_PATH ="path";
	public static final String METADATA_SEPARATOR = "_";
	public static final String XML_FILE_PATH = "temp";
	public static final String METADATA_QUALIFIER = "--";
	public static final String CHANGESET_PREFIX = METADATA_QUALIFIER+"CHANGESET";
	public static final String SYSDBA_PREFIX = METADATA_QUALIFIER+"SYSDBA";
	public static final String METADATA_EXT_QUALIFIER = "@";
	
	public static final String SLASH = "/";
	public static final String BACKSLASH = "\\";
	public static final String NEW_LINE = "\\n";
	public static final String CHUNK_SEPARATOR = ":";
	public static final String CURRENT_PATH = ".";
	public static final String SEMICOLON = ";";
	public static final String COMMA = ",";
	public static final String DOT = CURRENT_PATH;
	public static final String SQLSCRIPT_TERMINATOR = "£";
	public static final String UTF_8 = "UTF-8";
	public final static String TIPO_ENV = "tipoEnv";
	public final static String RELEASE = "release";
	public final static String SQLOUTPUT_FILENAME = "fullChangelog.sql";
	public final static String TMP_DIRNAME = "tmp";
	public final static String END_KEYWORD = "end";
	public final static String UNKNOWN = "unknown";
	public final static String EXCEPTION_CAUGHT = "Exception caught";
	public final static String GIT_USN = "gitUsername";
	public final static String GIT_PWD = "gitPassword";
	
	public final static String ORACLE_DRIVER_CLASSNAME = "oracle.jdbc.driver.OracleDriver";
	public static final String JDBC_URL_PREFIX = "jdbc:oracle:thin";
	public static final String SPRING_DS_PREFIX = "spring.datasource";
	public static final String DBFIXER_DS_NAME = "DbFixerDS";
	public static final String APPLICATION = "application";
    //Regex for acceptable logins
    public static final String LOGIN_REGEX = "^[_'.@A-Za-z0-9-]*$";

    public static final String SYSTEM_ACCOUNT = "system";
    public static final String ANONYMOUS_USER = "anonymoususer";
    public static final String ROLE_USER = "ROLE_USER";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String CROWD = "CROWD";
    public static final String CROWD_DBFIXER_ADMIN_GROUP = "dbfixer-administrators";
    public static final String SYSTEM = "sys";
    public static final String UTENTE_DBA = "UTENTE_DBA";
    public static final String ADMIN = "Admin";
    public static final String MORECEDI = "MORE-CEDI";
    public static final String BRC_CORE_USER = "BRC_CORE";
    public static final String BRC_CORE_PWD = "BRC_CORE";
    
    public static final String FLYWAY = "flyway";
    public static final String LIQUIBASE = "liquibase";
    public static final String LIQUIBASE_METADATA_HEADER = METADATA_QUALIFIER+LIQUIBASE+" formatted sql";
    public static final String SENTRY_DSN = "sentry.dsn";
    public static final String CHANGELOG = "changelog";
    
    public static final String SUCCESS = "SUCCESS";
    public static final String FAILURE = "FAILURE";
    
    public static final String CHANGELOG_MASTER_FILENAME = "changelog.master.xml";
    public static final String SOURCE = "src";
    public static final String MAIN = "main";
    public static final String RESOURCES = "resources";
    public static final String PATCHES = "patches";
    public static final String OBJECT = "object";   
    public static final String SQL = "sql";
    public static final String XML = "xml";
    public static final String TEMP = "temp";
    public static final String TEMP_SUFFIX = METADATA_SEPARATOR+TEMP;
    public static final String FILE_SEPARATOR = "file.separator";
    public static final String PARENT_PATH = "..";
    public static final String PATH_PARTS = "pathParts";
    public static final String LATEST = "latest";
    public static final String MASTER = "master";
    public static final String BRANCH_PREFIX_NAME = "Fix_";
    
  //endpoint params
  	public static final String CUSTOMER_ID = "customerId";
  	public static final String PRODUCT_ID = "productId";
  	public static final String BUILD_ID = "buildId";
  	public static final String STAGE_ID = "stageId";
  	public static final String ENV_ID = "envId";
  	public static final String VERSION_ID = "versionId";
  	public static final String PATCH_ID = "patchId";
  	public static final String PATCH_NAME_PREFIX = "patchNamePrefix";
  	public static final String METADATA_ONLY = "metadataOnly";
  	
  	//endpoint resources
  	public static final String PRODUCT = "product";
  	public static final String PRODUCTS = PRODUCT+"s";
  	public static final String CUSTOMER = "customer";
  	public static final String CUSTOMERS = CUSTOMER+"s";
  	public static final String DBENV = "dbenv";
  	public static final String MIGRATION = "migration";
  	public static final String MIGRATIONS = MIGRATION+"s";
  	public static final String STAGE = "stage";
  	public static final String VERSION = "version";
  	public static final String VERSIONS = "versions";
  	public static final String SQLOUTPUT = "sqloutput";
  	public static final String TAG = "tag";
  	public static final String SCHEMA = "schema";
  	public static final String ACTIVITY = "activity";
  	public static final String ACTIVITIES = "activities";
  	public static final String METADATA = "metadata";
  	public static final String ALIAS = "alias";
  	public static final String ENV = "env";
  	public static final String ENV_PREFIX = METADATA_QUALIFIER+ENV;
	public static final String CUSTOMER_PREFIX = METADATA_QUALIFIER+CUSTOMER;
  	
  	public static final String SQLPLUS_DEFINE = "DEFINE";
  	public static final String SQLPLUS_CONNECT = "CONNECT";
  	
  	public static final String IT_LANGUAGE = "it";
  	public static final String EN_LANGUAGE = "en";
  	
  	
  	//Messages 	
  	public static final String MANUAL_APPLIED_PATCHSET = "Patchset applicato manualmente";
  	public static final String NO_ACTIVITY_FOUND_FOR_PATCHSET = "No Activity found for patchset";
  	public static final String DB_CLEAN_SUCCESSFUL = "DB clean eseguita con successo";
  	public static final String DB_TAG_SUCCESSFUL = "DB tag eseguito con successo";
  	
  	//Exception
  	public static final String METADATA_FIXING_ERROR_MSG = "Impossibile eseguire il fixing dei metadati";
  	public static final String PATCHSET_NOT_FOUND_ERROR_MSG = "Patchset non trovato nel changelog";
  	public static final String EXC_CAUSE_MSG = "Exception cause";
  	public static final String EXC_SENDING_MAIL_MSG = "Exception in sending mail";
  	public static final String EXC_GETTING_ACTIVITY_PERSIST = "Exception in getting activity persist";
  	public static final String EXC_GETTING_DBENV = "Exception in getting DbEnv";
  	public static final String EXC_GETTING_DBALIASES = "Exception in getting DbAliases";
  	public static final String EXC_GETTING_PRODUCT = "Exception in getting Product";
 	public static final String EXC_GETTING_CUSTOMER = "Exception in getting Customer";
 	
 	
    private Constants() {
    }
}

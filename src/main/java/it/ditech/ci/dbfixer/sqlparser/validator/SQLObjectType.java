package it.ditech.ci.dbfixer.sqlparser.validator;

/**
 * Tipi di oggetti SQL
 * 
 * @author mgambini
 *
 */
public enum SQLObjectType {
	
	FNC, PKG, PRC, TBL, TRG, TYP, VIEW;
	
	public static String concatValues() {
		StringBuffer toRet = new StringBuffer();
		for (SQLObjectType objType : SQLObjectType.values()) {
			toRet = toRet.append(objType).append(",");
		}
		toRet.deleteCharAt(toRet.length()-1);
		return toRet.toString();
	}

}

package it.ditech.ci.dbfixer.service.dto;

import java.io.Serializable;
import java.util.Date;

public class MessageDTO implements Serializable {
	
	private static final long serialVersionUID = 8933070374762339091L;
	
	private String[] receivers;
	private String subject;
	private String text;
	private Date sendDate;
	
	public String[] getReceivers() {
		return receivers;
	}
	public void setReceivers(String[] receivers) {
		this.receivers = receivers;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getSendDate() {
		return sendDate;
	}
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

}

package it.ditech.ci.dbfixer.executor;

import static it.ditech.ci.dbfixer.config.Constants.DOT;
import static it.ditech.ci.dbfixer.config.Constants.FILE_SEPARATOR;
import static it.ditech.ci.dbfixer.config.Constants.MAIN;
import static it.ditech.ci.dbfixer.config.Constants.PATCHES;
import static it.ditech.ci.dbfixer.config.Constants.RESOURCES;
import static it.ditech.ci.dbfixer.config.Constants.SOURCE;
import static it.ditech.ci.dbfixer.config.Constants.SQL;
import static it.ditech.ci.dbfixer.config.Constants.SQLOUTPUT_FILENAME;
import static it.ditech.ci.dbfixer.config.Constants.TMP_DIRNAME;
import static it.ditech.ci.dbfixer.config.Constants.UNKNOWN;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import it.ditech.ci.dbfixer.cache.MigrationsCache;
import it.ditech.ci.dbfixer.dao.impl.MigrationDAOException;
import it.ditech.ci.dbfixer.domain.Activity;
import it.ditech.ci.dbfixer.domain.Migration;
import it.ditech.ci.dbfixer.executor.configuration.IExecutorConfiguration;
import it.ditech.ci.dbfixer.executor.configuration.LiquibaseExecutorConfiguration;
import it.ditech.ci.dbfixer.sqlparser.ChangelogParser;
import it.ditech.ci.dbfixer.sqlparser.validator.SQLValidatorException;
import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.configuration.GlobalConfiguration;
import liquibase.configuration.LiquibaseConfiguration;

/**
 * Wrapper di un esecutore Liquibase
 * 
 * E' caratterizzato da:
 * 
 *  - facade per l'interazione con Liquibase
 *  - configuration bean
 *  
 * @author mgambini
 *
 */
public class LiquibaseExecutor extends Executor implements ILiquibaseExecutor {
	
	private Liquibase liquibaseExec;
	private LiquibaseExecutorConfiguration liquibaseExecConfiguration;
	
	public LiquibaseExecutor(Liquibase liquibase, LiquibaseExecutorConfiguration liquibaseExecConf) {
		this.liquibaseExec = liquibase;
		this.liquibaseExecConfiguration = liquibaseExecConf;
	}

	
	@Override
	@Transactional
	public int fixMetadata(Activity activity) throws ExecutorException {
		try {
				return getConfiguration().getMetadataDAO().fixMetadata(getConfiguration().getDatasource(), activity);
		} catch (Exception ex){
			throw new ExecutorException(ExecutorOperation.REPAIR, ex.getMessage(), ex.getCause());
		}
	}
	
	@Override
	public int addMetadata(List<PatchsetDescriptor> patchsetGroup, Long buildId) throws ExecutorException {
		int toRet = 0;
		try {
			List<Changeset> changesets = convertSqlToXml(patchsetGroup);
			toRet = doAddMetadata(changesets, buildId);
			deleteTempDir();

		} catch (FileNotFoundException | UnsupportedEncodingException | SQLValidatorException| MigrationDAOException ex) {
			throw new ExecutorException(ExecutorOperation.ADDMETADATA, ex.getMessage(), ex);
		}
		return toRet;
	}

	private int doAddMetadata(List<Changeset> changeSet, Long buildId) throws ExecutorException {
		int toRet = 0;
		try {
			for (Changeset change : changeSet) {			
				String schemaName = this.getConfiguration().getDatasource().getConnection().getSchema();	
				Migration aMigration = new Migration();				
				//version
				aMigration.setVersion(change.getVersion());			
				//scriptName
				aMigration.setScriptName(change.getScriptname());
				//Author
				aMigration.setAuthor(change.getAuthor());
				//Success
				aMigration.setSuccess(Boolean.TRUE);
				//Description
				aMigration.setDescription(change.getDescription());
				//Schema
				aMigration.setInstalledBy(schemaName);
				//Log (success)
				aMigration.setLog("Patchset applicato manualmente.");
				//Liquibase Filename
				aMigration.setFileName(change.getFilepath());
				//Liquibase Md5sum
				aMigration.setMd5sum(UNKNOWN);									
				MigrationsCache.getInstance().setMigrationByBuildId(buildId, aMigration);
				toRet++;
			}
		} catch (SQLException ex) {
			throw new ExecutorException(ExecutorOperation.ADDMETADATA, ex.getMessage(), ex);
		}
		return toRet;
	}
	
	
	@Override
	public void upgrade(List<PatchsetDescriptor> patchsetGroup) throws ExecutorException {
		try {
			convertSqlToXml(patchsetGroup);
			doUpgrade();
			deleteTempDir();

		} catch (FileNotFoundException | UnsupportedEncodingException | SQLValidatorException | MigrationDAOException ex) {
			throw new ExecutorException(ExecutorOperation.MIGRATE, ex.getMessage(), ex);
		}
	}
	
	
	private void doUpgrade() throws ExecutorException {
		try {
			//evaluate the current Context
			Contexts contexts = new Contexts();
			String currentEnvType = getConfiguration().getStageEnvType().name();
			String currentProduct = getConfiguration().getCustomerDTO().getName();
			contexts.add(currentEnvType);
			contexts.add(currentProduct);
			liquibaseExec.update(contexts);	
		} catch (Exception ex){
			throw new ExecutorException(ExecutorOperation.MIGRATE, ex.getMessage(), ex);
		}
	}
	
	private void doGenerateSQLChangeLog() throws ExecutorException {
		try {
			//evaluate the current Context
			Contexts contexts = new Contexts();
			String currentEnvType = getConfiguration().getStageEnvType().name();
			String currentProduct = getConfiguration().getProductDTO().getName();
			contexts.add(currentEnvType);
			contexts.add(currentProduct);
			liquibaseExec.update(contexts, new LabelExpression(), getOutputWriter());			
		} catch (Exception ex){
			throw new ExecutorException(ExecutorOperation.GENERATESQL, ex.getMessage(), ex);
		}
	}
	
	/**
	 * Genera il SQL log corrispondente all'elenco di patchset passati in argomento
	 * 
	 * @param l'elenco del descrittori di patchset 
	 */
	@Override
	public void generateSQLChangeLog(List<PatchsetDescriptor> patchsetGroup) throws ExecutorException {
		try {
			convertSqlToXml(patchsetGroup);
			doGenerateSQLChangeLog();
			deleteTempDir();
		} catch (Exception ex){
			throw new ExecutorException(ExecutorOperation.GENERATESQL, ex.getMessage(), ex);
		} 
	}
	
	/**
	 * Restituisce un <code>java.io.Writer</code> per la scrittura del file SQL log 
	 * 
	 * @return 
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	private Writer getOutputWriter() throws UnsupportedEncodingException, IOException {
		String fileSeparator = System.getProperty(FILE_SEPARATOR);
		String charsetName = LiquibaseConfiguration.getInstance().getConfiguration(GlobalConfiguration.class).getOutputEncoding();
		String sqlOutputDir = fileSeparator+TMP_DIRNAME;
		String outputFile = sqlOutputDir+fileSeparator+SQLOUTPUT_FILENAME;
        FileOutputStream fileOut = null;
        try {
                fileOut = new FileOutputStream(outputFile, Boolean.TRUE);
                return new OutputStreamWriter(fileOut, charsetName);
        } catch (IOException e) {
                System.err.printf("Could not create output file %s\n", outputFile);
                throw e;
        }
    }

	@Override
	public void getInfo() throws ExecutorException {
		// TODO Auto-generated method stub

	}

	@Override
	public void clean() throws ExecutorException {
		try {
			//liquibaseExec.dropAll();
		} catch (Exception ex){
			throw new ExecutorException(ExecutorOperation.CLEAN, ex.getMessage(), ex.getCause());
		}
	}

	/**
	 * Ricerca su filesystem il patchset identificato dall'ID passato in argomento
	 * 
	 * @param patchPrefixId identificativo del patchset 
	 * @param execOp tipo di contesto operativo entro cui si svolge la ricerca
	 * @return una lista di PatchsetDescriptor raggruppati per subset di esecuzione
	 * @throws ExecutorException
	 */
	public List<List<PatchsetDescriptor>> splitPatchsetByPatchPrefixId(String patchPrefixId, ExecutorOperation execOp) throws ExecutorException {
		List<List<PatchsetDescriptor>> toRet = new ArrayList<List<PatchsetDescriptor>>();
		String fileSeparator = System.getProperty(FILE_SEPARATOR);
		File dirToScan = new File(getConfiguration().getBasePath()+fileSeparator+SOURCE+fileSeparator+MAIN+fileSeparator+RESOURCES+fileSeparator+PATCHES+fileSeparator);
        File fileToScan = findFileToParse(dirToScan, patchPrefixId);
        if (fileToScan == null)
        	throw new ExecutorException(execOp,"Patchset not found: "+patchPrefixId+DOT+SQL);
        
        
        List<PatchsetDescriptor> filesDescList = new ArrayList<PatchsetDescriptor>();
        PatchsetDescriptor pd;
        try {
	        if (fileToScan.getName().toLowerCase().endsWith(SQL)) {
	        	if (ChangelogParser.isToExecAsSysDba(fileToScan)){
					pd = new PatchsetDescriptor(0, fileToScan, PatchsetExecutionRole.SYSDBA);
				} else {
					pd = new PatchsetDescriptor(0, fileToScan, PatchsetExecutionRole.NORMAL);
				}
				filesDescList.add(pd);
				toRet.add(filesDescList);
	        }
        } catch (Exception ex) {
			throw new ExecutorException(execOp, ex.getMessage(), ex);
		}
		return toRet;
	}
	
	/**
	 * Suddivide i patchset da applicare in base all'ID progressivo ed al 
	 * livello di privilegi con cui devono essere eseguiti 
	 * 
	 * @return una lista di PatchsetDescriptor raggruppati per subset di esecuzione
	 * @throws ExecutorException
	 */
	@Override
	public List<List<PatchsetDescriptor>> splitPatchsetByExecRole() throws ExecutorException {
		ArrayList<List<PatchsetDescriptor>> toRet = new ArrayList<List<PatchsetDescriptor>>();
		String fileSeparator = System.getProperty(FILE_SEPARATOR);
		File dirToScan = new File(getConfiguration().getBasePath()+fileSeparator+SOURCE+fileSeparator+MAIN+fileSeparator+RESOURCES+fileSeparator+PATCHES+fileSeparator);
		File[] files = dirToScan.listFiles();
		Arrays.sort(files);
		List<File> filesList = Arrays.asList(files);
		
		//Associo a ciascun patchset un descrittore che definisce l'appartenza ad un subset
		List<PatchsetDescriptor> filesDescList = new ArrayList<PatchsetDescriptor>();
		
		try {
			int groupId = 0;
			for (File fileToScan : filesList){
				PatchsetDescriptor pd;
				if (fileToScan.getName().toLowerCase().endsWith(SQL)) {
					if (ChangelogParser.isToExecAsSysDba(fileToScan)){
						groupId++;
						pd = new PatchsetDescriptor(groupId, fileToScan, PatchsetExecutionRole.SYSDBA);
						groupId++;
					} else {
						pd = new PatchsetDescriptor(groupId, fileToScan, PatchsetExecutionRole.NORMAL);
					}
					filesDescList.add(pd);
				}
			}
			
			//Ripartisco i patchset in subset secondo quanto definito nei descrittori
			groupId = 0;
			ArrayList<PatchsetDescriptor> fileGroup = new ArrayList<PatchsetDescriptor>();
			for (PatchsetDescriptor pd : filesDescList){
				if (pd.getPatchsetId() == groupId) {
					fileGroup.add(pd);
				} else {
					groupId++;
					toRet.add(fileGroup);
					fileGroup = new ArrayList<PatchsetDescriptor>();
					fileGroup.add(pd);
				}
			}
			toRet.add(fileGroup);
			
		} catch (Exception ex) {
			throw new ExecutorException(ExecutorOperation.MIGRATE, ex.getMessage(), ex);
		}
		return toRet;
	}
	
	
	/**
	 * Converte la lista dei patchset sql passati in argomento nel formato XML proprio di Liquibase
	 * 
	 * @param patchsetGroup
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 * @throws SQLValidatorException
	 * @throws MigrationDAOException
	 */
	private List<Changeset> convertSqlToXml(List<PatchsetDescriptor> patchsetGroup)  throws FileNotFoundException, UnsupportedEncodingException, SQLValidatorException, MigrationDAOException {
		List<Changeset> toRet = new ArrayList<Changeset>();
		String fileSeparator = System.getProperty(FILE_SEPARATOR);
		File dirToScan = new File(getConfiguration().getBasePath()+fileSeparator+SOURCE+fileSeparator+MAIN+fileSeparator+RESOURCES+fileSeparator+PATCHES+fileSeparator);
		for (PatchsetDescriptor pd : patchsetGroup) {
			ChangelogParser parser = new ChangelogParser();
           	Changeset change = parser.parseChangelog(this, dirToScan.getPath(), pd.getPatchset().getName(), Boolean.TRUE, getConfiguration().getSupportedCustomers());
           	toRet.add(change);
		}
		return toRet;
	}
	
	/**
	 * Ricerca nella directory il file identificato dal progressivo passato in argomento
	 * 
	 * @param dirToScan il patch della cartella da scansionare
	 * @param patchPrefixId il progressivo numerico che identifica il patchset
	 * @return il File corrispondente al patchset
	 */
	private File findFileToParse(File dirToScan, String patchPrefixId){
		File toRet = null;
		File[] filesList = dirToScan.listFiles();
		Arrays.sort(filesList);
		for(File f : filesList){
			if (f.getName().startsWith(patchPrefixId)){
				toRet = f;
				break;
			}
		}
		return toRet;
	}
	
	/**
	 * Restitisce l'esecutore Liquibase
	 */
	@Override
	public Liquibase getExecutor() {
		return liquibaseExec;
	}
	
	/**
	 * Restituisce la configurazione dell'esecutore Liquibase
	 * 
	 * @return la configurazione dell'esecutore
	 */
	@Override
	public IExecutorConfiguration getConfiguration() {
		return this.liquibaseExecConfiguration;
	}

}

package it.ditech.ci.dbfixer.service;

import java.util.List;

import it.ditech.ci.dbfixer.dao.IActivityDAO;
import it.ditech.ci.dbfixer.dao.ICustomerDAO;
import it.ditech.ci.dbfixer.dao.IDBEnvDAO;
import it.ditech.ci.dbfixer.dao.IProductDAO;
import it.ditech.ci.dbfixer.executor.ExecutorException;
import it.ditech.ci.dbfixer.service.dto.ActivityDTO;
import it.ditech.ci.dbfixer.service.dto.AliasDTO;
import it.ditech.ci.dbfixer.service.dto.CustomerDTO;
import it.ditech.ci.dbfixer.service.dto.DbVersionMetadataDTO;
import it.ditech.ci.dbfixer.service.dto.OracleDbEnvDTO;
import it.ditech.ci.dbfixer.service.dto.ProductDTO;
import it.ditech.ci.dbfixer.service.dto.ProductVersionDTO;

/**
 * Contratto Service per la gestione delle Migrazioni
 * 
 * @author mgambini
 */
public interface IMigrationService {
		
	
	/**
	 * Restituisce il DAO relativo alle attività
	 * 
	 * @return il DAO
	 */
	public IActivityDAO getActivityDAO();
	
	/**
	 * Restituisce il DAO relativo ai prodotti
	 * 
	 * @return il DAO
	 */
	public IProductDAO getProductDAO();
	
	/**
	 * Restituisce il DAO relativo ai clienti
	 * 
	 * @return il DAO
	 */
	public ICustomerDAO getCustomerDAO();
	
	/**
	 * Restituisce il DAO relativo agli ambienti
	 * 
	 * @return il DAO
	 */
	public IDBEnvDAO getDbEnvDAO();
	
	/**
	 * Restituisce l'elenco delle attività effettuate su un ambiente DB target
	 * 
	 * @param dbDto il DTO che identifica il DB target della migrazione
	 * @return la lista delle attività effettuate
	 */
	public List<ActivityDTO> findAll(Long productId, Long stageId);
	
	
	/**
	 * Restituisce i dettagli dell'attività dato il suo ID
	 * 
	 * @param activityId l'ID dell'attività
	 * @return i dettagli dell'attività
	 */
	public ActivityDTO findOne(Long activityId);
	
	
	/**
	 * Estrae lo stato 
	 * @param productId
	 * @param stageId
	 * @return
	 */
	public DbVersionMetadataDTO findDbVersionMetadataByProductIdAndStageId(Long productId, Long stageId);
	
	/**
	 * Restituisce l'elenco dei clienti
	 * @return la lista dei clienti
	 */
	public List<CustomerDTO> getCustomers();
	
	/**
	 * Restituisce i dettagli del cliente dato il suo ID
	 * @param customerId
	 * @return
	 */
	public CustomerDTO getCustomerById(Long customerId);
	
	/**
	 * Restituisce i dettagli del prodotto dato il suo ID
	 * @param productId
	 * @return
	 */
	public ProductDTO getProductById(Long productId);
	
	/**
	 * Restituisce l'elenco dei prodotti associati all'ID cliente passato in argomento
	 * 
	 * @param customerId l'identificativo del cliente 
	 * @return l'elenco dei prodotti
	 */
	public List<ProductDTO> getProductsByCustomerId(Long customerId);
	
	/**
	 * Restituisce i dettagli della versione dei prodotto dato il suo ID
	 * @param productVersionId
	 * @return
	 */
	public ProductVersionDTO getProductVersionById(Long productVersionId);
	
	/**
	 * Restituisce l'elenco delle versioni associate all'ID prodotto passato in argomento
	 * 
	 * @param productId l'identificativo del prodotto 
	 * @return l'elenco delle versioni
	 */
	public List<ProductVersionDTO> getProductVersionsByProductId(Long productId);
	
	/**
	 * Restituisce il descrittore dell'ambiente DB associato all'ID di prodotto ed all'ID dello stage passati in argomento
	 * 
	 * @param productId l'ID del prodotto
	 * @param stageId l'ID dello stage
	 * @return il descrittore dell'ambiente DB 
	 */
	public OracleDbEnvDTO getDbEnvByProductIdAndStageId(Long productId, Long stageId);
	
	/**
	 * Restituisce il descrittore dei DB schema associati all'ID dello stage passati in argomento
	 * @param stageId l'ID dello stage
	 * @return il descrittore dei DB schema
	 */
	public AliasDTO getDbAliasesByEnvId(Long envId);
	
	/**
	 * Persiste i metadati relativi al patchset caratterizzato dall'identificativo e dalla versione di prodotto passati in argomento 
	 * 
	 * @param dbDto il DTO che identifica il DB target della migrazione
	 * @param versionId identificativo della versione cui si riferisce il patchset oggetto dell'aggiunta di metadati
	 * @param buildId identificativo della build corrente
	 * @param patchPrefixId progressivo numerico che identifica il patchset
	 * @return true in caso di migrazione eseguita con successo
	 */
	public boolean addDbMetadata(OracleDbEnvDTO dbDto, String versionId, Long buildId, String patchPrefixId);
	
	/**
	 * Applica il patchset caratterizzato dall'identificativo e dalla versione di prodotto passati in argomento 
	 * 
	 * @param dbDto il DTO che identifica il DB target della migrazione
	 * @param versionId identificativo della versione di cui eseguire l'upgrade
	 * @param buildId identificativo della build corrente
	 * @param patchPrefixId progressivo numerico che identifica il patchset
	 * @return true in caso di migrazione eseguita con successo
	 */
	public boolean doMigration(OracleDbEnvDTO dbDto, String versionId, Long buildId, String patchPrefixId);
	
	/**
	 * Applica i patchset appartenenti alla versione di prodotto passata in argomento
	 * 
	 * @param dbDto il DTO che identifica il DB target della migrazione
	 * @param versionId identificativo della versione di cui eseguire l'upgrade
	 * @param buildId identificativo della build corrente
	 * @return true in caso di migrazione eseguita con successo
	 */
	public boolean doMigration(OracleDbEnvDTO dbDto, String versionId, Long buildId);
	
	/**
	 * Persiste come eseguito un patchset senza applicarlo al DB target
	 * 
	 * @param dbDto il DTO che identifica il DB target della migrazione
	 * @param buildId identificativo della build corrente
	 * @param patchId identificativo della patch da marcare come applicata
	 * @return true in caso di definizione avvenuta con successo
	 * @throws ExecutorException 
	 */
	public boolean doFixMetadata(OracleDbEnvDTO dbDto, Long buildId, Long patchId);
	
	
	/**
	 * Svuota il DB target eliminando tutti i suoi oggetti
	 * 
	 * @param dbDto il DTO che identifica il DB target dell'operazione
	 * @param buildId identificativo della build corrente
	 * @return true in caso di svuotamento del DB eseguito con successo 
	 */
	public boolean doClean(OracleDbEnvDTO dbDto, Long buildId);
	
	/**
	 * Periste un tag di chiusura versione
	 * 
	 * @param dbDto il DTO che identifica il DB target dell'operazione
	 * @param version il tag da persistere
	 * @param buildId identificativo della build correntee
	 * @return true in caso di tag persistito con successo
	 */
	public boolean doTag(OracleDbEnvDTO dbDto, String version, Long buildId);
	
	/**
	 * Produce il file di log degli statement SQL e del codice PL/SQL corrispondenti ai patchset da applicare
	 * 
	 * @param dbDto il DTO che identifica il DB target dell'operazione
	 * @param versionId identificativo della versione di cui produrre il file di log
	 * @param buildId identificativo della build corrente
	 * @return true in caso di file di log prodotto con successo
	 */
	public boolean generateSQLChangeLog(OracleDbEnvDTO dbDto, String versionId, Long buildId);
	
}


package it.ditech.ci.dbfixer.service.validator;

import java.util.Map;

/**
 * Interfaccia di un validatore per i parametri degli endpoint RESTful
 * 
 * @author mgambini
 *
 */
public interface IMigrationValidator {
	
	public void validate(Map<String,Object> values) throws MigrationValidatorException;

}

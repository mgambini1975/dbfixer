package it.ditech.ci.dbfixer.sqlparser.validator;

import static it.ditech.ci.dbfixer.config.Constants.CHANGELOG_PATH;
import static it.ditech.ci.dbfixer.config.Constants.CHANGESET_PREFIX;
import static it.ditech.ci.dbfixer.config.Constants.CHUNK_SEPARATOR;
import static it.ditech.ci.dbfixer.config.Constants.ENV_PREFIX;
import static it.ditech.ci.dbfixer.config.Constants.FILE_SEPARATOR;
import static it.ditech.ci.dbfixer.config.Constants.LIQUIBASE_METADATA_HEADER;
import static it.ditech.ci.dbfixer.config.Constants.METADATA_SEPARATOR;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Scanner;

/**
 * Validator della sinstassi degli script .sql che definiscono DDL/DML
 * 
 * @author mgambini
 *
 */
public class SQLChangelogValidator implements ISQLChangelogValidator {

	@Override
	public void validate(Map<String, Object> values) throws SQLValidatorException {
		
		String pathName = (String) values.get(CHANGELOG_PATH);
		
		//Verifico esistenza fisica del path
		File filename = new File(pathName);
		if(!filename.isFile()) { 
			throw new SQLValidatorException("Changelog file not exist: "+pathName, null, pathName);
		}
		
		//Verifico la sintassi del nome del patchset
		String patchsetFilename = pathName.substring(pathName.lastIndexOf(System.getProperty(FILE_SEPARATOR))+1);
		String[] verAndName = patchsetFilename.split(METADATA_SEPARATOR);
		if (verAndName.length < 2 || verAndName.length > 3) 
			throw new SQLValidatorException("Il nome del changelog deve rispettare la sintassi: <progressivoNumerico>[_<issueJIRA>]_<descrizione>.sql: "+patchsetFilename, null, patchsetFilename);
		if (verAndName.length == 3 && !verAndName[1].contains("-"))
			throw new SQLValidatorException("Il codice della issue JIRA non esiste: "+verAndName[1], null, verAndName[1]);
			
		//Verifico la presenza dei metadati
		Scanner input = null;
		try {
			 input = new Scanner(filename);
			 boolean firstLine = Boolean.TRUE;
			 while(input.hasNext()) {
				 String nextLine = input.nextLine();
				 //Scarto eventuali righe vuote
				 if (nextLine.isEmpty() || nextLine.trim().length()==0)
					 continue;
				 if (firstLine){
					 if (!nextLine.startsWith(LIQUIBASE_METADATA_HEADER)){
						 throw new SQLValidatorException("La prima riga del chengelog deve essere: \""+LIQUIBASE_METADATA_HEADER+"\": "+ patchsetFilename, null, patchsetFilename);
					 } 
					 firstLine = Boolean.FALSE;
					 continue;
				 } else {
					 if (!nextLine.toUpperCase().startsWith(CHANGESET_PREFIX) || !nextLine.contains(CHUNK_SEPARATOR)){
						 throw new SQLValidatorException("La seconda riga del chengelog deve essere: \""+CHANGESET_PREFIX+" <author>:<description>\": "+ patchsetFilename, null, patchsetFilename);
					 } else {
						 break;
					 }
				 }	 
			 }
			 input.close();
			 input = new Scanner(filename);
			 while (input.hasNext()) {
				 String nextLine = input.nextLine();
				 //Scarto eventuali righe vuote
				 if (nextLine.isEmpty() || nextLine.trim().length()==0)
					 continue;
				 if (nextLine.toUpperCase().startsWith(ENV_PREFIX)){
					 if (!nextLine.contains(CHUNK_SEPARATOR)){
						 throw new SQLValidatorException("Il tag \""+ENV_PREFIX+"\" deve rispettare la sintassi: "+ENV_PREFIX+":<env1>[,<env2>,...,<env-n>]", null, patchsetFilename);
					 }
					 String[] chunks = nextLine.split(CHUNK_SEPARATOR);
					 if (chunks.length != 2) {
						 throw new SQLValidatorException("Il tag \""+ENV_PREFIX+"\" deve rispettare la sintassi: "+ENV_PREFIX+":<env1>[,<env2>,...,<env-n>]", null, patchsetFilename);
					 }
					 break;
				 }				 
			 }

		} catch (FileNotFoundException e) {
			throw new SQLValidatorException("Changelog file not exist: "+values.get(CHANGELOG_PATH), null, patchsetFilename);
		} finally {
			input.close();
		}
		
	}

}

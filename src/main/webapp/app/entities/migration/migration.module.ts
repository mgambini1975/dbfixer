import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DbFixerSharedModule } from '../../shared';
import {
    MigrationService,
    MigrationComponent,
    MigrationDetailComponent,
    migrationRoute,
} from './';

const ENTITY_STATES = [
    ...migrationRoute,
];

@NgModule({
    imports: [
        DbFixerSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        MigrationComponent,
        MigrationDetailComponent,
    ],
    entryComponents: [
        MigrationComponent,
        MigrationDetailComponent,
    ],
    providers: [
        MigrationService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DbFixerMigrationModule {}

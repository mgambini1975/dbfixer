package it.ditech.ci.dbfixer.service.mapper;

import java.net.MalformedURLException;
import java.net.URL;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.ditech.ci.dbfixer.domain.OracleDbEnv;
import it.ditech.ci.dbfixer.service.dto.OracleDbEnvDTO;

/**
 * Mapper for the entity OracleDbEnv and its DTO OracleDbEnvDTO.
 */
@Mapper(componentModel = "spring", uses = {ProductMapper.class, })
public interface OracleDbEnvMapper extends EntityMapper <OracleDbEnvDTO, OracleDbEnv> {
	
	final Logger log = LoggerFactory.getLogger(OracleDbEnvMapper.class);
	
    @Mapping(source = "product.id", target = "productId")
    default OracleDbEnvDTO toDto(OracleDbEnv oracleDbEnv){
    	 	
    	OracleDbEnvDTO toRet = new OracleDbEnvDTO();
    	if ( oracleDbEnv == null) {
    		return null;
    	}
    	toRet.setEnv_desc(oracleDbEnv.getEnv_desc());
    	toRet.setEnvType(oracleDbEnv.getEnvType());
    	toRet.setHostname(oracleDbEnv.getHostname());
    	toRet.setListenerPort(oracleDbEnv.getListenerPort());

    	try {
			toRet.setPatchsetdbrepourl(new URL(oracleDbEnv.getPatchsetdbrepourl()));
		} catch (MalformedURLException e) {
			log.error("Errore in formato URL: "+oracleDbEnv.getPatchsetdbrepourl());
		}
    	toRet.setPwd(oracleDbEnv.getPwd());
    	toRet.setSid(oracleDbEnv.getSid());
    	toRet.setUser(oracleDbEnv.getUser());
    	toRet.setId(oracleDbEnv.getId());
    	return toRet;
    }

    @Mapping(source = "productId", target = "product")
    @Mapping(target = "aliases", ignore = true)
    default OracleDbEnv toEntity(OracleDbEnvDTO oracleDbEnvDTO){
    	OracleDbEnv toRet = new OracleDbEnv();
    	if (oracleDbEnvDTO == null) {
    		return null;
    	}
    	toRet.setEnv_desc(oracleDbEnvDTO.getEnv_desc());
    	toRet.setEnvType(oracleDbEnvDTO.getEnvType());
    	toRet.setHostname(oracleDbEnvDTO.getHostname());
    	toRet.setListenerPort(oracleDbEnvDTO.getListenerPort());
    	toRet.setPatchsetdbrepourl(oracleDbEnvDTO.getPatchsetdbrepourl().toString());
    	toRet.setPwd(oracleDbEnvDTO.getPwd());
    	toRet.setSid(oracleDbEnvDTO.getSid());
    	toRet.setUser(oracleDbEnvDTO.getUser());
    	toRet.setId(oracleDbEnvDTO.getId());
    	return toRet;
    }
    
    default OracleDbEnv fromId(Long id) {
        if (id == null) {
            return null;
        }
        OracleDbEnv oracleDbEnv = new OracleDbEnv();
        oracleDbEnv.setId(id);
        return oracleDbEnv;
    }
}

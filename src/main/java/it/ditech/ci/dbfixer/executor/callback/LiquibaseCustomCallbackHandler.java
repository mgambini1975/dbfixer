package it.ditech.ci.dbfixer.executor.callback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import liquibase.change.Change;
import liquibase.changelog.ChangeSet;
import liquibase.changelog.ChangeSet.ExecType;
import liquibase.changelog.ChangeSet.RunStatus;
import liquibase.changelog.DatabaseChangeLog;
import liquibase.changelog.visitor.ChangeExecListener;
import liquibase.database.Database;
import liquibase.exception.PreconditionErrorException;
import liquibase.exception.PreconditionFailedException;
import liquibase.precondition.core.PreconditionContainer.ErrorOption;
import liquibase.precondition.core.PreconditionContainer.FailOption;

/**
 * Gestore delle callback di eventi triggerati da Liquibase
 * 
 * @author mgambini
 *
 */
public class LiquibaseCustomCallbackHandler extends AbstractLiquibaseCustomCallbackHandler implements ChangeExecListener, ILiquibaseCustomCallbackHandler {
	
	private final Logger log = LoggerFactory.getLogger(LiquibaseCustomCallbackHandler.class);
		
	
	public boolean processCallbacks(CallbackType callbackType, 
			ChangeSet changeSet, 
			DatabaseChangeLog databaseChangeLog, 
			Database database) {
		return this.processCallbacks(callbackType, changeSet, databaseChangeLog, database, null, null);
	}
	
	
	/**
	 * Notifica le callback del tipo passato in argomento
	 * 
	 * @param callbackType il tipo delle callback da notificare
	 * @return true in caso di elaborazione avvenuta con successo
	 */
	@Override
	public boolean processCallbacks(CallbackType callbackType, 
			ChangeSet changeSet, 
			DatabaseChangeLog databaseChangeLog, 
			Database database,
			ExecType execType,
			Exception exception) {
		for (AbstractCustomCallback callback : this.getCallbacks()) {
			if ((callbackType.name().equals(callback.getCallbackType().name()) || 
					callbackType.name().equals(CallbackType.ALLOUTCOME.name())) && callback.isEnabled()) {
				((LiquibaseCustomCallback)callback).process(changeSet, databaseChangeLog, database, execType, exception);
			}
		}
		return Boolean.TRUE;
	}

	/**
	 * Callback invocata subito prima l'applicazione del patchset passato in argomento
	 */
	@Override
	public void willRun(ChangeSet changeSet, DatabaseChangeLog databaseChangeLog, Database database,
			RunStatus runStatus) {
		this.processCallbacks(CallbackType.WILLRUN, changeSet, databaseChangeLog, database);
	}
	
	/**
	 * Callback invocata subito prima l'applicazione del patchset passato in argomento
	 */
	@Override
	public void willRun(Change change, ChangeSet changeSet, DatabaseChangeLog changeLog, Database database) {
		this.processCallbacks(CallbackType.WILLRUN, changeSet, changeLog, database);
	}

	/**
	 * Callback invocata subito dopo l'esecuzione con successo del patchset passato in argomento
	 */
	@Override
	public void ran(ChangeSet changeSet, DatabaseChangeLog databaseChangeLog, Database database, ExecType execType) {
		this.processCallbacks(CallbackType.SUCCESS, changeSet, databaseChangeLog, database, execType, null);
	}
	
	/**
	 * Callback invocata subito dopo l'esecuzione con successo del patchset passato in argomento
	 */
	@Override
	public void ran(Change change, ChangeSet changeSet, DatabaseChangeLog changeLog, Database database) {
		// TODO Auto-generated method stub	
	}
	
	/**
	 * Callback invocata subito dopo l'esecuzione fallita del patchset passato in argomento
	 */
	@Override
	public void runFailed(ChangeSet changeSet, DatabaseChangeLog databaseChangeLog, Database database, Exception exception) {
		this.processCallbacks(CallbackType.FAILURE, changeSet, databaseChangeLog, database, null, exception);
	}

	@Override
	public void rolledBack(ChangeSet changeSet, DatabaseChangeLog databaseChangeLog, Database database) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void preconditionFailed(PreconditionFailedException error, FailOption onFail) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void preconditionErrored(PreconditionErrorException error, ErrorOption onError) {
		// TODO Auto-generated method stub
		
	}

}

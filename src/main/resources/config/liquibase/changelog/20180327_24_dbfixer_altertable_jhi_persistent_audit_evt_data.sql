--liquibase formatted sql
--changeset mgambini:alter tabella jhi_persistent_audit_evt_data

ALTER TABLE jhi_persistent_audit_evt_data MODIFY value VARCHAR(500);
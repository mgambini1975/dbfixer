package it.ditech.ci.dbfixer.executor.callback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import liquibase.changelog.ChangeSet;
import liquibase.changelog.ChangeSet.ExecType;
import liquibase.changelog.DatabaseChangeLog;
import liquibase.database.Database;

/**
 * Implementazione di una callback per l'applicazione di un patchset con ruolo SYSDBA
 * 
 * @author mgambini
 */
public class LiquibaseSysdbaExecCustomCallback extends LiquibaseCustomCallback {
	
	private final Logger log = LoggerFactory.getLogger(LiquibaseSysdbaExecCustomCallback.class);
	
	public LiquibaseSysdbaExecCustomCallback(Long buildId){
		this(Boolean.TRUE, buildId);
	}
	
	public LiquibaseSysdbaExecCustomCallback(boolean enabled, Long buildId){
		super(CallbackType.WILLRUN, enabled, buildId);
	}

	@Override
	public void process(ChangeSet changeSet, DatabaseChangeLog databaseChangeLog, Database database, ExecType execType,
			Exception exception) {
		
		log.debug("SysDBA callback...");

	}

}

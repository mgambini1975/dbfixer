package it.ditech.ci.dbfixer.domain.enumeration;

/**
 * Activity type enumeration
 * 
 * CLEAN: erases DbFixer metadata (only for development/test purposes)
 * SQL: applies patchset(s)
 * REPAIR: fixes patchset metadata
 * TAG: applies a DB tag
 * DRYRUN: emulates patchset application producing a log file
 * ADDMETADATA: adds DbFixer metadata related to a manually executed patchset
 * 
 * @author mgambini
 *
 */
public enum ActivityType {
	CLEAN(0), SQL(1), REPAIR(2), TAG(3), DRYRUN(4), ADDMETADATA(5);
	
	private final int actType;
    
    ActivityType(int type){
    	this.actType = type;
    }
    
    int getActType(){
    	return this.actType;
    }
    
    public static ActivityType valueOf(long code){
    	for (ActivityType actType : ActivityType.values()){
    		if (actType.getActType() == code)
    			return actType;
    	}
    	return null;
    }
    
    public static Long valueOf(ActivityType envType){
    	Long toRet;
    	switch (envType) {
    		case CLEAN:			toRet = new Long(0);
    							break;
    		case SQL:			toRet = new Long(1);
	    						break;
    		case REPAIR:		toRet = new Long(2);
    							break;
	    	case TAG:			toRet = new Long(3);
	    						break;	    	
	    	case DRYRUN:		toRet = new Long(4);
								break;	    	
	    	case ADDMETADATA:	toRet = new Long(5);
	    						break;
	    	default:		toRet = null;
    	}
    	return toRet;
    }
	
};

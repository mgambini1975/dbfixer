--liquibase formatted sql
--changeset mgambini:creazione tabella Jhi_authority

-- --------------------------------------------------------

--
-- Struttura della tabella `jhi_authority`
--

CREATE TABLE IF NOT EXISTS `jhi_authority` (
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

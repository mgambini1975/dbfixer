package it.ditech.ci.dbfixer.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.ditech.ci.dbfixer.service.dto.ProductVersionDTO;

public interface ProductVersionService {
	
	 /**
     * Save a productVersion.
     *
     * @param productVersionDTO the entity to save
     * @return the persisted entity
     */
    ProductVersionDTO save(ProductVersionDTO productDTO);

    /**
     *  Get all the productVersions.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ProductVersionDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" productVersion.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ProductVersionDTO findOne(Long id);

    /**
     *  Delete the "id" product.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

}

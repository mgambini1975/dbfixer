package it.ditech.ci.dbfixer.security;

import org.springframework.stereotype.Component;

import com.atlassian.crowd.integration.springsecurity.RemoteCrowdAuthenticationProvider;

@Component
public class CustomRemoteCrowdAuthenticationProvider extends RemoteCrowdAuthenticationProvider {
	
	
	public CustomRemoteCrowdAuthenticationProvider(CustomCrowdClient authenticationManager,
			CustomCrowdHttpAuthenticator crowdHttpAuthenticator, 
			CustomCrowdUserDetailsServiceImpl crowdUserDetailsService) {
		super(authenticationManager, crowdHttpAuthenticator, crowdUserDetailsService);

	}

}

package it.ditech.ci.dbfixer.executor;

import liquibase.Liquibase;

/**
 * Contratto per un wrapper del motore Liquibase
 * 
 * @author mgambini
 *
 */
public interface ILiquibaseExecutor extends IExecutor {
	
	public Liquibase getExecutor();

}

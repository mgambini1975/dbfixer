package it.ditech.ci.dbfixer.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * A Product.
 */
@Entity
@Table(name = "product")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(min = 4)
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @NotNull
    @Size(min = 5, max = 5)
    @Column(name = "version", length = 5, nullable = false)
    private String version;

    @ManyToOne
    private Customer customer;

    @OneToMany(mappedBy = "product")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<WildflyEnv> wildflyEnvs = new HashSet<>();

    @OneToMany(mappedBy = "product")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<OracleDbEnv> oracleDbEnvs = new HashSet<>();

    @OneToMany(mappedBy = "product")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<TomcatEnv> tomcatEnvs = new HashSet<>();
    

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Product name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Product description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVersion() {
        return version;
    }

    public Product version(String version) {
        this.version = version;
        return this;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Product customer(Customer customer) {
        this.customer = customer;
        return this;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Set<WildflyEnv> getWildflyEnvs() {
        return wildflyEnvs;
    }

    public Product wildflyEnvs(Set<WildflyEnv> wildflyEnvs) {
        this.wildflyEnvs = wildflyEnvs;
        return this;
    }

    public Product addWildflyEnvs(WildflyEnv wildflyEnv) {
        this.wildflyEnvs.add(wildflyEnv);
        wildflyEnv.setProduct(this);
        return this;
    }

    public Product removeWildflyEnvs(WildflyEnv wildflyEnv) {
        this.wildflyEnvs.remove(wildflyEnv);
        wildflyEnv.setProduct(null);
        return this;
    }

    public void setWildflyEnvs(Set<WildflyEnv> wildflyEnvs) {
        this.wildflyEnvs = wildflyEnvs;
    }

    public Set<OracleDbEnv> getOracleDbEnvs() {
        return oracleDbEnvs;
    }

    public Product oracleDbEnvs(Set<OracleDbEnv> oracleDbEnvs) {
        this.oracleDbEnvs = oracleDbEnvs;
        return this;
    }

    public Product addOracleDbEnvs(OracleDbEnv oracleDbEnv) {
        this.oracleDbEnvs.add(oracleDbEnv);
        oracleDbEnv.setProduct(this);
        return this;
    }

    public Product removeOracleDbEnvs(OracleDbEnv oracleDbEnv) {
        this.oracleDbEnvs.remove(oracleDbEnv);
        oracleDbEnv.setProduct(null);
        return this;
    }

    public void setOracleDbEnvs(Set<OracleDbEnv> oracleDbEnvs) {
        this.oracleDbEnvs = oracleDbEnvs;
    }

    public Set<TomcatEnv> getTomcatEnvs() {
        return tomcatEnvs;
    }

    public Product tomcatEnvs(Set<TomcatEnv> tomcatEnvs) {
        this.tomcatEnvs = tomcatEnvs;
        return this;
    }

    public Product addTomcatEnvs(TomcatEnv tomcatEnv) {
        this.tomcatEnvs.add(tomcatEnv);
        tomcatEnv.setProduct(this);
        return this;
    }

    public Product removeTomcatEnvs(TomcatEnv tomcatEnv) {
        this.tomcatEnvs.remove(tomcatEnv);
        tomcatEnv.setProduct(null);
        return this;
    }

    public void setTomcatEnvs(Set<TomcatEnv> tomcatEnvs) {
        this.tomcatEnvs = tomcatEnvs;
    }
    
    
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Product product = (Product) o;
        if (product.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), product.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Product{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", version='" + getVersion() + "'" +
            "}";
    }

}

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
/* componenti PrimeNG */
import {DataTableModule} from 'primeng/primeng';




import { DbFixerSharedModule } from '../../shared';
import {
    OperationService,
    OperationComponent,
    OperationDetailComponent,
    operationRoute,
} from './';

const ENTITY_STATES = [
    ...operationRoute,
];

@NgModule({
    imports: [
        DbFixerSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
          /* componenti PrimeNG */
	    DataTableModule,
    ],
    declarations: [
        OperationComponent,
        OperationDetailComponent,
    ],
    entryComponents: [
        OperationComponent,
        OperationDetailComponent,
    ],
    providers: [
        OperationService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DbFixerOperationModule {}

--liquibase formatted sql
--changeset mgambini:insert into tabella Jhi_authority

INSERT INTO `jhi_authority` (`name`) VALUES
('ROLE_ADMIN'),
('ROLE_USER');

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Migration } from './migration.model';
import { MigrationService } from './migration.service';

@Component({
    selector: 'jhi-migration-detail',
    templateUrl: './migration-detail.component.html'
})
export class MigrationDetailComponent implements OnInit, OnDestroy {

    migration: Migration;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private migrationService: MigrationService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInMigrations();
    }

    load(id) {
        this.migrationService.find(id).subscribe((migration) => {
            this.migration = migration;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInMigrations() {
        this.eventSubscriber = this.eventManager.subscribe(
            'migrationListModification',
            (response) => this.load(this.migration.id)
        );
    }
}

package it.ditech.ci.dbfixer.service.impl;

import static it.ditech.ci.dbfixer.config.Constants.ADMIN;
import static it.ditech.ci.dbfixer.config.Constants.DB_CLEAN_SUCCESSFUL;
import static it.ditech.ci.dbfixer.config.Constants.DB_TAG_SUCCESSFUL;
import static it.ditech.ci.dbfixer.config.Constants.DOT;
import static it.ditech.ci.dbfixer.config.Constants.EMPTY_STRING;
import static it.ditech.ci.dbfixer.config.Constants.EXC_CAUSE_MSG;
import static it.ditech.ci.dbfixer.config.Constants.EXC_GETTING_ACTIVITY_PERSIST;
import static it.ditech.ci.dbfixer.config.Constants.EXC_GETTING_CUSTOMER;
import static it.ditech.ci.dbfixer.config.Constants.EXC_GETTING_DBENV;
import static it.ditech.ci.dbfixer.config.Constants.EXC_GETTING_PRODUCT;
import static it.ditech.ci.dbfixer.config.Constants.FAILURE;
import static it.ditech.ci.dbfixer.config.Constants.IT_LANGUAGE;
import static it.ditech.ci.dbfixer.config.Constants.MANUAL_APPLIED_PATCHSET;
import static it.ditech.ci.dbfixer.config.Constants.METADATA_FIXING_ERROR_MSG;
import static it.ditech.ci.dbfixer.config.Constants.METADATA_SEPARATOR;
import static it.ditech.ci.dbfixer.config.Constants.NO_ACTIVITY_FOUND_FOR_PATCHSET;
import static it.ditech.ci.dbfixer.config.Constants.PATCHSET_NOT_FOUND_ERROR_MSG;
import static it.ditech.ci.dbfixer.config.Constants.SLASH;
import static it.ditech.ci.dbfixer.config.Constants.SQL;
import static it.ditech.ci.dbfixer.config.Constants.SUCCESS;
import static it.ditech.ci.dbfixer.config.Constants.SYSTEM;
import static it.ditech.ci.dbfixer.config.Constants.UNKNOWN;
import static it.ditech.ci.dbfixer.config.Constants.UTENTE_DBA;
import static it.ditech.ci.dbfixer.config.Constants.XML;
import static it.ditech.ci.dbfixer.config.Constants.XML_FILE_PATH;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.ditech.ci.dbfixer.cache.MigrationsCache;
import it.ditech.ci.dbfixer.config.ApplicationProperties;
import it.ditech.ci.dbfixer.dao.IActivityDAO;
import it.ditech.ci.dbfixer.dao.IAliasDAO;
import it.ditech.ci.dbfixer.dao.ICustomerDAO;
import it.ditech.ci.dbfixer.dao.IDBEnvDAO;
import it.ditech.ci.dbfixer.dao.IProductDAO;
import it.ditech.ci.dbfixer.dao.IProductVersionDAO;
import it.ditech.ci.dbfixer.dao.ITargetDbMetadataDAO;
import it.ditech.ci.dbfixer.dao.impl.ActivityDAOException;
import it.ditech.ci.dbfixer.dao.impl.MigrationDAOException;
import it.ditech.ci.dbfixer.domain.Activity;
import it.ditech.ci.dbfixer.domain.DbVersionMetadata;
import it.ditech.ci.dbfixer.domain.Migration;
import it.ditech.ci.dbfixer.domain.User;
import it.ditech.ci.dbfixer.domain.enumeration.ActivityType;
import it.ditech.ci.dbfixer.domain.enumeration.StageEnvType;
import it.ditech.ci.dbfixer.executor.ExecutorBuilder;
import it.ditech.ci.dbfixer.executor.ExecutorException;
import it.ditech.ci.dbfixer.executor.ExecutorOperation;
import it.ditech.ci.dbfixer.executor.IExecutor;
import it.ditech.ci.dbfixer.executor.PatchsetDescriptor;
import it.ditech.ci.dbfixer.executor.PatchsetExecutionRole;
import it.ditech.ci.dbfixer.service.IMigrationService;
import it.ditech.ci.dbfixer.service.MailService;
import it.ditech.ci.dbfixer.service.UserService;
import it.ditech.ci.dbfixer.service.dto.ActivityDTO;
import it.ditech.ci.dbfixer.service.dto.AliasDTO;
import it.ditech.ci.dbfixer.service.dto.CustomerDTO;
import it.ditech.ci.dbfixer.service.dto.DbVersionMetadataDTO;
import it.ditech.ci.dbfixer.service.dto.OracleDbEnvDTO;
import it.ditech.ci.dbfixer.service.dto.ProductDTO;
import it.ditech.ci.dbfixer.service.dto.ProductVersionDTO;
import it.ditech.ci.dbfixer.service.util.DbUtil;
import it.ditech.ci.dbfixer.sqlparser.validator.SQLValidatorException;

/**
 * Servizio che gestisce le migrazioni di un Db target
 * 
 * @author mgambini
 */
@Service
@Transactional
public class MigrationServiceImpl extends GenericServiceImpl implements IMigrationService {
	
	@Autowired
	private ExecutorBuilder execFactory;
	
	@Autowired
	private IProductDAO productDAO;
	
	@Autowired
	private IProductVersionDAO productVersionDAO;
	
	@Autowired
	private ICustomerDAO customerDAO;
	
	@Autowired
	private IDBEnvDAO dbEnvDAO;
	
	@Autowired
	private IActivityDAO activityDAO;
	
	@Autowired
	private IAliasDAO aliasDAO;
	
	@Autowired
	private ITargetDbMetadataDAO executorDAO;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private MailService mailService;
	
	@Autowired
	private ApplicationProperties applicationProperties;
	
	private final Logger log = LoggerFactory.getLogger(MigrationServiceImpl.class);
	
	@Override
	@Transactional(readOnly = true)
	public List<ActivityDTO> findAll(Long productId, Long stageId){
		List<Activity> activities = new ArrayList<Activity>();
		List<ActivityDTO> toRet = null;
		try {
			activities = ((IActivityDAO)getActivityDAO()).getActivities(productId, stageId);
			toRet = convertActivityToDTO(activities);
		} catch (ActivityDAOException | MigrationDAOException e) {
			log.error("Exception in getting activity report: "+e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		return toRet;
	}
	
	@Override
	@Transactional(readOnly = true)
	public ActivityDTO findOne(Long activityId){
		List<Activity> activities = new ArrayList<Activity>();
		ActivityDTO toRet = null;
		try {
			Activity activity = ((IActivityDAO)getActivityDAO()).getActivity(activityId);
			if (activity != null){
				activities.add(activity);
				toRet = convertActivityToDTO(activities).get(0);
			}
		} catch (ActivityDAOException | MigrationDAOException e) {
			log.error("Exception in getting activity report: "+e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		return toRet;
	}
	
	@Override
	public DbVersionMetadataDTO findDbVersionMetadataByProductIdAndStageId(Long productId, Long stageId) {
		DbVersionMetadataDTO toRet = null;
		try {
			DbVersionMetadata metadata = activityDAO.getDbVersionMetadata(productId, stageId);
			if (metadata!=null){
				toRet = convertDbVersionMetadataToDTO(metadata);
				toRet.setProduct(productDAO.getProductFromId(productId).getName());
				toRet.setStage(dbEnvDAO.getDbEnvByProductAndStage(productId, stageId).getEnvType().name());
			}
		} catch (ActivityDAOException | MigrationDAOException e) {
			log.error("Exception in getting activity report: "+e.getMessage());
			throw new RuntimeException(e.getMessage());
		} 
		return toRet;
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<CustomerDTO> getCustomers() {
		List<CustomerDTO> customers = new ArrayList<CustomerDTO>();
		try {
			customers = (getCustomerDAO()).getCustomers();
		} catch (MigrationDAOException e) {
			log.error("Exception in getting customers: "+e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		return customers;
	}

	@Override
	@Transactional(readOnly = true)
	public List<ProductDTO> getProductsByCustomerId(Long customerId) {
		List<ProductDTO> products = new ArrayList<ProductDTO>();
		try {
			products = ((IProductDAO)getProductDAO()).getProductsByCustomerId(customerId);
		} catch (MigrationDAOException e) {
			log.error("Exception in getting products: "+e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		return products;
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<ProductVersionDTO> getProductVersionsByProductId(Long productId) {
		List<ProductVersionDTO> productVersions = new ArrayList<ProductVersionDTO>();
		try {
			productVersions = ((IProductVersionDAO)getProductVersionDAO()).getProductVersionsByProductId(productId);
		} catch (MigrationDAOException e) {
			log.error("Exception in getting productVersions: "+e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		return productVersions;
	}

	@Override
	@Transactional(readOnly = true)
	public OracleDbEnvDTO getDbEnvByProductIdAndStageId(Long productId, Long stageId) {
		OracleDbEnvDTO dbEnvDTO = new OracleDbEnvDTO(); 
		try {
			dbEnvDTO = ((IDBEnvDAO)getDbEnvDAO()).getDbEnvByProductAndStage(productId, stageId);		
		} catch (MigrationDAOException e) {
			log.error(EXC_GETTING_DBENV+": "+e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		return dbEnvDTO;
	}
	
	@Override
	@Transactional(readOnly = true)
	public CustomerDTO getCustomerById(Long customerId) {
		CustomerDTO customerDTO = new CustomerDTO();
		try {
			customerDTO = getCustomerDAO().getCustomerById(customerId);
		} catch (MigrationDAOException e) {
			log.error(EXC_GETTING_CUSTOMER+": "+e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		return customerDTO;
	}
	
	@Override
	@Transactional(readOnly = true)
	public ProductDTO getProductById(Long productId) {
		ProductDTO productDTO = new ProductDTO();
		try {
			productDTO = getProductDAO().getProductFromId(productId);
		} catch (MigrationDAOException e) {
			log.error(EXC_GETTING_PRODUCT+": "+e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		return productDTO;
	}

	@Override
	@Transactional(readOnly = true)
	public ProductVersionDTO getProductVersionById(Long productVersionId) {
		ProductVersionDTO productVersionDTO = new ProductVersionDTO();
		try {
			productVersionDTO = getProductVersionDAO().getProductVersionById(productVersionId);
		} catch (MigrationDAOException e) {
			log.error(EXC_GETTING_PRODUCT+": "+e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		return productVersionDTO;
	}
	

	@Override
	@Transactional(readOnly = true)
	public AliasDTO getDbAliasesByEnvId(Long envId) {
		AliasDTO aliasDTO = new AliasDTO();
		try {
			aliasDTO = ((IAliasDAO)getAliasDAO()).getDbAliasesByEnvId(envId);
		} catch (MigrationDAOException e) {
			log.error(EXC_GETTING_DBENV+": "+e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		return aliasDTO;
	}
	
	@Override
	@Transactional
	public synchronized boolean addDbMetadata(OracleDbEnvDTO dbDto, String versionId, Long buildId, String patchPrefixId) {
		boolean toRet = Boolean.FALSE;
		try {	

			IExecutor executor = getExecutor(dbDto, executorDAO, ActivityType.ADDMETADATA, buildId);
			executor.cloneRepo(dbDto.getPatchsetdbrepourl(), versionId);
			List<List<PatchsetDescriptor>> groupedPatchsets = executor.splitPatchsetByPatchPrefixId(patchPrefixId, ExecutorOperation.ADDMETADATA);		
			//Recupero metadati dal patchset...
			iterateOnGroupedPatchset(ActivityType.ADDMETADATA, groupedPatchsets, buildId, dbDto);				
			toRet = Boolean.TRUE;			
		} catch (ExecutorException ex) {
			//Gestione errore generico
			this.handleExecutorException(ex, buildId, versionId);
		} finally {
			//Persistenza dell'esito dell'applicazione dei patchset
			finalizeOperation(ActivityType.ADDMETADATA, dbDto, buildId);
		}		
		return toRet;
	}

			
	@Override
	@Transactional
	public boolean doFixMetadata(OracleDbEnvDTO dbDto, Long buildId, Long patchId) {
		boolean toRet = Boolean.FALSE;
		try {
			Activity anActivity = activityDAO.getActivity(patchId);
			if (anActivity != null){
				IExecutor executor;
				//Seleziono connessione in base allo username dell'Activity da fixare
				boolean isSysDBA = UTENTE_DBA.equals(anActivity.getUsername());
				if (isSysDBA) {
					executor = getExecutor(dbDto, executorDAO, ActivityType.REPAIR, Boolean.TRUE, buildId);
				} else {
					executor = getExecutor(dbDto, executorDAO, ActivityType.REPAIR, buildId);
				}
				//Fixo metadati su tabella Liquibase
				executor.fixMetadata(anActivity);
				// Aggiorno metadati su tabella DBFixer
				String logMsg = MANUAL_APPLIED_PATCHSET;				
				this.persistActivity(dbDto, 
						anActivity.getActType(), 
						anActivity.getVersion(), 
						anActivity.getScriptName(), 
						dbDto.getUser(), 
						anActivity.getAuthor(), 
						anActivity.getDescription(), 
						Boolean.TRUE, 
						logMsg, 
						buildId, 
						EMPTY_STRING, 
						EMPTY_STRING);
				log.debug(logMsg+": "+anActivity.getScriptName());
				toRet = Boolean.TRUE;
			} else {
				String logMsg = METADATA_FIXING_ERROR_MSG+"."+PATCHSET_NOT_FOUND_ERROR_MSG+": "+patchId;
				log.warn(logMsg);
				this.persistActivity(dbDto, ActivityType.REPAIR, null, null, dbDto.getUser(), null, null, Boolean.FALSE, logMsg, buildId, EMPTY_STRING, EMPTY_STRING);
			}
		} catch (ExecutorException | ActivityDAOException ex) {
			String causeMsg = ex.getCause() != null ? ex.getCause().getMessage() : null;
			String logMsg = ex.getMessage()+" ."+EXC_CAUSE_MSG+": "+causeMsg;
			log.error(logMsg);
			this.persistActivity(dbDto, ActivityType.REPAIR, null, null, dbDto.getUser(), null, null, Boolean.FALSE, logMsg, buildId, EMPTY_STRING, EMPTY_STRING);
		} finally {
			MigrationsCache.getInstance().cleanMigrations();
		}
		return toRet;
	}
	
	@Override
	@Transactional
	public synchronized boolean generateSQLChangeLog(OracleDbEnvDTO dbDto, String versionId, Long buildId) {
		boolean toRet = Boolean.FALSE;
		try {
			IExecutor executor = getExecutor(dbDto, executorDAO, ActivityType.DRYRUN, buildId);
			executor.deleteSQLChangelog();
			executor.cloneRepo(dbDto.getPatchsetdbrepourl(), versionId);
			List<List<PatchsetDescriptor>> groupedPatchsets = executor.splitPatchsetByExecRole();		
			iterateOnGroupedPatchset(ActivityType.DRYRUN, groupedPatchsets, buildId, dbDto);	
			toRet = Boolean.TRUE;
			
		} catch (ExecutorException ex) {
			this.handleExecutorException(ex, buildId, versionId);
		} finally {
			finalizeOperation(ActivityType.DRYRUN, dbDto, buildId);
		}
		return toRet;
	}
	
	@Override
	public boolean doMigration(OracleDbEnvDTO dbDto, String versionId, Long buildId) {
		return this.doMigration(dbDto, versionId, buildId, null);
	}
	
	@Override
	@Transactional
	public synchronized boolean doMigration(OracleDbEnvDTO dbDto, String versionId, Long buildId, String patchPrefixId) {
		boolean toRet = Boolean.FALSE;	
		try {
			
			IExecutor executor = getExecutor(dbDto, executorDAO, ActivityType.SQL, buildId);
			executor.cloneRepo(dbDto.getPatchsetdbrepourl(), versionId);
			List<List<PatchsetDescriptor>> groupedPatchsets;
			if (patchPrefixId == null) {
				groupedPatchsets = executor.splitPatchsetByExecRole();
			} else {
				groupedPatchsets = executor.splitPatchsetByPatchPrefixId(patchPrefixId, ExecutorOperation.MIGRATE);
			}			
			iterateOnGroupedPatchset(ActivityType.SQL, groupedPatchsets, buildId, dbDto);		
			toRet = Boolean.TRUE;
			
		} catch (ExecutorException ex) {
			//Gestione errore generico
			this.handleExecutorException(ex, buildId, versionId);
		} finally {
			//Persistenza dell'esito dell'applicazione dei patchset
			finalizeOperation(ActivityType.SQL, dbDto, buildId);
		}
		return toRet;
	}
	
	private void finalizeOperation(ActivityType actType, OracleDbEnvDTO dbDto, Long buildId){	
		try {
			List<Migration> migrations = MigrationsCache.getInstance().getMigrationsByBuildId(buildId);
			if (migrations!=null && !ActivityType.DRYRUN.equals(actType.name())){
				for (Migration migration : migrations){		
					persistActivity(dbDto, actType, migration.getVersion(), migration.getScriptName(), migration.getInstalledBy(), 
							migration.getAuthor(), migration.getDescription(), migration.getSuccess(), 
							migration.getLog(), buildId, migration.getFileName(), migration.getMd5sum());
					//Invio notifica by Mail se sto aggiornando CI
					if (migration.getAuthor() != null && ActivityType.SQL.equals(actType) && StageEnvType.CI.equals(dbDto.getEnvType())){
						sendMailNotification(dbDto, migration);
					}
				}
			}
			try {
				IExecutor executor = getExecutor(dbDto, executorDAO, actType, Boolean.FALSE, buildId);
				executor.deleteRepo(dbDto.getPatchsetdbrepourl());
			} catch (ExecutorException ex) {
				String causeMsg = ex.getCause() != null ? ex.getCause().getMessage() : ex.getMessage();
				String logMsg = EXC_CAUSE_MSG+": "+causeMsg;
				log.error(logMsg);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			MigrationsCache.getInstance().cleanMigrations();
		}
	}
	
	
	private void handleExecutorException(ExecutorException ex, Long buildId, String versionId){
		if (MigrationsCache.getInstance().getMigrationsByBuildId(buildId) == null){
			String causeMsg = ex.getCause() != null ? ex.getCause().getMessage() : ex.getMessage();
			String logMsg = EXC_CAUSE_MSG+": "+causeMsg;
			log.error(logMsg);
			Migration aMigration = new Migration();
			String version;
			String filename = null;
			if (ex.getCause()!= null && (ex.getCause() instanceof SQLValidatorException)) {
				filename = ((SQLValidatorException)ex.getCause()).getFilename();			
				version = filename.substring(0,filename.indexOf(METADATA_SEPARATOR));
			} else {
				if (ex.getMessage()!= null && ex.getMessage().contains(XML)){
					String filenamePrefix = SLASH+XML_FILE_PATH+SLASH; 
					String filenameSuffix = DOT+XML;
					filename = ex.getMessage().substring(ex.getMessage().lastIndexOf(filenamePrefix)+filenamePrefix.length(), ex.getMessage().lastIndexOf(filenameSuffix))+DOT+SQL;
				} else {
					filename = UUID.randomUUID().toString();
				}
				version = versionId;
			}
			aMigration.setScriptName(filename);
			aMigration.setFileName(filename);
			aMigration.setSuccess(Boolean.FALSE);
			aMigration.setLog(logMsg);
			aMigration.setAuthor(SYSTEM);
			aMigration.setVersion(version);
			aMigration.setMd5sum(UNKNOWN);	
			aMigration.setInstalledBy(SYSTEM);
			MigrationsCache.getInstance().setMigrationByBuildId(buildId, aMigration);
		}
	}
	
	private void iterateOnGroupedPatchset(ActivityType actType, List<List<PatchsetDescriptor>> groupedPatchsets, Long buildId, OracleDbEnvDTO dbDto) throws ExecutorException{
		IExecutor executor;
		for (List<PatchsetDescriptor> aPatchsetGroup : groupedPatchsets){
			if (aPatchsetGroup.size()==1 && PatchsetExecutionRole.SYSDBA.equals(aPatchsetGroup.get(0).getPatchsetRole())){
				//Operate con ruolo sysdba
				executor = getExecutor(dbDto, executorDAO, actType, Boolean.TRUE, buildId);
			} else {
				//Operate con ruolo normal
				executor = getExecutor(dbDto, executorDAO, actType, Boolean.FALSE, buildId);
			}
			if (ActivityType.DRYRUN.equals(actType)) {		
					executor.generateSQLChangeLog(aPatchsetGroup);
			} else if (ActivityType.SQL.equals(actType)) {
					executor.upgrade(aPatchsetGroup);
			} else {
				executor.addMetadata(aPatchsetGroup, buildId);
			}
		}
	}
	
	private void sendMailNotification(OracleDbEnvDTO dbDto, Migration aMigration) {
		User author = userService.getUserByLogin(aMigration.getAuthor());
		if (author == null) {
			author = new User();
			author.setFirstName(ADMIN);
			author.setEmail(applicationProperties.getMail().getBcc());
			author.setLangKey(IT_LANGUAGE);
		}
		Activity anActivity = new Activity();
		try {
			  anActivity = activityDAO.getActivity(aMigration.getVersion(), aMigration.getFileName(), aMigration.getSuccess());
		} catch (ActivityDAOException e) {
			throw new RuntimeException(EXC_CAUSE_MSG+". "+NO_ACTIVITY_FOUND_FOR_PATCHSET+": "+aMigration.getFileName());
		}
		mailService.sendMigrationResultMail(author, anActivity, dbDto);
	}
	
	
	@Override
	public boolean doClean(OracleDbEnvDTO dbDto, Long buildId) {
		boolean toRet = Boolean.FALSE;
		try {
			getCleanExecutor(dbDto).clean();
			toRet = Boolean.TRUE;
			persistActivity(dbDto, ActivityType.CLEAN, null, null, dbDto.getUser(), null, null, Boolean.TRUE, DB_CLEAN_SUCCESSFUL, buildId, EMPTY_STRING, EMPTY_STRING);	
		} catch (ExecutorException ex) {
			String causeMsg = ex.getCause() != null ? ex.getCause().getMessage() : null;
			String logMsg = ex.getMessage()+" ."+EXC_CAUSE_MSG+": "+causeMsg;
			log.error(logMsg);
			persistActivity(dbDto, ActivityType.CLEAN, null, null, dbDto.getUser(), null, null, Boolean.FALSE, logMsg, buildId, EMPTY_STRING, EMPTY_STRING);
			toRet = Boolean.FALSE;
		} finally {
			MigrationsCache.getInstance().cleanMigrations();
		}
		return toRet;
	}
	
	@Override
	@Transactional
	public boolean doTag(OracleDbEnvDTO dbDto, String version, Long buildId) {
		boolean persistOnDbFixerSchema = Boolean.FALSE;
		boolean persistOnTargetSchema = Boolean.FALSE;
		try {
			Long prodId = dbDto.getProductId();
			AliasDTO aliasDTO = getDbAliasesByEnvId(dbDto.getId());
			String coreSchemaName = aliasDTO.getCoreSchemaName();
			DbVersionMetadataDTO dbVersionMetadataDTO = this.findDbVersionMetadataByProductIdAndStageId(prodId, dbDto.getEnvType().getEnvType());
			dbVersionMetadataDTO.setVersion(version);
			DataSource ds = DbUtil.buildDataSource(dbDto, Boolean.FALSE);
			persistOnTargetSchema = executorDAO.insertMetadataOnTargetSchema(dbVersionMetadataDTO, coreSchemaName, ds) == 1;
			persistOnDbFixerSchema = persistActivity(dbDto, ActivityType.TAG, version, null, dbDto.getUser(), null, null, Boolean.TRUE, DB_TAG_SUCCESSFUL, buildId, EMPTY_STRING, EMPTY_STRING);
							
		} catch (MigrationDAOException ex) {
			String causeMsg = ex.getCause() != null ? ex.getCause().getMessage() : null;
			String logMsg = ex.getMessage()+" ."+EXC_CAUSE_MSG+": "+causeMsg;
			log.error(logMsg);
			persistActivity(dbDto, ActivityType.TAG, null, null, dbDto.getUser(), null, null, Boolean.FALSE, logMsg, buildId, EMPTY_STRING, EMPTY_STRING);
		} finally {
			MigrationsCache.getInstance().cleanMigrations();
		}
		return (persistOnDbFixerSchema && persistOnTargetSchema);
	}
	
	
	/**
	 * Persiste su schema del DBFixer l'esito dell'operazione eseguita sul DB target
	 * 
	 * @param dbDto coordinate del DB target
	 * @param actType il tipo di azione eseguita sul DB target
	 * @param version il tag di chiusura versione
	 * @param scriptName
	 * @param schemaName
	 * @param author
	 * @param desc
	 * @param success true in caso di azione eseguita correttamente
	 * @param logMsg le info a corredo dell'esecuzione
	 * @param buildId
	 * @param filename
	 * @param md5sum
	 * @return true se la persistenza su schema DBFixer ha avuto esito positivo
	 */
	private boolean persistActivity(OracleDbEnvDTO dbDto, ActivityType actType, String version, 
			String scriptName, String schemaName, String author, String desc, 
			boolean success, String logMsg, Long buildId, String filename, String md5sum){
		List<Activity> activities = new ArrayList<Activity>();
		Activity act = new Activity();
		act.setTargetEnvType(dbDto.getEnvType());
		Long custId;
		boolean toRet = Boolean.FALSE;
		try {
			custId = customerDAO.getCustomerByProductId(dbDto.getProductId()).getId();
			act.setTargetEnvType(dbDto.getEnvType());
			act.setCustomerId(custId);
			act.setProductId(dbDto.getProductId());
			act.setVersion(version);
			act.setActType(actType);
			act.setDescription(desc);
			act.setScriptName(scriptName);
			act.setAuthor(author);
			act.setUsername(schemaName);
			act.setApplyDate(Timestamp.valueOf(LocalDateTime.now()));
			act.setLog(logMsg);
			act.setBuildId(buildId);
			act.setSuccess(success);
			act.setL_filename(filename);
			act.setL_md5sum(md5sum);
			activities.add(act);
			toRet = getActivityDAO().setActivities(activities) == 1;
		} catch (MigrationDAOException | ActivityDAOException e) {
			log.error(EXC_GETTING_ACTIVITY_PERSIST+": "+e.getMessage());
			throw new RuntimeException(e);
		}
		return toRet;
	}
	
	
	/**
	 * Recupera l'istanza di un esecutore
	 * 
	 * @param dbDto coordinate del DB target
	 * @param metadataDAO il DAO per l'accesso ai metadati del motore di applicazione delle patches
	 * @param activityType il tipo di attività da eseguire
	 * @return l'istanza dell'esecutore
	 */
	private IExecutor getExecutor(OracleDbEnvDTO dbDto, ITargetDbMetadataDAO metadataDAO, ActivityType activityType, Long buildId) throws ExecutorException{
		return this.getExecutor(dbDto, metadataDAO, activityType, Boolean.FALSE, buildId);
	}
	
	/**
	 * Recupera l'istanza di un esecutore
	 * 
	 * @param dbDto coordinate del DB target
	 * @param metadataDAO il DAO per l'accesso ai metadati del motore di applicazione delle patches
	 * @param activityType il tipo di attività da eseguire
	 * @param asSys se true richiede esecuzione con privilegio SYSDBA
	 * @param buildId identificativo della build corrente
	 * @return l'istanza dell'esecutore
	 * @throws ExecutorException 
	 */
	private IExecutor getExecutor(OracleDbEnvDTO dbDto, ITargetDbMetadataDAO metadataDAO, ActivityType activityType, Boolean asSys, Long buildId) throws ExecutorException{
		DataSource ds = DbUtil.buildDataSource(dbDto, asSys);
		ProductDTO productDTO = getProductById(dbDto.getProductId());
		AliasDTO aliasDTO = getDbAliasesByEnvId(dbDto.getId());
		if (aliasDTO.getId() == null) {
			throw new RuntimeException("No DbAlias associated to DbEnvID: "+dbDto.getId());
		}
		CustomerDTO customerDTO = getCustomerById(productDTO.getCustomerId());
		List<CustomerDTO> supportedCustomers = getCustomers();
		
		return getExecFactory().createExecutor(ds, dbDto.getEnvType(), dbDto.getPatchsetdbrepourl(), metadataDAO, activityType, buildId, aliasDTO, productDTO, customerDTO, supportedCustomers);
	}
	
	/**
	 * Recupera l'istanza di un esecutore in grado di resettare il DB target
	 * 
	 * N.B.: da utilizzarsi solo in ambiente DEV
	 * @param dbDto coordinate del DB target
	 * @return l'istanza dell'esecutore
	 */
	private IExecutor getCleanExecutor(OracleDbEnvDTO dbDto){
		/**IExecutor executor = ExecutorFactory.getInstance().createCleanExecutor();
		DataSource ds = DbUtil.buildDataSource(dbDto.getUser(), dbDto.getPwd(), DbUtil.buildDbURL(dbDto));
		executor.setConfiguration(ExecutorConfigurationFactory.getInstance().createCleanExecutorConfiguration(ds));
		return executor;**/
		return null;
	}
	
	
	/**
	 * Converte un oggetto <code>DbVersionMetadata</code> nel corrispondente DTO
	 * 
	 * @param metadata il model da convertire
	 * @return il DTO convertito
	 */
	private DbVersionMetadataDTO convertDbVersionMetadataToDTO(DbVersionMetadata metadata) {
		DbVersionMetadataDTO toRet = new DbVersionMetadataDTO();
		toRet.setVersion(metadata.getVersion());
		toRet.setVersionAppliedDate(metadata.getVersionAppliedDate());
		toRet.setPatchset(metadata.getPatchset());
		toRet.setPatchsetAppliedDate(metadata.getPatchsetAppliedDate());		
		return toRet;
	}
	
	/**
	 * Converte una lista di attività di migrazione nei corrispondenti DTO
	 * 
	 * @param toConvert la lista delle attività
	 * @return la lista dei corrispondeti DTO
	 * @throws MigrationDAOException 
	 */
	private List<ActivityDTO> convertActivityToDTO(List<Activity> toConvert) throws MigrationDAOException{
		List<ActivityDTO> toRet = new ArrayList<ActivityDTO>();
		for (Activity act : toConvert){
			ActivityDTO newAct = new ActivityDTO();
			newAct.setId(act.getId());
			newAct.setBuildId(act.getBuildId());
			newAct.setTargetEnvType(act.getTargetEnvType());
			CustomerDTO custDTO = customerDAO.getCustomerById(act.getCustomerId());
			newAct.setCustomerName(custDTO.getName());
			ProductDTO prodDTO = productDAO.getProductFromId(act.getProductId());
			newAct.setProductName(prodDTO.getName());
			newAct.setVersion(act.getVersion());
			newAct.setActType(act.getActType().name());
			newAct.setDescription(act.getDescription());
			newAct.setScriptName(act.getScriptName());
			newAct.setUsername(act.getUsername());
			newAct.setAuthor(act.getAuthor());
			newAct.setChecksum(act.getChecksum());
			newAct.setApplyDate(act.getApplyDate());
			//TODO: modifica....
			newAct.setExecTime(act.getExecTime() == null ? new Time(0,0,0) : act.getExecTime());
			newAct.setLog(act.getLog());
			if (act.isSuccess())
				newAct.setSuccess(SUCCESS);
			else
				newAct.setSuccess(FAILURE);
			
			toRet.add(newAct);
		}
		return toRet;
	}
	
	@Override
	public IActivityDAO getActivityDAO(){
		return activityDAO;
	}


	@Override
	public IProductDAO getProductDAO() {
		return productDAO;
	}


	@Override
	public ICustomerDAO getCustomerDAO() {
		return customerDAO;
	}


	@Override
	public IDBEnvDAO getDbEnvDAO() {
		return dbEnvDAO;
	}

	public IAliasDAO getAliasDAO() {
		return aliasDAO;
	}

	public ExecutorBuilder getExecFactory() {
		return execFactory;
	}

	public IProductVersionDAO getProductVersionDAO() {
		return productVersionDAO;
	}

}

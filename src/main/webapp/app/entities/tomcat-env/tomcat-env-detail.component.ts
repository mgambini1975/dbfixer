import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TomcatEnv } from './tomcat-env.model';
import { TomcatEnvService } from './tomcat-env.service';

@Component({
    selector: 'jhi-tomcat-env-detail',
    templateUrl: './tomcat-env-detail.component.html'
})
export class TomcatEnvDetailComponent implements OnInit, OnDestroy {

    tomcatEnv: TomcatEnv;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private tomcatEnvService: TomcatEnvService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTomcatEnvs();
    }

    load(id) {
        this.tomcatEnvService.find(id).subscribe((tomcatEnv) => {
            this.tomcatEnv = tomcatEnv;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTomcatEnvs() {
        this.eventSubscriber = this.eventManager.subscribe(
            'tomcatEnvListModification',
            (response) => this.load(this.tomcatEnv.id)
        );
    }
}

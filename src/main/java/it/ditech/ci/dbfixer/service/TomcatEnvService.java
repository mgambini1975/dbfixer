package it.ditech.ci.dbfixer.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.ditech.ci.dbfixer.service.dto.TomcatEnvDTO;

/**
 * Service Interface for managing TomcatEnv.
 */
public interface TomcatEnvService {

    /**
     * Save a tomcatEnv.
     *
     * @param tomcatEnvDTO the entity to save
     * @return the persisted entity
     */
    TomcatEnvDTO save(TomcatEnvDTO tomcatEnvDTO);

    /**
     *  Get all the tomcatEnvs.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<TomcatEnvDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" tomcatEnv.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    TomcatEnvDTO findOne(Long id);

    /**
     *  Delete the "id" tomcatEnv.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}

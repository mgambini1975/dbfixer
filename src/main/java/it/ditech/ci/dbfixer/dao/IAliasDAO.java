package it.ditech.ci.dbfixer.dao;

import it.ditech.ci.dbfixer.dao.impl.MigrationDAOException;
import it.ditech.ci.dbfixer.service.dto.AliasDTO;

public interface IAliasDAO extends IGenericDAO {
	
	public AliasDTO getDbAliasesByEnvId(Long envId) throws MigrationDAOException;

}

package it.ditech.ci.dbfixer.service.util;

/**
 * Definisce una coppia di credenziali (user e pwd)
 * 
 * @author mgambini
 *
 */
public class Credentials {
	
	private String username;
	private String password;
	
	public Credentials(String usn, String pwd){
		this.username = usn;
		this.password = pwd;
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getPassword() {
		return password;
	}
	
}

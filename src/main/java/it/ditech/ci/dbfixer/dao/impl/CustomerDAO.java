package it.ditech.ci.dbfixer.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import it.ditech.ci.dbfixer.dao.ICustomerDAO;
import it.ditech.ci.dbfixer.service.dto.CustomerDTO;
import static it.ditech.ci.dbfixer.sql.SQLQueries.CUSTOMER_SELECT_ALL;
import static it.ditech.ci.dbfixer.sql.SQLQueries.CUSTOMER_SELECT_CUSTOMER_ID_BY_ID;
import static it.ditech.ci.dbfixer.sql.SQLQueries.CUSTOMER_SELECT_NAME_BY_ID;
import static it.ditech.ci.dbfixer.sql.SQLQueries.CUSTOMER_SELECT_ALL_BY_ID;

/**
 * Gestore della persistenza per i clienti
 * 
 * @author mgambini
 */
@Repository
public class CustomerDAO extends GenericDAO implements ICustomerDAO {

	private final Logger log = LoggerFactory.getLogger(CustomerDAO.class);
	
	@Override
	public List<CustomerDTO> getCustomers() throws MigrationDAOException {
		List<CustomerDTO> toRet = new ArrayList<CustomerDTO>();
		String sql = CUSTOMER_SELECT_ALL;		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {	
				CustomerDTO aCustomer = new CustomerDTO();
				aCustomer.setId(rs.getLong(1));
				aCustomer.setName(rs.getString(2));
				aCustomer.setDescription(rs.getString(3));
				toRet.add(aCustomer);
			}		
		} catch (SQLException e) {
			DAOExceptionHandler.handleMigrationDAOException(e, this.getClass());
		} finally {
			closeResourceGracefully(rs, ps, conn);
		}
		return toRet;
	}
	
	@Override
	public CustomerDTO getCustomerByProductId(Long productId) throws MigrationDAOException {
		String sql = CUSTOMER_SELECT_CUSTOMER_ID_BY_ID;	
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		CustomerDTO aCustomer = new CustomerDTO();
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setLong(1, new Long(productId));
			rs = ps.executeQuery();
			if (rs.next()) {	
				aCustomer.setId(rs.getLong(1));
			}
			
			sql = CUSTOMER_SELECT_NAME_BY_ID;
			ps = conn.prepareStatement(sql);
			ps.setLong(1, aCustomer.getId());
			rs = ps.executeQuery();
			if (rs.next()) {	
				aCustomer.setName(rs.getString(1));
			}
						
		} catch (SQLException e) {
			DAOExceptionHandler.handleMigrationDAOException(e, this.getClass());
		} finally {
			closeResourceGracefully(rs, ps, conn);
		}
		return aCustomer;
	}

	@Override
	public CustomerDTO getCustomerById(Long customerId) throws MigrationDAOException {
		String sql = CUSTOMER_SELECT_ALL_BY_ID;	
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		CustomerDTO aCustomer = null;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setLong(1, new Long(customerId));
			rs = ps.executeQuery();
			if (rs.next()) {
				aCustomer = new CustomerDTO();
				aCustomer.setId(rs.getLong(1));
				aCustomer.setName(rs.getString(2));
				aCustomer.setDescription(rs.getString(3));
			}
			
		} catch (SQLException e) {
			DAOExceptionHandler.handleMigrationDAOException(e, this.getClass());
		} finally {
			closeResourceGracefully(rs, ps, conn);
		}
		return aCustomer;
	}
}

package it.ditech.ci.dbfixer.executor;

import java.io.File;

/**
 * Descrittore di un patchset
 * 
 * @author mgambini
 */
public class PatchsetDescriptor {
	
	private int patchsetId;
	private File patchset;
	private PatchsetExecutionRole patchsetRole;
	
	/**
	 * Costruttore che accetta un identificativo di gruppo, il <code>File</code> del patchset ed il livello di privilegio 
	 * con cui deve essere eseguito
	 */
	public PatchsetDescriptor(int id, File patchset, PatchsetExecutionRole role){
		this.patchsetId = id;
		this.patchset = patchset;
		this.patchsetRole = role;
	}

	public int getPatchsetId() {
		return this.patchsetId;
	}

	public File getPatchset() {
		return this.patchset;
	}

	public PatchsetExecutionRole getPatchsetRole() {
		return this.patchsetRole;
	}

}

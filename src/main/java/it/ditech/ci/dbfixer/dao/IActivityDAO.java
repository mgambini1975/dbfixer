package it.ditech.ci.dbfixer.dao;

import java.util.List;

import it.ditech.ci.dbfixer.dao.impl.ActivityDAOException;
import it.ditech.ci.dbfixer.domain.Activity;
import it.ditech.ci.dbfixer.domain.DbVersionMetadata;

/**
 * Contratto DAO per la gestione dei metadati relativi alle migrazioni DB
 * 
 * @author agranifero
 * 
 */
public interface IActivityDAO extends IGenericDAO {
	
	/**
	 * Restituisce i dettagli relativi all'Activity identificata dall'ID passato in argomento
	 * 
	 * @param activityId identificativo dell'Activity
	 * @return i dettagli dell'Activity
	 * @throws ActivityDAOException
	 */
	public Activity getActivity(Long activityId) throws ActivityDAOException;
	
	/**
	 * Restituisce i dettagli relativi all'Activity identificata dalla versione di prodotto e 
	 * patchset filename passati in argomento
	 * 
	 * @param version identificativo della versione di prodotto
	 * @param fileName nome del patchset
	 * @return i dettagli dell'Activity
	 * @throws ActivityDAOException
	 */
	public Activity getActivity(String version, String fileName) throws ActivityDAOException;
	
	/**
	 * Restituisce i dettagli relativi all'Activity identificata dalla versione di prodotto, 
	 * patchset filename ed esito passati in argomento
	 * 
	 * @param version identificativo della versione di prodotto
	 * @param fileName nome del patchset
	 * @param success esito dell'esecuzione del patchset
	 * @return i dettagli dell'Activity
	 * @throws ActivityDAOException
	 */
	public Activity getActivity(String version, String fileName, Boolean success) throws ActivityDAOException;
	
	/**
	 * Restituisce i dettagli relativi alle Activity identificate dal prodotto e dallo stage passati in argomento
	 *  
	 * @param productId identificativo del prodotto
	 * @param StageId identificativo dello stage
	 * @return i dettagli delle Activity
	 * @throws ActivityDAOException
	 */
	public List<Activity> getActivities(Long productId, Long StageId) throws ActivityDAOException;
	
	/**
	 * Restituisce un descrittore di versione per il DB target identificato dal prodotto e dallo stage passati in argomento
	 * 
	 * @param productId identificativo del prodotto
	 * @param StageId identificativo dello stage
	 * @return il descrittore dell'ambiente DB
	 * @throws ActivityDAOException
	 */
	public DbVersionMetadata getDbVersionMetadata(Long productId, Long stageId) throws ActivityDAOException;
	
	/**
	 * Persiste la lista delle Activity passata in argomento
	 * 
	 * @param activities la lista delle Activity da persistere
	 * @return il numero delle Activity persistite
	 * @throws ActivityDAOException
	 */
	public int setActivities(List<Activity> activities) throws ActivityDAOException;
	
	
}

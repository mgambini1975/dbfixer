package it.ditech.ci.dbfixer.executor.callback;

import liquibase.changelog.ChangeSet;
import liquibase.changelog.ChangeSet.ExecType;
import liquibase.changelog.DatabaseChangeLog;
import liquibase.database.Database;

/**
 * Ancestor di una callback che risponde ad eventi Liquibase
 * 
 * @author mgambini
 *
 */
public abstract class LiquibaseCustomCallback extends AbstractCustomCallback implements ILiquibaseCustomCallback {

	LiquibaseCustomCallback(CallbackType type, boolean enabled, Long buildId) {
		super(type, enabled, buildId);
		
	}

	@Override
	public abstract void process(ChangeSet changeSet, DatabaseChangeLog databaseChangeLog, Database database, ExecType execType, Exception exception); 
	

}

package it.ditech.ci.dbfixer.domain;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import static it.ditech.ci.dbfixer.config.Constants.ORACLE_DRIVER_CLASSNAME;

/**
 * Factory di JDBC data source
 * @author mgambini
 *
 */
public class DataSourceFactory {
	
	private static final Logger log = LoggerFactory.getLogger(DataSourceFactory.class);
	
	
	public static DataSource getDataSource(DataSourceConfiguration dsConfig){
		return DataSourceBuilder.create()
				.username(dsConfig.getUsername())
				.password(dsConfig.getPassword())
				.url(dsConfig.getJdbcUrl())
				.driverClassName(ORACLE_DRIVER_CLASSNAME)
				.build();
	}

}

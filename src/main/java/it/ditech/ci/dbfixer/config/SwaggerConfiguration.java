package it.ditech.ci.dbfixer.config;

import static springfox.documentation.builders.PathSelectors.regex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.util.StopWatch;

import com.google.common.collect.Lists;

import io.github.jhipster.config.JHipsterProperties;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.GrantType;
import springfox.documentation.service.OAuth;
import springfox.documentation.service.ResourceOwnerPasswordCredentialsGrant;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Configurazione custom per Swagger doc
 * 
 * @author mgambini
 *
 */
@Configuration
@EnableSwagger2
@Profile("swaggerdoc")
public class SwaggerConfiguration {
	
	private final Logger log = LoggerFactory.getLogger(SwaggerConfiguration.class);
	
	private final JHipsterProperties jHipsterProperties;

    public SwaggerConfiguration(JHipsterProperties jHipsterProperties) {
        this.jHipsterProperties = jHipsterProperties;
    }
    
    
    private OAuth securitySchema() {

        List<AuthorizationScope> authorizationScopeList = new ArrayList<AuthorizationScope>();
        authorizationScopeList.add(new AuthorizationScope("read", "read all"));
        authorizationScopeList.add(new AuthorizationScope("trust", "trust all"));
        authorizationScopeList.add(new AuthorizationScope("write", "access all"));

        List<GrantType> grantTypes = new ArrayList<GrantType>();
        String baseUrl = jHipsterProperties.getMail().getBaseUrl();
        GrantType creGrant = new ResourceOwnerPasswordCredentialsGrant(baseUrl+"/api/authenticate");

        grantTypes.add(creGrant);

        return new OAuth("oauth2schema", authorizationScopeList, grantTypes);

    }
    

    @Bean
    SecurityConfiguration security() {
    	String secretKey = jHipsterProperties.getSecurity().getAuthentication().getJwt().getSecret();
    	return new SecurityConfiguration("test",secretKey,null,null,null,ApiKeyVehicle.HEADER,""," ");
    }

    
	@Bean
    public Docket api() { 
		log.debug("Starting Swagger");
		StopWatch watch = new StopWatch();
        watch.start();
        Contact contact = new Contact(
            jHipsterProperties.getSwagger().getContactName(),
            jHipsterProperties.getSwagger().getContactUrl(),
            jHipsterProperties.getSwagger().getContactEmail());

        ApiInfo apiInfo = new ApiInfo(
            jHipsterProperties.getSwagger().getTitle(),
            jHipsterProperties.getSwagger().getDescription(),
            jHipsterProperties.getSwagger().getVersion(),
            jHipsterProperties.getSwagger().getTermsOfServiceUrl(),
            contact,
            jHipsterProperties.getSwagger().getLicense(),
            jHipsterProperties.getSwagger().getLicenseUrl(),
            new ArrayList<>());
        
        
        
        return new Docket(DocumentationType.SWAGGER_2)  
        		.apiInfo(apiInfo)
        		.select()                                  
        			.apis(RequestHandlerSelectors.any())              
        			.paths(regex("/migrations-api.*")) 
        			.build()
        		.securityContexts(Arrays.asList(securityContext()))
        		.securitySchemes(Lists.newArrayList(securitySchema()));
        		
    }
	
	 private SecurityContext securityContext() {
		    return SecurityContext.builder()
		        .securityReferences(defaultAuth())
		        .forPaths(regex("/migrations-api.*"))
		        .build();
		  }

		  List<SecurityReference> defaultAuth() {
		    AuthorizationScope authorizationScope
		        = new AuthorizationScope("global", "accessEverything");
		    AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
		    authorizationScopes[0] = authorizationScope;
		    return new ArrayList<SecurityReference>(){{
		        new SecurityReference("oauth2schema", authorizationScopes);}};
		  }

}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ProductVersion } from './productversion.model';
import { ProductVersionPopupService } from './productversion-popup.service';
import { ProductVersionService } from './productversion.service';

@Component({
    selector: 'jhi-productversion-delete-dialog',
    templateUrl: './productversion-delete-dialog.component.html'
})
export class ProductVersionDeleteDialogComponent {

    productversion: ProductVersion;

    constructor(
        private productversionService: ProductVersionService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.productversionService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'productversionListModification',
                content: 'Deleted a product version'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-productversion-delete-popup',
    template: ''
})
export class ProductVersionDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private productversionPopupService: ProductVersionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.productversionPopupService
                .open(ProductVersionDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Alias } from './alias.model';
import { AliasPopupService } from './alias-popup.service';
import { AliasService } from './alias.service';

@Component({
    selector: 'jhi-alias-dialog',
    templateUrl: './alias-dialog.component.html'
})
export class AliasDialogComponent implements OnInit {

    alias: Alias;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private aliasService: AliasService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.alias.id !== undefined) {
            this.subscribeToSaveResponse(
                this.aliasService.update(this.alias));
        } else {
            this.subscribeToSaveResponse(
                this.aliasService.create(this.alias));
        }
    }

    private subscribeToSaveResponse(result: Observable<Alias>) {
        result.subscribe((res: Alias) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: Alias) {
        this.eventManager.broadcast({ name: 'aliasListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-alias-popup',
    template: ''
})
export class AliasPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private aliasPopupService: AliasPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.aliasPopupService
                    .open(AliasDialogComponent as Component, params['id']);
            } else {
                this.aliasPopupService
                    .open(AliasDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

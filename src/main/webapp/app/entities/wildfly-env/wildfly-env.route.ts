import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { WildflyEnvComponent } from './wildfly-env.component';
import { WildflyEnvDetailComponent } from './wildfly-env-detail.component';
import { WildflyEnvPopupComponent } from './wildfly-env-dialog.component';
import { WildflyEnvDeletePopupComponent } from './wildfly-env-delete-dialog.component';

export const wildflyEnvRoute: Routes = [
    {
        path: 'wildfly-env',
        component: WildflyEnvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.wildflyEnv.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'wildfly-env/:id',
        component: WildflyEnvDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.wildflyEnv.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const wildflyEnvPopupRoute: Routes = [
    {
        path: 'wildfly-env-new',
        component: WildflyEnvPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.wildflyEnv.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'wildfly-env/:id/edit',
        component: WildflyEnvPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.wildflyEnv.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'wildfly-env/:id/delete',
        component: WildflyEnvDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.wildflyEnv.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];

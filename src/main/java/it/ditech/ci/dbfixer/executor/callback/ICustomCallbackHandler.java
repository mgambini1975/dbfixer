package it.ditech.ci.dbfixer.executor.callback;

import java.util.List;

/**
 * Contratto per un gestore di callback degli eventi generati dal motore di applicazione delle patches
 * 
 * @author mgambini
 *
 */
public interface ICustomCallbackHandler {
	
	public boolean registerCallback(AbstractCustomCallback callback);
	
	public List<AbstractCustomCallback> getCallbacks();

}

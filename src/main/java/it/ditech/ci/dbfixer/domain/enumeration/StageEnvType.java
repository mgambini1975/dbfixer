package it.ditech.ci.dbfixer.domain.enumeration;

/**
 * Deployment env type enumeration.
 * 
 * DEV: ambiente di sviluppo
 * CI: ambiente per esecuzione test automatici
 * CQ: ambiente per esecuzione di test manuali
 * PREPROD: ambiente di preproduzione
 * PROD: ambiente di produzione 
 */
public enum StageEnvType {
    DEV(1), CI(2), CQ(3), PREPROD(4), PROD(5);
    
    private final long envType;
    
    StageEnvType(long type){
    	this.envType = type;
    }
    
    public long getEnvType(){
    	return this.envType;
    }
    
    public static StageEnvType valueOf(long code){
    	for (StageEnvType envType : StageEnvType.values()){
    		if (envType.getEnvType() == code)
    			return envType;
    	}
    	return null;
    }
    
    public static Long valueOf(StageEnvType envType){
    	Long toRet;
    	switch (envType) {
	    	case DEV:					toRet = new Long(1);
	    								break;
	    	case CI:					toRet = new Long(2);
	    								break;
	    	case CQ:					toRet = new Long(3);
	    								break;
	    	case PREPROD:				toRet = new Long(4);
	    								break;
	    	case PROD:					toRet = new Long(5);
	    								break;
	    	default:					toRet = null;
    	}
    	return toRet;
    }
}

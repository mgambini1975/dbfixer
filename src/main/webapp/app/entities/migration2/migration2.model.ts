import { BaseEntity } from './../../shared';

export class Migration2 implements BaseEntity {
    constructor(
        public id?: number,
        public buildId?: number,
        public targetEnvType?: string,
        public customerName?: string,
        public productName?: string,
        public version?: string,
        public actType?: string,
        public description?: string,
        public scriptName?: string,
        public username?: string,
        public author?: string,
        public checksum?: number,
        public applyDate?: Date,
        public execTime?: number,
        public log?: string,
        public success?: number
    ) {
    }
}

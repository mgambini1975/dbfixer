import { BaseEntity } from './../../shared';

const enum DEPLOYMENT_ENV_TYPE {
    'DEV',
    'CI',
    'CQ',
    'PREPROD',
    'PROD'
}

export class WildflyEnv implements BaseEntity {
    constructor(
        public id?: number,
        public envType?: DEPLOYMENT_ENV_TYPE,
        public hostname?: string,
        public httpPort?: number,
        public httpsPort?: number,
        public debugPort?: number,
        public contextRoot?: string,
        public product?: BaseEntity,
    ) {
    }
}

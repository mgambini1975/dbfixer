import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DbFixerSharedModule } from '../../shared';
import {
    OracleDbEnvService,
    OracleDbEnvPopupService,
    OracleDbEnvComponent,
    OracleDbEnvDetailComponent,
    OracleDbEnvDialogComponent,
    OracleDbEnvPopupComponent,
    OracleDbEnvDeletePopupComponent,
    OracleDbEnvDeleteDialogComponent,
    oracleDbEnvRoute,
    oracleDbEnvPopupRoute,
} from './';

const ENTITY_STATES = [
    ...oracleDbEnvRoute,
    ...oracleDbEnvPopupRoute,
];

@NgModule({
    imports: [
        DbFixerSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        OracleDbEnvComponent,
        OracleDbEnvDetailComponent,
        OracleDbEnvDialogComponent,
        OracleDbEnvDeleteDialogComponent,
        OracleDbEnvPopupComponent,
        OracleDbEnvDeletePopupComponent,
    ],
    entryComponents: [
        OracleDbEnvComponent,
        OracleDbEnvDialogComponent,
        OracleDbEnvPopupComponent,
        OracleDbEnvDeleteDialogComponent,
        OracleDbEnvDeletePopupComponent,
    ],
    providers: [
        OracleDbEnvService,
        OracleDbEnvPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DbFixerOracleDbEnvModule {}

package it.ditech.ci.dbfixer.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.ditech.ci.dbfixer.domain.WildflyEnv;
import it.ditech.ci.dbfixer.repository.WildflyEnvRepository;
import it.ditech.ci.dbfixer.service.WildflyEnvService;
import it.ditech.ci.dbfixer.service.dto.WildflyEnvDTO;
import it.ditech.ci.dbfixer.service.mapper.WildflyEnvMapper;


/**
 * Service Implementation for managing WildflyEnv.
 */
@Service
@Transactional
public class WildflyEnvServiceImpl implements WildflyEnvService{

    private final Logger log = LoggerFactory.getLogger(WildflyEnvServiceImpl.class);

    private final WildflyEnvRepository wildflyEnvRepository;

    private final WildflyEnvMapper wildflyEnvMapper;
    public WildflyEnvServiceImpl(WildflyEnvRepository wildflyEnvRepository, WildflyEnvMapper wildflyEnvMapper) {
        this.wildflyEnvRepository = wildflyEnvRepository;
        this.wildflyEnvMapper = wildflyEnvMapper;
    }

    /**
     * Save a wildflyEnv.
     *
     * @param wildflyEnvDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public WildflyEnvDTO save(WildflyEnvDTO wildflyEnvDTO) {
        log.debug("Request to save WildflyEnv : {}", wildflyEnvDTO);
        WildflyEnv wildflyEnv = wildflyEnvMapper.toEntity(wildflyEnvDTO);
        wildflyEnv = wildflyEnvRepository.save(wildflyEnv);
        return wildflyEnvMapper.toDto(wildflyEnv);
    }

    /**
     *  Get all the wildflyEnvs.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WildflyEnvDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WildflyEnvs");
        return wildflyEnvRepository.findAll(pageable)
            .map(wildflyEnvMapper::toDto);
    }

    /**
     *  Get one wildflyEnv by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public WildflyEnvDTO findOne(Long id) {
        log.debug("Request to get WildflyEnv : {}", id);
        WildflyEnv wildflyEnv = wildflyEnvRepository.findOne(id);
        return wildflyEnvMapper.toDto(wildflyEnv);
    }

    /**
     *  Delete the  wildflyEnv by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete WildflyEnv : {}", id);
        wildflyEnvRepository.delete(id);
    }
}

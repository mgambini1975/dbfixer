package it.ditech.ci.dbfixer.config;

import static it.ditech.ci.dbfixer.config.Constants.APPLICATION;
import static it.ditech.ci.dbfixer.config.Constants.EMPTY_STRING;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to JHipster.
 * <p>
 * Properties are configured in the application.yml file.
 */
@ConfigurationProperties(prefix = APPLICATION, ignoreUnknownFields = false)
public class ApplicationProperties {
	
	private final Mail mail = new Mail();
	private String gitUsername;
	private String gitPassword;
	private String engine;
	
	public Mail getMail() {
		return mail;
	}

	public static class Mail {
		
		private String bcc = EMPTY_STRING;

		public String getBcc() {
			return bcc;
		}

		public void setBcc(String bcc) {
			this.bcc = bcc;
		}	
	}

	public String getGitUsername() {
		return gitUsername;
	}

	public String getGitPassword() {
		return gitPassword;
	}

	public void setGitUsername(String gitUsername) {
		this.gitUsername = gitUsername;
	}

	public void setGitPassword(String gitPassword) {
		this.gitPassword = gitPassword;
	}

	public String getEngine() {
		return engine;
	}

	public void setEngine(String engine) {
		this.engine = engine;
	}

}

package it.ditech.ci.dbfixer.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import it.ditech.ci.dbfixer.domain.ProductVersion;
import it.ditech.ci.dbfixer.service.dto.ProductVersionDTO;

/**
 * Mapper for the entity ProductVersion and its DTO ProductVersionDTO.
 */
@Mapper(componentModel = "spring", uses = {ProductMapper.class, })
public interface ProductVersionMapper extends EntityMapper <ProductVersionDTO, ProductVersion> {
	
	 	@Mapping(source = "product.id", target = "productId")
	    ProductVersionDTO toDto(ProductVersion productVersion); 

	    @Mapping(source = "productId", target = "product")
	    ProductVersion toEntity(ProductVersionDTO productVersionDTO); 
	    default ProductVersion fromId(Long id) {
	        if (id == null) {
	            return null;
	        }
	        ProductVersion productVersion = new ProductVersion();
	        productVersion.setId(id);
	        return productVersion;
	    }

}

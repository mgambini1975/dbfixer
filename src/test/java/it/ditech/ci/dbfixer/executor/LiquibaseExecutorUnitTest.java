package it.ditech.ci.dbfixer.executor;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import it.ditech.ci.dbfixer.executor.configuration.LiquibaseExecutorConfiguration;
import liquibase.Liquibase;

public class LiquibaseExecutorUnitTest {
	
	private LiquibaseExecutor testingLiquibaseExec;
	
	@Mock
	Liquibase liquibaseMockInstance;
	@Mock
	LiquibaseExecutorConfiguration liquibaseExecConfig;
	
	@Before
    public void setup() {
		MockitoAnnotations.initMocks(this);
		testingLiquibaseExec = new LiquibaseExecutor(getLiquibaseMockInstance(), getLiquibaseExecConfig());
	}
		
	
	@Test
	public void getExecutorShouldReturnOneInstance() {
		assertThat(getTestingLiquibaseExec().getExecutor()).isInstanceOf(Liquibase.class);
	}
	
	@Test
	public void getExecutorConfigShouldReturnOneInstance() {
		assertThat(getTestingLiquibaseExec().getConfiguration()).isInstanceOf(LiquibaseExecutorConfiguration.class);
	}
	
	public LiquibaseExecutor getTestingLiquibaseExec() {
		return testingLiquibaseExec;
	}


	public Liquibase getLiquibaseMockInstance() {
		return liquibaseMockInstance;
	}


	public LiquibaseExecutorConfiguration getLiquibaseExecConfig() {
		return liquibaseExecConfig;
	}

}

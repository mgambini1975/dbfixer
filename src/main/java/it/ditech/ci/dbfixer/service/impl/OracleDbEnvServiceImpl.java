package it.ditech.ci.dbfixer.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.ditech.ci.dbfixer.domain.OracleDbEnv;
import it.ditech.ci.dbfixer.repository.OracleDbEnvRepository;
import it.ditech.ci.dbfixer.service.OracleDbEnvService;
import it.ditech.ci.dbfixer.service.dto.OracleDbEnvDTO;
import it.ditech.ci.dbfixer.service.mapper.OracleDbEnvMapper;


/**
 * Service Implementation for managing OracleDbEnv.
 */
@Service
@Transactional
public class OracleDbEnvServiceImpl implements OracleDbEnvService{

    private final Logger log = LoggerFactory.getLogger(OracleDbEnvServiceImpl.class);

    private final OracleDbEnvRepository oracleDbEnvRepository;

    private final OracleDbEnvMapper oracleDbEnvMapper;
    
    public OracleDbEnvServiceImpl(OracleDbEnvRepository oracleDbEnvRepository, OracleDbEnvMapper oracleDbEnvMapper) {
        this.oracleDbEnvRepository = oracleDbEnvRepository;
        this.oracleDbEnvMapper = oracleDbEnvMapper;
    }

    /**
     * Save a oracleDbEnv.
     *
     * @param oracleDbEnvDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public OracleDbEnvDTO save(OracleDbEnvDTO oracleDbEnvDTO) {
        log.debug("Request to save OracleDbEnv : {}", oracleDbEnvDTO);
        OracleDbEnv oracleDbEnv = oracleDbEnvMapper.toEntity(oracleDbEnvDTO);
        oracleDbEnv = oracleDbEnvRepository.save(oracleDbEnv);
        return oracleDbEnvMapper.toDto(oracleDbEnv);
    }

    /**
     *  Get all the oracleDbEnvs.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OracleDbEnvDTO> findAll(Pageable pageable) {
        log.debug("Request to get all OracleDbEnvs");
        return oracleDbEnvRepository.findAll(pageable)
            .map(oracleDbEnvMapper::toDto);
    }

    /**
     *  Get one oracleDbEnv by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public OracleDbEnvDTO findOne(Long id) {
        log.debug("Request to get OracleDbEnv : {}", id);
        OracleDbEnv oracleDbEnv = oracleDbEnvRepository.findOne(id);
        return oracleDbEnvMapper.toDto(oracleDbEnv);
    }

    /**
     *  Delete the  oracleDbEnv by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete OracleDbEnv : {}", id);
        oracleDbEnvRepository.delete(id);
    }
}

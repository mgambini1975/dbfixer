import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { WildflyEnv } from './wildfly-env.model';
import { WildflyEnvService } from './wildfly-env.service';

@Component({
    selector: 'jhi-wildfly-env-detail',
    templateUrl: './wildfly-env-detail.component.html'
})
export class WildflyEnvDetailComponent implements OnInit, OnDestroy {

    wildflyEnv: WildflyEnv;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private wildflyEnvService: WildflyEnvService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInWildflyEnvs();
    }

    load(id) {
        this.wildflyEnvService.find(id).subscribe((wildflyEnv) => {
            this.wildflyEnv = wildflyEnv;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInWildflyEnvs() {
        this.eventSubscriber = this.eventManager.subscribe(
            'wildflyEnvListModification',
            (response) => this.load(this.wildflyEnv.id)
        );
    }
}

import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { AliasComponent } from './alias.component';
import { AliasDetailComponent } from './alias-detail.component';
import { AliasPopupComponent } from './alias-dialog.component';
import { AliasDeletePopupComponent } from './alias-delete-dialog.component';

export const aliasRoute: Routes = [
    {
        path: 'alias',
        component: AliasComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.alias.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'alias/:id',
        component: AliasDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.alias.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const aliasPopupRoute: Routes = [
    {
        path: 'alias-new',
        component: AliasPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.alias.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'alias/:id/edit',
        component: AliasPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.alias.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'alias/:id/delete',
        component: AliasDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.alias.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];

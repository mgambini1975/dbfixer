package it.ditech.ci.dbfixer.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import it.ditech.ci.dbfixer.domain.WildflyEnv;
import it.ditech.ci.dbfixer.domain.enumeration.StageEnvType;
import it.ditech.ci.dbfixer.repository.WildflyEnvRepository;
import it.ditech.ci.dbfixer.web.rest.errors.ExceptionTranslator;
/**
 * Test class for the WildflyEnvResource REST controller.
 *
 * @see WildflyEnvResource
 */
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = DbFixerApp.class)
public class WildflyEnvResourceIntTest {

    private static final StageEnvType DEFAULT_ENV_TYPE = StageEnvType.DEV;
    private static final StageEnvType UPDATED_ENV_TYPE = StageEnvType.CI;

    private static final String DEFAULT_HOSTNAME = "AAAAAAAAAA";
    private static final String UPDATED_HOSTNAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_HTTP_PORT = 1;
    private static final Integer UPDATED_HTTP_PORT = 2;

    private static final Integer DEFAULT_HTTPS_PORT = 1;
    private static final Integer UPDATED_HTTPS_PORT = 2;

    private static final Integer DEFAULT_DEBUG_PORT = 1;
    private static final Integer UPDATED_DEBUG_PORT = 2;

    private static final String DEFAULT_CONTEXT_ROOT = "AAAAAAAAAA";
    private static final String UPDATED_CONTEXT_ROOT = "BBBBBBBBBB";

    @Autowired
    private WildflyEnvRepository wildflyEnvRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restWildflyEnvMockMvc;

    private WildflyEnv wildflyEnv;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final WildflyEnvResource wildflyEnvResource = new WildflyEnvResource(wildflyEnvRepository);
        this.restWildflyEnvMockMvc = MockMvcBuilders.standaloneSetup(wildflyEnvResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WildflyEnv createEntity(EntityManager em) {
        WildflyEnv wildflyEnv = new WildflyEnv()
            .envType(DEFAULT_ENV_TYPE)
            .hostname(DEFAULT_HOSTNAME)
            .httpPort(DEFAULT_HTTP_PORT)
            .httpsPort(DEFAULT_HTTPS_PORT)
            .debugPort(DEFAULT_DEBUG_PORT)
            .contextRoot(DEFAULT_CONTEXT_ROOT);
        return wildflyEnv;
    }

    @Before
    public void initTest() {
        wildflyEnv = createEntity(em);
    }

    //@Test
    @Transactional
    public void createWildflyEnv() throws Exception {
        int databaseSizeBeforeCreate = wildflyEnvRepository.findAll().size();

        // Create the WildflyEnv
        restWildflyEnvMockMvc.perform(post("/api/wildfly-envs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wildflyEnv)))
            .andExpect(status().isCreated());

        // Validate the WildflyEnv in the database
        List<WildflyEnv> wildflyEnvList = wildflyEnvRepository.findAll();
        assertThat(wildflyEnvList).hasSize(databaseSizeBeforeCreate + 1);
        WildflyEnv testWildflyEnv = wildflyEnvList.get(wildflyEnvList.size() - 1);
        assertThat(testWildflyEnv.getEnvType()).isEqualTo(DEFAULT_ENV_TYPE);
        assertThat(testWildflyEnv.getHostname()).isEqualTo(DEFAULT_HOSTNAME);
        assertThat(testWildflyEnv.getHttpPort()).isEqualTo(DEFAULT_HTTP_PORT);
        assertThat(testWildflyEnv.getHttpsPort()).isEqualTo(DEFAULT_HTTPS_PORT);
        assertThat(testWildflyEnv.getDebugPort()).isEqualTo(DEFAULT_DEBUG_PORT);
        assertThat(testWildflyEnv.getContextRoot()).isEqualTo(DEFAULT_CONTEXT_ROOT);
    }

    //@Test
    @Transactional
    public void createWildflyEnvWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = wildflyEnvRepository.findAll().size();

        // Create the WildflyEnv with an existing ID
        wildflyEnv.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restWildflyEnvMockMvc.perform(post("/api/wildfly-envs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wildflyEnv)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<WildflyEnv> wildflyEnvList = wildflyEnvRepository.findAll();
        assertThat(wildflyEnvList).hasSize(databaseSizeBeforeCreate);
    }

    //@Test
    @Transactional
    public void checkEnvTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = wildflyEnvRepository.findAll().size();
        // set the field null
        wildflyEnv.setEnvType(null);

        // Create the WildflyEnv, which fails.

        restWildflyEnvMockMvc.perform(post("/api/wildfly-envs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wildflyEnv)))
            .andExpect(status().isBadRequest());

        List<WildflyEnv> wildflyEnvList = wildflyEnvRepository.findAll();
        assertThat(wildflyEnvList).hasSize(databaseSizeBeforeTest);
    }

    //@Test
    @Transactional
    public void checkHostnameIsRequired() throws Exception {
        int databaseSizeBeforeTest = wildflyEnvRepository.findAll().size();
        // set the field null
        wildflyEnv.setHostname(null);

        // Create the WildflyEnv, which fails.

        restWildflyEnvMockMvc.perform(post("/api/wildfly-envs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wildflyEnv)))
            .andExpect(status().isBadRequest());

        List<WildflyEnv> wildflyEnvList = wildflyEnvRepository.findAll();
        assertThat(wildflyEnvList).hasSize(databaseSizeBeforeTest);
    }

    //@Test
    @Transactional
    public void checkHttpPortIsRequired() throws Exception {
        int databaseSizeBeforeTest = wildflyEnvRepository.findAll().size();
        // set the field null
        wildflyEnv.setHttpPort(null);

        // Create the WildflyEnv, which fails.

        restWildflyEnvMockMvc.perform(post("/api/wildfly-envs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wildflyEnv)))
            .andExpect(status().isBadRequest());

        List<WildflyEnv> wildflyEnvList = wildflyEnvRepository.findAll();
        assertThat(wildflyEnvList).hasSize(databaseSizeBeforeTest);
    }

    //@Test
    @Transactional
    public void getAllWildflyEnvs() throws Exception {
        // Initialize the database
        wildflyEnvRepository.saveAndFlush(wildflyEnv);

        // Get all the wildflyEnvList
        restWildflyEnvMockMvc.perform(get("/api/wildfly-envs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(wildflyEnv.getId().intValue())))
            .andExpect(jsonPath("$.[*].envType").value(hasItem(DEFAULT_ENV_TYPE.toString())))
            .andExpect(jsonPath("$.[*].hostname").value(hasItem(DEFAULT_HOSTNAME.toString())))
            .andExpect(jsonPath("$.[*].httpPort").value(hasItem(DEFAULT_HTTP_PORT)))
            .andExpect(jsonPath("$.[*].httpsPort").value(hasItem(DEFAULT_HTTPS_PORT)))
            .andExpect(jsonPath("$.[*].debugPort").value(hasItem(DEFAULT_DEBUG_PORT)))
            .andExpect(jsonPath("$.[*].contextRoot").value(hasItem(DEFAULT_CONTEXT_ROOT.toString())));
    }

    //@Test
    @Transactional
    public void getWildflyEnv() throws Exception {
        // Initialize the database
        wildflyEnvRepository.saveAndFlush(wildflyEnv);

        // Get the wildflyEnv
        restWildflyEnvMockMvc.perform(get("/api/wildfly-envs/{id}", wildflyEnv.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(wildflyEnv.getId().intValue()))
            .andExpect(jsonPath("$.envType").value(DEFAULT_ENV_TYPE.toString()))
            .andExpect(jsonPath("$.hostname").value(DEFAULT_HOSTNAME.toString()))
            .andExpect(jsonPath("$.httpPort").value(DEFAULT_HTTP_PORT))
            .andExpect(jsonPath("$.httpsPort").value(DEFAULT_HTTPS_PORT))
            .andExpect(jsonPath("$.debugPort").value(DEFAULT_DEBUG_PORT))
            .andExpect(jsonPath("$.contextRoot").value(DEFAULT_CONTEXT_ROOT.toString()));
    }

    //@Test
    @Transactional
    public void getNonExistingWildflyEnv() throws Exception {
        // Get the wildflyEnv
        restWildflyEnvMockMvc.perform(get("/api/wildfly-envs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    //@Test
    @Transactional
    public void updateWildflyEnv() throws Exception {
        // Initialize the database
        wildflyEnvRepository.saveAndFlush(wildflyEnv);
        int databaseSizeBeforeUpdate = wildflyEnvRepository.findAll().size();

        // Update the wildflyEnv
        WildflyEnv updatedWildflyEnv = wildflyEnvRepository.findOne(wildflyEnv.getId());
        updatedWildflyEnv
            .envType(UPDATED_ENV_TYPE)
            .hostname(UPDATED_HOSTNAME)
            .httpPort(UPDATED_HTTP_PORT)
            .httpsPort(UPDATED_HTTPS_PORT)
            .debugPort(UPDATED_DEBUG_PORT)
            .contextRoot(UPDATED_CONTEXT_ROOT);

        restWildflyEnvMockMvc.perform(put("/api/wildfly-envs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedWildflyEnv)))
            .andExpect(status().isOk());

        // Validate the WildflyEnv in the database
        List<WildflyEnv> wildflyEnvList = wildflyEnvRepository.findAll();
        assertThat(wildflyEnvList).hasSize(databaseSizeBeforeUpdate);
        WildflyEnv testWildflyEnv = wildflyEnvList.get(wildflyEnvList.size() - 1);
        assertThat(testWildflyEnv.getEnvType()).isEqualTo(UPDATED_ENV_TYPE);
        assertThat(testWildflyEnv.getHostname()).isEqualTo(UPDATED_HOSTNAME);
        assertThat(testWildflyEnv.getHttpPort()).isEqualTo(UPDATED_HTTP_PORT);
        assertThat(testWildflyEnv.getHttpsPort()).isEqualTo(UPDATED_HTTPS_PORT);
        assertThat(testWildflyEnv.getDebugPort()).isEqualTo(UPDATED_DEBUG_PORT);
        assertThat(testWildflyEnv.getContextRoot()).isEqualTo(UPDATED_CONTEXT_ROOT);
    }

    //@Test
    @Transactional
    public void updateNonExistingWildflyEnv() throws Exception {
        int databaseSizeBeforeUpdate = wildflyEnvRepository.findAll().size();

        // Create the WildflyEnv

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restWildflyEnvMockMvc.perform(put("/api/wildfly-envs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wildflyEnv)))
            .andExpect(status().isCreated());

        // Validate the WildflyEnv in the database
        List<WildflyEnv> wildflyEnvList = wildflyEnvRepository.findAll();
        assertThat(wildflyEnvList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    //@Test
    @Transactional
    public void deleteWildflyEnv() throws Exception {
        // Initialize the database
        wildflyEnvRepository.saveAndFlush(wildflyEnv);
        int databaseSizeBeforeDelete = wildflyEnvRepository.findAll().size();

        // Get the wildflyEnv
        restWildflyEnvMockMvc.perform(delete("/api/wildfly-envs/{id}", wildflyEnv.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<WildflyEnv> wildflyEnvList = wildflyEnvRepository.findAll();
        assertThat(wildflyEnvList).hasSize(databaseSizeBeforeDelete - 1);
    }

    //@Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(WildflyEnv.class);
        WildflyEnv wildflyEnv1 = new WildflyEnv();
        wildflyEnv1.setId(1L);
        WildflyEnv wildflyEnv2 = new WildflyEnv();
        wildflyEnv2.setId(wildflyEnv1.getId());
        assertThat(wildflyEnv1).isEqualTo(wildflyEnv2);
        wildflyEnv2.setId(2L);
        assertThat(wildflyEnv1).isNotEqualTo(wildflyEnv2);
        wildflyEnv1.setId(null);
        assertThat(wildflyEnv1).isNotEqualTo(wildflyEnv2);
    }
}

package it.ditech.ci.dbfixer.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.ditech.ci.dbfixer.domain.Alias;
import it.ditech.ci.dbfixer.repository.AliasRepository;
import it.ditech.ci.dbfixer.service.AliasService;
import it.ditech.ci.dbfixer.service.dto.AliasDTO;
import it.ditech.ci.dbfixer.service.mapper.AliasMapper;


/**
 * Service Implementation for managing Alias.
 */
@Service
@Transactional
public class AliasServiceImpl implements AliasService {

	 private final Logger log = LoggerFactory.getLogger(AliasServiceImpl.class);

	    private final AliasRepository aliasRepository;

	    private final AliasMapper aliasMapper;
	    public AliasServiceImpl(AliasRepository aliasRepository, AliasMapper aliasMapper) {
	        this.aliasRepository = aliasRepository;
	        this.aliasMapper = aliasMapper;
	    }

	    /**
	     * Save a alias.
	     *
	     * @param aliasDTO the entity to save
	     * @return the persisted entity
	     */
	    @Override
	    public AliasDTO save(AliasDTO aliasDTO) {
	        log.debug("Request to save Alias : {}", aliasDTO);
	        Alias alias = aliasMapper.toEntity(aliasDTO);
	        alias = aliasRepository.save(alias);
	        return aliasMapper.toDto(alias);
	    }

	    /**
	     *  Get all the aliass.
	     *
	     *  @param pageable the pagination information
	     *  @return the list of entities
	     */
	    @Override
	    @Transactional(readOnly = true)
	    public Page<AliasDTO> findAll(Pageable pageable) {
	        log.debug("Request to get all Aliass");
	        return aliasRepository.findAll(pageable)
	            .map(aliasMapper::toDto);
	    }

	    /**
	     *  Get one alias by id.
	     *
	     *  @param id the id of the entity
	     *  @return the entity
	     */
	    @Override
	    @Transactional(readOnly = true)
	    public AliasDTO findOne(Long id) {
	        log.debug("Request to get Alias : {}", id);
	        Alias alias = aliasRepository.findOne(id);
	        return aliasMapper.toDto(alias);
	    }

	    /**
	     *  Delete the  alias by id.
	     *
	     *  @param id the id of the entity
	     */
	    @Override
	    public void delete(Long id) {
	        log.debug("Request to delete Alias : {}", id);
	        aliasRepository.delete(id);
	    }
	

}

package it.ditech.ci.dbfixer.service.dto;


import java.io.Serializable;
import java.net.URL;
import java.util.Objects;

import javax.validation.constraints.NotNull;

import it.ditech.ci.dbfixer.domain.enumeration.StageEnvType;

/**
 * A DTO for the OracleDbEnv entity.
 */
public class OracleDbEnvDTO implements Serializable {

	private static final long serialVersionUID = 8293329920199914054L;

	private Long id;

    @NotNull
    private StageEnvType envType;

    @NotNull
    private String hostname;

    @NotNull
    private Integer listenerPort;

    @NotNull
    private String user;

    @NotNull
    private String pwd;
    
    private String sysPwd;

	@NotNull
    private String sid;


    @NotNull
    private Long productId;
    
    @NotNull
    private URL patchsetdbrepourl;
    
    private String env_desc;

    
    public URL getPatchsetdbrepourl() {
		return patchsetdbrepourl;
	}

	public void setPatchsetdbrepourl(URL patchsetDBRepoUrl) {
		this.patchsetdbrepourl = patchsetDBRepoUrl;
	}

	public String getEnv_desc() {
		return env_desc;
	}

	public void setEnv_desc(String env_desc) {
		this.env_desc = env_desc;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StageEnvType getEnvType() {
        return envType;
    }

    public void setEnvType(StageEnvType envType) {
        this.envType = envType;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public Integer getListenerPort() {
        return listenerPort;
    }

    public void setListenerPort(Integer listenerPort) {
        this.listenerPort = listenerPort;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }
    
    public String getSysPwd() {
		return sysPwd;
	}

	public void setSysPwd(String sysPwd) {
		this.sysPwd = sysPwd;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OracleDbEnvDTO oracleDbEnvDTO = (OracleDbEnvDTO) o;
        if(oracleDbEnvDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), oracleDbEnvDTO.getId());
    }
    

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OracleDbEnvDTO{" +
            "id=" + getId() +
            ", envType='" + getEnvType() + "'" +
            ", hostname='" + getHostname() + "'" +
            ", listenerPort='" + getListenerPort() + "'" +
            ", user='" + getUser() + "'" +
            ", pwd='" + "******" + "'" +
            ", sid='" + getSid() + "'" +
            ", env_desc'" + getEnv_desc()  + "'" +
            ", patchsetdbrepourl'" + getPatchsetdbrepourl()  + "'" +
            "}";
    }
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { WildflyEnv } from './wildfly-env.model';
import { WildflyEnvPopupService } from './wildfly-env-popup.service';
import { WildflyEnvService } from './wildfly-env.service';
import { Product, ProductService } from '../product';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-wildfly-env-dialog',
    templateUrl: './wildfly-env-dialog.component.html'
})
export class WildflyEnvDialogComponent implements OnInit {

    wildflyEnv: WildflyEnv;
    isSaving: boolean;

    products: Product[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private wildflyEnvService: WildflyEnvService,
        private productService: ProductService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.productService.query()
            .subscribe((res: ResponseWrapper) => { this.products = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.wildflyEnv.id !== undefined) {
            this.subscribeToSaveResponse(
                this.wildflyEnvService.update(this.wildflyEnv));
        } else {
            this.subscribeToSaveResponse(
                this.wildflyEnvService.create(this.wildflyEnv));
        }
    }

    private subscribeToSaveResponse(result: Observable<WildflyEnv>) {
        result.subscribe((res: WildflyEnv) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: WildflyEnv) {
        this.eventManager.broadcast({ name: 'wildflyEnvListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackProductById(index: number, item: Product) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-wildfly-env-popup',
    template: ''
})
export class WildflyEnvPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private wildflyEnvPopupService: WildflyEnvPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.wildflyEnvPopupService
                    .open(WildflyEnvDialogComponent as Component, params['id']);
            } else {
                this.wildflyEnvPopupService
                    .open(WildflyEnvDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

package it.ditech.ci.dbfixer.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "db_alias")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Alias implements Serializable {

	 private static final long serialVersionUID = 1021424795061391841L;

	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 private Long id;
	 
	 @NotNull
	 @Column(name = "env_id", nullable = false)
	 private Long dbEnvId;
	 
	 @OneToOne
	 @JsonIgnore
	 @PrimaryKeyJoinColumn
	 private OracleDbEnv dbEnv;
	 
	 @NotNull
	 @Column(name = "comm_name", nullable = true)
	 private String commSchemaName;
	 
	 @NotNull
	 @Column(name = "core_name", nullable = true)
	 private String coreSchemaName;
	 
	 @NotNull
	 @Column(name = "pu_name", nullable = true)
	 private String puSchemaName;
	 
	 @NotNull
	 @Column(name = "dcc_name", nullable = true)
	 private String dccSchemaName;
	 
	 @NotNull
	 @Column(name = "inte_name", nullable = true)
	 private String inteSchemaName;
	 
	 @NotNull
	 @Column(name = "share_name", nullable = true)
	 private String shareSchemaName;

	 @NotNull
	 @Column(name = "mntr_name", nullable = true)
	 private String mntrSchemaName;
	 
	 @NotNull
	 @Column(name = "schd_name", nullable = true)
	 private String schdSchemaName;
	 
	 @NotNull
	 @Column(name = "divu_name", nullable = true)
	 private String divuSchemaName;
	 
	 @NotNull
	 @Column(name = "logi_name", nullable = true)
	 private String logiSchemaName;
	 
	 @NotNull
	 @Column(name = "arch_name", nullable = true)
	 private String archSchemaName;
	 
	 @NotNull
	 @Column(name = "ecom_name", nullable = true)
	 private String ecomSchemaName;
	 
	 @NotNull
	 @Column(name = "cow_name", nullable = true)
	 private String cowSchemaName;
	 
	 @NotNull
	 @Column(name = "peng_name", nullable = true)
	 private String pengSchemaName;
	 
	 @NotNull
	 @Column(name = "cami_name", nullable = true)
	 private String camiSchemaName;
	 
	 @NotNull
	 @Column(name = "ibc_name", nullable = true)
	 private String ibcSchemaName;
	 
	 @NotNull
	 @Column(name = "fbdc_name", nullable = true)
	 private String fbdcSchemaName;
	 
	 @NotNull
	 @Column(name = "wst_name", nullable = true)
	 private String wstSchemaName;
	 
	 @NotNull
	 @Column(name = "scmso_name", nullable = true)
	 private String scmsoSchemaName;
	 
	 @Override
	 public boolean equals(Object o) {
	        if (this == o) {
	            return true;
	        }
	        if (o == null || getClass() != o.getClass()) {
	            return false;
	        }
	        Alias alias = (Alias) o;
	        if (alias.getId() == null || getId() == null) {
	            return false;
	        }
	        return Objects.equals(getId(), alias.getId());
	 }

	 @Override
	 public int hashCode() {
	        return Objects.hashCode(getId());
	 }

	 @Override
	 public String toString() {
	        return "Alias{" +
	            "id=" + getId() +
	            ", comm='" + getCommSchemaName() + "'" +
	            ", core='" + getCoreSchemaName() + "'" +
	            ", pu='" + getPuSchemaName() + "'" +
	            ", dcc='" + getDccSchemaName() + "'" +
	            ", inte='" + getInteSchemaName() + "'" +
	            ", share='" + getShareSchemaName() + "'" +
	            ", mntr='" + getMntrSchemaName() + "'" +
	            ", schd='" + getSchdSchemaName() + "'" +
	            ", divu='" + getDivuSchemaName() + "'" +
	            ", logi='" + getLogiSchemaName() + "'" +
	            ", arch='" + getArchSchemaName() + "'" +
	            ", ecom='" + getEcomSchemaName() + "'" +
	            ", cow='" + getCowSchemaName() + "'" +
	            ", peng='" + getPengSchemaName() + "'" +
	            ", cami='" + getCamiSchemaName() + "'" +
	            ", ibc='" + getIbcSchemaName() + "'" +
	            ", fbdc='" + getFbdcSchemaName() + "'" +
	            ", wst='" + getWstSchemaName() + "'" +
	            ", scmso='" + getScmsoSchemaName() + "'" +
	            "}";
	 }
	 
	 public Long getId() {
		 return id;
	 }

	 public void setId(Long id) {
		 this.id = id;
	 }

	public String getCommSchemaName() {
		return commSchemaName;
	}

	public void setCommSchemaName(String commSchemaName) {
		this.commSchemaName = commSchemaName;
	}

	public String getCoreSchemaName() {
		return coreSchemaName;
	}

	public void setCoreSchemaName(String coreSchemaName) {
		this.coreSchemaName = coreSchemaName;
	}

	public String getPuSchemaName() {
		return puSchemaName;
	}

	public void setPuSchemaName(String puSchemaName) {
		this.puSchemaName = puSchemaName;
	}

	public String getDccSchemaName() {
		return dccSchemaName;
	}

	public void setDccSchemaName(String dccSchemaName) {
		this.dccSchemaName = dccSchemaName;
	}

	public String getInteSchemaName() {
		return inteSchemaName;
	}

	public void setInteSchemaName(String inteSchemaName) {
		this.inteSchemaName = inteSchemaName;
	}

	public String getShareSchemaName() {
		return shareSchemaName;
	}

	public void setShareSchemaName(String shareSchemaName) {
		this.shareSchemaName = shareSchemaName;
	}

	public String getMntrSchemaName() {
		return mntrSchemaName;
	}

	public void setMntrSchemaName(String mntrSchemaName) {
		this.mntrSchemaName = mntrSchemaName;
	}

	public String getSchdSchemaName() {
		return schdSchemaName;
	}

	public void setSchdSchemaName(String schdSchemaName) {
		this.schdSchemaName = schdSchemaName;
	}

	public String getDivuSchemaName() {
		return divuSchemaName;
	}

	public void setDivuSchemaName(String divuSchemaName) {
		this.divuSchemaName = divuSchemaName;
	}

	public String getLogiSchemaName() {
		return logiSchemaName;
	}

	public void setLogiSchemaName(String logiSchemaName) {
		this.logiSchemaName = logiSchemaName;
	}

	public String getArchSchemaName() {
		return archSchemaName;
	}

	public void setArchSchemaName(String archSchemaName) {
		this.archSchemaName = archSchemaName;
	}

	public Long getDbEnvId() {
		return dbEnvId;
	}

	public void setDbEnvId(Long dbEnvId) {
		this.dbEnvId = dbEnvId;
	}

	public OracleDbEnv getDbEnv() {
		return dbEnv;
	}

	public void setDbEnv(OracleDbEnv dbEnv) {
		this.dbEnv = dbEnv;
	}

	public String getEcomSchemaName() {
		return ecomSchemaName;
	}

	public void setEcomSchemaName(String ecomSchemaName) {
		this.ecomSchemaName = ecomSchemaName;
	}

	public String getCowSchemaName() {
		return cowSchemaName;
	}

	public void setCowSchemaName(String cowSchemaName) {
		this.cowSchemaName = cowSchemaName;
	}

	public String getPengSchemaName() {
		return pengSchemaName;
	}

	public void setPengSchemaName(String pengSchemaName) {
		this.pengSchemaName = pengSchemaName;
	}

	public String getCamiSchemaName() {
		return camiSchemaName;
	}

	public void setCamiSchemaName(String camiSchemaName) {
		this.camiSchemaName = camiSchemaName;
	}

	public String getIbcSchemaName() {
		return ibcSchemaName;
	}

	public void setIbcSchemaName(String ibcSchemaName) {
		this.ibcSchemaName = ibcSchemaName;
	}

	public String getFbdcSchemaName() {
		return fbdcSchemaName;
	}

	public void setFbdcSchemaName(String fbdcSchemaName) {
		this.fbdcSchemaName = fbdcSchemaName;
	}

	public String getWstSchemaName() {
		return wstSchemaName;
	}

	public void setWstSchemaName(String wstSchemaName) {
		this.wstSchemaName = wstSchemaName;
	}

	public String getScmsoSchemaName() {
		return scmsoSchemaName;
	}

	public void setScmsoSchemaName(String scmsoSchemaName) {
		this.scmsoSchemaName = scmsoSchemaName;
	}

}

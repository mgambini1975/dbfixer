import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Migration } from './migration.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class MigrationService {

    private migrationsResourceUrl = 'migrations-api/activities';
 
    constructor(private http: Http) { }

    find(id: number): Observable<Migration> {
        return this.http.get(`${this.migrationsResourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }


    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.migrationsResourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(migration: Migration): Migration {
        const copy: Migration = Object.assign({}, migration);
        return copy;
    }
}

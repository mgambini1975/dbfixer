import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { OracleDbEnv } from './oracle-db-env.model';
import { OracleDbEnvService } from './oracle-db-env.service';

@Injectable()
export class OracleDbEnvPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private oracleDbEnvService: OracleDbEnvService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.oracleDbEnvService.find(id).subscribe((oracleDbEnv) => {
                    this.ngbModalRef = this.oracleDbEnvModalRef(component, oracleDbEnv);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.oracleDbEnvModalRef(component, new OracleDbEnv());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    oracleDbEnvModalRef(component: Component, oracleDbEnv: OracleDbEnv): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.oracleDbEnv = oracleDbEnv;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}

package it.ditech.ci.dbfixer.dao;

import java.util.List;

import it.ditech.ci.dbfixer.dao.impl.MigrationDAOException;
import it.ditech.ci.dbfixer.service.dto.ProductVersionDTO;

/**
 * Contratto DAO per la gestione delle versioni di prodotto
 * 
 * author @mgambini
 */
public interface IProductVersionDAO extends IGenericDAO {
	
	/**
	 * Restituisce l'elenco delle versioni di prodotto per l'ID prodotto passato in argomento
	 * @param productId id del prodotto
	 * @return l'elenco delle versioni di prodotto 
	 */
	public List<ProductVersionDTO> getProductVersionsByProductId(Long productId) throws MigrationDAOException;
	
	/**
	 * Restituisce le info della versione corrispondenti all'ID passato in argomento 
	 * @param productVersionId l'ID di versione
	 * @return le info sulla versione
	 * @throws MigrationDAOException
	 */
	public ProductVersionDTO getProductVersionById(Long productVersionId) throws MigrationDAOException;

}

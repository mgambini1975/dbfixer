import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { Message } from 'primeng/primeng';
import { Migration2 } from './migration2.model';
import { Migration2Service } from './migration2.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import {BlockUIModule} from 'primeng/primeng';


@Component({
    selector: 'jhi-migration2',
    templateUrl: './migration2.component.html',
    providers: [Migration2Service]
})
export class Migration2Component implements OnInit, OnDestroy {

    migrations: Migration2[];
    currentAccount: any;
    msgs: Message[] = [];
    serverMessage: string;
    blockedDocument: boolean = false;

    constructor(
        private migrationService: Migration2Service,
        private principal: Principal
    ) {
        // ....
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
       
    }
    
    private loadAll(): void {
     console.log('Load All Activities...');
     this.blockedDocument = true;
     this.migrationService.loadActivities(this.msgs).then(
	     attivita => {
	        this.migrations = attivita;
	        this.blockedDocument = false;
	      },
	      serverMessage =>  this.serverMessage = this.serverMessage + ' - ' + <any>serverMessage);
    }

    ngOnDestroy() {
       
    }

    
}

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DbFixerSharedModule } from '../../shared';
import {
    AliasService,
    AliasPopupService,
    AliasComponent,
    AliasDetailComponent,
    AliasDialogComponent,
    AliasPopupComponent,
    AliasDeletePopupComponent,
    AliasDeleteDialogComponent,
    aliasRoute,
    aliasPopupRoute,
} from './';

const ENTITY_STATES = [
    ...aliasRoute,
    ...aliasPopupRoute,
];

@NgModule({
    imports: [
        DbFixerSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AliasComponent,
        AliasDetailComponent,
        AliasDialogComponent,
        AliasDeleteDialogComponent,
        AliasPopupComponent,
        AliasDeletePopupComponent,
    ],
    entryComponents: [
        AliasComponent,
        AliasDialogComponent,
        AliasPopupComponent,
        AliasDeleteDialogComponent,
        AliasDeletePopupComponent,
    ],
    providers: [
        AliasService,
        AliasPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DbFixerAliasModule {}

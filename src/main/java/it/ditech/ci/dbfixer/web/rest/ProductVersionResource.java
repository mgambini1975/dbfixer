package it.ditech.ci.dbfixer.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import it.ditech.ci.dbfixer.domain.ProductVersion;
import it.ditech.ci.dbfixer.repository.ProductVersionRepository;
import it.ditech.ci.dbfixer.web.rest.util.HeaderUtil;
import it.ditech.ci.dbfixer.web.rest.util.PaginationUtil;

/**
 * REST controller for managing ProductVersion.
 */
@RestController
@RequestMapping("/api")
public class ProductVersionResource {
	
	 private final Logger log = LoggerFactory.getLogger(ProductVersionResource.class);

	    private static final String ENTITY_NAME = "productversion";
	    
	    private final ProductVersionRepository productVersionRepository;
	    
	    public ProductVersionResource(ProductVersionRepository productVersionRepository) {
	        this.productVersionRepository = productVersionRepository;
	    }

	    /**
	     * POST  /productVersions : Create a new productVersion.
	     *
	     * @param productVersion the productVersion to create
	     * @return the ResponseEntity with status 201 (Created) and with body the new productVersion, or with status 400 (Bad Request) if the productVersion has already an ID
	     * @throws URISyntaxException if the Location URI syntax is incorrect
	     */
	    @PostMapping("/productVersions")
	    @Timed
	    public ResponseEntity<ProductVersion> createProductVersion(@Valid @RequestBody ProductVersion productVersion) throws URISyntaxException {
	        log.debug("REST request to save ProductVersion : {}", productVersion);
	        if (productVersion.getId() != null) {
	            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new productVersion cannot already have an ID")).body(null);
	        }
	        ProductVersion result = productVersionRepository.save(productVersion);
	        return ResponseEntity.created(new URI("/api/productVersions/" + result.getId()))
	            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
	            .body(result);
	    }

	    /**
	     * PUT  /productVersions : Updates an existing productVersion.
	     *
	     * @param productVersion the productVersion to update
	     * @return the ResponseEntity with status 200 (OK) and with body the updated productVersion,
	     * or with status 400 (Bad Request) if the productVersion is not valid,
	     * or with status 500 (Internal Server Error) if the productVersion couldn't be updated
	     * @throws URISyntaxException if the Location URI syntax is incorrect
	     */
	    @PutMapping("/productVersions")
	    @Timed
	    public ResponseEntity<ProductVersion> updateProductVrsion(@Valid @RequestBody ProductVersion productVersion) throws URISyntaxException {
	        log.debug("REST request to update ProductVersion : {}", productVersion);
	        if (productVersion.getId() == null) {
	            return createProductVersion(productVersion);
	        }
	        ProductVersion result = productVersionRepository.save(productVersion);
	        return ResponseEntity.ok()
	            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, productVersion.getId().toString()))
	            .body(result);
	    }

	    /**
	     * GET  /productVersions : get all the productVersions.
	     *
	     * @param pageable the pagination information
	     * @return the ResponseEntity with status 200 (OK) and the list of productVersions in body
	     */
	    @GetMapping("/productVersions")
	    @Timed
	    public ResponseEntity<List<ProductVersion>> getAllProductVersions(@ApiParam Pageable pageable) {
	        log.debug("REST request to get a page of ProductVersions");
	        Page<ProductVersion> page = productVersionRepository.findAll(pageable);
	        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/productVersions");
	        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	    }

	    /**
	     * GET  /productVersions/:id : get the "id" productVersion.
	     *
	     * @param id the id of the productVersion to retrieve
	     * @return the ResponseEntity with status 200 (OK) and with body the productVersion, or with status 404 (Not Found)
	     */
	    @GetMapping("/productVersions/{id}")
	    @Timed
	    public ResponseEntity<ProductVersion> getProductVersion(@PathVariable Long id) {
	        log.debug("REST request to get ProductVersion : {}", id);
	        ProductVersion productVersion = productVersionRepository.findOne(id);
	        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(productVersion));
	    }

	    /**
	     * DELETE  /productVersions/:id : delete the "id" productVersion.
	     *
	     * @param id the id of the productVersion to delete
	     * @return the ResponseEntity with status 200 (OK)
	     */
	    @DeleteMapping("/productVersions/{id}")
	    @Timed
	    public ResponseEntity<Void> deleteProductVersion(@PathVariable Long id) {
	        log.debug("REST request to delete ProductVersion : {}", id);
	        productVersionRepository.delete(id);
	        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	    }

}

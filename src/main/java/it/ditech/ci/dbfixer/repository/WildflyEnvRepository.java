package it.ditech.ci.dbfixer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.ditech.ci.dbfixer.domain.WildflyEnv;


/**
 * Spring Data JPA repository for the WildflyEnv entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WildflyEnvRepository extends JpaRepository<WildflyEnv, Long> {

}

/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { DbFixerTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { WildflyEnvDetailComponent } from '../../../../../../main/webapp/app/entities/wildfly-env/wildfly-env-detail.component';
import { WildflyEnvService } from '../../../../../../main/webapp/app/entities/wildfly-env/wildfly-env.service';
import { WildflyEnv } from '../../../../../../main/webapp/app/entities/wildfly-env/wildfly-env.model';

describe('Component Tests', () => {

    describe('WildflyEnv Management Detail Component', () => {
        let comp: WildflyEnvDetailComponent;
        let fixture: ComponentFixture<WildflyEnvDetailComponent>;
        let service: WildflyEnvService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DbFixerTestModule],
                declarations: [WildflyEnvDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    WildflyEnvService,
                    JhiEventManager
                ]
            }).overrideTemplate(WildflyEnvDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(WildflyEnvDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(WildflyEnvService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new WildflyEnv(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.wildflyEnv).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});

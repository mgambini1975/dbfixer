package it.ditech.ci.dbfixer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.ditech.ci.dbfixer.domain.TomcatEnv;


/**
 * Spring Data JPA repository for the TomcatEnv entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TomcatEnvRepository extends JpaRepository<TomcatEnv, Long> {

}

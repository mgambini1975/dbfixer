package it.ditech.ci.dbfixer.executor.configuration;

import java.net.URL;
import java.util.List;

import javax.sql.DataSource;

import it.ditech.ci.dbfixer.dao.ITargetDbMetadataDAO;
import it.ditech.ci.dbfixer.domain.enumeration.StageEnvType;
import it.ditech.ci.dbfixer.service.dto.AliasDTO;
import it.ditech.ci.dbfixer.service.dto.CustomerDTO;
import it.ditech.ci.dbfixer.service.dto.ProductDTO;

/**
 * Placeholder per contratto di configurazione di un esecutore
 * @author mgambini
 *
 */
public interface IExecutorConfiguration {
	
	public DataSource getDatasource();
	
	public URL getRepoUrl();

	public StageEnvType getStageEnvType();
	
	public ProductDTO getProductDTO();
	
	public CustomerDTO getCustomerDTO();
	
	public String getBasePath();
	
	public ITargetDbMetadataDAO getMetadataDAO();
	
	public AliasDTO getAliasDTO();
	
	public List<CustomerDTO> getSupportedCustomers();
}

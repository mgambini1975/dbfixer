package it.ditech.ci.dbfixer.executor.callback;

import liquibase.changelog.ChangeSet;
import liquibase.changelog.ChangeSet.ExecType;
import liquibase.changelog.DatabaseChangeLog;
import liquibase.database.Database;

/**
 * Contratto per una callback di risposta agli eventi di Liquibase
 * 
 * @author mgambini
 *
 */
public interface ILiquibaseCustomCallback extends ICustomCallback {
	
	public void process(ChangeSet changeSet, DatabaseChangeLog databaseChangeLog, Database database, ExecType execType, Exception exception);

}

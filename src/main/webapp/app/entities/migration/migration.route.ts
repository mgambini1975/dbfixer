import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { MigrationComponent } from './migration.component';
import { MigrationDetailComponent } from './migration-detail.component';

export const migrationRoute: Routes = [
    {
        path: 'migration',
        component: MigrationComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.migration.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'migration/:id',
        component: MigrationDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dbFixerApp.migration.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

--liquibase formatted sql
--changeset mgambini:creazione tabella db_alias

-- --------------------------------------------------------

--
-- Struttura della tabella `db_alias`
--

CREATE TABLE IF NOT EXISTS `db_alias` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `env_id` bigint(20) DEFAULT NULL, 
  `comm_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `core_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `pu_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `dcc_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `inte_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `share_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `mntr_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `schd_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `divu_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `logi_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `arch_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `ecom_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `cow_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `peng_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `cami_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `ibc_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `fbdc_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `wst_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `scmso_name` varchar(255) COLLATE utf8_bin NOT NULL,
 
  PRIMARY KEY (`id`),
  KEY `fk_db_alias_product_id` (`env_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

import { BaseEntity } from './../../shared';

export class Version implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public desc?: string,
	public product?: BaseEntity,
    ) {
    }
}

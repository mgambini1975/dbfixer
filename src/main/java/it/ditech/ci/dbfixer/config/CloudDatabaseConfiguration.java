package it.ditech.ci.dbfixer.config;

import static it.ditech.ci.dbfixer.config.Constants.DBFIXER_DS_NAME;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.Cloud;
import org.springframework.cloud.CloudFactory;
import org.springframework.cloud.config.java.AbstractCloudConfig;
import org.springframework.cloud.service.ServiceInfo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import static it.ditech.ci.dbfixer.config.Constants.CLOUD;


@Configuration
@Profile(CLOUD)
public class CloudDatabaseConfiguration extends AbstractCloudConfig {

    private final Logger log = LoggerFactory.getLogger(CloudDatabaseConfiguration.class);
    

    @Bean(name=DBFIXER_DS_NAME)
    @Primary
    public DataSource dataSource() {
        log.info("Configuring JDBC datasource from a cloud provider");
        //return connectionFactory().dataSource();
        CloudFactory cloudFactory = new CloudFactory();
        Cloud cloud = cloudFactory.getCloud();
        ServiceInfo serviceInfo = cloud.getServiceInfo("DbFixer-DB");
        
        return cloud.getServiceConnector(serviceInfo.getId(), DataSource.class, null);
    }
}

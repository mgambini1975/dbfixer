package it.ditech.ci.dbfixer.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import it.ditech.ci.dbfixer.domain.enumeration.StageEnvType;

/**
 * A WildflyEnv.
 */
@Entity
@Table(name = "wildfly_env")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class WildflyEnv implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "env_type", nullable = false)
    private StageEnvType envType;

    @NotNull
    @Column(name = "hostname", nullable = false)
    private String hostname;

    @NotNull
    @Column(name = "http_port", nullable = false)
    private Integer httpPort;

    @Column(name = "https_port")
    private Integer httpsPort;

    @Column(name = "debug_port")
    private Integer debugPort;

    @Column(name = "context_root")
    private String contextRoot;

    @ManyToOne
    private Product product;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StageEnvType getEnvType() {
        return envType;
    }

    public WildflyEnv envType(StageEnvType envType) {
        this.envType = envType;
        return this;
    }

    public void setEnvType(StageEnvType envType) {
        this.envType = envType;
    }

    public String getHostname() {
        return hostname;
    }

    public WildflyEnv hostname(String hostname) {
        this.hostname = hostname;
        return this;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public Integer getHttpPort() {
        return httpPort;
    }

    public WildflyEnv httpPort(Integer httpPort) {
        this.httpPort = httpPort;
        return this;
    }

    public void setHttpPort(Integer httpPort) {
        this.httpPort = httpPort;
    }

    public Integer getHttpsPort() {
        return httpsPort;
    }

    public WildflyEnv httpsPort(Integer httpsPort) {
        this.httpsPort = httpsPort;
        return this;
    }

    public void setHttpsPort(Integer httpsPort) {
        this.httpsPort = httpsPort;
    }

    public Integer getDebugPort() {
        return debugPort;
    }

    public WildflyEnv debugPort(Integer debugPort) {
        this.debugPort = debugPort;
        return this;
    }

    public void setDebugPort(Integer debugPort) {
        this.debugPort = debugPort;
    }

    public String getContextRoot() {
        return contextRoot;
    }

    public WildflyEnv contextRoot(String contextRoot) {
        this.contextRoot = contextRoot;
        return this;
    }

    public void setContextRoot(String contextRoot) {
        this.contextRoot = contextRoot;
    }

    public Product getProduct() {
        return product;
    }

    public WildflyEnv product(Product product) {
        this.product = product;
        return this;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WildflyEnv wildflyEnv = (WildflyEnv) o;
        if (wildflyEnv.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wildflyEnv.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WildflyEnv{" +
            "id=" + getId() +
            ", envType='" + getEnvType() + "'" +
            ", hostname='" + getHostname() + "'" +
            ", httpPort='" + getHttpPort() + "'" +
            ", httpsPort='" + getHttpsPort() + "'" +
            ", debugPort='" + getDebugPort() + "'" +
            ", contextRoot='" + getContextRoot() + "'" +
            "}";
    }
}

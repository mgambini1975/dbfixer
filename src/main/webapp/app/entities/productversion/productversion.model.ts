import { BaseEntity } from './../../shared';

export class ProductVersion implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public product?: BaseEntity,
       
    ) {
    }
}

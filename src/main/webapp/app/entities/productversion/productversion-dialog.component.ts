import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ProductVersion } from './productversion.model';
import { ProductVersionPopupService } from './productversion-popup.service';
import { ProductVersionService } from './productversion.service';
import { Product, ProductService } from '../product';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-productversion-dialog',
    templateUrl: './productversion-dialog.component.html'
})
export class ProductVersionDialogComponent implements OnInit {

    productversion: ProductVersion;
    isSaving: boolean;

    products: Product[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private productversionService: ProductVersionService,
        private productService: ProductService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.productService.query()
            .subscribe((res: ResponseWrapper) => { this.products = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.productversion.id !== undefined) {
            this.subscribeToSaveResponse(
                this.productversionService.update(this.productversion));
        } else {
            this.subscribeToSaveResponse(
                this.productversionService.create(this.productversion));
        }
    }

    private subscribeToSaveResponse(result: Observable<ProductVersion>) {
        result.subscribe((res: ProductVersion) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: ProductVersion) {
        this.eventManager.broadcast({ name: 'productversionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackProductById(index: number, item: Product) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-productversion-popup',
    template: ''
})
export class ProductVersionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private productversionPopupService: ProductVersionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.productversionPopupService
                    .open(ProductVersionDialogComponent as Component, params['id']);
            } else {
                this.productversionPopupService
                    .open(ProductVersionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

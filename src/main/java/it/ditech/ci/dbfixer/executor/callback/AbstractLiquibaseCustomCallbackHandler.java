package it.ditech.ci.dbfixer.executor.callback;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Ancestor per la gestione delle callback che rispondono agli eventi del motore di applicazione delle patches
 * 
 * @author mgambini
 *
 */
public class AbstractLiquibaseCustomCallbackHandler implements ICustomCallbackHandler  {
	
	private final Logger log = LoggerFactory.getLogger(AbstractLiquibaseCustomCallbackHandler.class);
	
	private List<AbstractCustomCallback> callbacks = new ArrayList<AbstractCustomCallback>();
	
	/**
	 * Aggiunge la callback passata in argomento
	 * 
	 * @param callback la callback da registrare
	 * @return true in caso di registrazione avveuta con successo
	 */
	@Override
	public boolean registerCallback(AbstractCustomCallback callback) {
		return getCallbacks().add(callback);
	}
	
	@Override
	public List<AbstractCustomCallback> getCallbacks() {
		return this.callbacks;
	}


}

package it.ditech.ci.dbfixer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.ditech.ci.dbfixer.domain.OracleDbEnv;


/**
 * Spring Data JPA repository for the OracleDbEnv entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OracleDbEnvRepository extends JpaRepository<OracleDbEnv, Long> {

}

package it.ditech.ci.dbfixer.executor;

/**
 * Tipologie di operazioni
 * 
 * @author mgambini
 *
 */
public enum ExecutorOperation {
	CONFIGURE, CLEAN, MIGRATE, REPAIR, ADDMETADATA, GENERATESQL, CLONE_REPO, DELETE_REPO

}

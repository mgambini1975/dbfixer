import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DbFixerSharedModule } from '../../shared';
import {
    TomcatEnvService,
    TomcatEnvPopupService,
    TomcatEnvComponent,
    TomcatEnvDetailComponent,
    TomcatEnvDialogComponent,
    TomcatEnvPopupComponent,
    TomcatEnvDeletePopupComponent,
    TomcatEnvDeleteDialogComponent,
    tomcatEnvRoute,
    tomcatEnvPopupRoute,
} from './';

const ENTITY_STATES = [
    ...tomcatEnvRoute,
    ...tomcatEnvPopupRoute,
];

@NgModule({
    imports: [
        DbFixerSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        TomcatEnvComponent,
        TomcatEnvDetailComponent,
        TomcatEnvDialogComponent,
        TomcatEnvDeleteDialogComponent,
        TomcatEnvPopupComponent,
        TomcatEnvDeletePopupComponent,
    ],
    entryComponents: [
        TomcatEnvComponent,
        TomcatEnvDialogComponent,
        TomcatEnvPopupComponent,
        TomcatEnvDeleteDialogComponent,
        TomcatEnvDeletePopupComponent,
    ],
    providers: [
        TomcatEnvService,
        TomcatEnvPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DbFixerTomcatEnvModule {}

/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { DbFixerTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { TomcatEnvDetailComponent } from '../../../../../../main/webapp/app/entities/tomcat-env/tomcat-env-detail.component';
import { TomcatEnvService } from '../../../../../../main/webapp/app/entities/tomcat-env/tomcat-env.service';
import { TomcatEnv } from '../../../../../../main/webapp/app/entities/tomcat-env/tomcat-env.model';

describe('Component Tests', () => {

    describe('TomcatEnv Management Detail Component', () => {
        let comp: TomcatEnvDetailComponent;
        let fixture: ComponentFixture<TomcatEnvDetailComponent>;
        let service: TomcatEnvService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DbFixerTestModule],
                declarations: [TomcatEnvDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    TomcatEnvService,
                    JhiEventManager
                ]
            }).overrideTemplate(TomcatEnvDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TomcatEnvDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TomcatEnvService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new TomcatEnv(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.tomcatEnv).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});

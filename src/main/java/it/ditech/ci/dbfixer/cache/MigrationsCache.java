package it.ditech.ci.dbfixer.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.ditech.ci.dbfixer.domain.Migration;

/**
 * Singleton cache delle migrations indicizzate per schema
 * @author mgambini
 *
 */
public class MigrationsCache {
	
	private final static MigrationsCache instance = new MigrationsCache();
	
	private final Map<Long,List<Migration>> registry;
	
	private MigrationsCache(){
		this.registry = new HashMap<Long, List<Migration>>(); 
	}
	
	public static MigrationsCache getInstance(){
		return instance;
	}
	
	private Map<Long,List<Migration>> getRegistry(){
		return this.registry;
	}
	
	/**
	 * Recupera la lista delle migrazioni corrispondenti al buildId passato in argomento 
	 * @param buildId l'identificativo della build corrente
	 * @return la lista delle migrazioni
	 */
	public List<Migration> getMigrationsByBuildId(Long buildId){
		return getRegistry().get(buildId);
	}
	
	/**
	 * Aggiunge alla cache la migrazione avente per chiave il buildId passato in argomento
	 * @param buildId l'identificativo della build corrente
	 * @param migrations la lista delle migrazioni
	 */
	public void setMigrationByBuildId(Long buildId, Migration migration){
		if (getRegistry().get(buildId) == null)
			getRegistry().put(buildId, new ArrayList<Migration>());
		getRegistry().get(buildId).add(migration);
	}
	
	/**
	 * Rimuove dalla cache l'insieme delle migrazioni avente per chiave l'identificativo della build passato in argomento
	 * @param buildId l'identificativo della build
	 */
	public void removeMigrationByBuildId(Long buildId){
		if(getRegistry().get(buildId) != null){
			getRegistry().remove(buildId);
		}
	}

	/**
	 * Ripulisce la cache
	 */
	public void cleanMigrations(){
		getRegistry().clear();
	}
	
}

package it.ditech.ci.dbfixer.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import it.ditech.ci.dbfixer.dao.IDBEnvDAO;
import it.ditech.ci.dbfixer.domain.enumeration.StageEnvType;
import it.ditech.ci.dbfixer.service.dto.OracleDbEnvDTO;

import static it.ditech.ci.dbfixer.sql.SQLQueries.DBENV_SELECT_ALL;
import static it.ditech.ci.dbfixer.sql.SQLQueries.DBENV_SELECT_ALL_BY_ID;

/**
 * Gestore della persistenza per gli ambienti DB
 * 
 * @author mgambini
 */
@Repository
public class DBEnvDAO extends GenericDAO implements IDBEnvDAO {

	private final Logger log = LoggerFactory.getLogger(DBEnvDAO.class);
	
	@Override
	public OracleDbEnvDTO getDbEnvByProductAndStage(Long productId, Long stageId) throws MigrationDAOException {
		OracleDbEnvDTO aDbEnv = new OracleDbEnvDTO();
		String sql = DBENV_SELECT_ALL;
		if (productId != null || stageId != null){
			sql = sql + " WHERE ";
		}
		if (productId != null){
			sql = sql + "PRODUCT_ID = ?";
		}
		if (productId != null && stageId != null){
			sql = sql + " AND ";
		}
		if (stageId != null){
			sql = sql + "ENV_TYPE = ?";
		}
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(sql);
			if (productId != null && stageId == null)
				ps.setLong(1, productId);
			if (productId == null && stageId != null)
				ps.setString(1, StageEnvType.valueOf(stageId).name());
			if (productId != null && stageId != null){
				ps.setLong(1, productId);
				StageEnvType envType = StageEnvType.valueOf(stageId);
				if (envType == null) {
					String environments = "";
					for (StageEnvType env :StageEnvType.values()) {
						environments = environments + env.getEnvType()+"("+env.name()+"), ";
					}
					String errorMsg = "StageID not found: "+stageId+". Stage IDs available: "+environments;
					log.error(errorMsg);
					throw new MigrationDAOException(errorMsg);
				}
				ps.setString(2, envType.name());
			}
			rs = ps.executeQuery();
			if (rs.next()) {	
				log.debug("Found DbEnv with ID: "+rs.getLong(1)+" for ProductID: "+productId+" and StageID: "+stageId);
				aDbEnv = new OracleDbEnvDTO();
				aDbEnv.setId(rs.getLong(1));
				aDbEnv.setEnvType(StageEnvType.valueOf(rs.getString(2)));
				aDbEnv.setHostname(rs.getString(3));
				aDbEnv.setListenerPort(rs.getInt(4));
				aDbEnv.setUser(rs.getString(5));
				aDbEnv.setPwd(rs.getString(6));
				aDbEnv.setSysPwd(rs.getString(7));
				aDbEnv.setSid(rs.getString(8));
				aDbEnv.setProductId(rs.getLong(9));
				aDbEnv.setEnv_desc(rs.getString(10));
				aDbEnv.setPatchsetdbrepourl(rs.getURL(11));
			} else {
				log.debug("No DbEnv found for ProductID: "+productId+" and StageID: "+stageId);
			}
			
		} catch (SQLException e) {
			DAOExceptionHandler.handleMigrationDAOException(e, this.getClass());
		} finally {
			closeResourceGracefully(rs, ps, conn);
		}
		return aDbEnv;
	}

	@Override
	public OracleDbEnvDTO getDbEnvById(Long envId) throws MigrationDAOException {
		OracleDbEnvDTO aDbEnv = new OracleDbEnvDTO();
		String sql = DBENV_SELECT_ALL_BY_ID;		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = dataSource.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setLong(1, envId);
			rs = ps.executeQuery();
			
			if (rs.next()) {	
				aDbEnv.setId(rs.getLong(1));
				aDbEnv.setEnvType(StageEnvType.valueOf(rs.getString(2)));
				aDbEnv.setHostname(rs.getString(3));
				aDbEnv.setListenerPort(rs.getInt(4));
				aDbEnv.setUser(rs.getString(5));
				aDbEnv.setPwd(rs.getString(6));
				aDbEnv.setSysPwd(rs.getString(7));
				aDbEnv.setSid(rs.getString(8));
				aDbEnv.setProductId(rs.getLong(9));
				aDbEnv.setEnv_desc(rs.getString(10));
				aDbEnv.setPatchsetdbrepourl(rs.getURL(11));
				
			}
			
		} catch (SQLException e) {
			DAOExceptionHandler.handleMigrationDAOException(e, this.getClass());
		} finally {
			closeResourceGracefully(rs, ps, conn);
		}
		return aDbEnv;
	}

}

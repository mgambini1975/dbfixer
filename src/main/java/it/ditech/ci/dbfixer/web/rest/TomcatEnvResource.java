package it.ditech.ci.dbfixer.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import it.ditech.ci.dbfixer.domain.TomcatEnv;
import it.ditech.ci.dbfixer.repository.TomcatEnvRepository;
import it.ditech.ci.dbfixer.web.rest.util.HeaderUtil;
import it.ditech.ci.dbfixer.web.rest.util.PaginationUtil;

/**
 * REST controller for managing TomcatEnv.
 */
@RestController
@RequestMapping("/api")
public class TomcatEnvResource {

    private final Logger log = LoggerFactory.getLogger(TomcatEnvResource.class);

    private static final String ENTITY_NAME = "tomcatEnv";

    private final TomcatEnvRepository tomcatEnvRepository;
    public TomcatEnvResource(TomcatEnvRepository tomcatEnvRepository) {
        this.tomcatEnvRepository = tomcatEnvRepository;
    }

    /**
     * POST  /tomcat-envs : Create a new tomcatEnv.
     *
     * @param tomcatEnv the tomcatEnv to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tomcatEnv, or with status 400 (Bad Request) if the tomcatEnv has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tomcat-envs")
    @Timed
    public ResponseEntity<TomcatEnv> createTomcatEnv(@Valid @RequestBody TomcatEnv tomcatEnv) throws URISyntaxException {
        log.debug("REST request to save TomcatEnv : {}", tomcatEnv);
        if (tomcatEnv.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new tomcatEnv cannot already have an ID")).body(null);
        }
        TomcatEnv result = tomcatEnvRepository.save(tomcatEnv);
        return ResponseEntity.created(new URI("/api/tomcat-envs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tomcat-envs : Updates an existing tomcatEnv.
     *
     * @param tomcatEnv the tomcatEnv to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tomcatEnv,
     * or with status 400 (Bad Request) if the tomcatEnv is not valid,
     * or with status 500 (Internal Server Error) if the tomcatEnv couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tomcat-envs")
    @Timed
    public ResponseEntity<TomcatEnv> updateTomcatEnv(@Valid @RequestBody TomcatEnv tomcatEnv) throws URISyntaxException {
        log.debug("REST request to update TomcatEnv : {}", tomcatEnv);
        if (tomcatEnv.getId() == null) {
            return createTomcatEnv(tomcatEnv);
        }
        TomcatEnv result = tomcatEnvRepository.save(tomcatEnv);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tomcatEnv.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tomcat-envs : get all the tomcatEnvs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of tomcatEnvs in body
     */
    @GetMapping("/tomcat-envs")
    @Timed
    public ResponseEntity<List<TomcatEnv>> getAllTomcatEnvs(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of TomcatEnvs");
        Page<TomcatEnv> page = tomcatEnvRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tomcat-envs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /tomcat-envs/:id : get the "id" tomcatEnv.
     *
     * @param id the id of the tomcatEnv to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tomcatEnv, or with status 404 (Not Found)
     */
    @GetMapping("/tomcat-envs/{id}")
    @Timed
    public ResponseEntity<TomcatEnv> getTomcatEnv(@PathVariable Long id) {
        log.debug("REST request to get TomcatEnv : {}", id);
        TomcatEnv tomcatEnv = tomcatEnvRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tomcatEnv));
    }

    /**
     * DELETE  /tomcat-envs/:id : delete the "id" tomcatEnv.
     *
     * @param id the id of the tomcatEnv to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tomcat-envs/{id}")
    @Timed
    public ResponseEntity<Void> deleteTomcatEnv(@PathVariable Long id) {
        log.debug("REST request to delete TomcatEnv : {}", id);
        tomcatEnvRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}

package it.ditech.ci.dbfixer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import it.ditech.ci.dbfixer.domain.Authority;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}

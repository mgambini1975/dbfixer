package it.ditech.ci.dbfixer.dao.impl;

public class ActivityDAOException extends Exception {

	private static final long serialVersionUID = -7178230665833612325L;

	public ActivityDAOException(Throwable cause){
		super("Exception operating on DbFixer internal schema: ", cause);
	}
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Migration2 } from './migration2.model';
import { Migration2Service } from './migration2.service';

@Component({
    selector: 'jhi-migration-detail',
    templateUrl: './migration2-detail.component.html'
})
export class Migration2DetailComponent implements OnInit, OnDestroy {

    migration: Migration2;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private migrationService: Migration2Service,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInMigrations();
    }

    load(id) {
        this.migrationService.find(id).subscribe((migration) => {
            this.migration = migration;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInMigrations() {
        this.eventSubscriber = this.eventManager.subscribe(
            'migrationListModification',
            (response) => this.load(this.migration.id)
        );
    }
}

import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { Message } from 'primeng/primeng';
import { Operation } from './operation.model';
import { Version } from './version.model';
import { OperationType } from './operationType.model';
import { OperationService } from './operation.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { BlockUIModule } from 'primeng/primeng';
import {DataTableModule} from 'primeng/primeng';
import {PanelModule} from 'primeng/primeng';
import {Product} from '../product/product.model';
import { OracleDbEnv } from '../oracle-db-env/oracle-db-env.model';
import {ConfirmDialogModule} from 'primeng/primeng';
import {ConfirmationService} from 'primeng/components/common/api';


@Component({
    selector: 'jhi-operation',
    templateUrl: './operation.component.html',
    providers: [OperationService, ConfirmationService]
})
export class OperationComponent implements OnInit, OnDestroy {

    operations: Operation[];
    currentAccount: any;
    msgs: Message[] = [];
    serverMessage: string;
    blockedDocument: boolean;
    contextRestApi: String = '';
    displayFgTipoAggiornamento: boolean = false;
    displayFgSingleUpdate: boolean = false;
    displayFgTagVersion: boolean = false;
    displayFgFix: boolean = false;

    displaySuccessMessage: boolean;
    displayFailureMessage: boolean;
    displayGenericFailureMessage: boolean;
    displayRunningMessage: boolean;

    selectedProdotto: string = '';
    selectedAmbiente: string = '';
    selectedVersione: string = '';
    selectedOperazione: string = '';
    selectedTipoAggiornamento: string = '';
    patchId: string = '';
    tagName: string = '';
    metadataOnly: boolean = true;
    activityName: string = '';

    prodotti: Product[] = [];
    ambienti: OracleDbEnv[];
    ambientiCached: OracleDbEnv[];
    versioni: Version[];
    versioniCached: Version[];
    operazioni: OperationType[];


    constructor(
    	private confirmationService: ConfirmationService,
        private operationService: OperationService,
        private principal: Principal,
    ) {
    	//Nothing todo here...
    }

    ngOnInit() {
    	//this.selectedProdotto = new Product(-1, "Seleziona un prodotto...");
        this.loadProdotti();
        this.loadAmbienti();
        this.loadVersioni();
        this.loadOperazioni();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
    }

    public filterAmbientiAndVersioni(filterVal: any){
    	if (parseInt(filterVal) !== 0){
    		this.ambienti = this.ambientiCached.filter((item) => { return item.product.id === parseInt(filterVal) });
		this.versioni = this.versioniCached.filter((item) => { return item.product.id === parseInt(filterVal) });
    	}
    }


    public onChangeOperazione(filterVal: any){
    	if (parseInt(filterVal) === 1){
    		this.displayFgTipoAggiornamento = true;
    	} else {
    		this.displayFgTipoAggiornamento = false;
    		this.displayFgSingleUpdate = false;
    	}
    	if (parseInt(filterVal) === 3){
    		this.displayFgFix = true;
    	} else {
    		this.displayFgFix = false;
    	}
    	if (parseInt(filterVal) === 4){
    		this.displayFgTagVersion = true;
    	} else {
    		this.displayFgTagVersion = false;
    	}
    }

    public onChangeTipoAggiornamento(filterVal: any){
    	if (parseInt(filterVal) === 1){
    		this.displayFgSingleUpdate = true;
    	} else {
    		this.displayFgSingleUpdate = false;
    	}
    }


    private loadProdotti(): void {
      console.log('Load prodotti...');
      this.operationService.loadProdotti(this.msgs).then(
	     prodotti => {
	     	this.prodotti = prodotti;
	        this.blockedDocument = false;
	      },
	      serverMessage =>  this.serverMessage = this.serverMessage + ' - ' + <any>serverMessage);
    }

    private loadAmbienti(): void {
      console.log('Load ambienti...');
      this.operationService.loadAmbienti(this.msgs).then(
	     ambienti => {
	        this.ambienti = ambienti;
	        this.ambientiCached = ambienti;
	        this.blockedDocument = false;
	      },
	      serverMessage =>  this.serverMessage = this.serverMessage + ' - ' + <any>serverMessage);
    }

    private loadVersioni(): void {
      console.log('Load versioni...');
      this.operationService.loadVersioni(this.msgs).then(
	     versioni => {
	        this.versioni = versioni;
		this.versioniCached = versioni;
	        this.blockedDocument = false;
	      },
	      serverMessage =>  this.serverMessage = this.serverMessage + ' - ' + <any>serverMessage);
    }


    private loadOperazioni(): void {
      console.log('Load operazioni...');
      /*this.operationService.loadOperazioni(this.msgs).then(
	     operazioni => {
	        this.operazioni = operazioni;
	        this.blockedDocument = false;
	      },
	      serverMessage =>  this.serverMessage = this.serverMessage + ' - ' + <any>serverMessage);*/
	  this.operazioni = [{"id":1, "name":"UPGRADE", "desc":"Aggiornamento"},
		{"id":2, "name":"EMULATE", "desc":"Simulazione"},
		{"id":3, "name":"FIX", "desc":"Fix metadata"},
		{"id":4, "name":"TAG", "desc":"Tag version"}];
    }

  	public handleOperation(){
  		this.displaySuccessMessage = false;
  		this.displayFailureMessage = false;
  		this.displayGenericFailureMessage = false;

  		if (this.validate()){
  			console.log("Validazione OK");
  			if(parseInt(this.selectedOperazione) === 1){
  				this.doUpgradeRequest();
  			} else if (parseInt(this.selectedOperazione) === 2){
  				this.doEmulateRequest();
			} else if (parseInt(this.selectedOperazione) === 3){
  				this.doFixRequest();
			} else if (parseInt(this.selectedOperazione) === 4){
  				this.doTagRequest();
			}

			else {
  				alert("Non hai i privilegi necessari per eseguire questa operazione");
  			}
  		} else {
  			console.log("Validazione KO");
  		}
  	}

	private doEmulateRequest(): void {
		this.msgs = [];
  		this.blockedDocument = true;
  		this.displayRunningMessage = true;
		this.operationService.doEmulateRequest(this.selectedProdotto,
  			this.selectedAmbiente,
  			this.selectedVersione,
			this.msgs).then(
		result => {
			this.blockedDocument = false;
		     	//alert(result);
		     	if (result === undefined){
		     		this.displayRunningMessage = false;
		     		this.serverMessage = this.msgs[0].detail;
		     		this.displayGenericFailureMessage = true;
		     	} else if (result){
		     			this.displayRunningMessage = false;
		     			this.displaySuccessMessage = true;
		     			this.displayGenericFailureMessage = false;
		     		} else {
		     			this.displayRunningMessage = false;
		     			this.displayFailureMessage = true;
		     			this.displayGenericFailureMessage = false;
		     		}
	      },
	      serverMessage =>  {
	      	this.blockedDocument = false;
	      	this.displayRunningMessage = false;
	      	this.displayFailureMessage = false;
	      	this.displayGenericFailureMessage = true;
	      	this.serverMessage = this.serverMessage + ' - ' + <any>serverMessage.description;

	      })
	}

	private doTagRequest(): void {
		this.msgs = [];
  		this.blockedDocument = true;
  		this.displayRunningMessage = true;
		this.operationService.doTagRequest(this.selectedProdotto,
  			this.selectedAmbiente,
  			this.selectedVersione,
			this.msgs).then(
		result => {
			this.blockedDocument = false;
		     	//alert(result);
		     	if (result === undefined){
		     		this.displayRunningMessage = false;
		     		this.serverMessage = this.msgs[0].detail;
		     		this.displayGenericFailureMessage = true;
		     	} else if (result){
		     			this.displayRunningMessage = false;
		     			this.displaySuccessMessage = true;
		     			this.displayGenericFailureMessage = false;
		     		} else {
		     			this.displayRunningMessage = false;
		     			this.displayFailureMessage = true;
		     			this.displayGenericFailureMessage = false;
		     		}
	      },
	      serverMessage =>  {
	      	this.blockedDocument = false;
	      	this.displayRunningMessage = false;
	      	this.displayFailureMessage = false;
	      	this.displayGenericFailureMessage = true;
	      	this.serverMessage = this.serverMessage + ' - ' + <any>serverMessage.description;

	      })
	}

	private doFixRequest(): void {
		this.msgs = [];
  		this.blockedDocument = true;
  		this.displayRunningMessage = true;
		this.operationService.doFixRequest(this.selectedProdotto,
  			this.selectedAmbiente,
  			this.selectedVersione,
			this.activityName,
			this.msgs).then(
		result => {
			this.blockedDocument = false;
		     	//alert(result);
		     	if (result === undefined){
		     		this.displayRunningMessage = false;
		     		this.serverMessage = this.msgs[0].detail;
		     		this.displayGenericFailureMessage = true;
		     	} else if (result){
		     			this.displayRunningMessage = false;
		     			this.displaySuccessMessage = true;
		     			this.displayGenericFailureMessage = false;
		     		} else {
		     			this.displayRunningMessage = false;
		     			this.displayFailureMessage = true;
		     			this.displayGenericFailureMessage = false;
		     		}
	      },
	      serverMessage =>  {
	      	this.blockedDocument = false;
	      	this.displayRunningMessage = false;
	      	this.displayFailureMessage = false;
	      	this.displayGenericFailureMessage = true;
	      	this.serverMessage = this.serverMessage + ' - ' + <any>serverMessage.description;

	      })

	}

  	private doUpgradeRequest(): void {
  		this.msgs = [];
  		this.blockedDocument = true;
  		this.displayRunningMessage = true;
  		this.operationService.doUpgradeRequest(this.selectedProdotto,
  			this.selectedAmbiente,
  			this.selectedVersione,
  			this.selectedTipoAggiornamento,
  			this.patchId,
  			this.metadataOnly,
  			this.msgs).then(
	     result => {
	        this.blockedDocument = false;
	     	//alert(result);
	     	if (result === undefined){
	     		this.displayRunningMessage = false;
	     		this.serverMessage = this.msgs[0].detail;
	     		this.displayGenericFailureMessage = true;
	     	} else if (result){
	     			this.displayRunningMessage = false;
	     			this.displaySuccessMessage = true;
	     			this.displayGenericFailureMessage = false;
	     		} else {
	     			this.displayRunningMessage = false;
	     			this.displayFailureMessage = true;
	     			this.displayGenericFailureMessage = false;
	     		}
	      },
	      serverMessage =>  {
	      	this.blockedDocument = false;
	      	this.displayRunningMessage = false;
	      	this.displayFailureMessage = false;
	      	this.displayGenericFailureMessage = true;
	      	this.serverMessage = this.serverMessage + ' - ' + <any>serverMessage.description;

	      })
  	}

  	private validate(): boolean{
  		if (this.selectedProdotto === ''){
  			alert("Devi selezionare un prodotto...");
  			return false;
  		}
  		else if (this.selectedAmbiente === ''){
  			alert ("Devi selezionare un ambiente...");
  			return false;
  		}
  		else if (this.selectedVersione === ''){
  			alert ("Devi selezionare una versione...");
  			return false;
  		}
  		else if (this.selectedOperazione === ''){
  			alert ("Devi selezionare un'operazione...");
  			return false;
  		}
  		if (parseInt(this.selectedOperazione) === 1 && this.selectedTipoAggiornamento === ''){
  			alert ("Devi selezionare un tipo aggiornamento...");
  			return false;
  		}
  		if (parseInt(this.selectedOperazione) === 3 && this.activityName === ''){
  			alert ("Devi specificare l'ID dell'attività da fixare...");
  			return false;
  		}
  		if (parseInt(this.selectedOperazione) === 4 && this.tagName === ''){
  			alert ("Devi specificare il nome del Tag...");
  			return false;
  		}
  		if (parseInt(this.selectedOperazione) === 1 && parseInt(this.selectedTipoAggiornamento) == 1 && this.patchId === ''){
  			alert ("Devi specificare l'ID della patch da applicare...");
  			return false;
  		}

  		return true;
  	}

    ngOnDestroy() {

    }
}


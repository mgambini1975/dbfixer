package it.ditech.ci.dbfixer.dao;

import java.util.List;

import it.ditech.ci.dbfixer.dao.impl.MigrationDAOException;
import it.ditech.ci.dbfixer.service.dto.CustomerDTO;

/**
 * Contratto DAO per la gestione dei clienti
 * 
 * @author mgambini
 */
public interface ICustomerDAO extends IGenericDAO {
	
	/**
	 * Restituisce le info cliente il cui id è passato in argomento
	 * @param customerId id cliente
	 * @return le info cliente
	 * @throws MigrationDAOException
	 */
	public CustomerDTO getCustomerById(Long customerId) throws MigrationDAOException;

	/**
	 * Restituisce l'elenco dei clienti
	 * @return l'elenco dei clienti
	 */
	public List<CustomerDTO> getCustomers() throws MigrationDAOException;
	
	/**
	 * Restituisce le info cliente a cui è associato il prodotto passato in argomento
	 * @param productId id del cliente 
	 * @return le info cliente
	 */
	public CustomerDTO getCustomerByProductId(Long productId) throws MigrationDAOException;
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TomcatEnv } from './tomcat-env.model';
import { TomcatEnvPopupService } from './tomcat-env-popup.service';
import { TomcatEnvService } from './tomcat-env.service';
import { Product, ProductService } from '../product';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-tomcat-env-dialog',
    templateUrl: './tomcat-env-dialog.component.html'
})
export class TomcatEnvDialogComponent implements OnInit {

    tomcatEnv: TomcatEnv;
    isSaving: boolean;

    products: Product[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private tomcatEnvService: TomcatEnvService,
        private productService: ProductService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.productService.query()
            .subscribe((res: ResponseWrapper) => { this.products = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.tomcatEnv.id !== undefined) {
            this.subscribeToSaveResponse(
                this.tomcatEnvService.update(this.tomcatEnv));
        } else {
            this.subscribeToSaveResponse(
                this.tomcatEnvService.create(this.tomcatEnv));
        }
    }

    private subscribeToSaveResponse(result: Observable<TomcatEnv>) {
        result.subscribe((res: TomcatEnv) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: TomcatEnv) {
        this.eventManager.broadcast({ name: 'tomcatEnvListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackProductById(index: number, item: Product) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-tomcat-env-popup',
    template: ''
})
export class TomcatEnvPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tomcatEnvPopupService: TomcatEnvPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.tomcatEnvPopupService
                    .open(TomcatEnvDialogComponent as Component, params['id']);
            } else {
                this.tomcatEnvPopupService
                    .open(TomcatEnvDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

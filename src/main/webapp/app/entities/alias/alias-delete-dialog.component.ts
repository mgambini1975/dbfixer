import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Alias } from './alias.model';
import { AliasPopupService } from './alias-popup.service';
import { AliasService } from './alias.service';

@Component({
    selector: 'jhi-alias-delete-dialog',
    templateUrl: './alias-delete-dialog.component.html'
})
export class AliasDeleteDialogComponent {

    alias: Alias;

    constructor(
        private aliasService: AliasService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.aliasService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'aliasListModification',
                content: 'Deleted an alias'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-alias-delete-popup',
    template: ''
})
export class AliasDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private aliasPopupService: AliasPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.aliasPopupService
                .open(AliasDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

--liquibase formatted sql
--changeset mgambini:insert into tabella Product

--
-- Dump dei dati per la tabella `product`
--

INSERT INTO `product` (`id`, `name`, `description`, `version`, `customer_id`) VALUES
(1, 'MORE-CEDI-BRENDOLAN', 'MORE-CEDI per Maxidi', '1.0.0', 1),
(4, 'MORE-CEDI-NORDICONAD', 'MORE-CEDI per Nordiconad', '1.0.0', 2),
(5, 'MORE-CEDI-CEDIMARCHE', 'MORE-CEDI per Cedimarche', '1.0.0', 3);

--liquibase formatted sql
--changeset mgambini:creazione tabella Activity

-- --------------------------------------------------------

--
-- Struttura della tabella `activity`
--

CREATE TABLE IF NOT EXISTS `activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buildId` int(11) NOT NULL,
  `targetEnvType` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `version` varchar(50) DEFAULT NULL,
  `actionType` int(11) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `scriptName` varchar(100) DEFAULT NULL,
  `author` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `username` varchar(25) NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `applyDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `execTime` time DEFAULT NULL,
  `log` varchar(5000) DEFAULT NULL,
  `success` tinyint(1) DEFAULT NULL,
  `l_filename` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'filename Liquibase',
  `l_md5sum` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'checksum liquibase',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=341 DEFAULT CHARSET=utf8;

package it.ditech.ci.dbfixer.service.util;

import static it.ditech.ci.dbfixer.config.Constants.CHUNK_SEPARATOR;
import static it.ditech.ci.dbfixer.config.Constants.JDBC_URL_PREFIX;
import static it.ditech.ci.dbfixer.config.Constants.METADATA_EXT_QUALIFIER;
import static it.ditech.ci.dbfixer.config.Constants.SLASH;
import static it.ditech.ci.dbfixer.config.Constants.UTENTE_DBA;

import javax.sql.DataSource;

import it.ditech.ci.dbfixer.domain.DataSourceConfiguration;
import it.ditech.ci.dbfixer.domain.DataSourceFactory;
import it.ditech.ci.dbfixer.service.dto.OracleDbEnvDTO;

/**
 * Classe di utilità per l'interfacciamento con il DB
 * 
 * @author mgambini
 *
 */
public class DbUtil {
		
	/**
	 * Ottiene le credenziali da un bean di configurazione per l'ambiente DB
	 * @param dbDto bean di configurazione per l'ambiente DB
	 * @param asSys true se richiesta connessione al DB con ruolo SYSDBA 
	 * @return
	 */
	private static Credentials getCredentials(OracleDbEnvDTO dbDto, boolean asSys){
		String user;
		if (asSys) {
				user = UTENTE_DBA;
		} else {
			user = dbDto.getUser();
		}

		String pwd = asSys ? dbDto.getSysPwd() : dbDto.getPwd();
		Credentials toRet = new Credentials(user, pwd);
		return toRet;
	}
	
	/**
	 * Costruisce una stringa di connessione al DB via JDBC	
	 * 
	 * @param dbDto Un bean di configurazione per l'ambiente DB
	 * @param asSys true se richiesta connessione al DB con ruolo SYSDBA 
	 */
	private static String buildDbURL(OracleDbEnvDTO dbDto, boolean asSys){
		Credentials credentials = getCredentials(dbDto, asSys);
		String url = JDBC_URL_PREFIX+CHUNK_SEPARATOR+credentials.getUsername()+SLASH+
				credentials.getPassword()+METADATA_EXT_QUALIFIER+SLASH+SLASH+
				dbDto.getHostname()+CHUNK_SEPARATOR+
				dbDto.getListenerPort()+SLASH+
				dbDto.getSid();
		return url;
	}
	
	/**
	 * Costruisce un DataSource per l'interazione con il DB via JDBC
	 * 
	 * @param dbDto descrittore di configurazione di un ambiente DB
	 * @param asSys true se richiesta connessione al DB con ruolo SYSDBA 
	 * 
	 * @return il DataSource
	 */
	public static DataSource buildDataSource (OracleDbEnvDTO dbDto, boolean asSys){
		String url = buildDbURL(dbDto, asSys);
		Credentials credentials = getCredentials(dbDto, asSys);
		DataSourceConfiguration dsConfig = new DataSourceConfiguration(credentials.getUsername(), credentials.getPassword(), url);
		DataSource ds = DataSourceFactory.getDataSource(dsConfig);
		return ds;
	}
}
 
import './vendor.ts';

import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Ng2Webstorage } from 'ng2-webstorage';

import { DbFixerSharedModule, UserRouteAccessService } from './shared';
import { DbFixerHomeModule } from './home/home.module';
import { DbFixerAdminModule } from './admin/admin.module';
import { DbFixerAccountModule } from './account/account.module';
import { DbFixerEntityModule } from './entities/entity.module';

import { customHttpProvider } from './blocks/interceptor/http.provider';
import { PaginationConfig } from './blocks/config/uib-pagination.config';

import * as Raven from 'raven-js';

Raven
  .config('https://47ec5ce23dd44c98bad0d6721b0f143b@sentry.io/306153')
  .install();

export class RavenErrorHandler implements ErrorHandler {
  handleError(err:any) : void {
    Raven.captureException(err.originalError || err);
  }
}


// jhipster-needle-angular-add-module-import JHipster will add new module here

import {
    JhiMainComponent,
    LayoutRoutingModule,
    NavbarComponent,
    FooterComponent,
    ProfileService,
    PageRibbonComponent,
    ActiveMenuDirective,
    ErrorComponent
} from './layouts';

@NgModule({
    imports: [
        BrowserModule,
        LayoutRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-'}),
        DbFixerSharedModule,
        DbFixerHomeModule,
        DbFixerAdminModule,
        DbFixerAccountModule,
        DbFixerEntityModule,
        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        ActiveMenuDirective,
        FooterComponent
    ],
    providers: [
        ProfileService,
        customHttpProvider(),
        PaginationConfig,
        UserRouteAccessService,
        { provide: ErrorHandler, useClass: RavenErrorHandler }
    ],
    bootstrap: [ JhiMainComponent ]
})
export class DbFixerAppModule {}

package it.ditech.ci.dbfixer.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.ditech.ci.dbfixer.service.dto.WildflyEnvDTO;

/**
 * Service Interface for managing WildflyEnv.
 */
public interface WildflyEnvService {

    /**
     * Save a wildflyEnv.
     *
     * @param wildflyEnvDTO the entity to save
     * @return the persisted entity
     */
    WildflyEnvDTO save(WildflyEnvDTO wildflyEnvDTO);

    /**
     *  Get all the wildflyEnvs.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<WildflyEnvDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" wildflyEnv.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    WildflyEnvDTO findOne(Long id);

    /**
     *  Delete the "id" wildflyEnv.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
